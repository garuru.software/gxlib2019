//-------------------------------------------------------------------------------
// CPPとJAVAをつなぐJNIルーチン
//-------------------------------------------------------------------------------

package jp.co.garuru;

import android.content.Context;

public class GameGirlJNI {

	 static {
		 System.loadLibrary("gxLib2018");
	 }

	public static native void appInit();
	public static native void appRestart();
	public static native void appResume(int width, int height);
	public static native void appPause();
	public static native void appStop();
	public static native void appEnd();
	public static native void appUpdate();
	public static native void cursorUpdate( int id , int px , int py , int bPush );
	public static native void inputUpdate( int id, int push , int keyCode );
	public static native void UpdateCensors( float[] data );
	public static native void analogUpdate( int id, float[] analog );
	public static native void ReturnHttpResult( int id , byte[] resultText  , boolean bSuccess);

	public static native void SetJava2Cpp(byte[] recvData , int size);
	public static native byte[] GetCppState();

	//public static native int SetOBBPath( String str );
	public static native int SetSDPath( String str );

	public static native byte[] GetBTData();
	public static native void SetBTData( byte[] recvData , int size );

	public static native void SetAndroidContext(Context context);

}
