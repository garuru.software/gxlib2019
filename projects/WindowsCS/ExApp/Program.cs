﻿using System;
using System.Runtime.InteropServices;

namespace ExampleApp
{
    class Program
    {
        [DllImport("ExDll.dll")]
        private static extern int Example();
        [DllImport("ExDll.dll")]
        private static extern void InitLib();
        [DllImport("ExDll.dll")]
        private static extern void Save();
        [DllImport("ExDll.dll")]
        private static extern void CloseLib();

        static void Main(string[] args)
        {
            Console.WriteLine(Example());
            InitLib();
            //Save();
            CloseLib();
        }
    }
}