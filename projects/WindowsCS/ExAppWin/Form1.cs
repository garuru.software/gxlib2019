﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace ExAppWin
{
    public partial class Form1 : Form
    {
        [DllImport("ExDll.dll")]
        private static extern int Example();

        [DllImport("ExDll.dll")]
        private static extern void InitLib();

        [DllImport("ExDll.dll")]
        private static extern void Save();

        [DllImport("ExDll.dll")]
        private static extern void CloseLib();

        [DllImport("ExDll.dll")]
        private static extern int waitExitLib();

        [DllImport("ExDll.dll")]
        private static extern void GetText(StringBuilder s, Int32 bufsize);

        [DllImport("ExDll.dll")]
        private static extern void GetBuffer(byte[] p, ref int maxSize);

        public Form1()
        {
            int num = Example();
            InitializeComponent();
            //textBox1.Text = String.Format("dll answer={0}", num); ;
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            ForeColor = Color.Gold;
            InitLib();
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            CloseLib();

            while(true)
            {
                if (waitExitLib()==0)
                {
                    break;
                }
                Thread.Sleep(100);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int num = Example();
            //System.Text.StringBuilder sb = new System.Text.StringBuilder(256);
            //GetText(sb, sb.Capacity);

            int kBufSize = 1024;
            byte[] buf = new byte[kBufSize];

            GCHandle handle = GCHandle.Alloc(buf, GCHandleType.Pinned);
            // 領域を確保して Pinned する事でアドレスが固定される。
            IntPtr p = handle.AddrOfPinnedObject();
            int size = buf.Length;
            GetBuffer(buf, ref size); // DLLImport された C++ の関数
            handle.Free();


            //textBox1.Text = String.Format("dll rand={0}", num); ;
            //Console.WriteLine(Example());
            //Save();
        }
    }
}
