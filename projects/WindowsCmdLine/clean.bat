rem del -S -Q debug\*.obj debug\*.txt debug\*.*db debug\*.log debug\*.ilk debug\*.res
rem del -S -Q release\*.obj debug\*.txt debug\*.*db debug\*.log debug\*.tlog debug\*.ilk debug\*.res
cd x64
ren Release R2
ren Debug D2
md Release
md Debug

copy D2\*.exe Debug
copy D2\*.dll Debug
copy R2\*.exe Release
copy R2\*.dll Release

rmdir /S /Q D2
rmdir /S /Q R2
rmdir /S /Q "..\.vs"
cd ..
