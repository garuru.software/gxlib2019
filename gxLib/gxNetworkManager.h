#ifndef _GXNETORKMANAGER_H_
#define _GXNETORKMANAGER_H_

//ネットワーク管理
#include <iostream>
#include <map>
#include <mutex>

class gxNetworkManager
{
public:
	//enum {
	//	enHttpOpenMax = 1024,
	//};

	struct StHTTP
	{
		StHTTP()
		{
			m_URL[0] = 0x00;
			PostData = nullptr;
			m_PostSize = 0;
			pData = NULL;
			bRequest = gxFalse;
			m_Seq = 0;
			uFileSize = 0;
			uReadFileSize = 0;
			index = 0;
			Error = 0;
			bDone  = gxFalse;
			bClose = gxTrue;
		}

		~StHTTP()
		{
			if( PostData )
			{
				SAFE_DELETES(PostData);
			}
		}

		void SetError( Sint32 err )
		{
			Error = err;
		}

		void SetSeq( Sint32 seq )
		{
			m_Seq = seq;
		}
		
		Sint32 GetSeq()
		{
			return m_Seq;
		}

		gxBool IsNowLoading()
		{
			if( m_Seq == 0 || m_Seq == 999 ) return gxFalse;
			return gxTrue;
		}

		void MoveData( Uint8 *p , size_t size )
		{
            SAFE_DELETES(pData);
			pData = new Uint8[size];
            gxUtil::MemCpy( pData , p , size );
			uFileSize = size;
		}

		void ClearData()
		{
			SAFE_DELETES(pData);
			uFileSize = 0;
		}

		gxChar m_URL[1024];
		gxChar* PostData = nullptr;
		size_t  m_PostSize = 0;
		Uint8 *pData;
		gxBool bRequest;
		size_t uFileSize;
		size_t uReadFileSize;
		Sint32 index;
		gxBool bClose = gxFalse;
		std::function<void(gxBool bSuccess, uint8_t* p, size_t size)> func = nullptr;
		Sint32 Error = 0;
	private:
		Sint32 m_Seq;
		gxBool bDone = gxFalse;


	};

	gxNetworkManager()
	{
		//m_pHttp = new StHTTP[ enHttpOpenMax ];
		m_sHttpRequestMax = 0;
	}

	~gxNetworkManager()
	{
		//SAFE_DELETES( m_pHttp );
	}

	void Action();

	Sint32 OpenURL(
		gxChar const *pOpenURL ,
		gxChar const *pPostData = nullptr ,
		size_t PostSize = 0 ,
		std::function<void(gxBool bSuccess , uint8_t *p , size_t size)> func=nullptr );

	gxBool CloseURL(Uint32 uIndex);

	StHTTP* GetHTTPInfo( Uint32 uIndex )
	{
		return &m_pHttp[ uIndex ];
	}

	StHTTP* GetNextReq();

	SINGLETON_DECLARE( gxNetworkManager );

private:

	std::map<int , StHTTP> m_pHttp;
	Sint32 m_sHttpRequestMax;
	std::mutex m_UpdateLock; 
};

#endif
