﻿//--------------------------------------------------
//
// gxSoundManager.h
//
//--------------------------------------------------

#ifndef _GXSOUND_MANAGER_H_
#define _GXSOUND_MANAGER_H_

#include "util/CFileWave.h"

enum {
	enSoundReqNone       = 0x00,
	enSoundReqLoad       = 0x01,
	enSoundReqPlay       = 0x02,
	enSoundReqStop       = 0x04,
	enSoundReqVolume     = 0x08,
	enSoundReqChangeFreq = 0x10,
	enSoundReqChangePan  = 0x20,
	enSoundReqSetReverb  = 0x40,
};

enum {
	enMasterVolumeLow,
	enMasterVolumeMiddle,
};

struct StPlayInfo
{
	gxBool  bOverWrap = gxFalse;
	gxBool  bReq      = gxFalse;
	gxBool  bLoop     = gxFalse;
	gxBool  bUse      = gxFalse;
	gxBool  bPlayNow  = gxFalse;
	Uint32  uStatus   = 0x00;
	Float32 fVolume   = 0.0f;
	Float32 fVolumeAdd = 0.0f;
	Float32 fFreqRatio = 0.0f;

	Float32 fPanning   = 0.0f;
	Float32 bReverb   = gxFalse;

	CFileWave m_WaveData;
	gxChar fileName[FILENAMEBUF_LENGTH];
};

class gxSoundManager
{
public:
	gxSoundManager();
	~gxSoundManager();

	void Action();
	void Play();

	StPlayInfo* GetPlayInfo(Uint32 uIndex)
	{
		return &m_Info[ uIndex ];
	}

	gxBool Play( Uint32 index , Float32 fVolume , gxBool bOverWrap=gxFalse , gxBool bLoop=gxFalse );

	gxBool ReadAudioFile( Uint32 uIndex , Uint8* pMemory ,size_t uSize );
	gxBool LoadAudioFile( Uint32 uIndex , const gxChar* pFileName );
	gxBool PlayAudio( Uint32 uIndex , Float32 fVolume = 0.8f , gxBool bOverWrap=gxFalse , gxBool bLoop=gxFalse );

	void ResumeAllSounds();

	void ReleaseAllWaves();

	//gxBool SetForceVolumeLevel( Sint32 type );
	gxBool SetMasterVolumeLevel( Float32 fVol );

	gxBool  SetVolume(Uint32 index, Float32 fVol);
	Float32 GetVolume(Uint32 index );
	gxBool SetFreq( Uint32 index , Float32 freq);
	gxBool SetPanning( Uint32 index , Float32 pan );
	gxBool SetReverb( Uint32 index , gxBool bEnable );

	gxBool StopAudio( Uint32 uIndex );
	gxBool SetFade( Sint32 uIndex , Float32 fVolume , Uint32 frm );
	gxBool IsPlay( Uint32 uIndex );

	void PlayNow( Uint32 uIndex , gxBool bPlay )
	{
		m_Info[ uIndex ].bPlayNow = bPlay;
	}

	Float32 GetMasterVolume()
	{
		return m_fMasterVolume;
	}

	SINGLETON_DECLARE( gxSoundManager );

private:

	StPlayInfo m_Info[MAX_SOUND_NUM];
	Float32 m_fMasterVolume    = 0.5f;
	Float32 m_fOldMasterVolume = 0.5f;


};

#endif

