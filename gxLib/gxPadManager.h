﻿//--------------------------------------------------
//
// gxPadManager.h
//
//--------------------------------------------------
#ifndef GXPADMANAGER_H_
#define GXPADMANAGER_H_

#define MAX_REPLAY_DATA_LENGTH (60*60*10)	//１０分まで

class gxPadManager
{
public:
	enum {
		enJoypadButtonNum = BTN_MAX,
		enTouchMax        = GX_TOUCH_MAX,	//3点までタッチ対応
	};

	struct StAnalog 
	{
		gxVector2 left;
		gxVector2 right;
		Float32 leftTrigger;
		Float32 rightTrigger;
	};

	struct StSensor
	{
		gxVector3 gyro;
		gxVector3 accel;
		gxVector3 magne;
		gxVector3 orient;
	};

	enum {
		ANALOG_LX,
		ANALOG_LY,
		ANALOG_RX,
		ANALOG_RY,
		ANALOG_LT,
		ANALOG_RT,
		ANALOG_MAX,
		NOUSE_ANALOG_AXIS,
	};

	enum ERotationType {
		KEYBOARD_ROT0,
		KEYBOARD_ROT90,
		KEYBOARD_ROT180,
		KEYBOARD_ROT270,
	};

	enum {
		enIDKeyboard   = 0x0000,
		enIDMouse      = 0x0100,
		enIDTouch      = 0x0110,
		enIDJoypad1    = enIDTouch + enTouchMax,

		enIDJoypad2    = enIDJoypad1 + enJoypadButtonNum,//0x0140,
		enIDJoypad3    = enIDJoypad2 + enJoypadButtonNum,//0x0140,
		enIDJoypad4    = enIDJoypad3 + enJoypadButtonNum,//0x0140,
		enIDJoypad5    = enIDJoypad4 + enJoypadButtonNum,//0x0140,

		enIDMax        = enIDJoypad5 + enJoypadButtonNum,//0x01C0,
	};

	enum {
		enSensorTypeGyro,
		enSensorTypeAccel,
		enSensorTypeMagne,
		enSensorTypeOrient,
	};

	gxPadManager();
	~gxPadManager();

	void Action();

	gxBool IsEnableController()
	{
		return m_bEnableController;
	}

	void SetEnableController( gxBool bEnable )
	{
		m_bEnableController = bEnable;
	}

	void SetKeyDown(Uint32 uKey);
	void SetKeyUp(Uint32 uKey);
	void SetMouseButtonUp( Uint32 uLR );
	void SetMouseButtonDown( Uint32 uLR );
	void SetMousePosition(Uint32 uLRM , Sint32 x , Sint32 y );
	void SetMouseWheel(Sint32 n);

	StJoyStat* GetJoyStatus(Sint32 n=0)
	{
		return &m_stStatus[n];
	}


	void SetTouchInfo ( Uint32 uId , gxBool bPush , Sint32 x , Sint32 y );	//ユニークID,タッチ位置x , タッチ位置y
	void SetPadInfo   ( Sint32 playerID , Uint32 button );
	void SetVPadInfo  ( Uint32 button );
	void SetVPadAnalog(Float32 fx, Float32 fy)
	{
		m_VPadAnalog.x = fx;
		m_VPadAnalog.y = fy;
	}

	//void SetAnalogInfo( Sint32 playerID , Float32 *pAnalogArry );
	void SetAnalogInfo( Sint32 playerID , StAnalog *pInfo );


	//void SetSensorInfo( Sint32 type , Float32 *pAnalogArry );
	void SetSensorInfo( StSensor* pInfo );

	Uint8 GetKeyBoardStatus( Uint32 n );

	Sint32 GetDeviceNum()
	{
		return m_sDeviceNum;
	}

	void SetDeviceNum(Sint32 sNum)
	{
		//使用できるジョイパッドデバイスの数を記録しておく（念のため）
		m_sDeviceNum = sNum;
	}

	StTouch* GetTouchStatus( Sint32 n );

	//振動の設定を返す

	gxBool GetMotorStat( Sint32 id , Sint32 motorID , Float32 *fRatio )
	{
		//振動機能の状態を得る

		*fRatio =  fRatio[ motorID ];

		if( m_Motor[ id ].frm == 0 || ( !m_Motor[ id ].bUseRumble ) )
		{
			//再生フレームがないか振動機能を使わない場合はゼロで返す
			*fRatio = 0.0f;
			return gxFalse;
		}

		*fRatio =  m_Motor[ id ].fRatio[ motorID ];

		return gxTrue;
	}

	void EnableRumble( Sint32 playerID , gxBool bRumbleOn )
	{
		//振動機能のON/OFFを切り替える
		m_Motor[ playerID ].bUseRumble = bRumbleOn;
	}

	void SetRumble(Sint32 playerID , Sint32 motorID, Float32 fRatio , Sint32 frm);

	//端末内臓のコントローラーのスティックの入力方向を９０度単位で回転させる
	void SetHardwareKeyboardRotation( ERotationType rot )
	{
		m_HardwareKeyboardRotation = rot;
	}

	//-------------------------------------------
	//コンフィグ用
	//-------------------------------------------

	//特定のキーに新しい入力をアサインする
	gxBool SetConfigKey( Uint32 playerID , EJoyBit key1 , EJoyBit key2 );

	//設定をデフォルトに戻してセーブデータに反映する（セーブするとは言ってない）
	void SetDefaultConfig( Sint32 playerID );

	//セーブデータにキーコンフィグを反映する（セーブするとは言ってない）
	void UpdateConfigtoSaveData();

	Uint8   CnvJoytoIndex(EJoyBit joy);

	Uint8   GetConfiguredButtonID(Sint32 playerID, EJoyBit pos);

	EJoyBit GetConfiguredButtonBit(Uint32 playerID, EJoyBit pos);

	void SetEnableConfig( gxBool bConfigOn)
	{
		m_bEnableConfig = bConfigOn;
	}


	//-------------------------------------------
	//リプレイ用
	//-------------------------------------------

	void   replay();
	Uint8* GetReplayData(  Uint32 *uSize );
	void   RecordInput( gxBool bRecornOn );
	void   ReplayStop();
	void   ReplayStart( Uint8 *pData , Uint32 uSize );

	Uint32 GetReplayStat()
	{
		Uint32 ret = 0x00;

		if( m_bReplay ) ret |= 0x01;
		if( m_bRecord ) ret |= 0x02;

		return ret;
	}

	SINGLETON_DECLARE( gxPadManager );

private:

	void MixingKeyboardStat();
	void MixingVPadStat();
	void SetDirectAccessBit();
	void SetCurrentStatus();

	//指定IDのタッチデバイスインデックスを返す
	Sint32 getTouchDeviceIndex( Uint32 n );

	//--------------------------------------------
	//キーコンフィグ関連
	//--------------------------------------------

	Uint8 m_uButton[enIDMax]    = {};
	Uint8 m_uCurrent[enIDMax]   = {};
	Uint8 m_uRepeatCnt[enIDMax] = {};
	Uint8 m_uPushCnt[enIDMax]   = {};
	Uint8 m_uCurStatus[enIDMax] = {};
	Uint8 m_uDClickCnt[enIDMax] = {};

	//最終的なコントローラー情報

	StJoyStat m_stStatus[ PLAYER_MAX ];

	//--------------------------------------------
	//デバイス状態管理用
	//--------------------------------------------
	Sint32 m_sDeviceNum;

	//コントローラーの入力状態
	Uint32  m_JoyButton[ PLAYER_MAX ];
	Uint32  m_VPadButton;
	gxVector2 m_VPadAnalog;

	//Uint32 m_KeyBoard[ 256 ];

	Sint32  _mouse_x,_mouse_y;
	Float32 _mouse_whl;
	//Float32 analogAxis[PLAYER_MAX][ANALOG_MAX];

	StAnalog tempAnalog[PLAYER_MAX];
	StSensor tempSensor;

/*
	gxVec   temp_sensor_gyro;
	gxVec   temp_sensor_accl;
	gxVec   temp_sensor_magne;
	gxVec   temp_sensor_orient;
*/

	typedef struct StTouchStat
	{
		Sint32 x,y;		//現在座標
		Uint32 uStat;	//入力状態( repeat , dcl , trg push )

		Uint32 uId;		//デバイスID
		gxBool bPush;	//デバイス入力状態( ON / OFF )

		void Reset()
		{
			uId = 0xffffffff;
			bPush = gxFalse;
			x = y = 0;
			uStat = 0;
		}

	} StTouchStat;

	StTouchStat m_stTouchStat[ enTouchMax ];
	StTouch m_stTouch[ enTouchMax ];

	//振動

	struct StRumble
	{
		gxBool  bUseRumble = gxTrue;
		Sint32  frm        = 0;
		Float32 fRatio[2]  = { 0.0f , 0.0f };

	};

	StRumble m_Motor[PLAYER_MAX];//[eMoterNum];

	gxBool m_bEnableController;
	gxBool m_bEnableConfig = gxTrue;

	//コントローラーのコンフィグ状況を保存する
	Uint8 m_ConvAssignData[PLAYER_MAX][32]={};

	//ハードウェア内蔵型のコントローラーの場合、端末の回転に応じて入力方向を切り替えられるようにする
	ERotationType m_HardwareKeyboardRotation;


	//デバッグ用
	gxBool m_bGremlineTest = gxFalse;
	void gremlineTouch();


	//-------------------------------------------
	//リプレイ用
	//-------------------------------------------

	Sint32 m_uReplayFrameMax = 0;
	Sint32 m_uReplayFrameNow = 0;
	Uint8  *m_pInputData = NULL;
	gxBool m_bReplay = gxFalse;
	gxBool m_bRecord = gxFalse;

};


enum {
	//androidOS & iOS
	TOUCH_iCBK1  = 1,
	TOUCH_iCBK2  = 2,
	TOUCH_iCBK3  = 3,
	TOUCH_iCBK4  = 4,
	TOUCH_iCBK5  = 5,
	TOUCH_iCBK6  = 8,
	TOUCH_iCBK7  = 7,
	TOUCH_iCBK8  = 9,
	TOUCH_iCBK9  = 13,
	TOUCH_iCBK10 = 15,

	TOUCH_aCBK1  = 0,
	TOUCH_aCBK2  = 2,
	TOUCH_aCBK3  = 3,
	TOUCH_aCBK4  = 4,
	TOUCH_aCBK5  = 5,
	TOUCH_aCBK6  = 9,
	TOUCH_aCBK7  = 13,
	TOUCH_aCBK8  = 15,
	TOUCH_aCBK9  = 17,
	TOUCH_aCBK10 = 14,
};


#endif



