﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>
#include "CHttpClient.h"

#define NAME_APRICATION APPLICATION_NAME

extern Sint32 s_ThreadNum;

int exitApp = 0;

void* Thread( void *pFunc )
{
	CDeviceManager::GetInstance()->AppInit();

	CGameGirl::GetInstance()->Init();
	CGameGirl::GetInstance()->AdjustScreenResolution();
	//CGraphics::GetInstance()->Init();
	//CAudio::GetInstance()->Init();
	//CGamePad::GetInstance()->Init();
	//gxLib::SetVirtualPad( CWindows::GetInstance()->m_bVirtualPad );

	//void BlueToothThreadMake();
	//BlueToothThreadMake();

	MemoryInit();

	while (gxTrue)
	{
		CGameGirl::GetInstance()->Action();

		if (CGameGirl::GetInstance()->IsAppFinish())
		{
			break;
		}
		if (exitApp) break;
	}

	exitApp = 2;

	MemoryDestroy();

	//CBlueToothManager::GetInstance()->DestroyBlueToothThread();
	CGameGirl::DeleteInstance();

	//CGraphics::DeleteInstance();
	//CAudio::DeleteInstance();
	//CGamePad::DeleteInstance();

	CDeviceManager::DeleteInstance();
	//CBlueToothManager::DeleteInstance();


Retry:

	if (s_ThreadNum > 0)
	{
		//ASSERT(0);
		Sleep(10);
//		goto Retry;
	}

	gxLib::Destroy();

	exitApp = 3;
	return nullptr;
}

int main(int argc, char** argv)
{
	//if (agv[1] == "-batchmode")
	{
		CDeviceManager::GetInstance()->SetBatchMode();
	}

	for (Sint32 ii = 0; ii < argc; ii++)
	{
		gxChar* pText = CDeviceManager::SJIStoUTF8((gxChar*)argv[ii]);
		CGameGirl::GetInstance()->DragAndDropArgs.push_back(pText);
	}
	auto& agv = CGameGirl::GetInstance()->DragAndDropArgs;

	gxLib::CreateThread(Thread , nullptr );

Retry:

	if (exitApp != 3)
	{
		//ASSERT(0);
		Sleep(10);
		goto Retry;
	}

	return 0;

}
