﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxFileManager.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>

#include "CHttpClient.h"

SINGLETON_DECLARE_INSTANCE( CDeviceManager );

#define NAME_APRICATION APPLICATION_NAME

class DirList
{
public:

	DirList()
	{
	}

	~DirList()
	{
	}

	std::string m_RootDir;

	std::vector<std::string> Search(std::string rootDir)
	{
		std::replace(rootDir.begin(), rootDir.end(), '\\', '/');

		m_RootDir = rootDir;

		searchDir(rootDir, "*.*");


		for (auto itr = m_TempDirectoryList.begin(); itr != m_TempDirectoryList.end(); ++itr)
		{
			m_DirectoryList.push_back(itr->first);
		}

		return m_DirectoryList;
	}

#if 1
	void searchDir(std::string dirName, std::string filter)
	{
		//zipのフォルダ構造を再帰的に取得

		if (m_TempDirectoryList.count(dirName) > 0)
		{
			//既に調査済みのフォルダは再調査しない
			return;
		}

		HANDLE hFind;
		WIN32_FIND_DATA win32fd;

		if (dirName == "")
		{
			hFind = FindFirstFile((dirName + ".\\" + filter/**.*"*/).c_str(), &win32fd);
		}
		else
		{
			hFind = FindFirstFile((dirName + "\\" + filter/**.*"*/).c_str(), &win32fd);
		}

		if (hFind == INVALID_HANDLE_VALUE)
		{
			return;
		}

		do {
			if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (win32fd.cFileName[0] != '.')
				{
					std::string directory = "";
					if (dirName == "")
					{
						directory = win32fd.cFileName;
					}
					else
					{
						directory = dirName + "\\" + win32fd.cFileName;
					}
					searchDir(directory, filter);
				}
			}
			else
			{
				std::string fileName = "";

				if (dirName == "")
				{
					fileName = win32fd.cFileName;
				}
				else
				{
					fileName = dirName + "\\" + win32fd.cFileName;
				}

				std::replace(fileName.begin(), fileName.end(), '\\', '/');
				m_TempDirectoryList[fileName] = true;
			}

		} while (FindNextFile(hFind, &win32fd));

		FindClose(hFind);
	}
#endif

	std::vector<std::string> m_DirectoryList;
	std::map<std::string, gxBool> m_TempDirectoryList;
};


bool CreateDirectories(char* pURL )
{
	//フォルダを作成する
	//とりあえずわたってきた名前をそのまま使う
	//Write時には２回（sjis,u8の２回処理が来ることに留意）

	gxBool bSuccess = gxTrue;

	//size_t len = strlen(pURL);
	//char *pBuf = new char[len+1];
	//char *pSeparate[256];
	//int  cnt = 0;
	std::vector<char*> separates;
	std::string url = pURL;;
	std::replace(url.begin(), url.end(), '\\', '/' );

	char *pBuf = new char[ url.size()+1];
	sprintf( pBuf, "%s", url.c_str());

	Sint32 cnt = 0;
	for ( Sint32 ii= Sint32(url.size()); ii>=0; ii-- )
	{
		if ( pBuf[ii] == ':')
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( ii == 0 )
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( pBuf[ii] == '/')
		{
			pBuf[ii] = 0x00;
			if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
			cnt++;
		}
	}

	size_t max = separates.size();
	std::string path = "";

	for (Sint32 ii = 0; ii < max; ii++)
	{
		path += separates[max - 1 - ii];

			if (!CreateDirectory( path.c_str(), NULL))
		{
			bSuccess = gxFalse;
		}

		path += "/";
	}

	delete[] pBuf;

	return bSuccess;
}


CDeviceManager::CDeviceManager()
{
}


CDeviceManager::~CDeviceManager()
{
	CHttpClient::DeleteInstance();
}


void CDeviceManager::AppInit()
{
	static int nnn = 0;

	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
		//CAudio::GetInstance()->Init();
		//COpenGLES2::GetInstance()->Init();
	} else{
        CGameGirl::GetInstance()->SetResume();
    }

	CGameGirl::GetInstance()->AdjustScreenResolution();

	nnn ++;
}


void   CDeviceManager::GameInit()
{

}


gxBool CDeviceManager::GameUpdate()
{
	return gxTrue;
}


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}


void   CDeviceManager::Render()
{
	if( CGameGirl::GetInstance()->IsResume() ) return;

//	COpenGLES2::GetInstance()->Update();
//	COpenGLES2::GetInstance()->Render();

}


void   CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = gxLib::GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = gxLib::GetTime();
		if (_TimeNow < _TimeOld - 100.0f)
		{
			int nnn = 0;
			nnn++;
		}
	} while (_TimeNow < (_TimeOld + (1.0f / FRAME_PER_SECOND)));

	_TimeOld = _TimeNow;

}


void   CDeviceManager::Flip()
{
//	COpenGLES2::GetInstance()->Present();
}


void   CDeviceManager::Resume()
{
}


void   CDeviceManager::Movie()
{
}


void   CDeviceManager::Play()
{
//	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	CHttpClient::GetInstance()->Action();

	return gxTrue;
}



void   CDeviceManager::UploadTexture(Sint32 sBank)
{
//    COpenGLES2::GetInstance()->ReadTexture( sBank );
}

void   CDeviceManager::LogDisp(char* pString)
{
	gxChar* pSJIS = CDeviceManager::UTF8toSJIS(pString);

	printf("%s" LF , pSJIS);

	SAFE_DELETES(pSJIS);
}


void CDeviceManager::Clock(gxClock* pClock)
{
	//現在の時刻をミリ秒で取得する
	std::chrono::system_clock::time_point now;
	now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(now);
	tm local_tm = *localtime(&tt);

	std::chrono::system_clock::duration tp = now.time_since_epoch();
	std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tp);

	pClock->Year = local_tm.tm_year + 1900;	// years since 1900
	pClock->Month = local_tm.tm_mon + 1;		// months since January - [0, 11]
	pClock->Day = local_tm.tm_mday;   		// day of the month - [1, 31]
	pClock->DOW = local_tm.tm_wday; 			// days since Sunday - [0, 6]
	pClock->Hour = local_tm.tm_hour; 			// hours since midnight - [0, 23]
	pClock->Min = local_tm.tm_min; 			// minutes after the hour - [0, 59]
	pClock->Sec = local_tm.tm_sec; 			// seconds after the minute - [0, 60] including leap second
	pClock->MSec = (us.count() / 1000) % (1000);
	pClock->USec = us.count() % (1000);

}


Uint8* loadFile2( const gxChar* pFileNameU8 , size_t* pLength )
{
	//------------------------------------

	int fh;

	Uint8* pBuffer = NULL;
	struct stat filestat;
	long sz, readsz;
	unsigned long pos = 0;
	int ret = 1;

	//まずはSJISでチャレンジ
	char buf[1024];
	GetCurrentDirectory(1024,buf);
	gxChar *pFileNameSJIS = CDeviceManager::UTF8toSJIS((gxChar*)pFileNameU8);
	fh = open( (char*)pFileNameSJIS, O_RDONLY | O_BINARY );
	SAFE_DELETES(pFileNameSJIS);

	if ( fh < 0 )
	{
		//生データ名でチャレンジ
		fh = open((char*)pFileNameU8, O_RDONLY | O_BINARY);

		if (fh < 0)
		{
			return NULL;
		}
	}

	fstat(fh, &filestat);
	readsz = sz = filestat.st_size;

	if( filestat.st_size == 0 )
	{
		//sizeがzeroでnullを返すわけにもいかないのでとりあえず１byteのバッファを返すことにする
		pBuffer = new Uint8[1];
		pBuffer[0] = 0x00;
		close(fh);
		GX_DEBUGLOG(" -LoadFile2::ZeroByte File --%s",pFileNameU8);
		return pBuffer;
	}

	*pLength = filestat.st_size;
	pBuffer = new Uint8[readsz];

	if (pBuffer == NULL) return NULL;

	while (ret > 0)
	{
		if (readsz > 1024) readsz = 1024;

		ret = read(fh, &pBuffer[pos], readsz);
		pos += ret;
		sz -= ret;
		readsz = sz;
	}

	close(fh);

	return pBuffer;

}


Uint8* CDeviceManager::LoadFile( const gxChar* pFileNameU8 , size_t* pLength , Uint32 location )
{
	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if (std::string::npos != url.find(':'))
	{
		location = STORAGE_LOCATION_EXTERNAL;
	}

	static Sint32 s_bEnableGxDirectory = -1;

	if (s_bEnableGxDirectory == -1)
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する
		struct stat statBuf;
		if (stat("Storage\\01_rom", &statBuf) == 0)
		{
			//存在する
			s_bEnableGxDirectory = 1;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = 0;
		}
	}

	if (s_bEnableGxDirectory == 0)
	{
		//Storage/01_romフォルダがない場合は実行ファイル直下にファイルを読みに行く
		location = STORAGE_LOCATION_EXTERNAL;
	}

	Uint8 *pData = nullptr;

	switch( location ){
	case STORAGE_LOCATION_ROM:
		//LoadFile(2)

		//disc
		fileName = "Storage\\02_disc\\" + url;
		pData = loadFile2(fileName.c_str(), pLength);
		if( pData == nullptr )
		{
			//rom
			fileName = "Storage\\01_rom\\" + url;
			pData = loadFile2(fileName.c_str(), pLength);
			if( pData == nullptr )
			{
				return nullptr;
			}
		}
		break;

	case STORAGE_LOCATION_INTERNAL:
		//LoadStorageFile / SaveStorage

		//disk
		fileName = "Storage\\03_disk\\" + url;
		pData = loadFile2(fileName.c_str(), pLength);
		if( pData == nullptr )
		{
			//disc
			fileName = "Storage\\02_disc\\" + url;
			pData = loadFile2(fileName.c_str(), pLength);
			if( pData == nullptr )
			{
				//rom
				fileName = "Storage\\01_rom\\" + url;
				pData = loadFile2(fileName.c_str(), pLength);
				if( pData == nullptr )
				{
					return nullptr;
				}
			}
		}
		break;

	case STORAGE_LOCATION_EXTERNAL:
	default:
		//LoadFile(1) / SaveFile
		fileName = url;
		pData = loadFile2(fileName.c_str(), pLength);
		break;
	}

	return pData;
}


gxBool CDeviceManager::SaveFile( const gxChar* pFileNameU8 , Uint8* pReadBuf , size_t uSize, Uint32 location )
{
	//ファイルの書き込み

	Uint32 len = gxUtil::StrLen(pFileNameU8);

	std::string url = pFileNameU8;
	std::replace(url.begin(), url.end(), '/', '\\');
	std::string fileName = "";

	if ( std::string::npos !=  url.find(':') )
	{
		location = STORAGE_LOCATION_EXTERNAL;
	}
	else
	{
		// : を発見できなかったらカレントディレクトリ直下に作成
		if (location == STORAGE_LOCATION_EXTERNAL)
		{
			char buf[1024];
			GetCurrentDirectory(1024, buf);
			std::string tmp;
			tmp = buf;
			tmp += '\\';
			tmp += url;
			url = tmp;
		}
	}

	static Sint32 s_bEnableGxDirectory = -1;

	if(s_bEnableGxDirectory == -1 )
	{
		//Storageディレクトリが存在しない場合はexeの実行環境にファイルを保存する
		struct stat statBuf;
		if (stat("Storage\\03_disk", &statBuf) == 0)
		{
			//存在する
			s_bEnableGxDirectory = 1;
		}
		else
		{
			//存在しない
			s_bEnableGxDirectory = 0;
		}
	}

	switch( location ){
	case STORAGE_LOCATION_ROM:
		return gxFalse;

	case STORAGE_LOCATION_INTERNAL:
		fileName = "Storage\\03_disk\\" + url;
		if(s_bEnableGxDirectory == 0 )
		{
			fileName = url;
		}
		break;

	case STORAGE_LOCATION_EXTERNAL:
	default:
		fileName = url;
		break;
	}

	// ---------------------------------------------------------------------------------
	//U8をSJISに変換
	//※Zipからの展開だとZipに内包されたファイル名がSJISでわたってくる可能性がある
	gxChar *pFileNameSJIS = UTF8toSJIS( (gxChar*)fileName.c_str());
	// ---------------------------------------------------------------------------------

	int fh;
	Uint8* pBuffer = NULL;
	unsigned long pos=0;
	int ret=1;

	if (pReadBuf == nullptr)
	{
		//ファイルを削除する
		fh = open((char*)pFileNameSJIS, O_RDONLY | O_BINARY);
		if (fh < 0)
		{
			SAFE_DELETES(pFileNameSJIS);
			return false;
		}

		close(fh);

		//std::filesystem::filesystem::remove_all("test.yxt");
		//std::uintmax_t result = 
		//ret = (result == 0) ? 0 : 1;
		int ret = remove(pFileNameSJIS);
		SAFE_DELETES(pFileNameSJIS);

		if ( ret == 0)
		{
			return gxTrue;
		}
		return gxFalse;
	}

	fh = open( (gxChar*)pFileNameSJIS,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

	if(fh < 0)
	{
		//書き込みミス
		CreateDirectories(pFileNameSJIS );

		//フォルダを作って再度チャレンジ

		fh = open( (gxChar*)pFileNameSJIS,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

		if( fh < 0 )
		{
			SAFE_DELETES(pFileNameSJIS);

			//生ファイル名で再度チャレンジ

			fh = open((gxChar*)fileName.c_str(), O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

			if (fh < 0)
			{
				CreateDirectories((gxChar*)fileName.c_str());

				fh = open((gxChar*)fileName.c_str(), O_WRONLY | O_BINARY | O_TRUNC | O_CREAT, S_IREAD | S_IWRITE);

				if (fh < 0)
				{
					return gxFalse;
				}
			}
		}
	}

	write(fh,pReadBuf,Uint32(uSize));

	close(fh);

	SAFE_DELETES(pFileNameSJIS);

	return gxTrue;
}



/*
gxBool CDeviceManager::LoadConfig()
{
	return gxTrue;
}


gxBool CDeviceManager::SaveConfig()
{
	return gxTrue;
}
*/


void CDeviceManager::ToastDisp( gxChar* pMessage )
{
	LogDisp(pMessage);
}


void CDeviceManager::OpenWebClient( gxChar* pURL , gxBool bOpenBrowser )
{
}



gxBool CDeviceManager::SetAchievement( Uint32 index )
{
	return gxTrue;
}


gxBool CDeviceManager::GetAchievement( Uint32 index )
{
	return gxTrue;
}



void   CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	// スレッドの作成
	//void func( void *arg )型の関数ポインタを渡すこと！ 

	DWORD threadId;
	HANDLE hThread;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pFunc, (LPVOID)pArg, CREATE_SUSPENDED, &threadId);

	SetThreadPriority(hThread, THREAD_PRIORITY_HIGHEST);

	// スレッドの起動 
	ResumeThread(hThread);
}


void   CDeviceManager::Sleep( Uint32 msec )
{
	::Sleep(msec);
}


gxBool CDeviceManager::PadConfig()
{
	return gxTrue;
}



void CDeviceManager::UpdateMemoryStatus(size_t* uNow, size_t* uTotal, size_t* uMax)
{

}



static wchar_t WCharBuf[1024];
static wchar_t *pWChar = WCharBuf;// NULL;

wchar_t *CDeviceManager::SJIStoUTF16( gxChar* pString , size_t *pSize )
{
	//char から wcharに変換する

	size_t u8Len = strlen( pString );
	size_t maxSize = u8Len*4;

	wchar_t *pW1 = new wchar_t[maxSize];

	//UTF8 -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_ACP, 0, (char*)pString, int(u8Len), pW1, int(maxSize));

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8 *pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy( pW2 , pW1 , sz );
	pW2[sz+0] = 0x00;
	pW2[sz+1] = 0x00;

	SAFE_DELETES(pW1);

	if( pSize ) *pSize = sz;

	return (wchar_t*)pW2;
}


wchar_t* CDeviceManager::UTF8toUTF16(gxChar* pString, size_t* pSize)
{
	//char から wcharに変換する

	size_t u8Len = strlen(pString);
	size_t maxSize = u8Len * 4;

	wchar_t* pW1 = new wchar_t[maxSize];


	//UTF8 -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, int(u8Len), pW1, int(maxSize));

	//SJIS -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar(CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8* pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy(pW2, pW1, sz);
	pW2[sz + 0] = 0x00;
	pW2[sz + 1] = 0x00;

	SAFE_DELETES(pW1);

	if (pSize)* pSize = sz;

	return (wchar_t*)pW2;
}

gxChar* CDeviceManager::UTF16toUTF8(wchar_t *pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;

	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	if (u16len == 0)
	{
		gxChar* pBuf = new gxChar[1];
		if (pSize) *pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}


	gxChar* pBuf = new gxChar[u16len*4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.UTF-8");
	//size_t length = wcstombs( pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte( CP_UTF8, 0, pUTF16buf, -1, &pBuf[0], int(u16len * 4), NULL, NULL);

	if (length > u16len * 4)
	{
		if(pSize) *pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES( pBuf );

	if( pSize ) *pSize = length;

	return (gxChar*)pBuf2;
}


gxChar* CDeviceManager::UTF16toSJIS(wchar_t* pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;
	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	gxChar* pBuf = new gxChar[u16len * 4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.Shift_JIS");
	//size_t length = wcstombs(pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte(CP_ACP, 0, pUTF16buf, -1, pBuf, int(u16len*4), NULL, NULL);

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES(pBuf);

	if (pSize)* pSize = length;

	return (gxChar*)pBuf2;
}

gxChar* CDeviceManager::UTF8toSJIS( gxChar* pUTF8buf, size_t* pSize)
{
	wchar_t *p16  = UTF8toUTF16(pUTF8buf, pSize);
	gxChar* pSJIS = UTF16toSJIS(p16, pSize);
	SAFE_DELETES(p16);

	return pSJIS;
}

gxChar* CDeviceManager::SJIStoUTF8( gxChar* pSJISbuf, size_t* pSize)
{
	wchar_t* p16  = SJIStoUTF16(pSJISbuf, pSize);
	gxChar* pUTF8 = UTF16toUTF8(p16, pSize);
	SAFE_DELETES(p16);

	return pUTF8;

}


gxBool CDeviceManager::PadConfig(Sint32 padNo, Uint32 button)
{
	return gxTrue;
}

std::string CDeviceManager::GetClipBoardString()
{
	//未対応

	std::string str;// = CWindows::GetInstance()->GetClipBoard(0);

	return str;
}

void CDeviceManager::SetClipBoardString(std::string str)
{
	//未対応

	//CWindows::GetInstance()->SetClipBoard( (gxChar*) str.c_str() );
}

std::vector<std::string> CDeviceManager::GetDirList( std::string rootDir )
{
	DirList dir;

	return dir.Search( rootDir );
}

