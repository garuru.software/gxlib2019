﻿#pragma once

#include <gxLib.h>
#ifdef _USE_OPENAL

#ifdef PLATFORM_WINDESKTOP
	#include <Openal/alc.h>
	#include <Openal/al.h>
#endif

#ifdef PLATFORM_WINSTORE
#include <Openal/alc.h>
#include <Openal/al.h>
#endif

#ifdef PLATFORM_ANDROID
	#include "config.h"
	#include "AL/al.h"
	#include "AL/alc.h"
	#include "AL/alext.h"
#endif

#ifdef PLATFORM_IOS
	#import "OpenAL/al.h"
	#import "OpenAL/alc.h"
#endif

class COpenAL {

	enum {
		enSrcMax = 256,
		enBufMax = 256,
	};

public:
	 COpenAL();
	 ~COpenAL();

	void Init();
	void Action();

	gxBool LoadSound( Sint32 index , gxChar *pFileName );
	gxBool PlaySound( Uint32 uIndex , gxBool bLoop = gxFalse , gxBool bOverWrap = gxFalse , Float32 fVolume = 1.0f );
	gxBool StopSound( Uint32 uIndex );

	gxBool ReadSound( Sint32 index );
	gxBool PauseSound( Sint32 index );
	gxBool SetVolume( Uint32 uIndex , Float32 fVolume );
	gxBool SetFrequency(Sint32 index , Float32 fRatio );
	gxBool SetReverb(Sint32 index);

	gxBool IsPlay( Uint32 index);

	void AllCutOff();
	void ResumeAllSound(Float32 vFolume);

	SINGLETON_DECLARE( COpenAL );

private:

	gxBool isRunning();
	void finish();

	gxBool init( Sint32 channelCount );
//	gxBool loadFromAsset( char* fname, Sint32 index );

//	Float32 getChannelVolume( Sint32 index );
//	Float32 setChannelVolume( Sint32 index, Float32 volumeLevel );
//	Float32 getChannelMaxVolume( Sint32 index );
//
//	gxBool seekChannel( Sint32 index, Sint32 pos, Sint32 seekMode );
//	gxBool playChannel( Sint32 index );
//	gxBool resumeChannel( Sint32 index );

//	gxBool isLoaded( Sint32 channelIndex )

//	gxBool getChannelLooping( Sint32 index );
//	gxBool setChannelLooping( Sint32 index, gxBool enable );

	gxBool checkError( const char* message );

	ALCdevice  *m_Device;
	ALCcontext *m_Context;
	Sint32 m_MaxCount;
	ALuint m_Source[enSrcMax]={0};
	ALuint m_Buffer[enBufMax]={0};
	gxBool m_bUsed[enBufMax]={0};

};

#endif