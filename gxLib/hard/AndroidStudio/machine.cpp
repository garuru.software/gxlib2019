//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxDebug.h>
#include <gxLib/gxPadManager.h>
#include <gxLib/gxNetworkManager.h>
#include <gxLib/gxFileManager.h>
#include "COpenGLAndroid.h"
//#include "CAudio.h"
#include "CGamePad.h"

#include <time.h>


//save/load
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/hard/COpenAL.h>
#include "assetpack/assetControl.h"
#include <iostream>
#include<fstream>
#include <gxLib/util/CCsv.h>
#define  LOG_TAG    "gxLib_jni"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

SINGLETON_DECLARE_INSTANCE( CDeviceManager );
SINGLETON_DECLARE_INSTANCE( CAndroid )

static pthread_t test_thread;

enum {
    eLoadASSET,
    eLoadDATA,
    eLoadSD,

    eSaveDATA,
    eSaveSD,
	eSaveVSD,
};


gxBool saveFile2( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize , Uint32 location );

AAssetManager* GetAssetManager()
{
	JNIEnv* env;
	static AAssetManager *s_pAM = nullptr;

	//if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
	//{
	//	return NULL;
	//}

	if( s_pAM == nullptr )
	{
	    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );

		//クラス取得
		jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

		if (jc != 0)
		{
			jmethodID id = env->GetStaticMethodID( jc, "GetAssetManager", "()Ljava/lang/Object;");
			if( id != NULL )
			{
				jobject jobj = env->CallStaticObjectMethod(jc, id);

				//AAssetManager* pAM;
				s_pAM = AAssetManager_fromJava( env, jobj );

				env->DeleteLocalRef(jobj);
			}
		}
	}

	return s_pAM;
}


CDeviceManager::CDeviceManager()
{

}


CDeviceManager::~CDeviceManager()
{
	
}

void CDeviceManager::AppInit()
{
	static int nnn = 0;
	if( nnn == 0 )
	{
		CGameGirl::GetInstance()->Init();
		CAudio::GetInstance()->Init();
        COpenGLAndroid::GetInstance()->Init();
		MemoryInit();
	} else{
        CGameGirl::GetInstance()->SetResume();
    }


	nnn ++;
}

void   CDeviceManager::GameInit()
{
    //バックで受け取った入力機器の情報をgxLib側へ更新する
	CGamePad::GetInstance()->Action();
}


gxBool CDeviceManager::GamePause()
{
	return gxTrue;
}

gxBool CDeviceManager::GameUpdate()
{
	if( AssetController::GetInstance()->IsNowDownLoading() )
	{
		//gxLib::Printf(32,32,1000,ATR_DFLT , ARGB_DFLT , "Now DownLoading..." );
		//gxLib::DrawBox( 32,32,64,64,1000,gxTrue,ATR_DFLT , ARGB_DFLT);
	}

	return gxTrue;
}

void   CDeviceManager::Render()
{
	if( CGameGirl::GetInstance()->IsResume() ) return;

    COpenGLAndroid::GetInstance()->Update();
	COpenGLAndroid::GetInstance()->Render();
}

void CDeviceManager::vSync()
{
	//1/60秒の同期待ち
	static Float32 _TimeOld = gxLib::GetTime();

	Float32 _TimeNow;
	do
	{
		_TimeNow = gxLib::GetTime();

		//if( _TimeNow < _TimeOld )
		//{
		//	gxLib::DebugLog("_TimeNow = %f / %f" , _TimeNow , _TimeOld );
		//}
	}
	while( _TimeNow < ( _TimeOld + (1.0f/ FRAME_PER_SECOND) ) );

	_TimeOld = _TimeNow;

}


void   CDeviceManager::Flip()
{
	COpenGLAndroid::GetInstance()->Present();
}

extern Sint32 m_bInitilizedMemoryUnit;

void   CDeviceManager::Resume()
{
    m_bInitilizedMemoryUnit = 0;
	COpenGLAndroid::DeleteInstance();
	COpenGLAndroid::GetInstance()->Init();
    gxTexManager::GetInstance()->UploadTexture(gxTrue);

    m_bInitilizedMemoryUnit = 1;


/*
	CAudio::DeleteInstance();
	CAudio::GetInstance()->Init();
	gxSoundManager::GetInstance()->ResumeAllSounds();
*/
}


void   CDeviceManager::Movie()
{
	
}


void   CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	static gxNetworkManager::StHTTP *http = NULL;

	if( http == NULL )
	{
		http = gxNetworkManager::GetInstance()->GetNextReq();
        if( http )
        {
            CAndroid::GetInstance()->HttpRequest( http->index , http->m_URL );
        }
	}
	else
	{
		if( http->GetSeq() == 999 )
		{
			http = NULL;
		}
		return gxFalse;
	}

	return gxTrue;

}

void   CDeviceManager::UploadTexture(Sint32 sBank)
{
	COpenGLAndroid::GetInstance()->ReadTexture( sBank );
}

void   CDeviceManager::LogDisp(char* pString)
{
	//__android_log_print(ANDROID_LOG_WARN , APPLICATION_NAME , "%s" , pString );
    __android_log_print(ANDROID_LOG_ERROR , APPLICATION_NAME , "%s" , pString );

}

void CDeviceManager::Clock( gxClock *pClock )
{
	//現在の時刻をミリ秒で取得する
	std::chrono::system_clock::time_point now;
	now = std::chrono::system_clock::now();
	time_t tt = std::chrono::system_clock::to_time_t(now);
	tm local_tm = *localtime(&tt);

	std::chrono::system_clock::duration tp = now.time_since_epoch();
	std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(tp);

	pClock->Year  = local_tm.tm_year + 1900;	// years since 1900
	pClock->Month = local_tm.tm_mon + 1;		// months since January - [0, 11]
	pClock->Day   = local_tm.tm_mday;   		// day of the month - [1, 31]
	pClock->DOW   = local_tm.tm_wday; 			// days since Sunday - [0, 6]
	pClock->Hour  = local_tm.tm_hour; 			// hours since midnight - [0, 23]
	pClock->Min   = local_tm.tm_min; 			// minutes after the hour - [0, 59]
	pClock->Sec   = local_tm.tm_sec; 			// seconds after the minute - [0, 60] including leap second
	pClock->MSec = (us.count()/1000) % (1000);
	pClock->USec = us.count() % (1000);

}

std::string cnvertPlatformFileName( const gxChar* pFileName0 )
{
	//

	gxChar fileName[1024];
	sprintf( fileName , "%s" , pFileName0 );

	Uint32 len = strlen(fileName);

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName[ii] == '\\' )
		{
			fileName[ii] = '/';
		}
	}

	std::string ret = fileName;

	return ret;
}

Uint8* loadFile2( const gxChar* pFileName , size_t* pLength , Uint32 location )
{
	gxChar fileName[1024];

	switch( location ){

	case eLoadASSET:
        {
            // data/data/jp.co.garuru/01_rom/fileName
            sprintf( fileName, "01_rom/" );
            strcat ( fileName, pFileName );

			//AAB対応
			uint8_t *pData = nullptr;
			size_t uSize = 0;
			pData = AssetController::GetInstance()->LoadAABFile( fileName , &uSize );
	        *pLength = uSize;
	        return pData;
        }
        break;

	case eLoadDATA:
        {
            // data/data/jp.co.garuru/03_disk/fileName
            //sprintf( fileName, "03_disk/" );
            //strcat ( fileName, pFileName );
			sprintf( fileName, "/data/data/" );
			strcat ( fileName, CAndroid::GetInstance()->GetPackageName() );
			strcat ( fileName, "/03_disk/" );
			strcat ( fileName, pFileName );
        }
        break;

	case eLoadSD:		//				//読み書き、取り出しOK
		{
			// external SD / internal SD
			char filepath[512];
		    sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
			sprintf( fileName, "%s%s"  , filepath,pFileName );
		}
		break;

	default:
		break;
	}

	Sint32 fh = open((char*)fileName,O_RDONLY);

	if( fh < 0 )
	{
		//ファイルが開けなかった
		//gxLib::DebugLog("[Error:File Not Found:%s\n" , pFileName );	//別の場所で再挑戦して見つかる場合もあるのでエラーログをはかないようにしておく
		return nullptr ;
	}
	else
	{
		struct stat filestat;

		fstat(fh,&filestat);
		size_t sz = filestat.st_size;

		if( filestat.st_size == 0 )
		{
			//sizeがzeroでnullを返すわけにもいかないのでとりあえず１byteのバッファを返すことにする
            Uint8 *pBuffer = new Uint8[1];
			pBuffer[0] = 0x00;
			close(fh);
			GX_DEBUGLOG(" -LoadFile2::ZeroByte File --%s",(char*)fileName );
			return pBuffer;
		}

		size_t readsz = sz;

		Uint8 *pBuffer = new Uint8[ sz ];

		int ret = 1;

		if (readsz > 1024 * 1024 * 1024) readsz = 1024 * 1024 * 1024;
		size_t pos = 0;
		while (ret > 0)
		{
			if (sz < readsz) readsz = sz;

			ret = read(fh, &pBuffer[pos], readsz);
			pos += ret;
			sz -= ret;
		}

		//read(fh,pBuffer,sz );

		close( fh );

        *pLength = sz;

        return pBuffer;
	}
}


Uint8* loadFile( const gxChar* pFileName , size_t* pLength , Uint32 location )
{
	//assetからデータを読み出す(read only)

	Uint32 len = strlen(pFileName);

	gxChar *fileName0 = new gxChar[ 32+len ];

	len = sprintf( fileName0 , "%s" , pFileName );

	gxBool bDirect = gxFalse;

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '\\' )
		{
			fileName0[ii] = '/';
		}

		if( fileName0[ii] == ':' )
		{
			bDirect = gxTrue;
		}
	}

	Uint8 *pData = nullptr;

	if( location == STORAGE_LOCATION_ROM )
	{
		//loadFile

		//asset "01_rom"
		pData = loadFile2(fileName0 , pLength , eLoadASSET );
		//if( pData == nullptr )
		//{
		//	//"03_disk"
		//	pData = loadFile2(fileName0 , pLength , eLoadDATA );
		//}
	}
	else if( location == STORAGE_LOCATION_INTERNAL )
	{
		//loadStorageFile

		//"03_disk"
		pData = loadFile2(fileName0 , pLength , eLoadDATA );

	}
	else if( location == STORAGE_LOCATION_EXTERNAL )
	{
		//04_sd
		pData = loadFile2(fileName0 , pLength , eLoadSD );
	}

	SAFE_DELETES( fileName0 );

	return pData;
}



Uint8* CDeviceManager::LoadFile( const gxChar* pFileName , size_t* pLength , Uint32 location )
{

	Uint32 len = strlen(pFileName);

	gxChar fileName0[ 1024 ];

	len = sprintf( fileName0 , "%s" , pFileName );

	gxBool bDirect = gxFalse;

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName0[ii] == '\\' )
		{
			fileName0[ii] = '/';
		}

		if( fileName0[ii] == ':' )
		{
			bDirect = gxTrue;
		}
	}

    return loadFile( pFileName , pLength , location );
}


bool CreateDirectories(char* pURL )
{
	//フォルダを作成する
	//とりあえずわたってきた名前をそのまま使う
	//Write時には２回（sjis,u8の２回処理が来ることに留意）

	gxBool bSuccess = gxTrue;

	std::vector<char*> separates;
	std::string url = pURL;;
	std::replace(url.begin(), url.end(), '\\', '/' );

	char *pBuf = new char[ url.size()+1];
	sprintf( pBuf, "%s", url.c_str());

	Sint32 cnt = 0;

	for ( Sint32 ii= url.size(); ii>=0; ii-- )
	{
		if ( pBuf[ii] == ':')
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( ii == 0 )
		{
			separates.push_back(&pBuf[0]);
			cnt++;
			break;
		}
		else if ( pBuf[ii] == '/')
		{
			pBuf[ii] = 0x00;
			if( cnt > 0 ) separates.push_back(&pBuf[ii + 1]);
			cnt++;
		}
	}

	size_t max = separates.size();
	std::string path = "";

	for (Sint32 ii = 0; ii < max; ii++)
	{
		path += separates[max - 1 - ii];

		if (!mkdir( path.c_str(), S_IRWXU ))
		{
			bSuccess = gxFalse;
		}

		path += "/";
	}

	delete[] pBuf;

	return bSuccess;
}


gxBool saveFile2( const gxChar* pFileName , Uint8* pWritedBuf , Uint32 uSize , Uint32 location )
{
    char filepath[512];

    switch( location ){

        case eSaveDATA:
            //SaveStorageFile
            sprintf( filepath, "/data/data/" );
            strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
            strcat( filepath, "/03_disk/" );
            strcat( filepath, pFileName );
            break;


        case eSaveSD:
            //SaveFile://SD
            sprintf( filepath, "%s/",CAndroid::GetInstance()->GetSD_Path(0) );
            strcat( filepath, pFileName );
            break;

		case eSaveVSD:
			//SaveVirtualSD
			sprintf( filepath, "/data/data/" );
			strcat( filepath, CAndroid::GetInstance()->GetPackageName() );
			strcat( filepath, "/03_VirtualSD/" );
			strcat( filepath, pFileName );
			break;

        default:
            break;
    }

    int fh;

    fh = open( (char*)filepath , O_WRONLY|O_CREAT , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

    if( fh < 0 )
    {
        //書き込みミス

		//ディレクトリを作って再度試す

		CreateDirectories( (char*)filepath );

		{
		    fh = open( (char*)filepath , O_WRONLY|O_CREAT , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );

		    if( fh < 0 )
		    {
				return gxFalse;
			}
		}
    }

	write(fh,pWritedBuf,uSize);

    close( fh );

    return gxTrue;
}

gxBool CDeviceManager::SaveFile( const gxChar* pFileName , Uint8* pWritedBuf , size_t uSize , Uint32 location )
{
    switch( location ){
        case STORAGE_LOCATION_ROM:
            return gxFalse;

        case STORAGE_LOCATION_INTERNAL:
 			return saveFile2( pFileName , pWritedBuf , uSize , eSaveDATA );


        case STORAGE_LOCATION_EXTERNAL:
        	if( saveFile2( pFileName , pWritedBuf , uSize , eSaveSD ))
			{
				return gxTrue;
			}
			//保存失敗したらテンポラリに
			saveFile2( pFileName , pWritedBuf , uSize , eSaveVSD );
        	return gxFalse;

        default:
            break;
    }

	return gxTrue;
}

void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	pthread_create( &test_thread, NULL, pFunc, (void *)pArg);

}

void CDeviceManager::Sleep( Uint32 msec )
{
	struct timespec	 req, res;
	req.tv_sec  = 0;
	req.tv_nsec = msec * 1000000;

	nanosleep(&req, &res);
}

gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxFalse;
}


void CDeviceManager::UpdateMemoryStatus(size_t* uNow, size_t* uTotal, size_t* uMax)
{
	size_t uNowByte;
	size_t uTotalByte;
	size_t uMaxByte;

	::UpdateMemoryStatus(&uNowByte, &uTotalByte, &uMaxByte);

	*uNow = (uNowByte >> 10) >> 10;
	*uTotal = (uTotalByte >> 10) >> 10;
	*uMax = (uMaxByte >> 10) >> 10;

}


void CDeviceManager::ToastDisp( gxChar* pString )
{
	//Toastテキストを表示する

    JNIEnv* env;

    if( CAndroid::GetInstance()->m_pJavaVM == NULL )
    {
       return;
    }

    //if( CAndroid::GetInstance()->m_pJavaVM->GetEnv( (void**)&env, JNI_VERSION_1_6 ) != JNI_OK )
	//if( CAndroid::GetInstance()->GetJavaEnv() == 0 )
    //{
    //    return;
    //}

    CAndroid::GetInstance()->m_pJavaVM->AttachCurrentThread( &env, NULL );
    jclass jc = env->FindClass( JAVA_PROGRAM_PATH );

    if (jc != 0)
    {
        jmethodID id = env->GetStaticMethodID(jc, "SetToastText", "(Ljava/lang/String;)V");
        //if( id != 0 )
        {
            //static jchar buf[FILENAMEBUF_LENGTH];
            //int len = 0;
            //len = sprintf( (char*)buf , "%s",pString);
            const char* pStr = pString;

            jstring str = env->NewStringUTF( pStr );
            env->CallStaticVoidMethod(jc, id ,str );
        }
    }
}

/*
gxChar*  CDeviceManager::UTF8toUTF32(gxChar* pString, size_t* pSize)
{
    setlocale(LC_ALL, "JPN");
    static wchar_t destBuf[FILENAMEBUF_LENGTH];

    mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );

    size_t len = wcslen(destBuf);
    if(pSize)
    {
        *pSize = len*4;
    }

    return (gxChar*)destBuf;
}
*/

wchar_t* CDeviceManager::UTF8toUTF16( gxChar*  pString  , size_t* pSize )
{
	//Androidではこれで問題なし

	setlocale(LC_ALL, "JPN");
	static wchar_t destBuf[FILENAMEBUF_LENGTH];

	mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );

	return destBuf;
};


wchar_t* CDeviceManager::SJIStoUTF16( gxChar*  pString  , size_t* pSize )
{
//    setlocale(LC_ALL, "JPN");
//    static wchar_t destBuf[FILENAMEBUF_LENGTH];
//
//    mbstowcs(destBuf, pString, FILENAMEBUF_LENGTH );
//
//    return destBuf;
	return NULL;
};


gxChar*  CDeviceManager::UTF16toUTF8( wchar_t* pUTF16buf, size_t* pSize )
{
	setlocale(LC_ALL, "JPN");
	static char destBuf[FILENAMEBUF_LENGTH];

	wcstombs( destBuf, pUTF16buf , FILENAMEBUF_LENGTH );

	return destBuf;
};


gxChar*  CDeviceManager::UTF16toSJIS( wchar_t* pUTF16buf, size_t* pSize )
{
	return NULL;
};

gxChar*  CDeviceManager::UTF8toSJIS ( gxChar*  pUTF8buf , size_t* pSize )
{
	return NULL;
};

gxChar*  CDeviceManager::SJIStoUTF8 ( gxChar*  pSJISbuf , size_t* pSize )
{
	return NULL;
};

gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}


std::string CDeviceManager::GetClipBoardString()
{
	CAndroid::GetInstance()->m_bGetClipBoardText.store( gxTrue );

	while( CAndroid::GetInstance()->m_bGetClipBoardText.load() )
	{
		gxLib::Sleep(1);
	}

	return CAndroid::GetInstance()->m_GetClipBoardText;
}

void CDeviceManager::SetClipBoardString(std::string str)
{
	CAndroid::GetInstance()->SetClipBoardText( str );
}


void CDeviceManager::OpenWebClient( gxChar* pURL , gxBool bOpenBrowser )
{
    //WebViewを表示するリクエストを発行する

    sprintf( CAndroid::GetInstance()->m_WebViewURLString,"%s" , pURL );
	CAndroid::GetInstance()->m_bOpenWebBrowser = bOpenBrowser;
}


Float32 CDeviceManager::GetFreeStorageSize()
{
	CAndroid::GetInstance()->m_bGetAvailableSize.store( gxTrue );

	while( CAndroid::GetInstance()->m_bGetAvailableSize.load() )
	{
		gxLib::Sleep(1);
	}

	size_t size = CAndroid::GetInstance()->m_AvailableSize.load();

	size = size / 1024;
	return Float32( size / 1024.0f );
}

std::vector<std::string> CDeviceManager::GetDirList( std::string rootDir )
{
	std::vector<std::string> ret;
	return ret;

}
