#include <gxLib.h>
#include <gxLib/gx.h>
#include <string.h>
#include <iostream>
#include<fstream>
#include <fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <unistd.h>
#include <play/asset_pack.h>
#include <string>
#include <vector>
#include <android/log.h>
#include <android/asset_manager.h>
#include <stdio.h>

#include "assetControl.h"
#include "assetManagerInternal.h"
#include "GameAssetManager.h"

assetManagerInternal::assetManagerInternal(AAssetManager *assetManager, JavaVM *jvm, jobject nativeActivity)
{

	// アセットパックマネージャを初期化する

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackErrorCode assetPackErrorCode = AssetPackManager_init(jvm, nativeActivity);	// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------

	if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
	{
		//初期化成功
		GX_DEBUGLOG("GameAssetManager: Initialized Asset Pack Manager");
		mAssetPackErrorMessage = "No Error";
		mAssetPackManagerInitialized = true;

	}
	else
	{
		//エラーがあった、初期化失敗
		mAssetPackManagerInitialized = false;
		setAssetPackErrorStatus(assetPackErrorCode, NULL, "GameAssetManager: Asset Pack Manager initialization");
	}

	mAssetManager   = assetManager;				//アセットマネージャのポインタは使いまわせる？？？
	mNativeActivity = nativeActivity;			

	mRequestingMobileDownload = false;			//携帯でのダウンロードリクエスト中かどうか？


	//３種類のアセットパックを用意する

	mAssetPacks.push_back( new AssetPackInfo() );
	mAssetPacks[0]->mPackType = GAMEASSET_PACKTYPE_INTERNAL;
	mAssetPacks[0]->mPackName = INSTALL_ASSETPACK_NAME;

	mAssetPacks.push_back( new AssetPackInfo() );
	mAssetPacks[1]->mPackType = GAMEASSET_PACKTYPE_FASTFOLLOW;
	mAssetPacks[1]->mPackName = FASTFOLLOW_ASSETPACK_NAME;

	mAssetPacks.push_back( new AssetPackInfo() );
	mAssetPacks[2]->mPackType = GAMEASSET_PACKTYPE_ONDEMAND;
	mAssetPacks[2]->mPackName = ONDEMAND_ASSETPACK_NAME;


	for (int i = 0; i < mAssetPacks.size(); ++i)
	{
		//----------------------------------------------------------
		//AssetPackDefinition情報をAssetPackInfo情報に変換する
		//----------------------------------------------------------

		//各パックの状態を初期化する、INTERNALの場合はすでに準備ができていることにしている
		if (mAssetPacks[i]->mPackType == GAMEASSET_PACKTYPE_INTERNAL)
		{
			//内部の場合、ファイルはデバイス上に存在し、使用する準備ができていると想定します
			mAssetPacks[i]->mAssetPackStatus = GAMEASSET_READY;
			mAssetPacks[i]->mAssetPackCompletion = 1.0f;
		} else {
			// mark as waiting for status since the asset pack status query is an async operation
			// アセットパックのステータスクエリ(状態の問い合わせ)は非同期操作であるため、ステータスを待機しているとマークします
			//★？★　いつステータスが変わるのか？
			mAssetPacks[i]->mAssetPackStatus = GAMEASSET_WAITING_FOR_STATUS;
		}
	}

	if (mAssetPackManagerInitialized)
	{
		//初期化が済んでいればすべてのアセットパックの要求を同時に開始する
		// Start asynchronous requests to get information about our asset packs

		for (int i = 0; i < mAssetPacks.size(); ++i)
		{
			const char *packName = mAssetPacks[i]->mPackName;

			//----------------------------------------------------------------------------------------------------------------------------------------
			assetPackErrorCode = AssetPackManager_requestInfo(&packName, 1);	// --------------------- ★★★
			//----------------------------------------------------------------------------------------------------------------------------------------

			if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
			{
				GX_DEBUGLOG("GameAssetManager: Requested asset pack info for %s", packName);
			}
			else
			{
				mAssetPackManagerInitialized = false;
				setAssetPackErrorStatus(assetPackErrorCode, mAssetPacks[i],"GameAssetManager: requestInfo");

				//エラーが起きたらリクエスト中止
				break;
			}
		}
	}
}

assetManagerInternal::~assetManagerInternal()
{
	// Delete our allocated asset pack info structures
	for (std::vector<AssetPackInfo *>::iterator iter = mAssetPacks.begin(); iter != mAssetPacks.end(); ++iter)
	{
		delete *iter;
	}

	mAssetPacks.clear();

	// Shut down the asset pack manager

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackManager_destroy();	// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------
}

uint8_t* assetManagerInternal::loadExternal(const char *assetName, size_t *bufferSize)
{
	//外部ファイルを読み込む

	uint8_t *pData = nullptr;

	//フルパスを生成していつものCのfile関数で読み込みを行う

	FILE *fp = fopen(assetName, "rb");

	if (fp != NULL)
	{
		struct stat fileStats;
		uint64_t assetSize = 0;
		int statResult = fstat(fileno(fp), &fileStats);

		if (statResult == 0)
		{
			assetSize = fileStats.st_size;
			pData = new uint8_t[assetSize+1];
			fread(pData, assetSize, 1, fp);
			*bufferSize = assetSize;
			pData[assetSize] = 0x00;
		}
		else
		{
		}

		fclose(fp);

/*
		//AAB領域にファイルを書き込めるかテスト
		char buf[256];
		sprintf(buf,"%s_write",assetName);

		int fh;
		fh = open( (char*)buf , O_WRONLY|O_CREAT , S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH );
		write(fh,"test",4);
		close( fh );
*/

	}

	return pData;
}


uint8_t* assetManagerInternal::loadInternal(const char *assetName, size_t *bufferSize)
{
	//内部ファイルを読み込む（多分メインスレッド必須）

	AAsset *asset = AAssetManager_open(mAssetManager, assetName, AASSET_MODE_STREAMING);

	if (asset != NULL)
	{
		size_t assetSize = AAsset_getLength(asset);

		uint8_t *pData = new uint8_t[assetSize+1];

		AAsset_read(asset, pData, assetSize);

		*bufferSize = assetSize;
		pData[assetSize] = 0x00;

		AAsset_close(asset);

		return pData;
	}

	return nullptr;
}


AssetPackInfo *assetManagerInternal::GetAssetPackByName(const char *assetPackName)
{
	//アセットパック名からアセットパック情報を取得する

	AssetPackInfo *packInfo = NULL;

	for (int i = 0; i < mAssetPacks.size(); ++i)
	{
		if (strcmp(assetPackName, mAssetPacks[i]->mPackName) == 0)
		{
			packInfo = mAssetPacks[i];
			break;
		}
	}

	return packInfo;
}


bool assetManagerInternal::generateFullAssetPath(const char *assetName, const AssetPackInfo *packInfo, char *pathBuffer, const size_t bufferSize)
{
	//アセットのフルパスを作成する？

	bool generatedPath = false;

	const size_t requiredSize = strlen(assetName) + strlen(packInfo->mAssetPackBasePath) + 1;

	if (requiredSize < MAX_ASSET_PATH_LENGTH)
	{
		//ファイルパスの長さが想定内だったらファイルパスとして結合する
		generatedPath = true;
		strncpy(pathBuffer, packInfo->mAssetPackBasePath, MAX_ASSET_PATH_LENGTH);
		strncat(pathBuffer, assetName, MAX_ASSET_PATH_LENGTH);
	}

//	GX_DEBUGLOG("GameAssetManager: full external asset path for %s : %s", assetName, pathBuffer);
	GX_DEBUGLOG("%s : %s", assetName, pathBuffer);

	return generatedPath;
}


// New asset pack support functions
bool assetManagerInternal::RequestAssetPackDownload(const char *assetPackName)
{
	//アセットパックのダウンロードを要求する

	GX_DEBUGLOG("GameAssetManager: RequestAssetPackDownload %s", assetPackName);

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackErrorCode assetPackErrorCode = AssetPackManager_requestDownload(&assetPackName, 1);// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------

	bool success = (assetPackErrorCode == ASSET_PACK_NO_ERROR);

	if (success)
	{
		//Stateを「ダウンロード中」に変更する
		changeAssetPackStatus( GetAssetPackByName(assetPackName), GAMEASSET_DOWNLOADING );
	}
	else
	{
		setAssetPackErrorStatus(assetPackErrorCode, GetAssetPackByName(assetPackName), "GameAssetManager: requestDownload");
	}

	return success;
}


void assetManagerInternal::RequestAssetPackCancelDownload(const char *assetPackName)
{
	//アセットパックのダウンロードをキャンセルする

	GX_DEBUGLOG("GameAssetManager: RequestAssetPackCancelDownload %s", assetPackName);
	// Request cancellation of the download, this is a request, it is not guaranteed that the download will be canceled.
	// ダウンロードのキャンセルをリクエストする。これはリクエストです。ダウンロードがキャンセルされる保証はありません。

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackManager_cancelDownload(&assetPackName, 1);// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------
}

bool assetManagerInternal::RequestAssetPackRemoval(const char *assetPackName)
{
	//ファイルの削除をする
	GX_DEBUGLOG("GameAssetManager: RequestAssetPackRemoval %s", assetPackName);

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackErrorCode assetPackErrorCode = AssetPackManager_requestRemoval(assetPackName);// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------

	bool success = (assetPackErrorCode == ASSET_PACK_NO_ERROR);

	if (success)
	{
		changeAssetPackStatus(GetAssetPackByName(assetPackName), GAMEASSET_PENDING_ACTION);

	}
	else
	{
		setAssetPackErrorStatus(assetPackErrorCode, GetAssetPackByName(assetPackName), "GameAssetManager: requestDelete");
	}
	return success;
}


void assetManagerInternal::RequestMobileDataDownloads()
{
	//画面上のファイル名をクリックしたら呼び出されるやつ
	//WIFI以外でのダウンロードのリクエストを行う

	GX_DEBUGLOG("GameAssetManager: RequestMobileDataDownloads");

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackErrorCode assetPackErrorCode = AssetPackManager_showCellularDataConfirmation( mNativeActivity ); // --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------

	setAssetPackErrorStatus(assetPackErrorCode, NULL, "GameAssetManager: RequestCellularDownload");

	if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
	{
		mRequestingMobileDownload = true;
	}
}

void assetManagerInternal::updateAssetPackBecameAvailable(AssetPackInfo *assetPackInfo)
{
	//ASSET_PACK_DOWNLOAD_COMPLETEDになったら呼び出されファイルの状態によってパスを生成する

	GX_DEBUGLOG("GameAssetManager: ProcessAssetPackBecameAvailable : %s", assetPackInfo->mPackName);

	if (assetPackInfo->mAssetPackStatus != GAMEASSET_READY)
	{
		assetPackInfo->mAssetPackStatus = GAMEASSET_READY;
		assetPackInfo->mAssetPackCompletion = 1.0f;

		// Get the path of the directory containing the asset files for
		// this asset pack
		AssetPackLocation *assetPackLocation = NULL;

		//----------------------------------------------------------------------------------------------------------------------------------------
		//アセットパックのダウンロード場所を格納する
		AssetPackErrorCode assetPackErrorCode = AssetPackManager_getAssetPackLocation( assetPackInfo->mPackName, &assetPackLocation);// --------------------- ★★★
		//----------------------------------------------------------------------------------------------------------------------------------------

		if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
		{
			AssetPackStorageMethod storageMethod = AssetPackLocation_getStorageMethod( assetPackLocation );

			// ASSET_PACK_STORAGE_APK				アセットパックは、パックされたアセットファイルを含むAPKとしてインストールされます。アセットには、AAssetManagerを介してアクセスできます。
			// ASSET_PACK_STORAGE_FILES				アセットパックは、個々のアセットファイルを含むフォルダーに解凍されます。アセットには、標準のファイルAPIを介してアクセスできます。
			// ASSET_PACK_STORAGE_NOT_INSTALLED		アセットパックがインストールされていません。
			// ASSET_PACK_STORAGE_UNKNOWN			不明なエラー

			if (storageMethod == ASSET_PACK_STORAGE_FILES)
			{
				//通常のファイルIO関数でアクセス可能であればアセットのパスを取得しておく

				const char *assetPackPath = AssetPackLocation_getAssetsPath(assetPackLocation);

				if (assetPackPath != NULL)
				{
					// パスのコピーを作成し、パス区切り文字がまだ存在しない場合は最後に追加します

					size_t pathLength = strlen(assetPackPath);
					bool needPathDelimiter = (assetPackPath[pathLength] != '/');

					if (needPathDelimiter)
					{
						++pathLength;
					}

					char *pathCopy = new char[pathLength + 1];
					pathCopy[pathLength] = '\0';
					strncpy(pathCopy, assetPackPath, pathLength);

					if (needPathDelimiter)
					{
						pathCopy[pathLength - 1] = '/';
					}
					assetPackInfo->mAssetPackBasePath = pathCopy;
				}
			}

			//----------------------------------------------------------------------------------------------------------------------------------------
			AssetPackLocation_destroy(assetPackLocation);	// --------------------- ★★★
			//----------------------------------------------------------------------------------------------------------------------------------------

		}
		else
		{
			setAssetPackErrorStatus(assetPackErrorCode, assetPackInfo, "GameAssetManager: getAssetPackLocation" );
		}
	}
}

void assetManagerInternal::UpdateAssetPackFromDownloadState(AssetPackInfo *assetPackInfo, AssetPackDownloadState *downloadState)
{
	//パックのダウンロード情報が毎フレーム更新される

	//----------------------------------------------------------------------------------------------------------------------------------------
	AssetPackDownloadStatus downloadStatus = AssetPackDownloadState_getStatus( downloadState );	// --------------------- ★★★
	//----------------------------------------------------------------------------------------------------------------------------------------

	switch (downloadStatus) {
	case ASSET_PACK_UNKNOWN:
		//謎のアセットパック。AssetPackManager_requestInfo（）を呼び出してサイズを確認するか、AssetPackManager_requestDownload（）でダウンロードを開始してください。
		break;

	case ASSET_PACK_DOWNLOAD_PENDING:
		//AssetPackManager_requestDownload（）非同期リクエストは保留中です。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_DOWNLOADING);
		assetPackInfo->mAssetPackCompletion = 0.0f;
		break;

	case ASSET_PACK_DOWNLOADING: {
		//アセットパックのダウンロードが進行中です。
			changeAssetPackStatus(assetPackInfo, GAMEASSET_DOWNLOADING);
			uint64_t dlBytes = AssetPackDownloadState_getBytesDownloaded(downloadState);
			uint64_t totalBytes = AssetPackDownloadState_getTotalBytesToDownload(downloadState);
			double dlPercent = ((double) dlBytes) / ((double) totalBytes);
			assetPackInfo->mAssetPackCompletion = (float) dlPercent;
		}
		break;

	case ASSET_PACK_TRANSFERRING:
		//アセットパックがアプリに転送されています。
		break;

	case ASSET_PACK_DOWNLOAD_COMPLETED:
		//ダウンロードと転送が完了しました。アセットはアプリで利用できます。
		updateAssetPackBecameAvailable(assetPackInfo);
		break;

	case ASSET_PACK_DOWNLOAD_FAILED:
		//AssetPackManager_requestDownload（）が失敗しました。
		//AssetPackErrorCodeは対応するエラーであり、ASSET_PACK_NO_ERRORではありません。
		//このステータスでは、bytes_downloadedフィールドとtotal_bytes_to_downloadフィールドは信頼できません。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_ERROR);
		break;

	case ASSET_PACK_DOWNLOAD_CANCELED:
		//アセットパックのダウンロードはキャンセルされました。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_NEEDS_DOWNLOAD);
		assetPackInfo->mAssetPackCompletion = 0.0f;
		break;

	case ASSET_PACK_WAITING_FOR_WIFI:
		//アセットパックのダウンロードは、Wi-Fiが進行するのを待っています。
		//必要に応じて、AssetPackManager_showCellularDataConfirmation（）を呼び出して、セルラーデータを介したダウンロードの確認をユーザーに依頼します
		changeAssetPackStatus(assetPackInfo, GAMEASSET_NEEDS_MOBILE_AUTH);
		break;

	case ASSET_PACK_NOT_INSTALLED:
		{
			//アセットパックがインストールされていません。
			changeAssetPackStatus(assetPackInfo, GAMEASSET_NEEDS_DOWNLOAD);
			uint64_t totalBytes = AssetPackDownloadState_getTotalBytesToDownload(downloadState);

			if (totalBytes > 0)
			{
				assetPackInfo->mAssetPackDownloadSize = totalBytes;
			}
		}
		break;

	case ASSET_PACK_INFO_PENDING:
		//AssetPackManager_requestInfo（）非同期リクエストが開始されましたが、結果はまだ不明です。

		break;
	case ASSET_PACK_INFO_FAILED:
		//AssetPackManager_requestInfo（）非同期要求が失敗しました。
		//AssetPackErrorCodeは、対応するエラーになります。
		//ASSET_PACK_NO_ERRORは決してありません。
		//このステータスでは、bytes_downloadedフィールドとtotal_bytes_to_downloadフィールドは信頼できません。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_ERROR);
		break;

	case ASSET_PACK_REMOVAL_PENDING:
		//AssetPackManager_requestRemoval（）非同期リクエストが開始されました。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_PENDING_ACTION);
		break;

	case ASSET_PACK_REMOVAL_FAILED:
		//AssetPackManager_requestRemoval（）非同期要求が失敗しました。
		changeAssetPackStatus(assetPackInfo, GAMEASSET_READY);
		assetPackInfo->mAssetPackCompletion = 1.0f;
		break;
	default:
		break;
	}
}

void assetManagerInternal::setAssetPackErrorStatus(const AssetPackErrorCode assetPackErrorCode, AssetPackInfo *assetPackInfo, const char *message)
{
	//アセットパックのエラー状態を更新する

	switch (assetPackErrorCode) {
		case ASSET_PACK_NO_ERROR:
			// No error, so return immediately.
			return;
		case ASSET_PACK_APP_UNAVAILABLE:
			mAssetPackErrorMessage = "ASSET_PACK_APP_UNAVAILABLE";
			break;
		case ASSET_PACK_UNAVAILABLE:
			mAssetPackErrorMessage = "ASSET_PACK_UNAVAILABLE";
			break;
		case ASSET_PACK_INVALID_REQUEST:
			mAssetPackErrorMessage = "ASSET_PACK_INVALID_REQUEST";
			break;
		case ASSET_PACK_DOWNLOAD_NOT_FOUND:
			mAssetPackErrorMessage = "ASSET_PACK_DOWNLOAD_NOT_FOUND";
			break;
		case ASSET_PACK_API_NOT_AVAILABLE:
			mAssetPackErrorMessage = "ASSET_PACK_API_NOT_AVAILABLE";
			break;
		case ASSET_PACK_NETWORK_ERROR:
			mAssetPackErrorMessage = "ASSET_PACK_NETWORK_ERROR";
			break;
		case ASSET_PACK_ACCESS_DENIED:
			mAssetPackErrorMessage = "ASSET_PACK_ACCESS_DENIED";
			break;
		case ASSET_PACK_INSUFFICIENT_STORAGE:
			mAssetPackErrorMessage = "ASSET_PACK_INSUFFICIENT_STORAGE";
			break;
		case ASSET_PACK_PLAY_STORE_NOT_FOUND:
			mAssetPackErrorMessage = "ASSET_PACK_PLAY_STORE_NOT_FOUND";
			break;
		case ASSET_PACK_NETWORK_UNRESTRICTED:
			mAssetPackErrorMessage = "ASSET_PACK_NETWORK_UNRESTRICTED";
			break;
		case ASSET_PACK_INTERNAL_ERROR:
			mAssetPackErrorMessage = "ASSET_PACK_INTERNAL_ERROR";
			break;
		case ASSET_PACK_INITIALIZATION_NEEDED:
			mAssetPackErrorMessage = "ASSET_PACK_INITIALIZATION_NEEDED";
			break;
		case ASSET_PACK_INITIALIZATION_FAILED:
			mAssetPackErrorMessage = "ASSET_PACK_INITIALIZATION_FAILED";
			break;

		default:
			mAssetPackErrorMessage = "Unknown error code";
			break;
	}

	if (assetPackInfo == NULL) {
		LOGE("%s failed with error code %d : %s", message, static_cast<int>(assetPackErrorCode), mAssetPackErrorMessage);
	} else {
		assetPackInfo->mAssetPackStatus = GAMEASSET_ERROR;
		LOGE("%s failed on asset pack %s with error code %d : %s",
			 message, assetPackInfo->mPackName,
			 static_cast<int>(assetPackErrorCode),
			 mAssetPackErrorMessage);
	}
}

void assetManagerInternal::UpdateMobileDataRequestStatus()
{

	//Wifi以外のデータダウンロードを許可するか？

	if (mRequestingMobileDownload)
	{
		ShowCellularDataConfirmationStatus cellularStatus;

		//----------------------------------------------------------------------------------------------------------------------------------------
		AssetPackErrorCode assetPackErrorCode = AssetPackManager_getShowCellularDataConfirmationStatus(&cellularStatus);// --------------------- ★★★
		//----------------------------------------------------------------------------------------------------------------------------------------
	
		setAssetPackErrorStatus(assetPackErrorCode, NULL,"GameAssetManager: UpdateCellularRequestStatus");

		if (assetPackErrorCode == ASSET_PACK_NO_ERROR)
		{
			if (cellularStatus == ASSET_PACK_CONFIRM_USER_APPROVED)
			{
				//承認した

				mRequestingMobileDownload = false;
				GX_DEBUGLOG("GameAssetManager: User approved mobile data download");
			}
			else if (cellularStatus == ASSET_PACK_CONFIRM_USER_CANCELED)
			{
				//キャンセルした
				
				mRequestingMobileDownload = false;
				GX_DEBUGLOG("GameAssetManager: User declined mobile data download");
			}
		}
	}
}

