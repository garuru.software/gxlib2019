#ifndef assetControl_h
#define assetControl_h

//AABから複数のPAKをまたいでファイルを読み込む

enum GameAssetPackType {
    // This asset pack type is always available and included in the application package
    GAMEASSET_PACKTYPE_INTERNAL = 0,
    // This asset pack type is downloaded separately but is an automatic download after
    // app installation
    GAMEASSET_PACKTYPE_FASTFOLLOW,
    // This asset pack type is only downloaded when specifically requested
    GAMEASSET_PACKTYPE_ONDEMAND
};

enum GameAssetStatus {
    // The named asset pack was not recognized as a valid asset pack name
    GAMEASSET_NOT_FOUND = 0,

    // The asset pack is waiting for information about its status to become available
    GAMEASSET_WAITING_FOR_STATUS,

    // The asset pack needs to be downloaded to the device
    GAMEASSET_NEEDS_DOWNLOAD,

    // The asset pack is large enough to require explicit authorization to download
    // over a mobile data connection as wi-fi is currently unavailable
    GAMEASSET_NEEDS_MOBILE_AUTH,

    // The asset pack is in the process of downloading to the device
    GAMEASSET_DOWNLOADING,

    // The asset pack is ready to be used
    GAMEASSET_READY,

    // The asset pack is pending the results of a request for download cancellation,
    // deletion or cellular download authorization
    GAMEASSET_PENDING_ACTION,

    // The asset pack is in an error state and cannot be used or downloaded
    GAMEASSET_ERROR
};

class GameAssetManager;

class AssetController
{

public:

	std::string text[4];
	std::vector<gxUtil::RoundButton*> loadbtns;

	AssetController();
	~AssetController()
	{
	}

	static AssetController* GetInstance()
	{
		if( s_pInstance == nullptr )
		{
			s_pInstance = new AssetController();
		}

		return s_pInstance;
	}

	void Update();

	gxBool IsNowDownLoading()
	{
		return m_bDownLoading;
	}


	GameAssetStatus GetCurrentStatus( int n );

	uint8_t* LoadAABFile( char* pFileName , size_t *uSize );

	//デバッグ用
	std::string GetStatus( int n );
	std::string GetStatus2( int n );
	uint8_t* LoadTestFile( char* pFileName );
	Float32 m_fDownLoadStatus[3]={0};
	size_t m_PackSize[3]={0};

	void RequestDownLoad( int n );
	void RemovePak( int n );
	void RequestCancelDownLoad(int n);

	std::string GetAssetPackStatus( const char  *pAssetName );
    std::string GetErrorMessage(GameAssetStatus assetPackStatus);

	static AssetController* s_pInstance;

private:


	std::string m_CurrentStatus[3];
	std::string m_CurrentStatus2[3];

	GameAssetStatus m_AssetPackStatus[3]={GAMEASSET_WAITING_FOR_STATUS,GAMEASSET_WAITING_FOR_STATUS,GAMEASSET_WAITING_FOR_STATUS};
	GameAssetManager *p_gameAssetManager = nullptr;

	gxBool m_bDownLoading = gxFalse;
	Sint32 m_Sequence = 0;
	Sint32 m_ReloadCnt = 0;
};


#endif
