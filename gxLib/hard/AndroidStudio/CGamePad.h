﻿#ifndef _CGAMEPAD_H_
#define _CGAMEPAD_H_

class CGamePad {
public:
	typedef struct StMouse {
		gxBool m_bPush;
		Sint32 mx, my;

		void Reset() {
			m_bPush = gxFalse;
			mx = my = 0;
		}

	} StMouse;

	enum {
		enCursorMax = 8,
		enKeyMax = 256,
		enAnalogMax = 32,
	};

	CGamePad() {
		for (Sint32 ii = 0; ii < enCursorMax; ii++) {
			m_Cursor[ii].Reset();
		}

		for (Sint32 ii = 0; ii < enKeyMax; ii++) {
			m_KeyBoardPush[ii] = 0x00;
			//m_PadPush[ii] = 0x00;
		}
		//addController(100);
	}

	~CGamePad() {
	}

	void Action();


	void InputKeyCheck(Sint32 id, Sint32 push, Sint32 keyCode);

	void InputCursorCheck(Sint32 id, gxBool bPush, Sint32 px, Sint32 py);

	void SetAnalog(Sint32 id, Float32 *pAnalogInfo);

	void SetCensorData(Float32 *pData);

SINGLETON_DECLARE(CGamePad);

private:

	enum {
		enID_NoneAccel,            //
		enID_ChangeFullScreenMode,//= 11001,	//フルスクリーン切り替え
		enID_AppExit,//= 11002,	//アプリ終了
		enID_GamePause,//= 11003,	//ゲームのポーズ
		enID_GameStep,//= 11004,	//ゲームのステップ
		enID_PadEnableSwitch,//= 11005,	//コントローラー設定
		enID_DebugMode,//= 11006,	//デバッグモードのON/OFF
		enID_Reset,//= 11007,	//リセット
		enID_ScreenShot,//= 11008,	//スクリーンショット
		enID_FullSpeed,//= 11009,	//フルスピード
		enID_SoundSwitch,//= 11010,	//サウンドOn /Off
		enID_SamplingFilter,//= 11011,	//サンプリングフィルター

		enID_MasterVolumeAdd,//= 11012,	//Volume+
		enID_MasterVolumeSub,//= 11013,	//Volume+
		enID_SwitchVirtualPad,//= 11014,	//VirtualPad On/ Off
		enID_Switch3DView,//= 11015,	//3DView
		enAccelMax,//= 11,
	};

	Sint32 m_Accellarator[enAccelMax] = {0};
	StMouse m_Cursor[enCursorMax];

	Sint32 ConvertKeyNumber(Sint32 id, Sint32 *pKey, Sint32 *pPad);

	void adjustDefaultInput();

	void CheckAccellarator(Sint32 id, gxBool bPush);

	Uint8 m_KeyBoardPush[enKeyMax];
	std::map<Sint32, Uint32> m_PadPush;//[enKeyMax];
	std::map<Sint32, gxPadManager::StAnalog> m_fAnalog;//[enAnalogMax]={0};
	Float32 m_fCensors[3 * 4] = {0};
	std::vector<Sint32> m_PlayerID;

	void addController(Sint32 id)
	{
		//コントローラー番号とプレイヤー番号を関連付ける
		Sint32 n = getPlayerIndex(id);
		if( n == -1 )
		{
			m_PadPush[id] = 0x00;
			m_fAnalog[id] = {0x00};
			m_PlayerID.push_back(id);
		}
	}


	Sint32 getPlayerIndex(Sint32 controllerID)
	{
		//コントローラー番号のプレイヤー番号を取得する

		for( Sint32 ii=0; ii<m_PlayerID.size(); ii++ )
		{
			if(m_PlayerID[ii] == controllerID) return ii;
		}

		return -1;
	}

};

#endif

