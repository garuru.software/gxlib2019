﻿//-------------------------------------------------------------------------------------
//
//
// http client for Windows
//
//
//-------------------------------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gxNetworkManager.h>
#include "CHttpClient.h"

#pragma comment(lib,"wininet.lib")

#define URLBUFFER_SIZE		(4096)

static Sint32 m_CurrentID = -1;


SINGLETON_DECLARE_INSTANCE( CHttpClient );

//------------

CHttpClient::StDLTaskStat::~StDLTaskStat()
{
	//SAFE_DELETES( pData );
	SAFE_DELETES( pURL );
	SAFE_DELETES( pPostData );
}

CHttpClient::CHttpClient()
{
	//インターネットに接続
	m_hInternet = InternetOpen(
		NAME_USERAGENT,
		INTERNET_OPEN_TYPE_PRECONFIG,
		NULL,
		NULL,
		0);

	if (m_hInternet == NULL)
	{
		//しかし接続できる環境になかった
		m_bInternetConnectionExist = gxFalse;
		return;
	}

	m_bInternetConnectionExist = gxTrue;
}

CHttpClient::~CHttpClient()
{
	if( m_bInternetConnectionExist )
	{
		//切断
		InternetCloseHandle(m_hInternet);
	}

	SAFE_DELETES( m_pReadBuffer );
}


gxBool CHttpClient::IsActive()
{
	return m_bInternetConnectionExist;
}


gxBool CHttpClient::IsDownloaded( Sint32 id , Float32 *fRatio )
{
	//ダウンロードが完了しているか？

	id = id % enRequestMax;

	if( !m_bInternetConnectionExist )
	{
		return gxFalse;
	}

	if (fRatio)
	{
		size_t max = m_DownLoadTask[id].uMaxSize;
		size_t sz  = m_DownLoadTask[id].uReadSize;

		*fRatio = 100.0f * sz / max;
	}

	if( m_DownLoadTask[ id ].stat == enStatCompleteSuccessful )
	{
		//成功して終了
		return gxTrue;
	}

	if( m_DownLoadTask[ id ].stat == enStatCompleteFailed )
	{
		//（成功したとは言ってない）
		return gxTrue;
	}

	return gxFalse;
}

gxBool CHttpClient::IsSucceeded( Sint32 id  )
{
	//ダウンロードに成功したか？

	id = id % enRequestMax;

	if( !m_bInternetConnectionExist )
	{
		return gxFalse;
	}

	if( m_DownLoadTask[ id ].stat == enStatCompleteSuccessful )
	{
		return gxTrue;
	}

	return gxFalse;
}


Sint32 CHttpClient::Get( char *pURL )
{
	//URLのリクエストを受け付ける( GET通信 )

	Sint32 n = m_uRequestNum%enRequestMax;

/*
	if( m_uRequestNum > m_CurrentIndex + enRequestMax )
	{
		//リクエストバッファの上限値を超えていた場合は帰ってもらう
		return -1;
	}
*/

	//あえてdeleteしない（アプリ側で解放の義務）
	//SAFE_DELETES( m_DownLoadTask[ n ].pData );
	//SAFE_DELETES( m_DownLoadTask[ n ].pURL  );
	memset(&m_DownLoadTask[n], 0x00, sizeof(StDLTaskStat));

	size_t len = strlen(pURL);

	m_DownLoadTask[ n ].pURL = new char[len + 2];

	sprintf(m_DownLoadTask[n].pURL, "%s" , pURL );

	m_uRequestNum ++;

	return m_uRequestNum-1;
}


Sint32 CHttpClient::Post( char *pURL , char *pPostData , size_t sz=0 )
{
	//URLのリクエストを受け付ける( POST通信 )

	Sint32 n = m_uRequestNum%enRequestMax;

/*
	if( m_uRequestNum > m_CurrentIndex + enRequestMax )
	{
		//リクエストバッファの上限値を超えていた場合は帰ってもらう
		return -1;
	}
*/
	SAFE_DELETES( m_DownLoadTask[ n ].pData );
	SAFE_DELETES( m_DownLoadTask[ n ].pURL);
	SAFE_DELETES( m_DownLoadTask[ n ].pPostData );
	memset(&m_DownLoadTask[n], 0x00, sizeof(StDLTaskStat));

	size_t len = strlen(pURL);
	m_DownLoadTask[ n ].pURL = new char[len + 2];
	sprintf(m_DownLoadTask[n].pURL, "%s" , pURL );

	if (sz == 0)
	{
		len = strlen( pPostData );
	}

	m_DownLoadTask[ n ].pPostData = new char[sz];
	memcpy(m_DownLoadTask[n].pPostData , pPostData , sz );
	m_DownLoadTask[ n ].PostDataSize = sz;
	m_uRequestNum ++;

	return m_uRequestNum-1;
}


gxBool CHttpClient::seqReadInit()
{
	//URLを分解して初期化する

	if (!m_bInternetConnectionExist)
	{
		return gxFalse;
	}

	TCHAR		lpszUserName[256] = {0};
	TCHAR		lpszPassword[256] = {0};

	// URL解析

	URL_COMPONENTS urlcomponents;
	ZeroMemory( &urlcomponents, sizeof(URL_COMPONENTS) );

	urlcomponents.dwStructSize = sizeof(URL_COMPONENTS);

	TCHAR szHostName[URLBUFFER_SIZE];
	TCHAR szUrlPath[URLBUFFER_SIZE];

	urlcomponents.lpszHostName      = szHostName;
	urlcomponents.lpszUrlPath       = szUrlPath;
	urlcomponents.dwHostNameLength	= URLBUFFER_SIZE;
	urlcomponents.dwUrlPathLength	= URLBUFFER_SIZE;

	if( !InternetCrackUrl( getCurrent()->pURL , (DWORD)strlen(getCurrent()->pURL) , 0 , &urlcomponents ) )
	{
		// URLの解析に失敗
		//assert( !"URL解析に失敗" );
		//return false;
	}

	//--------------------

	DWORD dwFlags = 0;

	// HTTP要求の作成を行う

	BOOL bHttpSendRequest = false;
	BOOL bGet             = (getCurrent()->pPostData )? false : true;

	if( INTERNET_SCHEME_HTTP == urlcomponents.nScheme )
	{
		//HTTP
		dwFlags = INTERNET_FLAG_RELOAD						// 要求されたファイル、オブジェクト、またはフォルダ一覧を、キャッシュからではなく、元のサーバーから強制的にダウンロードします。
				| INTERNET_FLAG_DONT_CACHE					// 返されたエンティティをキャシュへ追加しません。
				| INTERNET_FLAG_NO_AUTO_REDIRECT;			// HTTP だけで使用され、リダイレクトが HttpSendRequest で処理されないことを指定します。

		m_hHttpSession = InternetConnect(
			m_hInternet,
			urlcomponents.lpszHostName,
			urlcomponents.nPort,//INTERNET_DEFAULT_HTTP_PORT,
			lpszUserName,
			lpszPassword,
			INTERNET_SERVICE_HTTP,
			0,
			0);

	}
	else if( INTERNET_SCHEME_HTTPS  == urlcomponents.nScheme )
	{
		//HTTPS
		dwFlags = INTERNET_FLAG_RELOAD						// 要求されたファイル、オブジェクト、またはフォルダ一覧を、キャッシュからではなく、元のサーバーから強制的にダウンロードします。
				| INTERNET_FLAG_DONT_CACHE					// 返されたエンティティをキャシュへ追加しません。
				| INTERNET_FLAG_NO_AUTO_REDIRECT			// HTTP だけで使用され、リダイレクトが HttpSendRequest で処理されないことを指定します。
				| INTERNET_FLAG_SECURE						// 安全なトランザクションを使用します。これにより、SSL/PCT を使うように変換され、HTTP 要求だけで有効です。 
				| INTERNET_FLAG_IGNORE_CERT_DATE_INVALID	// INTERNET_FLAG_IGNORE_CERT_DATE_INVALID、INTERNET_FLAG_IGNORE_CERT_CN_INVALID
				| INTERNET_FLAG_IGNORE_CERT_CN_INVALID;		// は、証明書に関する警告を無視するフラグ

		m_hHttpSession = InternetConnect(
			m_hInternet,
			urlcomponents.lpszHostName,
			urlcomponents.nPort,//INTERNET_DEFAULT_HTTPS_PORT,
			lpszUserName,
			lpszPassword,
			INTERNET_SERVICE_HTTP,
			0,
			0);
	}

	if( !m_hHttpSession )
	{
		errorCheck();
		getCurrent()->stat = enStatCompleteFailed;
		return gxFalse;
	}

	//リクエストを送信する

	if( bGet )
	{
		//Get通信
		m_hHttpRequest   = HttpOpenRequest( m_hHttpSession, "GET", urlcomponents.lpszUrlPath, NULL, NULL, NULL, dwFlags, 0 );
		bHttpSendRequest = HttpSendRequest(	m_hHttpRequest,	"", 0, NULL, 0 );
	}
	else
	{
		//POST通信
		size_t len     = getCurrent()->PostDataSize;
		char *postData = getCurrent()->pPostData;

		static const LPSTR  acceptType1[] = {_T("Accept: */*"),NULL};
		static const LPSTR  acceptType2 = _T("Content-Type: application/x-www-form-urlencoded"LF);
		size_t sz2 = strlen(acceptType2);

		m_hHttpRequest   = HttpOpenRequest( m_hHttpSession, _T("POST"), urlcomponents.lpszUrlPath, NULL, NULL, (LPCSTR*)acceptType1, dwFlags, 0 );
		bHttpSendRequest = HttpSendRequest( m_hHttpRequest, (LPCSTR)acceptType2, (DWORD)sz2, postData, (DWORD)len);
	}

	if( !bHttpSendRequest )
	{
		errorCheck();
		getCurrent()->stat = enStatCompleteFailed;
	    return gxFalse;
	}

	errorCheck();

	//接続後の状態を確認する

	Uint32 StatusCode = 0;
	Uint32 StatusCode_Size = sizeof( Uint32 );

	BOOL bHttpQueryInfo_STATUS = HttpQueryInfo(	m_hHttpRequest,	HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER ,	&StatusCode, (DWORD*)&StatusCode_Size,	NULL );

	if( StatusCode != HTTP_STATUS_OK )
	{
		//エラー処理

		InternetCloseHandle( m_hHttpRequest );
		InternetCloseHandle( m_hHttpSession );

		if( StatusCode == 403 )
		{
			m_uErrorNo = Error403_Forbidden;
		}
		else if( StatusCode == 404 )
		{
			m_uErrorNo = Error404_NotFound;
		}
		else
		{
			m_uErrorNo = Error999_UnknownError;
		}

		getCurrent()->stat = enStatCompleteFailed;

		return gxFalse;
	}

	//ファイルのダウンロードに必要とされるバッファのサイズを確認する。

	Uint32 RequiredBufSize = 0;
	Uint32 RequiredBufSize_Size = sizeof(Uint32);

	//エラーコードを数値として返してくれる。

	BOOL bHttpQueryInfo_ContentLength = HttpQueryInfo( m_hHttpRequest, HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER ,	&RequiredBufSize, (DWORD*)&RequiredBufSize_Size, NULL );

	DWORD availableSize = 0;
	if( !bHttpQueryInfo_ContentLength )
	{
		//https://cboard.cprogramming.com/windows-programming/116272-wininet-making-me-cry.html
		//Special Case: Header not found is expected. Not all pages output their content length
		//どうもすべてのページがコンテンツの長さを出力してくれるわけではないらしい...
		errorCheck();
		InternetQueryDataAvailable(m_hHttpRequest, &availableSize, 0, 0);

		RequiredBufSize = availableSize;
	}

	//バッファの初期化を行う

	if (RequiredBufSize <= 0)
	{
		errorCheck();
	}

	getCurrent()->uMaxSize  = RequiredBufSize;
	getCurrent()->uReadSize = 0;
	getCurrent()->downloadRatio = 0.0f;
	getCurrent()->stat      = enStatDownloading;

	m_RetryCnt = 0;

	return gxTrue;
}


gxBool CHttpClient::seqReadMain()
{
	//ファイルの読み込み

	DWORD sz = 0;
	gxBool bSuccess = gxFalse;

	StDLTaskStat *p = getCurrent();
	size_t sz3 = size_t(p->uReadSize);

	size_t readSize = enReadSize;

	DWORD availableSize = 0;
	InternetQueryDataAvailable(m_hHttpRequest, &availableSize, 0, 0);

	if (readSize > availableSize )//p->uMaxSize)
	{
		readSize = availableSize;
	}

	if ( p->uMaxSize < p->uReadSize + availableSize)
	{
		Uint8 *q = new Uint8[p->uReadSize + availableSize + 1];
		gxUtil::MemCpy(q, m_pReadBuffer, p->uReadSize);
		q[p->uReadSize] = 0x00;
		SAFE_DELETES(m_pReadBuffer);
		m_pReadBuffer = q;

		p->uMaxSize = p->uReadSize + availableSize;
	}

	bSuccess = InternetReadFile(m_hHttpRequest, &m_pReadBuffer[ sz3 ], (DWORD)readSize, &sz);

	size_t max = size_t(p->uMaxSize);
	size_t sz2 = size_t(p->uReadSize);

	if( sz < 0 )
	{
		//サイズ情報が異常
	}
	else
	{
		p->uReadSize += sz;

		max = size_t(p->uMaxSize);
		sz2 = size_t(p->uReadSize);

		m_pReadBuffer[ sz2 ] = 0x00;	//文字列用のターミネータを挿入しておく
	}

	if (max <= 0)
	{
		max = sz2;
	}

	p->downloadRatio = 100.0f * (1.0f*sz2 / max);

	if (p->downloadRatio >= 100.0f )
	{
		p->downloadRatio = 100.0f;
	}

	if (sz2 >= max && sz == 0)
	{
		//完了した
		p->stat = enStatCompleteSuccessful;
		return true;
	}

	//失敗している

	if( sz == 0 ) bSuccess = false;

	if (!bSuccess)
	{
		errorCheck();

		if( m_RetryCnt < 32 )
		{
			//もう１回トライ
			m_RetryCnt ++;
		}
		else
		{
			p->stat = enStatCompleteFailed;
			return gxTrue;
		}
	}
	return gxFalse;
}


gxBool CHttpClient::seqRead()
{
	//ファイルのアクセス状況の制御
	gxBool ret = gxTrue;

	switch( m_Sequence )	{
	case 0:
		//リクエスト待ち

		if (m_uRequestNum > m_CurrentIndex )
		{
			m_Sequence = 10;
		}
		break;

	case 10:
		//タスクの初期化

		if( !seqReadInit() )
		{
			//失敗して終了
			m_CurrentIndex ++;
			m_Sequence = 0;
			ret = gxFalse;
		}
		else
		{
			//初期化が成功した
			size_t fileSize = getCurrent()->uMaxSize;
			if (fileSize == 0)
			{
				fileSize = enMinimumBuffer;
			}
			m_pReadBuffer = new Uint8[ fileSize + 1 ];
			m_pReadBuffer[0] = 0x00;
			m_Sequence       = 100;
		}
		break;

	case 100:
		//ファイル読み込み待ち

		if( !seqReadMain() )
		{
			break;
		}

		if( getCurrent()->uReadSize >= getCurrent()->uMaxSize && getCurrent()->stat == enStatCompleteSuccessful )
		{
			//成功

			size_t sz = getCurrent()->uMaxSize;
			getCurrent()->pData = m_pReadBuffer;//new Uint8[ sz + 1];
			//gxUtil::MemCpy( getCurrent()->pData, m_pReadBuffer, sz );
			getCurrent()->pData[ sz ] = 0x00;
			m_pReadBuffer = nullptr;
		}
		else
		{
			//失敗
			gxLib::DebugLog("DownLoad Error");
		}
		//SAFE_DELETES(m_pReadBuffer);

		m_Sequence = 900;

		ret = gxFalse;

		//↓

	case 900:
		//終了処理
		m_CurrentIndex ++;
		m_Sequence = 999;

		InternetCloseHandle( m_hHttpRequest );
		InternetCloseHandle( m_hHttpSession );

		//↓

	case 999:
		m_Sequence = 0;
		break;

	default:
		break;
	}

	return ret;
}


Uint8* CHttpClient::GetDownLoadFile( Sint32 id , size_t *uSize )
{
	//ダウンロード済みのファイルを取得する

	id = id % enRequestMax;

	if( !m_bInternetConnectionExist )
	{
		return NULL;
	}

	*uSize = 0;

	if( m_DownLoadTask[ id ].stat == enStatCompleteSuccessful )
	{
		*uSize = m_DownLoadTask[id].uMaxSize;
		return m_DownLoadTask[ id ].pData;
	}

	return NULL;
}

void CHttpClient::DisConnect()
{
	//デバッグ用強制切断

	InternetCloseHandle( m_hInternet );
}


CHttpClient::StDLTaskStat* CHttpClient::getCurrent( Sint32 id )
{
	//現在作業中のタスク状況を得る

	if( !m_bInternetConnectionExist )
	{
#ifdef _RELEASE
		memset( &m_DownLoadTask[0] , 0x00 , sizeof( StDLTaskStat ) );		//なかった場合はとりあえず止まらないようにしておく
		return &m_DownLoadTask[0];
#else
		return NULL;
#endif
	}

	if (id == -1)
	{
		id = (m_CurrentIndex%enRequestMax);
	}
	else
	{
		id = id % enRequestMax;
	}
	return &m_DownLoadTask[ id ];
}


void CHttpClient::Action()
{
	//毎フレーム呼び出される
	static gxNetworkManager::StHTTP *httpInfo = NULL;

	seqRead();

	if( httpInfo )
	{
		Float32 fRatio = 0.0f;
		gxBool bLoadEnd = IsDownloaded(m_CurrentID, &fRatio);

		httpInfo->uReadFileSize  = getCurrent(m_CurrentID)->uReadSize;
		httpInfo->uFileSize      = getCurrent(m_CurrentID)->uMaxSize;


		if ( bLoadEnd )
		{
			if ( IsSucceeded( m_CurrentID ) )
			{
				//httpInfo->pData     = getCurrent(m_CurrentID)->pData;
				//httpInfo->uFileSize = getCurrent(m_CurrentID)->uMaxSize;
				httpInfo->pData     = getCurrent(m_CurrentID%enRequestMax)->pData;
				httpInfo->uFileSize = getCurrent(m_CurrentID%enRequestMax)->uMaxSize;
				m_CurrentID = -1;
				httpInfo->SetSeq( 100 );
				httpInfo->SetError(0);
				httpInfo = NULL;
			}
			else
			{
				httpInfo->pData     = NULL;
				httpInfo->uFileSize = 0x00;
				httpInfo->SetSeq( 100 );
				m_CurrentID = -1;
				httpInfo->SetError(1);
				httpInfo = NULL;
			}
		}

		return;
	}

	if (!m_bInternetConnectionExist ) return;

	httpInfo = gxNetworkManager::GetInstance()->GetNextReq();

	if( httpInfo )
	{
 		m_CurrentID = Get( httpInfo->m_URL );
		httpInfo->bRequest = gxFalse;
	}

}


void CHttpClient::Draw()
{
	//デバッグ表示

	for( Sint32 ii=0; ii<enRequestMax; ii++ )
	{
		Uint32 argb = 0xffffffff;
		char *pExist = " ";
		Float32 fRatio = 0.0f;

		if (getCurrent(ii))
		{
			pExist = (getCurrent(ii)->pURL) ? "ready" : "wait";
			if( getCurrent(ii)->stat == enStatCompleteSuccessful )
			{
				pExist = "done";
 				argb = 0xff00ffff;
 			}
			if( getCurrent(ii)->stat == enStatCompleteFailed )
			{
				pExist = "failed";
 				argb = 0xffff0000;
			}

			fRatio = getCurrent(ii)->downloadRatio;
		}
		else
		{
			pExist = "None";
		}

		if( m_CurrentIndex%enRequestMax == ii ) argb = 0xff00ff00;

		gxLib::Printf( 32 , 32+ii*16 ,100 , ATR_DFLT , argb , "No.%d %s - %.2f ％" , ii , pExist ,  fRatio );
	}
}


Sint32 CHttpClient::errorCheck()
{
	//エラーのチェック

	const char* pErrorMessage = NULL;

	DWORD err = GetLastError();

	switch( err ){
	case ERROR_INTERNET_OUT_OF_HANDLES:						pErrorMessage = "ERROR_INTERNET_OUT_OF_HANDLES";					break;
	case ERROR_INTERNET_TIMEOUT:							pErrorMessage = "ERROR_INTERNET_TIMEOUT";							break;
	case ERROR_INTERNET_EXTENDED_ERROR:						pErrorMessage = "ERROR_INTERNET_EXTENDED_ERROR";					break;
	case ERROR_INTERNET_INTERNAL_ERROR:						pErrorMessage = "ERROR_INTERNET_INTERNAL_ERROR";					break;
	case ERROR_INTERNET_INVALID_URL:						pErrorMessage = "ERROR_INTERNET_INVALID_URL";						break;
	case ERROR_INTERNET_UNRECOGNIZED_SCHEME:				pErrorMessage = "ERROR_INTERNET_UNRECOGNIZED_SCHEME";				break;
	case ERROR_INTERNET_NAME_NOT_RESOLVED:					pErrorMessage = "ERROR_INTERNET_NAME_NOT_RESOLVED";					break;
	case ERROR_INTERNET_PROTOCOL_NOT_FOUND:					pErrorMessage = "ERROR_INTERNET_PROTOCOL_NOT_FOUND";				break;
	case ERROR_INTERNET_INVALID_OPTION:						pErrorMessage = "ERROR_INTERNET_INVALID_OPTION";					break;
	case ERROR_INTERNET_BAD_OPTION_LENGTH:					pErrorMessage = "ERROR_INTERNET_BAD_OPTION_LENGTH";					break;
	case ERROR_INTERNET_OPTION_NOT_SETTABLE:				pErrorMessage = "ERROR_INTERNET_OPTION_NOT_SETTABLE";				break;
	case ERROR_INTERNET_SHUTDOWN:							pErrorMessage = "ERROR_INTERNET_SHUTDOWN";							break;
	case ERROR_INTERNET_INCORRECT_USER_NAME:				pErrorMessage = "ERROR_INTERNET_INCORRECT_USER_NAME";				break;
	case ERROR_INTERNET_INCORRECT_PASSWORD:					pErrorMessage = "ERROR_INTERNET_INCORRECT_PASSWORD";				break;
	case ERROR_INTERNET_LOGIN_FAILURE:						pErrorMessage = "ERROR_INTERNET_LOGIN_FAILURE";						break;
	case ERROR_INTERNET_INVALID_OPERATION:					pErrorMessage = "ERROR_INTERNET_INVALID_OPERATION";					break;
	case ERROR_INTERNET_OPERATION_CANCELLED:				pErrorMessage = "ERROR_INTERNET_OPERATION_CANCELLED";				break;
	case ERROR_INTERNET_INCORRECT_HANDLE_TYPE:				pErrorMessage = "ERROR_INTERNET_INCORRECT_HANDLE_TYPE";				break;
	case ERROR_INTERNET_INCORRECT_HANDLE_STATE:				pErrorMessage = "ERROR_INTERNET_INCORRECT_HANDLE_STATE";			break;
	case ERROR_INTERNET_NOT_PROXY_REQUEST:					pErrorMessage = "ERROR_INTERNET_NOT_PROXY_REQUEST";					break;
	case ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND:			pErrorMessage = "ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND";			break;
	case ERROR_INTERNET_BAD_REGISTRY_PARAMETER:				pErrorMessage = "ERROR_INTERNET_BAD_REGISTRY_PARAMETER";			break;
	case ERROR_INTERNET_NO_DIRECT_ACCESS:					pErrorMessage = "ERROR_INTERNET_NO_DIRECT_ACCESS";					break;
	case ERROR_INTERNET_NO_CONTEXT:							pErrorMessage = "ERROR_INTERNET_NO_CONTEXT";						break;
	case ERROR_INTERNET_NO_CALLBACK:						pErrorMessage = "ERROR_INTERNET_NO_CALLBACK";						break;
	case ERROR_INTERNET_REQUEST_PENDING:					pErrorMessage = "ERROR_INTERNET_REQUEST_PENDING";					break;
	case ERROR_INTERNET_INCORRECT_FORMAT:					pErrorMessage = "ERROR_INTERNET_INCORRECT_FORMAT";					break;
	case ERROR_INTERNET_ITEM_NOT_FOUND:						pErrorMessage = "ERROR_INTERNET_ITEM_NOT_FOUND";					break;
	case ERROR_INTERNET_CANNOT_CONNECT:						pErrorMessage = "ERROR_INTERNET_CANNOT_CONNECT";					break;
	case ERROR_INTERNET_CONNECTION_ABORTED:					pErrorMessage = "ERROR_INTERNET_CONNECTION_ABORTED";				break;
	case ERROR_INTERNET_CONNECTION_RESET:					pErrorMessage = "ERROR_INTERNET_CONNECTION_RESET";					break;
	case ERROR_INTERNET_FORCE_RETRY:						pErrorMessage = "ERROR_INTERNET_FORCE_RETRY";						break;
	case ERROR_INTERNET_INVALID_PROXY_REQUEST:				pErrorMessage = "ERROR_INTERNET_INVALID_PROXY_REQUEST";				break;
	case ERROR_INTERNET_NEED_UI:							pErrorMessage = "ERROR_INTERNET_NEED_UI";							break;
	case ERROR_INTERNET_HANDLE_EXISTS:						pErrorMessage = "ERROR_INTERNET_HANDLE_EXISTS";						break;
	case ERROR_INTERNET_SEC_CERT_DATE_INVALID:				pErrorMessage = "ERROR_INTERNET_SEC_CERT_DATE_INVALID";				break;
	case ERROR_INTERNET_SEC_CERT_CN_INVALID:				pErrorMessage = "ERROR_INTERNET_SEC_CERT_CN_INVALID";				break;
	case ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR:				pErrorMessage = "ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR";			break;
	case ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR:				pErrorMessage = "ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR";			break;
	case ERROR_INTERNET_MIXED_SECURITY:						pErrorMessage = "ERROR_INTERNET_MIXED_SECURITY";					break;
	case ERROR_INTERNET_CHG_POST_IS_NON_SECURE:				pErrorMessage = "ERROR_INTERNET_CHG_POST_IS_NON_SECURE";			break;
	case ERROR_INTERNET_POST_IS_NON_SECURE:					pErrorMessage = "ERROR_INTERNET_POST_IS_NON_SECURE";				break;
	case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED:			pErrorMessage = "ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED";			break;
	case ERROR_INTERNET_INVALID_CA:							pErrorMessage = "ERROR_INTERNET_INVALID_CA";						break;
	case ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP:				pErrorMessage = "ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP";				break;
	case ERROR_INTERNET_ASYNC_THREAD_FAILED:				pErrorMessage = "ERROR_INTERNET_ASYNC_THREAD_FAILED";				break;
	case ERROR_INTERNET_REDIRECT_SCHEME_CHANGE:				pErrorMessage = "ERROR_INTERNET_REDIRECT_SCHEME_CHANGE";			break;
	case ERROR_INTERNET_DIALOG_PENDING:						pErrorMessage = "ERROR_INTERNET_DIALOG_PENDING";					break;
	case ERROR_INTERNET_RETRY_DIALOG:						pErrorMessage = "ERROR_INTERNET_RETRY_DIALOG";						break;
	case ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR:			pErrorMessage = "ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR";			break;
	case ERROR_INTERNET_INSERT_CDROM:						pErrorMessage = "ERROR_INTERNET_INSERT_CDROM";						break;
	case ERROR_INTERNET_FORTEZZA_LOGIN_NEEDED:				pErrorMessage = "ERROR_INTERNET_FORTEZZA_LOGIN_NEEDED";				break;
	case ERROR_INTERNET_SEC_CERT_ERRORS:					pErrorMessage = "ERROR_INTERNET_SEC_CERT_ERRORS";					break;
	case ERROR_INTERNET_SEC_CERT_NO_REV:					pErrorMessage = "ERROR_INTERNET_SEC_CERT_NO_REV";					break;
	case ERROR_INTERNET_SEC_CERT_REV_FAILED:				pErrorMessage = "ERROR_INTERNET_SEC_CERT_REV_FAILED";				break;
/*　8.1非対応
	case ERROR_HTTP_HSTS_REDIRECT_REQUIRED:					pErrorMessage = "ERROR_HTTP_HSTS_REDIRECT_REQUIRED";				break;
	case ERROR_INTERNET_SEC_CERT_WEAK_SIGNATURE:			pErrorMessage = "ERROR_INTERNET_SEC_CERT_WEAK_SIGNATURE";			break;
*/
	case ERROR_FTP_TRANSFER_IN_PROGRESS:					pErrorMessage = "ERROR_FTP_TRANSFER_IN_PROGRESS";					break;
	case ERROR_FTP_DROPPED:									pErrorMessage = "ERROR_FTP_DROPPED";								break;
	case ERROR_FTP_NO_PASSIVE_MODE:							pErrorMessage = "ERROR_FTP_NO_PASSIVE_MODE";						break;
	case ERROR_GOPHER_PROTOCOL_ERROR:						pErrorMessage = "ERROR_GOPHER_PROTOCOL_ERROR";						break;
	case ERROR_GOPHER_NOT_FILE:								pErrorMessage = "ERROR_GOPHER_NOT_FILE";							break;
	case ERROR_GOPHER_DATA_ERROR:							pErrorMessage = "ERROR_GOPHER_DATA_ERROR";							break;
	case ERROR_GOPHER_END_OF_DATA:							pErrorMessage = "ERROR_GOPHER_END_OF_DATA";							break;
	case ERROR_GOPHER_INVALID_LOCATOR:						pErrorMessage = "ERROR_GOPHER_INVALID_LOCATOR";						break;
	case ERROR_GOPHER_INCORRECT_LOCATOR_TYPE:				pErrorMessage = "ERROR_GOPHER_INCORRECT_LOCATOR_TYPE";				break;
	case ERROR_GOPHER_NOT_GOPHER_PLUS:						pErrorMessage = "ERROR_GOPHER_NOT_GOPHER_PLUS";						break;
	case ERROR_GOPHER_ATTRIBUTE_NOT_FOUND:					pErrorMessage = "ERROR_GOPHER_ATTRIBUTE_NOT_FOUND";					break;
	case ERROR_GOPHER_UNKNOWN_LOCATOR:						pErrorMessage = "ERROR_GOPHER_UNKNOWN_LOCATOR";						break;
	case ERROR_HTTP_HEADER_NOT_FOUND:						pErrorMessage = "ERROR_HTTP_HEADER_NOT_FOUND";						break;
	case ERROR_HTTP_DOWNLEVEL_SERVER:						pErrorMessage = "ERROR_HTTP_DOWNLEVEL_SERVER";						break;
	case ERROR_HTTP_INVALID_SERVER_RESPONSE:				pErrorMessage = "ERROR_HTTP_INVALID_SERVER_RESPONSE";				break;
	case ERROR_HTTP_INVALID_HEADER:							pErrorMessage = "ERROR_HTTP_INVALID_HEADER";						break;
	case ERROR_HTTP_INVALID_QUERY_REQUEST:					pErrorMessage = "ERROR_HTTP_INVALID_QUERY_REQUEST";					break;
	case ERROR_HTTP_HEADER_ALREADY_EXISTS:					pErrorMessage = "ERROR_HTTP_HEADER_ALREADY_EXISTS";					break;
	case ERROR_HTTP_REDIRECT_FAILED:						pErrorMessage = "ERROR_HTTP_REDIRECT_FAILED";						break;
	case ERROR_HTTP_NOT_REDIRECTED:							pErrorMessage = "ERROR_HTTP_NOT_REDIRECTED";						break;
	case ERROR_HTTP_COOKIE_NEEDS_CONFIRMATION:				pErrorMessage = "ERROR_HTTP_COOKIE_NEEDS_CONFIRMATION";				break;
	case ERROR_HTTP_COOKIE_DECLINED:						pErrorMessage = "ERROR_HTTP_COOKIE_DECLINED";						break;
	case ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION:			pErrorMessage = "ERROR_HTTP_REDIRECT_NEEDS_CONFIRMATION";			break;
	case ERROR_INTERNET_SECURITY_CHANNEL_ERROR:				pErrorMessage = "ERROR_INTERNET_SECURITY_CHANNEL_ERROR";			break;
	case ERROR_INTERNET_UNABLE_TO_CACHE_FILE:				pErrorMessage = "ERROR_INTERNET_UNABLE_TO_CACHE_FILE";				break;
	case ERROR_INTERNET_TCPIP_NOT_INSTALLED:				pErrorMessage = "ERROR_INTERNET_TCPIP_NOT_INSTALLED";				break;
	case ERROR_INTERNET_DISCONNECTED:						pErrorMessage = "ERROR_INTERNET_DISCONNECTED";						break;
	case ERROR_INTERNET_SERVER_UNREACHABLE:					pErrorMessage = "ERROR_INTERNET_SERVER_UNREACHABLE";				break;
	case ERROR_INTERNET_PROXY_SERVER_UNREACHABLE:			pErrorMessage = "ERROR_INTERNET_PROXY_SERVER_UNREACHABLE";			break;
	case ERROR_INTERNET_BAD_AUTO_PROXY_SCRIPT:				pErrorMessage = "ERROR_INTERNET_BAD_AUTO_PROXY_SCRIPT";				break;
	case ERROR_INTERNET_UNABLE_TO_DOWNLOAD_SCRIPT:			pErrorMessage = "ERROR_INTERNET_UNABLE_TO_DOWNLOAD_SCRIPT";			break;
	case ERROR_INTERNET_SEC_INVALID_CERT:					pErrorMessage = "ERROR_INTERNET_SEC_INVALID_CERT";					break;
	case ERROR_INTERNET_SEC_CERT_REVOKED:					pErrorMessage = "ERROR_INTERNET_SEC_CERT_REVOKED";					break;
	case ERROR_INTERNET_FAILED_DUETOSECURITYCHECK:			pErrorMessage = "ERROR_INTERNET_FAILED_DUETOSECURITYCHECK";			break;
	case ERROR_INTERNET_NOT_INITIALIZED:					pErrorMessage = "ERROR_INTERNET_NOT_INITIALIZED";					break;
	case ERROR_INTERNET_NEED_MSN_SSPI_PKG:					pErrorMessage = "ERROR_INTERNET_NEED_MSN_SSPI_PKG";					break;
	case ERROR_INTERNET_LOGIN_FAILURE_DISPLAY_ENTITY_BODY:	pErrorMessage = "ERROR_INTERNET_LOGIN_FAILURE_DISPLAY_ENTITY_BODY";	break;
	case ERROR_INTERNET_DECODING_FAILED:					pErrorMessage = "ERROR_INTERNET_DECODING_FAILED";					break;
/*　8.1非対応
	case ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED_PROXY:		pErrorMessage = "ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED_PROXY";		break;
	case ERROR_INTERNET_SECURE_FAILURE_PROXY:				pErrorMessage = "ERROR_INTERNET_SECURE_FAILURE_PROXY";				break;
*/
	default:
		break;
	}

	if( pErrorMessage )
	{
		//エラー発生
		gxLib::DebugLog( "%s" , pErrorMessage );
		return err;
	}

	return 0;
}


gxBool HttpTest()
{
	//Test

	static CHttpClient* s_phttp = NULL;
	static Sint32 id[] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
	static Sint32 cnt = 0;
	static gxChar SAMPLE_URL2[] = "http://www.bizsoft.co.jp/trialdl/Setup_kaikei19_TRIAL.exe";

	if (s_phttp == NULL)
	{
		s_phttp = new CHttpClient();
	}

	s_phttp->Action();

	s_phttp->Draw();	//デバッグ用

	Float32 fRatio = 0.0f;

	if( gxLib::KeyBoard( gxKey::RETURN )&enStatTrig )
	{
		//check

		gxLib::DebugLog( "------------------------------------" );

		for( Sint32 ii=0; ii<10; ii++ )
		{
			if (id[ii] == -1) continue;

			if ( s_phttp->IsDownloaded( id[ii] , &fRatio ) )
			{
				if ( s_phttp->IsSucceeded( id[ii] ) )
				{
					size_t uSize = 0;
					Uint8 *pData;
					pData = s_phttp->GetDownLoadFile( id[ii] , &uSize );

					if( uSize <= 256 )
					{
						gxLib::DebugLog( "%d(%d)::%s" , ii , id[ii] , pData );
					}
					else
					{
						gxLib::DebugLog( "%d(%d)::Large File" , ii , id[ii] );
					}
				}
				else
				{
						gxLib::DebugLog( "%d(%d)::Failed" , ii , id[ii] );
				}
			}
		}
	}

	//デバッグ操作
	if( gxLib::KeyBoard(gxKey::NUM1)&enStatTrig )
	{
		//http - GET
		id[cnt%10] = s_phttp->Get( "http://german.suplex.net/test/test.php?getData=128" );	//SAMPLE_URL );
		cnt ++;
	}
	else if (gxLib::KeyBoard(gxKey::NUM2)&enStatTrig)
	{
		//http - POST
		id[cnt%10] = s_phttp->Post( "http://german.suplex.net/test/test.php" , "postData=256" );
		cnt ++;
	}
	else if( gxLib::Joy(0)->trg&BTN_C )
	{
		//大きなファイルのダウンロード
		id[cnt%10] = s_phttp->Get( SAMPLE_URL2 );
		cnt ++;
	}
	else if( gxLib::Joy(0)->trg&BTN_C )
	{
		//大きなファイルのダウンロード
		id[cnt%10] = s_phttp->Get( SAMPLE_URL2 );
		cnt ++;
	}
	else if( gxLib::Joy(0)->trg&BTN_X )
	{
		//間違えたURLの送信
		id[cnt%10] = s_phttp->Get( "http://german.suplex.net/test/test.php&getData=128" );
		cnt ++;
	}
	else if( gxLib::KeyBoard(gxKey::DEL)&enStatTrig )
	{
		//テスト用、強制切断
		s_phttp->DisConnect();
	}
	else if( gxLib::KeyBoard(gxKey::BS)&enStatTrig )
	{
		//再初期化
		SAFE_DELETE( s_phttp );
	}

	return gxTrue;
}


