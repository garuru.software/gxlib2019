﻿//---------------------------------------------------------------
//
// Windowsストア向け::サウンドメイン
//
//
//
//---------------------------------------------------------------

#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxSoundManager.h>
#include <gxLib/util/CFileWave.h>
#include <gxLib/gxSoundManager.h>
#include "CDirectSound.h"

#ifdef _USE_DIRECTSOUND

#pragma comment(lib, "dxguid.lib")

SINGLETON_DECLARE_INSTANCE( CDirectSound );

CDirectSound::CDirectSound()
{
	m_fMasterVolume = 1.0f;

	CoInitialize( NULL );

}


CDirectSound::~CDirectSound()
{
	CoUninitialize();
}



gxBool CDirectSound::Init()
{/*
	HRESULT hr;
	hr = CoCreateInstance(
				CLSID_DirectSound8,
				NULL, 
				CLSCTX_ALL,
				IID_IDirectSound8,
				(void **)&m_pDirectSound );

	if FAILED( hr )
	{
		return gxFalse;
	}

	// 初期化

	hr = m_pDirectSound->Initialize( NULL );

	if FAILED( hr )
	{
		return gxFalse;
	}

	// 協調レベルの設定

	if( FAILED( m_pDirectSound->SetCooperativeLevel( g_pWindows->m_hWindow, DSSCL_PRIORITY ) ) )
	{
		if( FAILED( m_pDirectSound->SetCooperativeLevel( g_pWindows->m_hWindow, DSSCL_NORMAL ) ) )
		{
			return gxFalse;
		}
	}

	hr = m_pDirectSound->Compact();

	if( FAILED( hr ) )
	{
		return gxFalse;
	}

	// プライマリサウンドバッファ生成

	DSBUFFERDESC desc = { sizeof(DSBUFFERDESC) };
	desc.dwFlags = DSBCAPS_PRIMARYBUFFER;

	if( FAILED( m_pDirectSound->CreateSoundBuffer( &desc, &m_pPrimaryBuffer, NULL ) ) )
	{
		return gxFalse;
	}

	// プライマリバッファフォーマット

	WAVEFORMATEX wfx = { 0 };
	wfx.wFormatTag		= 1;
	wfx.nChannels		= 2;
	wfx.nSamplesPerSec	= 44100;
	wfx.nAvgBytesPerSec	= 44100 * 4;
	wfx.nBlockAlign		= 4;
	wfx.wBitsPerSample	= 16;
	wfx.cbSize			= 0;

	hr = m_pPrimaryBuffer->SetFormat( &wfx );
	*/
	return gxTrue;
}


gxBool CDirectSound::Clear()
{
	return gxTrue;
}



void CDirectSound::Action()
{
	Float32 fMasterVolume = gxSoundManager::GetInstance()->GetMasterVolume();
	SetMasterVolume( fMasterVolume );

	for (Sint32 ii = 0; ii < MAX_SOUND_NUM; ii++)
	{
		StPlayInfo *pAudioInfo = gxSoundManager::GetInstance()->GetPlayInfo(ii);

		if ( pAudioInfo->bReq )
		{
			//load
			if (pAudioInfo->uStatus & enSoundReqLoad)
			{
				loadFile( ii , pAudioInfo->fileName );
			}

			//stop
			if (pAudioInfo->uStatus & enSoundReqStop)
			{
				StopSoundEffect(ii);
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = gxFalse;
			}

			//play
			if (pAudioInfo->uStatus & enSoundReqPlay)
			{
				PlaySoundEffect(ii);
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = gxTrue;
			}

			//volume
			if (pAudioInfo->uStatus & enSoundReqVolume )
			{
				SetVolume( ii , gxSoundManager::GetInstance()->GetPlayInfo(ii)->fVolume );
			}

			//freq
			if (pAudioInfo->uStatus & enSoundReqChangeFreq)
			{
				SetSoundFrequency(ii, pAudioInfo->fFreqRatio );
			}

			pAudioInfo->bReq = gxFalse;
			pAudioInfo->uStatus = enSoundReqNone;
		}

		gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow = IsPlay(ii);

	}
}


void CDirectSound::Release()
{
	
}




void CDirectSound::PlaySoundEffect(int sound)
{
	
}


void CDirectSound::StopSoundEffect( int sound)
{
	
}


void CDirectSound::SetSoundFrequency( int sound , Float32 ratio )
{
	
}


gxBool CDirectSound::IsPlay( int sound )
{
	return gxTrue;
}


void CDirectSound::SetAudioEnable( gxBool bSoundOn )
{
	
}


void CDirectSound::SetVolume( Sint32 no , Float32 fVolume )
{
	
}



void CDirectSound::SetMasterVolume( Float32 fMasterVolume )
{
	if( fMasterVolume <= 0.0f ) fMasterVolume = 0.0f;
	if( fMasterVolume >= 1.0f ) fMasterVolume = 1.0f;

	if( m_fMasterVolume == fMasterVolume ) return;

	m_fMasterVolume = fMasterVolume;

	for (Sint32 ii = 0; ii < MAX_SOUND_NUM; ii++)
	{
		if( !CWindows::GetInstance()->IsSoundEnable() )
		{
//			if( m_StSoundResource[ii].m_soundEffectSourceVoice )
			{
//				m_StSoundResource[ii].m_soundEffectSourceVoice->SetVolume( 0.0f );
				continue;
			}
		}
		else
		{
			StPlayInfo *pAudioInfo = gxSoundManager::GetInstance()->GetPlayInfo( ii );

//			if( m_StSoundResource[ii].m_soundEffectSourceVoice )
			{
//				m_StSoundResource[ii].m_soundEffectSourceVoice->SetVolume( pAudioInfo->fVolume * m_fMasterVolume );
			}
		}
	}

}

gxBool CDirectSound::loadFile( Sint32 n , char* pFileName )
{
	return gxTrue;
}


gxBool CDirectSound::readFile( Sint32 n , Uint8 *pData  , Uint32 uSize , StWAVEFORMATEX* pFormat )
{
	return gxTrue;
}

#endif
