﻿//------------------------------------------------------
//
// コントローラー制御( Direct Input version8使用)
//
//------------------------------------------------------
#define INITGUID
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include "CGamePad.h"
#include "CDirectInput.h"

#pragma comment(lib, "DXGUID.LIB")
#pragma comment(lib, "dinput8.lib")

#define AXIS_RANGE (65535/2.0f)

//--------------------------------------------------------------------
//コールバックによるローカル変数設定不可なグローバルな変数たち
//--------------------------------------------------------------------
BOOL CALLBACK EnumFFBJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext);
BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,VOID* pContext );
BOOL CALLBACK EnumObjectsCallback  ( const DIDEVICEOBJECTINSTANCE* pdidoi,VOID* pContext );
BOOL CALLBACK EnumAxesCallback     ( const DIDEVICEOBJECTINSTANCE* pdidoi,VOID* pContext );

//--------------------------------------------------------------------
//コールバックによるローカル変数設定不可なグローバルな変数たち
//--------------------------------------------------------------------

int   g_NowSettingDeviceNo;							//現在設定中のデバイスID
LPDIRECTINPUT8        g_pDirectInput     = NULL;	//DirectInputインターフェース
LPDIRECTINPUTDEVICE8  g_pJoystickDevice[CDirectInput::enControllerMax];	//ジョイスティックデバイス数

Uint32 FFBDeviceNum = 0;
GUID g_GUID_FFB_Device[CDirectInput::enControllerMax];

SINGLETON_DECLARE_INSTANCE( CDirectInput )

Uint32 cnvPOVValue( Sint32 rgbw )
{
	//何もなければ-1
	//↑:0
	//→:9000
	//↓:18000
	//←:27000

	if( rgbw == -1 ) return 0;

	Uint32 pad = 0;

	if( rgbw >= (27000 + 4500) || ( rgbw <= 4500 ) ) 	   pad |= JOY_U;
	if( ( rgbw >= 4500 )       && ( rgbw <= 9000+4500 ))   pad |= JOY_R;
	if( ( rgbw >= 9000+4500 )  && ( rgbw <= 18000+4500 ))  pad |= JOY_D;
	if( ( rgbw >= 18000+4500 ) && ( rgbw <= 27000+4500 ))  pad |= JOY_L;

	return pad;
}

//-----------------------------------------------------------------
//ゲームパッド関連情報を管理
//-----------------------------------------------------------------
CDirectInput::CDirectInput()
{
	GX_DEBUGLOG("【Control】パッドデバイスを初期化開始");

	m_bUseController = gxTrue;
	g_pDirectInput = NULL;

	for(Uint32 ii=0;ii<CDirectInput::enControllerMax;ii++)
	{
		g_pJoystickDevice[ii] = NULL;	//デバイスポインタをクリア
		g_pEffect[ii] 		  = NULL;	//FFエフェクト用ポインタをクリア

/*
		for(Uint32 jj=0;jj<GDBTN_MAX;jj++)
		{
			m_bEnable[ii][jj] = gxTrue;
		}
*/
	}

	m_uGamingDeviceNum = 0;		//デバイスの数はゼロ

}

CDirectInput::~CDirectInput()
{
	//--------------------------------------------------
	//ジョイパッドデバイスを解放する
	//--------------------------------------------------

	for(Uint32 i=0;i<CDirectInput::GetInstance()->GetDeviceNum();i++)
	{
		if(g_pJoystickDevice[i])
		{
			g_pJoystickDevice[i]->Unacquire();
			SAFE_RELEASE( g_pJoystickDevice[i] );
		}
	    SAFE_RELEASE( g_pEffect[i] );
	}

    SAFE_RELEASE( g_pDirectInput );

}


//DirectInputの初期化
void CDirectInput::Init()
{
	InitDirectInput();
	GX_DEBUGLOG("【Control】パッドデバイスの初期化完了");
}


bool CDirectInput::InitDirectInput()
{
	//---------------------------------------------------------
	//DirectInputオブジェクトを作成する
	//---------------------------------------------------------
	FFBDeviceNum = 0;
    // DirectInputサブシステムに登録しDirectInputオブジェクトを構築。
    // そのオブジェクトへのポインタを得てDirectInputインターフェースとする。
    if( FAILED( DirectInput8Create( GetModuleHandle(NULL), DIRECTINPUT_VERSION,IID_IDirectInput8, (VOID**)&g_pDirectInput, NULL ) ) ) {
		return gxFalse;
	}

    // Look for a simple joystick we can use for this sample program.
	if (FAILED(g_pDirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, NULL, DIEDFL_ATTACHEDONLY))) {
		return gxFalse;
	}

	//if( FAILED( g_pDirectInput->EnumDevices( DI8DEVCLASS_GAMECTRL, EnumFFBJoysticksCallback,NULL, DIEDFL_ATTACHEDONLY | DIEDFL_FORCEFEEDBACK) ) ) {
	//	return gxFalse;
	//}

	//コールバックに一度いきます【EnumJoysticksCallback】

	if(m_uGamingDeviceNum == 0)
	{
		//JOYSTICK検出できず
        return gxFalse;
    }

	//検出されたジョイパッド数を記録&
	//m_uGamingDeviceNum = CDirectInput::GetInstance()->GetDeviceNum();

/*
	for( Sint32 ii=0; ii<m_uGamingDeviceNum; ii++ )
	{
		m_pGamingDevice[ii] = CGamePad::GetInstance()->GetDevice();
	}
*/

	for(Uint32 n=0;n<m_uGamingDeviceNum;n++)
	{
		g_NowSettingDeviceNo = n;

	    if( FAILED( g_pJoystickDevice[n]->SetDataFormat( &c_dfDIJoystick2 ) ) )
	    {
	        return gxFalse;
		}

		//----------------------------------------------------------------------------
	    // 協調レベルの設定
		//----------------------------------------------------------------------------
	    if( FAILED( g_pJoystickDevice[n]->SetCooperativeLevel(CWindows::GetInstance()->m_hWindow, DISCL_EXCLUSIVE | DISCL_FOREGROUND ) ) )
	    {
	        return gxFalse;
		}
		//ジョイスティックの「機能」を列挙する。
		//コールバック関数内でユーザーインターフェースにある要素を見つけて
		//AXISの範囲を決定する。
		// ※DIDFT_ALL　サポートする機能をすべて調べる
	    if( FAILED( g_pJoystickDevice[n]->EnumObjects( EnumObjectsCallback, (VOID*)CWindows::GetInstance()->m_hWindow, DIDFT_ALL ) ) )
	    {
			return gxFalse;
		}

		//フォースフィードバック対応デバイスの軸数を調べる
	    if ( FAILED( g_pJoystickDevice[n]->EnumObjects( EnumAxesCallback, (VOID*)&m_pGamingDevice[n]->_ForceFeefBackAxis, DIDFT_AXIS ) ) )
	    {
			return gxFalse;
		}

		//軸数が２つ以上あるものはGGAでは、それ以上エフェクトの対象としない
		if (m_pGamingDevice[n]->_ForceFeefBackAxis > 0)
		{
			if (m_pGamingDevice[n]->_ForceFeefBackAxis > 2)
			{
				m_pGamingDevice[n]->_ForceFeefBackAxis = 2;

			}
			//フォースフィードバック可能か？
			CheckForceFeedBackEffect(n);
		}
	}

	GX_DEBUGLOG("【Control】使用可能なパッド数は%d",m_uGamingDeviceNum);

	return true;
}


void CDirectInput::CheckForceFeedBackEffect(int device_id )
{
	//-------------------------------------------
	//フォースフィードバック検出
	//-------------------------------------------

    DWORD           rgdwAxes[2]     = { DIJOFS_X, DIJOFS_Y };
    LONG            rglDirection[2] = { 0, 0 };
    DICONSTANTFORCE cf              = { 0 };
    DIEFFECT eff;
    ZeroMemory( &eff, sizeof(eff) );
    eff.dwSize                  = sizeof(DIEFFECT);
    eff.dwFlags                 = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
    eff.dwDuration              = INFINITE;
    eff.dwSamplePeriod          = 0;
    eff.dwGain                  = DI_FFNOMINALMAX;
    eff.dwTriggerButton         = DIEB_NOTRIGGER;
    eff.dwTriggerRepeatInterval = 0;
    eff.cAxes                   = m_pGamingDevice[device_id]->_ForceFeefBackAxis;
    eff.rgdwAxes                = rgdwAxes;
    eff.rglDirection            = rglDirection;
    eff.lpEnvelope              = 0;
    eff.cbTypeSpecificParams    = sizeof(DICONSTANTFORCE);
    eff.lpvTypeSpecificParams   = &cf;
    eff.dwStartDelay            = 0;

	Uint32 id = 0;

    //エフェクト用のポインタを作成
//	HRESULT hr = g_pJoystickDevice[device_id]->CreateEffect(GUID_ConstantForce, &eff, &g_pEffect[device_id], NULL);
	HRESULT hr = g_pJoystickDevice[device_id]->CreateEffect(GUID_ConstantForce, &eff, &g_pEffect[id], NULL);

	if( FAILED( hr ) )
    {
		m_pGamingDevice[device_id]->_bRumble = gxFalse;
		GX_DEBUGLOG("【Control】DirectInput デバイス%dでFFB不可",device_id);
		return;
    }

	//エフェクト用のポインタが確保できれば準備をすすめる
	if( g_pEffect[id] )
	{
		m_pGamingDevice[device_id]->_bRumble = gxTrue;	//振動可能
		GX_DEBUGLOG("【Control】DirectInputデバイス%dでFFB可能",device_id);
	}
	else
	{
		GX_DEBUGLOG("【Control】DirectInput デバイス%dでFFB不可",device_id);
	}
}


void CDirectInput::Action()
{

	//毎フレーム通る処理
	Uint32 i;

	for(i=0;i<m_uGamingDeviceNum;i++)
	{
		//使用しない機能について制限する

		bool bFlag;
		bFlag = true;

		if( !m_bUseController )
		{
			//そもそもコントローラーを使用しない場合には全てのボタンを認識させない
			bFlag = gxFalse;
		}

/*
		m_pGamingDevice[i]->_bUseAxisX			= (m_bEnable[i][0]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseAxisY			= (m_bEnable[i][1]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseAxisZ			= (m_bEnable[i][2]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationX		= (m_bEnable[i][3]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationY		= (m_bEnable[i][4]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationZ		= (m_bEnable[i][5]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV0			= (m_bEnable[i][6]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV1			= (m_bEnable[i][7]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV2			= (m_bEnable[i][8]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV3			= (m_bEnable[i][9]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseSlider0		= (m_bEnable[i][10] )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseSlider1		= (m_bEnable[i][11] )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseForceFeedBack	= (m_bEnable[i][10] )? bFlag : gxFalse;
*/

		UpdateGamingDevices(i);
	}

	//rumble();

}


void CDirectInput::UpdateGamingDevices(int n)
{
	//-----------------------------------------
	//ハードウェア情報を更新する
	//-----------------------------------------
    HRESULT     hr;

	CGameingDevice *pGameDevice = m_pGamingDevice[n];
	if(g_pJoystickDevice[n] == NULL) return;

	LPDIRECTINPUTDEVICE8 pPadDevice = g_pJoystickDevice[n];
	DIJOYSTATE2 js;           // ジョイスティックの状態を取得する構造体変数

	//ポーリング
	hr = pPadDevice->Poll();

	if( FAILED( hr ) )
	{
		//入力ストリームが途中で絶たれた。特殊なリセット処理を行い再度接続を試みる
		hr = pPadDevice->Acquire();
		while( hr  == DIERR_INPUTLOST )
		{
			//ロストしているだけなら再接続できるまで待つ
			hr = pPadDevice->Acquire();
		}
		return ;	//次の機会に接続再開してみる
	}

	if( FAILED(pPadDevice->GetDeviceState( sizeof(DIJOYSTATE2), &js ) ) )
	{
		//ジョイスティックの状態取得に失敗
		return ;
	}

	//状態の取得に成功した

	//---------------------------------
	//AXISについて
	//---------------------------------
	if( !pGameDevice->_bCarribrateCompleted )
	{
		pGameDevice->_bCarribrateCompleted = gxTrue;
		pGameDevice->_caribrate.x = js.lX;
		pGameDevice->_caribrate.y = js.lY;
		pGameDevice->_caribrate.z = js.lZ;

		pGameDevice->_caribrateR.x = js.lRx;
		pGameDevice->_caribrateR.y = js.lRy;
		pGameDevice->_caribrateR.z = js.lRz;
	}

	//if (js.lX > 10000)
	//{
	//	int n = 0;
	//	n++;
	//}

	pGameDevice->_Axis.x 		= (js.lX  - pGameDevice->_caribrate.x)  / AXIS_RANGE;
	pGameDevice->_Axis.y 		= (js.lY  - pGameDevice->_caribrate.y)  / AXIS_RANGE;
	pGameDevice->_Axis.z 		= (js.lZ  - pGameDevice->_caribrate.z)  / AXIS_RANGE;

	pGameDevice->_Rotation.x 	= (js.lRx - pGameDevice->_caribrateR.x) / AXIS_RANGE;
	pGameDevice->_Rotation.y 	= (js.lRy - pGameDevice->_caribrateR.y) / AXIS_RANGE;
	pGameDevice->_Rotation.z 	= (js.lRz - pGameDevice->_caribrateR.z) / AXIS_RANGE;

	pGameDevice->_Axis.x = CLAMP(pGameDevice->_Axis.x , - 1.0f, 1.0f);
	pGameDevice->_Axis.y = CLAMP(pGameDevice->_Axis.y , - 1.0f, 1.0f);
	pGameDevice->_Axis.z = CLAMP(pGameDevice->_Axis.z , - 1.0f, 1.0f);

	pGameDevice->_Rotation.x = CLAMP(pGameDevice->_Rotation.x , - 1.0f, 1.0f);
	pGameDevice->_Rotation.y = CLAMP(pGameDevice->_Rotation.y , - 1.0f, 1.0f);
	pGameDevice->_Rotation.z = CLAMP(pGameDevice->_Rotation.z , - 1.0f, 1.0f);

	//---------------------------------
	//スライダーコントロールについて
	//---------------------------------
	pGameDevice->_Slider0 = js.rglSlider[0];
	pGameDevice->_Slider1 = js.rglSlider[1];

	//---------------------------------
	//ハットスイッチについて
	//---------------------------------
	pGameDevice->_Pov0 = cnvPOVValue( js.rgdwPOV[0] );
	pGameDevice->_Pov1 = cnvPOVValue( js.rgdwPOV[1] );
	pGameDevice->_Pov2 = cnvPOVValue( js.rgdwPOV[2] );
	pGameDevice->_Pov3 = cnvPOVValue( js.rgdwPOV[3] );

	Sint32 xiTblpad[128]={
		//XINPUTに入力を合わせる
		GDBTN_BUTTON_A,//0 + 1,	//A
		GDBTN_BUTTON_B,//1 + 1,	//B
		GDBTN_BUTTON_X,//A3 + 1,	//X
		GDBTN_BUTTON_Y,//4 + 1,	//Y
		GDBTN_BUTTON_L1,// 6 + 1,	//L1
		GDBTN_BUTTON_R1,	//7 + 1,	//R1
		GDBTN_BUTTON_SL,//10 + 1,	//SL
		GDBTN_BUTTON_ST,//11 + 1,	//ST
		GDBTN_BUTTON_L3,//8 + 1,	//A
		GDBTN_BUTTON_R3,//9 + 1,	//A
		GDBTN_BUTTON_L2,//15 + 1,	//A
		GDBTN_BUTTON_R2,//16 + 1,	//A

/*
		//PS3コントローラ準拠
		GDBTN_BUTTON_X,	//A3 + 1,	//X
		GDBTN_BUTTON_A,	//0 + 1,	//A
		GDBTN_BUTTON_B,	//1 + 1,	//B
		GDBTN_BUTTON_Y,	//4 + 1,	//Y
		GDBTN_BUTTON_L1,// 6 + 1,	//L1
		GDBTN_BUTTON_R1,	//7 + 1,	//R1
		GDBTN_BUTTON_L2,//15 + 1,	//A
		GDBTN_BUTTON_R2,//16 + 1,	//A
		GDBTN_BUTTON_ST,//11 + 1,	//ST
		GDBTN_BUTTON_SL,//10 + 1,	//SL
		GDBTN_BUTTON_L3,//8 + 1,	//A
		GDBTN_BUTTON_R3,//9 + 1,	//A
*/

/*
		12 + 1,	//A
		13 + 1,	//A
		14 + 1,	//A
		2 + 1,	//C
		5+1,	//Z
		32,32,32,32,
		32,32,32,32,
*/
	};
	gxBool bPushed = gxFalse;
    for( int ii = 0; ii < sizeof(js.rgbButtons); ii++ )
    {
		Sint32 n = ii;

		if (ii >= ARRAY_LENGTH(xiTblpad))
		{
			//何も設定がなかった場合
			n = GDBTN_BUTTON_A + ii;
		}
		else
		{
			n = xiTblpad[ii];
			if (n == GDBTN_NONE )
			{
				n = GDBTN_BUTTON_A + ii;
			}
		}

		if (n >= 128)
		{
			n = CLAMP(n , 0,127);
		}
		
		if ( js.rgbButtons[ii] & 0x80 )
        {
			pGameDevice->setButton((GAMINGDEVICE_BTNID)n);// _button[n] = 1;
			bPushed = gxTrue;
		}
		else
		{
			pGameDevice->setButton((GAMINGDEVICE_BTNID)n,gxFalse);// _button[n] = 0;
		}
	}

	if (bPushed || ABS(pGameDevice->_Axis.x) > 0.5f || ABS(pGameDevice->_Axis.y) > 0.5f)
	{
		//一度でもコントローラーに入力したことがあるフラグを立てる
		pGameDevice->m_bUsedDevice = gxTrue;
	}

	Uint32 eID = 0;

	//フォースフィードバック使用可能か？
	if (!pGameDevice->_bRumble) return;
	
	//一度も入力を受け付けたことがなければ放置パッドなので振動させない
	if (!pGameDevice->m_bUsedDevice) return;

	if (g_pEffect[eID] == NULL) return;


	//該当のプレイヤー使用中のデバイスでなければ continue;
	//if( プレイヤー番号 != 割り振られたコントローラ番号 ) continue;
	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		if (pGameDevice->_PlayerID != ii) continue;

		Sint32 Motor1 = 0, Motor2 = 0;

		Float32 fRatio[2];
		gxPadManager::GetInstance()->GetMotorStat(ii, 0, &fRatio[0]);
		gxPadManager::GetInstance()->GetMotorStat(ii, 1, &fRatio[1]);

		if (fRatio[0] == 0 && fRatio[1] == 0)
		{
			if (g_pEffect[eID] && pGameDevice->_bRumbling)
			{
				g_pEffect[eID]->Stop();
				pGameDevice->_bRumbling = gxFalse;
			}
			break;
		}

		if (!pGameDevice->_bRumbling)
		{
			HRESULT hr;

			hr = g_pEffect[eID]->Start(1, 0);

			pGameDevice->_bRumbling = gxTrue;
			switch (hr) {
			case DI_OK:
				break;
			case DIERR_INCOMPLETEEFFECT:
			case DIERR_INVALIDPARAM:
			case DIERR_NOTEXCLUSIVEACQUIRED:
			case DIERR_NOTINITIALIZED:
			case DIERR_UNSUPPORTED:
			default:
				hr = hr;
				break;
			}
			break;

		}
	}



/*
	if (pGameDevice->_bXInput)
	{
		//XInoutと同時にくる場合
		if (pGameDevice->_Axis.z > 0.5f)
		{
			pGameDevice->_button[8] = 1;
		}
		if (pGameDevice->_Axis.z < -0.5f)
		{
			pGameDevice->_button[9] = 1;
		}
	}
*/
//	rumble();
}


void CDirectInput::AddDevice( gxChar *pDeviceName )
{
	m_pGamingDevice[ m_uGamingDeviceNum ] = CGamePad::GetInstance()->GetDevice();
	if (m_pGamingDevice[m_uGamingDeviceNum] == NULL) return;

	sprintf(m_pGamingDevice[m_uGamingDeviceNum]->_hardwarename, "%s" , pDeviceName );

	m_uGamingDeviceNum ++;
}

BOOL CALLBACK EnumFFBJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext)
{
	g_GUID_FFB_Device[FFBDeviceNum] = pdidInstance->guidInstance;
	FFBDeviceNum++;

	return DIENUM_CONTINUE;

}

BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,VOID* pContext )
{
	//-----------------------------------------------------------------------------
	//使用できるジョイパッドをコールバックにより列挙します。
	//１つでもみつけたら デバイスインターフェース(LPDIRECTINPUT8)を
	//プレイに使用できるように作成します。
	//※逆にいうと、１つもみつからなかったらLPDIRECTINPUT8には何も入らない。
	//-----------------------------------------------------------------------------

    // Obtain an interface to the enumerated joystick.
	if(FAILED( g_pDirectInput->CreateDevice( pdidInstance->guidInstance, &g_pJoystickDevice[CDirectInput::GetInstance()->GetDeviceNum()], NULL ) ) )
	{
		// デバイスを確認できなかったので使用できない。(おそらく接続されていない）
	    return DIENUM_STOP;
	}

	if( ( CDirectInput::GetInstance()->GetDeviceNum()+1 ) > CDirectInput::enControllerMax)
	{
		//１６本以上は検出しない
		return DIENUM_CONTINUE;
	}

	GX_DEBUGLOG("%s / %s", pdidInstance->tszInstanceName, pdidInstance->tszProductName);

	//ハードウェア名を記録
	//sprintf(CDirectInput::GetInstance()->m_pGamingDevice[CDirectInput::GetInstance()->GetDeviceNum()]._hardwarename,"%s",pdidInstance->tszInstanceName);

//	CDirectInput::GetInstance()->AddDeviceNum();//, pdidInstance->tszProductName );
	CDirectInput::GetInstance()->AddDevice( (gxChar*)&pdidInstance->tszInstanceName );//, pdidInstance->tszProductName );

	return DIENUM_CONTINUE;
}


//-----------------------------------------------------------------------------
// ジョイスティックの機能が検出されるたびにコールされる (axes, buttons, POVs) 
//-----------------------------------------------------------------------------
BOOL CALLBACK EnumObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi,VOID* pContext )
{
	int n = g_NowSettingDeviceNo;
	//HWND hDlg = (HWND)pContext;

	//-------------------------------------------------------------------
	// ジョイスティックの軸が検出されれば、その範囲（-AXIS_RANGE ～ +AXIS_RANGE）を設定する
	//-------------------------------------------------------------------

    if( pdidoi->dwType & DIDFT_AXIS ) {
        DIPROPRANGE diprg; 
        diprg.diph.dwSize       = sizeof(DIPROPRANGE); 
        diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
        diprg.diph.dwHow        = DIPH_BYID; 
        diprg.diph.dwObj        = pdidoi->dwType; // Specify the enumerated axis
        diprg.lMin              = -AXIS_RANGE; 
        diprg.lMax              = +AXIS_RANGE; 

		// AXISの範囲を設定する
        if( FAILED( g_pJoystickDevice[n]->SetProperty( DIPROP_RANGE, &diprg.diph ) ) )
        {
            return DIENUM_STOP;
        }
    }

	//-------------------------------------------------------------------
    // ジョイスティックがサポートしている機能を調査する
	//-------------------------------------------------------------------

    if (pdidoi->guidType == GUID_XAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisX = true;
    }
    if (pdidoi->guidType == GUID_YAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisY = true;
    }
    if (pdidoi->guidType == GUID_ZAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisZ = true;
    }
    if (pdidoi->guidType == GUID_RxAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisRX = true;
    }
    if (pdidoi->guidType == GUID_RyAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisRY = true;
    }
    if (pdidoi->guidType == GUID_RzAxis)
    {
		CDirectInput::GetInstance()->GetGamingDevice(n)->_bAxisRZ = true;
    }

    if (pdidoi->guidType == GUID_Slider)
    {
		switch( CDirectInput::GetInstance()->GetGamingDevice(n)->_num_slider++ ) {
		case 0 :
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bSlider[0] = true;
			break;

		case 1 :
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bSlider[1] = true;
			break;
		}
	}

	if (pdidoi->guidType == GUID_POV) {
		switch( CDirectInput::GetInstance()->GetGamingDevice(n)->_num_pov++ ) {
		case 0 :
			//ハットスイッチ１つ
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bPov[0] = true;
			break;
		case 1 :
			//ハットスイッチ２つ
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bPov[1] = true;
			break;
		case 2 :
			//ハットスイッチ３つ
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bPov[2] = true;
			break;
		case 3 :
			//ハットスイッチ４つ
			CDirectInput::GetInstance()->GetGamingDevice(n)->_bPov[3] = true;
		default:
			break;
		}
	}

	return DIENUM_CONTINUE;
}


//-----------------------------------------------------------------------------
// フォースフィードバック対応のデバイスについて
// 有効な軸を機能する数分だけコールバックされる
//-----------------------------------------------------------------------------
BOOL CALLBACK EnumAxesCallback( const DIDEVICEOBJECTINSTANCE* pdidoi,VOID* pContext )
{
    DWORD* pdwNumForceFeedbackAxis = (DWORD*) pContext;

    if( (pdidoi->dwFlags & DIDOI_FFACTUATOR) != 0 )
    {
        (*pdwNumForceFeedbackAxis)++;
    }

    return DIENUM_CONTINUE;
}


void CDirectInput::rumble()
{
	//振動

 //   LONG rglDirection[2] = { 180*DI_DEGREES, 0 };
//    DICONSTANTFORCE cf;
	DWORD rgdwAxes[2] = { DIJOFS_X , DIJOFS_Y };
	LONG  rglDirection[2] = { 1 , 1 };

	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{


		for (Uint32 n = 0; n < m_uGamingDeviceNum; n++)
		{
			if (!m_pGamingDevice[n]->_bRumble) continue;

			//該当のプレイヤー使用中のデバイスでなければ continue;

			if (g_pEffect[n] == NULL) continue;

			//一度も入力を受け付けたことがなければ放置パッドなので振動させない
			if (!m_pGamingDevice[n]->m_bUsedDevice) continue;

			//if( プレイヤー番号 != 割り振られたコントローラ番号 ) continue;
			if (m_pGamingDevice[n]->_PlayerID != ii) continue;

			Sint32 Motor1 = 0, Motor2 = 0;

			Float32 fRatio[2];
			gxPadManager::GetInstance()->GetMotorStat(ii, 0, &fRatio[0]);
			gxPadManager::GetInstance()->GetMotorStat(ii, 1, &fRatio[1]);

			if (fRatio[0] == 0 && fRatio[1] == 0)
			{
				if (g_pEffect[n] && m_pGamingDevice[n]->_bRumbling)
				{
					g_pEffect[n]->Stop();
					m_pGamingDevice[n]->_bRumbling = gxFalse;
				}
				continue;
			}

			{
				if (!m_pGamingDevice[n]->_bRumbling)
				{
					HRESULT hr;

					hr = g_pEffect[n]->Start(1, DIES_SOLO);

					m_pGamingDevice[n]->_bRumbling = gxTrue;
					switch (hr) {
					case DI_OK:
						break;
					case DIERR_INCOMPLETEEFFECT:
					case DIERR_INVALIDPARAM:
					case DIERR_NOTEXCLUSIVEACQUIRED:
					case DIERR_NOTINITIALIZED:
					case DIERR_UNSUPPORTED:
					default:
						hr = hr;
						break;
					}
				}

			}
		}



    }

	//エフェクト用のポインタが確保できれば準備をすすめる
//	if( g_pEffect[device_id] )
	{
	}

}


