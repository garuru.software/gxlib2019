﻿//-------------------------------------------------
//
// input
// 
//-------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include "winStore.h"
#include "../CXAudio2.h"
#include "CDx11Store.h"
#include "CGamePad.h"
#include "CStoreInput.h"
#include <vector>

SINGLETON_DECLARE_INSTANCE( CStoreInput );

CStoreInput::CStoreInput()
{
	m_bUseController = gxTrue;

	for(Uint32 ii=0;ii<enControllerMax;ii++)
	{
		m_pGamingDevice[ii] = NULL;
		for(Uint32 jj=0;jj<GDBTN_MAX;jj++)
		{
			m_bEnable[ii][jj] = gxTrue;
		}
	}

	m_uGamingDeviceNum = 0;		//デバイスの数はゼロ

	Init();
}

CStoreInput::~CStoreInput()
{
	
}


#include <collection.h>
using namespace DirectX;
using namespace Windows::Gaming::Input;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Platform::Collections;
using Microsoft::WRL::ComPtr;

Platform::Collections::Vector<Windows::Gaming::Input::Gamepad^>^ m_localCollection;

void CStoreInput::Init()
{
	for( Sint32 ii=0; ii<enAccelMax; ii ++ )
	{
		m_Accellarator[ii] = 0;
	}

	//Gamepad states
	m_localCollection = ref new Vector<Gamepad^>();

	auto gamepads = Gamepad::Gamepads;
	for (auto gamepad : gamepads)
	{
		m_localCollection->Append(gamepad);
	}
	Gamepad::GamepadAdded += ref new EventHandler<Gamepad^ >([=](Platform::Object^, Gamepad^ args)
	{
		int ii = m_uGamingDeviceNum;
		m_pGamingDevice[ii] = CGamePad::GetInstance()->GetDevice();
		m_pGamingDevice[ii]->_bPov[0] = gxTrue;			//ハットスイッチ0
		m_pGamingDevice[ii]->_bPov[1] = gxTrue;			//ハットスイッチ1
		m_pGamingDevice[ii]->_bPov[2] = gxTrue;			//ハットスイッチ2
		m_pGamingDevice[ii]->_bPov[3] = gxTrue;			//ハットスイッチ3
		m_pGamingDevice[ii]->_num_slider = 0;				//スライダーコントロール数
		m_pGamingDevice[ii]->_bAxisX =
		m_pGamingDevice[ii]->_bAxisY =
		m_pGamingDevice[ii]->_bAxisZ = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bAxisRX =
		m_pGamingDevice[ii]->_bAxisRY =
		m_pGamingDevice[ii]->_bAxisRZ = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bRumble = gxTrue;				//振動可能か？

		m_localCollection->Append(args);
		m_uGamingDeviceNum ++;
		//m_currentGamepadNeedsRefresh = true;
	});

	Gamepad::GamepadRemoved += ref new EventHandler<Gamepad^ >([=](Platform::Object^, Gamepad^ args)
	{
		unsigned int index;
		if (m_localCollection->IndexOf(args, &index))
		{
			m_localCollection->RemoveAt(index);
			m_uGamingDeviceNum--;
			//m_currentGamepadNeedsRefresh = true;
		}
	});
	

	//検出されたジョイパッド数を記録
//	m_uGamingDeviceNum = 1;

	for (Sint32 ii = 0; ii<m_uGamingDeviceNum; ii++)
	{
		m_pGamingDevice[ii] = CGamePad::GetInstance()->GetDevice();
		m_pGamingDevice[ii]->_bPov[0] = gxTrue;			//ハットスイッチ0
		m_pGamingDevice[ii]->_bPov[1] = gxTrue;			//ハットスイッチ1
		m_pGamingDevice[ii]->_bPov[2] = gxTrue;			//ハットスイッチ2
		m_pGamingDevice[ii]->_bPov[3] = gxTrue;			//ハットスイッチ3
		m_pGamingDevice[ii]->_num_slider = 0;				//スライダーコントロール数
		m_pGamingDevice[ii]->_bAxisX =
		m_pGamingDevice[ii]->_bAxisY =
		m_pGamingDevice[ii]->_bAxisZ = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bAxisRX =
		m_pGamingDevice[ii]->_bAxisRY =
		m_pGamingDevice[ii]->_bAxisRZ = gxTrue;				//サポートされているAXIS
		m_pGamingDevice[ii]->_bRumble = gxTrue;				//振動可能か？
	}

	GX_DEBUGLOG("【Control】使用可能なパッド数は%d", m_uGamingDeviceNum);

}

void CStoreInput::Action()
{
	//毎フレーム通る処理


	for(Uint32 i=0;i<m_uGamingDeviceNum;i++)
	{
		//使用しない機能について制限する

		bool bFlag;
		bFlag = true;

		if( !m_bUseController )
		{
			//そもそもコントローラーを使用しない場合には全てのボタンを認識させない
			bFlag = gxFalse;
		}

/*
	    DWORD dwResult;

        dwResult = XInputGetState( i, &GamePad360[i].state );

        if( dwResult != ERROR_SUCCESS )
		{
            GamePad360[i].bConnected = gxFalse;
		}

		if( !GamePad360[i].bConnected ) continue;
*/
		m_pGamingDevice[i]->_bUseAxisX			= (m_bEnable[i][0]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseAxisY			= (m_bEnable[i][1]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseAxisZ			= (m_bEnable[i][2]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationX		= (m_bEnable[i][3]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationY		= (m_bEnable[i][4]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseRotationZ		= (m_bEnable[i][5]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV0			= (m_bEnable[i][6]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV1			= (m_bEnable[i][7]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV2			= (m_bEnable[i][8]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUsePOV3			= (m_bEnable[i][9]  )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseSlider0		= (m_bEnable[i][10] )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseSlider1		= (m_bEnable[i][11] )? bFlag : gxFalse;
		m_pGamingDevice[i]->_bUseForceFeedBack	= (m_bEnable[i][10] )? bFlag : gxFalse;

		UpdateGamingDevices(i);
	}

	rumble();
}


void CStoreInput::UpdateGamingDevices( int n )
{
	//-----------------------------------------
	//ハードウェア情報を更新する
	//-----------------------------------------
	CGameingDevice *pGameDevice = m_pGamingDevice[n];

	//if( !GamePad360[n].bConnected ) return;
	//Uint32 Buttons = m_GamePad[n].Button;

	//状態の取得に成功した

	Gamepad^ pGamePad = m_localCollection->GetAt(n);
	auto m_reading = pGamePad->GetCurrentReading();

	//---------------------------------
	//AXISについて
	//---------------------------------

	pGameDevice->_Axis.x		= m_reading.LeftThumbstickX;
	pGameDevice->_Axis.y 		= -m_reading.LeftThumbstickY;
	pGameDevice->_Axis.z		= m_reading.LeftTrigger;

	pGameDevice->_Rotation.x 	= m_reading.RightThumbstickX;
	pGameDevice->_Rotation.y 	= -m_reading.RightThumbstickY;
	pGameDevice->_Rotation.z	= m_reading.RightTrigger;;

	//---------------------------------
	//スライダーコントロールについて
	//---------------------------------
	pGameDevice->_Slider0 = m_reading.LeftTrigger;
	pGameDevice->_Slider1 = m_reading.RightTrigger;

	//---------------------------------
	//ハットスイッチについて
	//---------------------------------
	pGameDevice->_Pov0 = 0;
	if(( m_reading.Buttons & GamepadButtons::DPadUp    ) == GamepadButtons::DPadUp   ) pGameDevice->_Pov0 |= JOY_U;
	if(( m_reading.Buttons & GamepadButtons::DPadRight ) == GamepadButtons::DPadRight) pGameDevice->_Pov0 |= JOY_R;
	if(( m_reading.Buttons & GamepadButtons::DPadDown  ) == GamepadButtons::DPadDown ) pGameDevice->_Pov0 |= JOY_D;
	if(( m_reading.Buttons & GamepadButtons::DPadLeft  ) == GamepadButtons::DPadLeft ) pGameDevice->_Pov0 |= JOY_L;

	pGameDevice->_Pov1 = 0;
	pGameDevice->_Pov2 = 0;
	pGameDevice->_Pov3 = 0;

	pGameDevice->_button[0] = ((m_reading.Buttons & GamepadButtons::A) == GamepadButtons::A) ? 1 : 0;
	pGameDevice->_button[1] = ((m_reading.Buttons & GamepadButtons::B) == GamepadButtons::B) ? 1 : 0;
	pGameDevice->_button[3] = ((m_reading.Buttons & GamepadButtons::X) == GamepadButtons::X) ? 1 : 0;
	pGameDevice->_button[4] = ((m_reading.Buttons & GamepadButtons::Y) == GamepadButtons::Y) ? 1 : 0;

	pGameDevice->_button[6]  = ((m_reading.Buttons & GamepadButtons::LeftShoulder) == GamepadButtons::LeftShoulder) ? 1 : 0;
	pGameDevice->_button[7]  = ((m_reading.Buttons & GamepadButtons::RightShoulder) == GamepadButtons::RightShoulder) ? 1 : 0;
	pGameDevice->_button[8] = (m_reading.LeftTrigger > 0.5f) ? 1 : 0;
	pGameDevice->_button[9] = (m_reading.RightTrigger > 0.5f) ? 1 : 0;
	pGameDevice->_button[10] = ((m_reading.Buttons & GamepadButtons::View) == GamepadButtons::View) ? 1 : 0;
	pGameDevice->_button[11] = ((m_reading.Buttons & GamepadButtons::Menu) == GamepadButtons::Menu) ? 1 : 0;

	pGameDevice->_button[12] = ((m_reading.Buttons & GamepadButtons::LeftThumbstick) == GamepadButtons::LeftThumbstick ) ? 1 : 0;
	pGameDevice->_button[13] = ((m_reading.Buttons & GamepadButtons::RightThumbstick) == GamepadButtons::RightThumbstick) ? 1 : 0;
}

void CStoreInput::rumble()
{
	//振動

	for (int n = 0; n<m_uGamingDeviceNum; n++)
	{
		//if( !m_pGamingDevice[ n ]._bRumble ) continue;

		Gamepad^ pGamePad = m_localCollection->GetAt(n);
		auto m_reading = pGamePad->GetCurrentReading();

		//該当yのプレイヤー使用中のデバイスでなければ continue;

		Float32 fRatio[2];
		gxPadManager::GetInstance()->GetMotorStat(n, 0, &fRatio[0]);
		gxPadManager::GetInstance()->GetMotorStat(n, 1, &fRatio[1]);

		Windows::Gaming::Input::GamepadVibration    m_vibration;

		if (fRatio[0] == 0 && fRatio[1] == 0)
		{
			ZeroMemory(&m_vibration, sizeof(GamepadVibration));

			m_vibration.LeftMotor    = 0;
			m_vibration.LeftTrigger  = 0;
			m_vibration.RightMotor   = 0;
			m_vibration.RightTrigger = 0;
			pGamePad->Vibration = m_vibration;

			continue;
		}

		ZeroMemory(&m_vibration, sizeof(GamepadVibration));
		m_vibration.LeftMotor    = fRatio[0];
		m_vibration.LeftTrigger  = fRatio[0];
		m_vibration.RightMotor   = fRatio[1];
		m_vibration.RightTrigger = fRatio[1];
		pGamePad->Vibration = m_vibration;
	}
}


void CStoreInput::CheckAccellarator( Sint32 id , gxBool bPush )
{
	Sint32 key = enID_NoneAccel;
	switch( id ){
	case gxKey::F1:
		key = enID_GamePause;
		break;
	case gxKey::F2:
		key = enID_SoundSwitch;
		break;
	case gxKey::F3:
		key = enID_GameStep;
		break;
	case gxKey::F4:
		key = enID_PadEnableSwitch;
		break;
	case gxKey::F5:
		key = enID_ChangeFullScreenMode;
		break;
	case gxKey::F6:
		key = enID_SamplingFilter;
		break;
	case gxKey::F7:
		key = enID_Reset;
		break;
	case gxKey::F8:
		key = enID_SwitchVirtualPad;
		break;
	case gxKey::F9:
		key = enID_DebugMode;
		break;
	case gxKey::F11:
		key = enID_FullSpeed;
		break;
	case gxKey::F12:
		key = enID_ScreenShot;
		break;
	case gxKey::PAGEUP:
		key = enID_MasterVolumeAdd;
		break;
	case gxKey::PAGEDOWN:
		key = enID_MasterVolumeSub;
		break;

	case gxKey::NUM3:
		key = enID_Switch3DView;
		break;

	case gxKey::ESC:
		if (CWindows::GetInstance()->IsFullScreen())
		{
			key = enID_ChangeFullScreenMode;
		}
		else
		{
			key = enID_AppExit;
		}
		break;
	default:
		break;

	}

	if( key == enID_NoneAccel ) return;

	if( bPush )
	{
		m_Accellarator[ key ] ++;
		if( m_Accellarator[ key ] != 1 ) return;
	}
	else
	{
		m_Accellarator[ key ] = 0;
		return;
	}

	switch( key ){
	case enID_ChangeFullScreenMode:	//フルスクリーン切り替え
		if (CWindows::GetInstance()->IsFullScreen())
		{
			CWindows::GetInstance()->SetFullScreen(gxFalse);
			auto av = Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
			av->ExitFullScreenMode();
			av->FullScreenSystemOverlayMode = Windows::UI::ViewManagement::FullScreenSystemOverlayMode::Standard;
		}
		else
		{
			CWindows::GetInstance()->SetFullScreen(gxTrue);
			auto av = Windows::UI::ViewManagement::ApplicationView::GetForCurrentView();
			if (av->TryEnterFullScreenMode())
			{
				av->FullScreenSystemOverlayMode = Windows::UI::ViewManagement::FullScreenSystemOverlayMode::Minimal;
			}
		}
		break;
/*
	case IDM_WINDOW_ORIGINAL:
		CWindows::GetInstance()->SetScreenMode( CWindows::enScreenModeOriginal );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;

	case IDM_WINDOW_ASPECT:
		CWindows::GetInstance()->SetScreenMode( CWindows:: enScreenModeAspect );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;

	case IDM_WINDOW_STRETCH:
		CWindows::GetInstance()->SetScreenMode( CWindows::enScreenModeFull );
		CDeviceManager::GetInstance()->AdjustScreenResolution();
		break;
*/
	case enID_GamePause			 : 	//ゲームのポーズ
		CGameGirl::GetInstance()->SetPause( !CGameGirl::GetInstance()->IsPause() );
		break;

	case enID_GameStep			 : 	//ゲームのステップ
		if( CGameGirl::GetInstance()->IsPause() )
		{
			CGameGirl::GetInstance()->PauseStep();
		}
		else
		{
			CGameGirl::GetInstance()->SetPause(gxTrue);
		}
		break;

	case enID_PadEnableSwitch	 : 	//コントローラー切り替え
		gxPadManager::GetInstance()->SetEnableController( !gxPadManager::GetInstance()->IsEnableController() );
		break;

	case enID_DebugMode			 : 	//デバッグモードの切り替え
		CGameGirl::GetInstance()->ToggleDeviceMode();
		break;

	case enID_SamplingFilter:
		//サンプリングフィルター切り替え
		if( CWindows::GetInstance()->GetRenderingFilter() == CWindows::enSamplingNearest )
		{
			CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingBiLenear );
		}
		else
		{
			CWindows::GetInstance()->SetRenderingFilter( CWindows::enSamplingNearest );
		}
		break;

	case enID_FullSpeed			 : 	//フルスピード
		CGameGirl::GetInstance()->WaitVSync( !CGameGirl::GetInstance()->IsWaitVSync() );
		break;

	case enID_SoundSwitch		 : 	//サウンドON/OFF
		if( CWindows::GetInstance()->IsSoundEnable() )
		{
			CWindows::GetInstance()->SetSoundEnable(gxFalse);
		}
		else
		{
			CWindows::GetInstance()->SetSoundEnable(gxTrue);
		}
		break;

	case enID_MasterVolumeAdd:
		//Volume ++
		{
			Float32 fVol = gxLib::GetAudioMasterVolume();
			gxLib::SetAudioMasterVolume(fVol += 0.1f);
		}
		break;

	case enID_MasterVolumeSub:
		//Volume --
		{
			Float32 fVol = gxLib::GetAudioMasterVolume();
			gxLib::SetAudioMasterVolume( fVol -= 0.1f );
		}
		break;

	case enID_SwitchVirtualPad:
		{
			CWindows::GetInstance()->m_bVirtualPad = !CWindows::GetInstance()->m_bVirtualPad;
			gxLib::SetVirtualPad(CWindows::GetInstance()->m_bVirtualPad );
		}
		break;
	case enID_Switch3DView:
		CGameGirl::GetInstance()->Set3DView( !CGameGirl::GetInstance()->Is3DView() );
		break;

	case enID_AppExit             :	//アプリ終了
		CGameGirl::GetInstance()->Exit();
        break;
	}
}


Sint32 _updateInputDevice( Uint32 id , bool bPush )
{
	//キーと配列をマッチングさせる

	Windows::System::VirtualKey vKey = (Windows::System::VirtualKey)id;

	Sint32 keyboard = -1;
	Uint32 joypad   = 0;

	switch(vKey){
	case VirtualKey::F1				:keyboard = gxKey::F1          ;break;
	case VirtualKey::F2             :keyboard = gxKey::F2          ;break;
	case VirtualKey::F3             :keyboard = gxKey::F3          ;break;
	case VirtualKey::F4             :keyboard = gxKey::F4          ;break;
	case VirtualKey::F5             :keyboard = gxKey::F5          ;break;
	case VirtualKey::F6             :keyboard = gxKey::F6          ;break;
	case VirtualKey::F7             :keyboard = gxKey::F7          ;break;
	case VirtualKey::F8             :keyboard = gxKey::F8          ;break;
	case VirtualKey::F9             :keyboard = gxKey::F9          ;break;
	case VirtualKey::F10            :keyboard = gxKey::F10         ;break;
	case VirtualKey::F11            :keyboard = gxKey::F11         ;break;
	case VirtualKey::F12            :keyboard = gxKey::F12         ;break;
	case VirtualKey::Escape         :keyboard = gxKey::ESC         ;break;
	case VirtualKey::Back           :keyboard = gxKey::BS          ;break;
	case VirtualKey::Tab            :keyboard = gxKey::TAB         ;break;
	case VirtualKey::Enter          :keyboard = gxKey::RETURN      ;break;
	case VirtualKey::LeftShift      :keyboard = gxKey::SHIFT       ;break;
	case VirtualKey::RightShift     :keyboard = gxKey::RSHIFT      ;break;
	case VirtualKey::LeftControl    :keyboard = gxKey::CTRL        ;break;
	case VirtualKey::RightControl   :keyboard = gxKey::RCTRL       ;break;
	case VirtualKey::LeftMenu       :keyboard = gxKey::ALT         ;break;
	case VirtualKey::RightMenu      :keyboard = gxKey::RALT        ;break;
	case VirtualKey::Up             :keyboard = gxKey::UP    		;break;
	case VirtualKey::Down           :keyboard = gxKey::DOWN  		;break;
	case VirtualKey::Left           :keyboard = gxKey::LEFT  		;break;
	case VirtualKey::Right          :keyboard = gxKey::RIGHT 		;break;
	case VirtualKey::Space          :keyboard = gxKey::SPACE       ;break;
	case VirtualKey::NumberPad0     :keyboard = gxKey::NUMPAD0          ;break;
	case VirtualKey::NumberPad1     :keyboard = gxKey::NUMPAD1          ;break;
	case VirtualKey::NumberPad2     :keyboard = gxKey::NUMPAD2          ;break;
	case VirtualKey::NumberPad3     :keyboard = gxKey::NUMPAD3          ;break;
	case VirtualKey::NumberPad4     :keyboard = gxKey::NUMPAD4          ;break;
	case VirtualKey::NumberPad5     :keyboard = gxKey::NUMPAD5          ;break;
	case VirtualKey::NumberPad6     :keyboard = gxKey::NUMPAD6          ;break;
	case VirtualKey::NumberPad7     :keyboard = gxKey::NUMPAD7          ;break;
	case VirtualKey::NumberPad8     :keyboard = gxKey::NUMPAD8          ;break;
	case VirtualKey::NumberPad9     :keyboard = gxKey::NUMPAD9          ;break;
	case VirtualKey::Add			:keyboard = gxKey::NUM_ADD		 ;break;
	case VirtualKey::Subtract		:keyboard = gxKey::NUM_SUB		 ;break;
	case VirtualKey::Multiply		:keyboard = gxKey::NUM_MULTI      ;break;
	case VirtualKey::Divide         :keyboard = gxKey::NUM_DIV        ;break;
	case VirtualKey::Decimal        :keyboard = gxKey::NUM_PERIOD     ;break;
	case VirtualKey::PageUp         :keyboard = gxKey::PAGEUP      ;break;
	case VirtualKey::PageDown       :keyboard = gxKey::PAGEDOWN    ;break;
	case VirtualKey::Delete         :keyboard = gxKey::DEL		  ;break;
	case VirtualKey::Insert         :keyboard = gxKey::INS		  ;break;
	case VirtualKey::Home           :keyboard = gxKey::HOME		  ;break;
	case VirtualKey::End            :keyboard = gxKey::END		  ;break;
	case VirtualKey::Number0        :keyboard = gxKey::NUM0           ;break;
	case VirtualKey::Number1        :keyboard = gxKey::NUM1           ;break;
	case VirtualKey::Number2        :keyboard = gxKey::NUM2           ;break;
	case VirtualKey::Number3        :keyboard = gxKey::NUM3           ;break;
	case VirtualKey::Number4        :keyboard = gxKey::NUM4           ;break;
	case VirtualKey::Number5        :keyboard = gxKey::NUM5           ;break;
	case VirtualKey::Number6        :keyboard = gxKey::NUM6           ;break;
	case VirtualKey::Number7        :keyboard = gxKey::NUM7           ;break;
	case VirtualKey::Number8        :keyboard = gxKey::NUM8           ;break;
	case VirtualKey::Number9        :keyboard = gxKey::NUM9           ;break;
	case VirtualKey::A              :keyboard = gxKey::A           ;break;
	case VirtualKey::B              :keyboard = gxKey::B           ;break;
	case VirtualKey::C              :keyboard = gxKey::C           ;break;
	case VirtualKey::D              :keyboard = gxKey::D           ;break;
	case VirtualKey::E              :keyboard = gxKey::E           ;break;
	case VirtualKey::F              :keyboard = gxKey::F           ;break;
	case VirtualKey::G              :keyboard = gxKey::G           ;break;
	case VirtualKey::H              :keyboard = gxKey::H           ;break;
	case VirtualKey::I              :keyboard = gxKey::I           ;break;
	case VirtualKey::J              :keyboard = gxKey::J           ;break;
	case VirtualKey::K              :keyboard = gxKey::K           ;break;
	case VirtualKey::L              :keyboard = gxKey::L           ;break;
	case VirtualKey::M              :keyboard = gxKey::M           ;break;
	case VirtualKey::N              :keyboard = gxKey::N           ;break;
	case VirtualKey::O              :keyboard = gxKey::O           ;break;
	case VirtualKey::P              :keyboard = gxKey::P           ;break;
	case VirtualKey::Q              :keyboard = gxKey::Q           ;break;
	case VirtualKey::R              :keyboard = gxKey::R           ;break;
	case VirtualKey::S              :keyboard = gxKey::S           ;break;
	case VirtualKey::T              :keyboard = gxKey::T           ;break;
	case VirtualKey::U              :keyboard = gxKey::U           ;break;
	case VirtualKey::V              :keyboard = gxKey::V           ;break;
	case VirtualKey::W              :keyboard = gxKey::W           ;break;
	case VirtualKey::X              :keyboard = gxKey::X           ;break;
	case VirtualKey::Y              :keyboard = gxKey::Y           ;break;
	case VirtualKey::Z              :keyboard = gxKey::Z           ;break;

	//ゲームパッド
/*
	//なんか微妙に反応が鈍いのでゲームパッド側での実装に変更しようと思います
	case VirtualKey::GamepadDPadUp:					joypad = JOY_U;	 break;
	case VirtualKey::GamepadDPadRight:				joypad = JOY_R; break;
	case VirtualKey::GamepadDPadDown:				joypad = JOY_D; break;
	case VirtualKey::GamepadDPadLeft:				joypad = JOY_L; break;
	case VirtualKey::GamepadLeftShoulder:			joypad = BTN_L1; break;
	case VirtualKey::GamepadLeftThumbstickButton:	joypad = BTN_L3; break;
	case VirtualKey::GamepadLeftThumbstickUp:		joypad = JOY_U;  break;
	case VirtualKey::GamepadLeftThumbstickRight:	joypad = JOY_R;  break;
	case VirtualKey::GamepadLeftThumbstickDown:		joypad = JOY_D;  break;
	case VirtualKey::GamepadLeftThumbstickLeft:		joypad = JOY_L;  break;
	case VirtualKey::GamepadLeftTrigger:			joypad = BTN_L2; break;
	case VirtualKey::GamepadRightShoulder:			joypad = BTN_R1; break;
	case VirtualKey::GamepadRightThumbstickButton:	joypad = BTN_R3; break;
	case VirtualKey::GamepadRightThumbstickUp:		break;//joypad = BTN_L1; break;
	case VirtualKey::GamepadRightThumbstickRight:	break;//joypad = BTN_L1; break;
	case VirtualKey::GamepadRightThumbstickDown:	break;//joypad = BTN_L1; break;
	case VirtualKey::GamepadRightThumbstickLeft:	break;//joypad = BTN_L1; break;
	case VirtualKey::GamepadRightTrigger:			joypad = BTN_R2; break;
	case VirtualKey::GoBack:						joypad = BTN_SELECT; break;
	case VirtualKey::GoForward:						joypad = BTN_START; break;
	case VirtualKey::GamepadMenu:					joypad = BTN_START; break;
	case VirtualKey::GamepadView:					joypad = BTN_SELECT; break;
	case VirtualKey::GamepadA:						joypad = BTN_A; break;
	case VirtualKey::GamepadB:						joypad = BTN_B; break;
	case VirtualKey::GamepadX:						joypad = BTN_X; break;
	case VirtualKey::GamepadY:						joypad = BTN_Y; break;
*/
	//マウス
	case VirtualKey::LeftButton:
		if( bPush )
		{
			gxPadManager::GetInstance()->SetMouseButtonDown( 0 );
		}
		else
		{
			gxPadManager::GetInstance()->SetMouseButtonUp( 0 );
		}
		break;
	case VirtualKey::RightButton:
		if( bPush )
		{
			gxPadManager::GetInstance()->SetMouseButtonDown( 1 );
		}
		else
		{
			gxPadManager::GetInstance()->SetMouseButtonUp( 1 );
		}
		break;
	case VirtualKey::MiddleButton:
		if( bPush )
		{
			gxPadManager::GetInstance()->SetMouseButtonDown( 2 );
		}
		else
		{
			gxPadManager::GetInstance()->SetMouseButtonUp( 2 );
		}
		break;

	default:
		keyboard = -1;
		break;
	}

	//キーボード

	if (keyboard != -1)
	{
		if( bPush )
		{
			CGamePad::GetInstance()->SetKeyDown( keyboard );
			//Gamepad側で管理しないと複数押されたことになってしまう
		}
		else
		{
			CGamePad::GetInstance()->SetKeyUp( keyboard );
		}
	}

	//ジョイパッド

	if( joypad )
	{
		if( bPush )
		{
			CStoreInput::GetInstance()->m_GamePad[0].Button |= joypad;
		}
		else
		{
			CStoreInput::GetInstance()->m_GamePad[0].Button &= ~joypad;
		}
		return 0;	//gxKeyに変換できない
	}

	return keyboard;
}


void App::OnKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args)
{
	//キーを押した

	int id = (int)args->VirtualKey;

	id = _updateInputDevice( id , gxTrue );

	if (id >= 0 )
	{
		CStoreInput::GetInstance()->CheckAccellarator( id , gxTrue );
	}
}


void App::OnKeyUp(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args)
{
	//キーを離した

	int id = (int)args->VirtualKey;

	id = _updateInputDevice( id , gxFalse );

	if (id >= 0 )
	{
		CStoreInput::GetInstance()->CheckAccellarator( id , gxFalse );
	}

}

void App::OnPointerMoved(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args)
{
	//マウスポインタの管理（ここでは生の値を渡してCGamePadでつじつま合わせを行う）

	Windows::Foundation::Point point;
	Float32 mx,my;

	mx = args->CurrentPoint->Position.X;
	my = args->CurrentPoint->Position.Y;

/*
	Sint32 gamew=WINDOW_W,gameh=WINDOW_H;
	Sint32 winw =WINDOW_W,winh =WINDOW_H;

	CWindows::GetInstance()->GetGameResolution   ( &gamew , &gameh );
	CWindows::GetInstance()->GetWindowsResolution( &winw  , &winh   );

	Float32 ofx = (winw - gamew) / 2;
	Float32 ofy = (winh - gameh) / 2;

	//ゲーム画面からの相対位置
	Float32 rx = mx - ofx;
	Float32 ry = my - ofy;

	Float32 mx2 = WINDOW_W * rx / gamew;
	Float32 my2 = WINDOW_H * ry / gameh;
*/
	CGamePad::GetInstance()->SetMousePosition( mx, my );
}


void App::OnPointerPressed  ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args )
{
	int id = 0;

	int whl_delta = args->CurrentPoint->Properties->MouseWheelDelta;

	if(whl_delta)
	{
		gxPadManager::GetInstance()->SetMouseWheel(whl_delta);
	}

	if (args->CurrentPoint->Properties->IsLeftButtonPressed)
	{
		gxPadManager::GetInstance()->SetMouseButtonDown( 0 );
	}
	else if (args->CurrentPoint->Properties->IsRightButtonPressed)
	{
		gxPadManager::GetInstance()->SetMouseButtonDown(1);
	}
	else if (args->CurrentPoint->Properties->IsMiddleButtonPressed)
	{
		gxPadManager::GetInstance()->SetMouseButtonDown(2);
	}

}


void App::OnPointerReleased ( Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::PointerEventArgs^ args )
{
	int id = 0;// (int)args->VirtualKey;

	gxPadManager::GetInstance()->SetMouseButtonUp(0);
	gxPadManager::GetInstance()->SetMouseButtonUp(1);
	gxPadManager::GetInstance()->SetMouseButtonUp(2);
}


