﻿//------------------------------------------------------------
//
// machine.cpp
// マシン固有のデバイスへのアクセスを行うものはここに書く
// ハードウェアに依存する部分
//
//------------------------------------------------------------
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxDebug.h>
#include <gxLib/gxFileManager.h>

#include "CDx11Store.h"
#ifdef _USE_OPENAL
#include "../../COpenAL.h"
#else
#include "../CXAudio2.h"
#endif
#include "CGamePad.h"
#include "CHttpClient.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>

typedef struct gxTimeval {
  long tv_sec;
  long tv_usec;
} gxTimeval;

inline Concurrency::task<bool> IsFileExistAsync(const std::wstring& filename, Sint32 _location);
inline Concurrency::task<std::vector<byte>> ReadDataAsync(const std::wstring& filename, Sint32 _location);
Float32 getFrameTime();
//inline void WriteDataAsync(const std::wstring& filename, Uint8 *pData, Uint32 uSize);
inline Concurrency::task<void> WriteDataAsync(const std::wstring& filename, Uint8 *pData, Uint32 uSize);
SINGLETON_DECLARE_INSTANCE( CDeviceManager );

using namespace Windows::Security::Cryptography;

CDeviceManager::CDeviceManager()
{

}

CDeviceManager::~CDeviceManager()
{

}

void CDeviceManager::AppInit()
{


}


void CDeviceManager::GameInit()
{
	//バックで受け取った入力機器の情報をgxLib側へ更新する

	CGamePad::GetInstance()->Action();

}


void CDeviceManager::Render()
{
	//描画処理
	CDirectX11::GetInstance()->Update();
	CDirectX11::GetInstance()->Render();
}


void CDeviceManager::vSync()
{
	//1/60秒の同期待ち

	static LONGLONG before;
	static LONGLONG VsyncFreq = 0;

	if( VsyncFreq == 0  )
	{
		// 高精度タイマーの初期化
		QueryPerformanceFrequency((LARGE_INTEGER*)&VsyncFreq );
		VsyncFreq = VsyncFreq / FRAME_PER_SECOND;
		QueryPerformanceCounter( (LARGE_INTEGER*)&before );
	}

	LONGLONG time;

	do
	{
		// ループ時間待機（最低一回は通る）
		QueryPerformanceCounter( (LARGE_INTEGER*)&time );				//現在の時間を取得
	}
	while( time < ( before + VsyncFreq ) );

	before = time;
}


#if 0
gxBool CDeviceManager::LoadConfig()
{
	//コンフィグファイルをロードする

	Uint8 *pData = NULL;
	Uint32 uSize = 0;

	pData = LoadFile(FILENAME_CONFIG, &uSize, STORAGE_LOCATION_EXTERNAL );

	if( pData )
	{
		if (pData[0] == 'G' && pData[1] == 'X')
		{
			gxUtil::MemCpy(&gxLib::SaveData, pData, sizeof(gxLib::StSaveData));
			SAFE_DELETES(pData);
			return gxTrue;
		}
	}

	return gxFalse;
}


gxBool CDeviceManager::SaveConfig()
{
	gxLib::SaveData.HEAD[0] = 'G';
	gxLib::SaveData.HEAD[1] = 'X';

	SaveFile(FILENAME_CONFIG, (Uint8*)&gxLib::SaveData , sizeof(gxLib::StSaveData), STORAGE_LOCATION_EXTERNAL );

	return gxTrue;
}
#endif

void CDeviceManager::Movie()
{
//	
}

void CDeviceManager::Resume()
{

}


void CDeviceManager::UploadTexture(Sint32 sBank)
{
	CDirectX11::GetInstance()->ReadTexture( sBank );
}


void CDeviceManager::Flip()
{
	//バックバッファに用意した画像を転送する

	CDirectX11::GetInstance()->Present();
}


void CDeviceManager::Play()
{
	CAudio::GetInstance()->Action();
}


gxBool CDeviceManager::NetWork()
{
	CHttpClient::GetInstance()->Action();

	return gxTrue;
}


void CDeviceManager::MakeThread( void* (*pFunc)(void*) , void * pArg )
{
	// スレッドの作成
	//void func( void *arg )型の関数ポインタを渡すこと！ 

	DWORD threadId; 
	HANDLE hThread;

	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)pFunc, (LPVOID)pArg, CREATE_SUSPENDED, &threadId); 

	SetThreadPriority( hThread , THREAD_PRIORITY_HIGHEST );

	// スレッドの起動 
	ResumeThread(hThread); 
}


void CDeviceManager::Sleep( Uint32 msec )
{
//	::Sleep(msec);



	Uint32 cnt = 0;
	gxClock time;
	Float32 fTime1 = gxLib::GetTime();

	for ( ;; )
	{
		if (CoreWindow::GetForCurrentThread())
		{
			if (CoreWindow::GetForCurrentThread()->Dispatcher)
			{
				CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
			}

		}
		::Sleep(10);
		//CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);

		Float32 fTime2 = gxLib::GetTime();

		if ( (fTime2 - fTime1)*1000.0f >= msec ) break;
	}

}

gxBool CDeviceManager::PadConfig( Sint32 padNo , Uint32 button )
{
	return gxTrue;
}


void  CDeviceManager::Clock( gxClock *pTime )
{
	//現在の時刻をミリ秒で取得する
#if 1
	struct gxTimeval gxTime; 

	LARGE_INTEGER time, freq;

	QueryPerformanceFrequency( &freq );
	QueryPerformanceCounter  ( &time );

	//1秒以上の現在時刻(sec) = 現在時刻(HPET) - 秒間周波数(HPET)
	gxTime.tv_sec = (long)(time.QuadPart / freq.QuadPart);

	//1秒未満の現在時刻(mSec) = 現在時刻(mSec) - ((マイクロ)現在秒)
	gxTime.tv_usec = (long)( ( time.QuadPart * 1000000.0 / freq.QuadPart ) - (gxTime.tv_sec * 1000000.0) );

	SYSTEMTIME st;
	GetSystemTime(&st);


	Sint32 Year = st.wYear;// +1900;
	Sint32 Month = st.wMonth;
	Sint32 Day   = st.wDay;
	//Sint32 time_st->tm_wday;

	Sint32 Hour = st.wHour+9;
	Sint32 Min  = st.wMinute;
	Sint32 Sec  = st.wSecond;
	Sint32 MSec = st.wMilliseconds;
	Sint32 USec = gxTime.tv_sec;	//実際の値とは異なる

	//CGameGirl::GetInstance()->SetTime( Year , Month , Day , Hour , Min , Sec , MSec , USec);
#endif

	//return getFrameTime();
}


void CDeviceManager::LogDisp(char* pString)
{
	//デバッグログの表示

	wchar_t* pWCharStr = UTF8toUTF16(pString);

	OutputDebugStringW(pWCharStr);
	OutputDebugStringW(L"\n");
}



Uint8* loadFile( gxChar* fileName , size_t * pLength , Uint32 location )
{
	std::wstring strfileName = CDeviceManager::GetInstance()->UTF8toUTF16(fileName);

	//---------------------------

	int fh;
	long sz, readsz;
	unsigned long pos = 0;
	int ret = 1;
	struct stat filestat;

	Uint8* pBuffer = NULL;
	Uint32 uSize = 0;
	gxBool bReadEnd = gxFalse;

	Sint32 cnt = 0;

	gxBool bFileExist = gxFalse;

	for(;;)
	{
		//ファイルあるか？

		auto fileCheckTask = IsFileExistAsync( strfileName , location );

		fileCheckTask.then([&bFileExist](bool bExist){
			bFileExist = bExist;
		});

		do
		{
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
			Sleep(1);

			if (fileCheckTask.is_done())
			{
				//ファイル存在テスト終了
				break;
			}

		} while (gxTrue);

		if (!bFileExist)
		{
			cnt++;
			GX_DEBUGLOG("読み込み失敗(%d)", cnt);
			if (cnt >= 10 )
			{
				//１０回やってダメならホントになさそう
				*pLength = 0;
				break;
			}
		}
		else
		{
			break;
		}
	}

	//-------------------------------------------
	//ファイルの生存確認完了
	//-------------------------------------------

	if (!bFileExist) return NULL;
		
	auto loadTask = ReadDataAsync(strfileName, location );

	auto readTask = loadTask.then([&pBuffer, &uSize, &bReadEnd](const std::vector<byte>& fileData){

		//読み込めたのでデータをコピー
		size_t sz;
		sz = fileData.size();

		uSize = 0;
		pBuffer = NULL;

		if (sz > 0)
		{
			pBuffer = new Uint8[sz];
			const Uint8* p = &fileData[0];
			gxUtil::MemCpy(pBuffer, (void*)p, sz);
			uSize = sz;
		}

		bReadEnd = gxTrue;
	});

	//読み込み完了まで待つ

	while (gxTrue)
	{
		CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
		Sleep(1);

		if (readTask.is_done())
		{
			//読み込み完了
			break;
		}
	}

	if (pBuffer == NULL)
	{
		//読み込めなかった
		uSize = 0;
	}
	else
	{
		while (!bReadEnd)
		{
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
			Sleep(1);
		}
	}

	*pLength = uSize;

	return pBuffer;
}

#include <gxLib/util/CFileZip.h>

Uint8    *g_pZipBuffer = NULL;
CFileZip *g_pAssetZip  = NULL;
gxChar m_ZipPath[FILENAMEBUF_LENGTH]={0};	//TODO::あとで開放すること

Uint8* getZipFileData( gxChar* pZipName , gxChar* pFileName , size_t *pLength )
{
	Uint32 len = strlen( pFileName );

	gxChar *fileName0 = new gxChar[32 + len];

	len = sprintf_s(fileName0, len + 32, "%s", pFileName);

	//zip時は / でパスを区切る

	for (int ii = 0; ii<len; ii++)
	{
		if (fileName0[ii] == '\\')
		{
			fileName0[ii] = '/';
		}
	}

	if( pLength )
	{
		*pLength = 0;
	}


	//------------------------------------
	int fh;

	Uint8* pBuffer = NULL;
	struct stat filestat;
	long sz, readsz;
	unsigned long pos = 0;
	int ret = 1;

	//-----------------------------------------

	Uint8 *pReturnData = NULL;
	gxBool bMakeNewZip = gxFalse;

	if( g_pAssetZip )
	{
		if( strcmp( m_ZipPath , pZipName ) != 0 )
		{
			bMakeNewZip = gxTrue;
			SAFE_DELETE( g_pAssetZip );
			SAFE_DELETES( g_pZipBuffer );

		}
	}
	else
	{
		bMakeNewZip = gxTrue;
	}

	if( bMakeNewZip )
	{
		size_t uSize = 0;
		Uint8* pBuffer = loadFile( pZipName , &uSize , STORAGE_LOCATION_ROM );

		g_pZipBuffer = pBuffer;

		g_pAssetZip = new CFileZip();

		if ( !g_pAssetZip->Read( pBuffer , uSize) )
		{
			//そもそもzipがなかった
			SAFE_DELETES( pBuffer );
			SAFE_DELETES( fileName0 );
			return NULL;
		}

		//最後に読んだZIP名を覚えておく
		sprintf( m_ZipPath ,"%s" , pZipName );
	}

	//Zipは読めている

	size_t uSize = 0;

	pReturnData = g_pAssetZip->Decode( fileName0, &uSize );

	*pLength = uSize;

	//SAFE_DELETE( pZip );		//使い回しがあるのでここでは消さない
	//SAFE_DELETES( pBuffer );	//使い回しがあるのでここでは消さない

	SAFE_DELETES( fileName0 );


	return pReturnData;
}


Uint8* CDeviceManager::LoadFile(const gxChar* pFileName, size_t* pLength, Uint32 location)
{
	//ファイルを読み込む

	Uint32 len = strlen(pFileName);

	gxChar fileName[1024];

	gxChar* fileName0 = new gxChar[32 + len];

	len = sprintf_s(fileName0, len + 32, "%s", pFileName);

	gxBool bDirect = gxFalse;

	for (int ii = 0; ii < len; ii++)
	{
		if (fileName0[ii] == '/')
		{
			fileName0[ii] = '\\';
		}

		if (fileName0[ii] == ':')
		{
			bDirect = gxTrue;
		}
	}


#ifdef FILE_FROM_ZIP

	if( !bDirect )
	{
		Uint8 *pRetData = NULL;

		switch( location ){
		case STORAGE_LOCATION_ROM:
			sprintf( fileName, "Assets\\rom.zip");
			pRetData = getZipFileData( fileName , fileName0 , pLength );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case STORAGE_LOCATION_INTERNAL:
			sprintf( fileName, "Assets\\disc.zip");
			pRetData = getZipFileData( fileName , fileName0 , pLength );
			if( pRetData )
			{
				return pRetData;
			}

			//ファイルが見つからなかったので実データを探しに行く

			break;

		case STORAGE_LOCATION_EXTERNAL:
		//case STORAGE_LOCATION_SD:
			//こいつらはファイル読み込みのみ対応
		default:
			break;
		}
	}


#endif
	if (bDirect)
	{
		sprintf(fileName, "%s", fileName0);
	}
	else
	{
		switch (location) {
		case STORAGE_LOCATION_ROM:
			sprintf(fileName, "Assets\\rom\\%s", fileName0);
			break;
		case STORAGE_LOCATION_EXTERNAL:
			sprintf(fileName, "Assets\\sd\\%s", fileName0);
			break;
		case STORAGE_LOCATION_INTERNAL:
			sprintf(fileName, "Assets\\disc\\%s", fileName0);
			break;
		//case STORAGE_LOCATION_SD:		//				//読み書き、取り出しOK
		//	sprintf(fileName, "%s", fileName0);
		//	break;
		default:
			break;
		}
	}

	Uint8 *pData = loadFile( fileName , pLength , location );

	return pData;
}



/*
Uint8* CDeviceManager::LoadStorageFile( const gxChar* pFileName , Uint32* pLength )
{
	//STORAGE_LOCATION_MEMCARD:

	Uint32 len = strlen(pFileName);

	gxChar *fileName = new gxChar[ len+32 ];
//	sprintf_s(fileName, len, "Assets\\Storage\\sd\\%s", pFileName );
	len = sprintf_s(fileName, len+32, "%s", pFileName);

	for( int ii=0; ii<len; ii++ )
	{
		if( fileName[ii] == '/' )
		{
			fileName[ii] = '\\';
		}
	}

	const std::wstring strfileName = UTF8toUTF16( fileName );

	SAFE_DELETE( fileName );

	//---------------------------

	int fh;
	long sz,readsz;
	unsigned long pos=0;
	int ret=1;
	struct stat filestat;

	Uint8* pBuffer = NULL;
	Uint32 uSize = 0;
	gxBool bReadEnd = gxFalse;

	Sint32 cnt = 0;

	for(;;)
	{
		auto fileCheckTask = IsFileExistAsync(strfileName, STORAGE_LOCATION_MEMCARD);

		gxBool bFileExist = gxFalse;
		fileCheckTask.then([&bFileExist](bool bExist)
		{
			bFileExist = bExist;
		});

		while (gxTrue)
		{
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
			Sleep(1);
			if (fileCheckTask.is_done())
			{
				break;
			}
		}

		if (!bFileExist)
		{
			cnt++;

			GX_DEBUGLOG("読み込み失敗(%d)",cnt);
			if (cnt >= 10 )
			{
				//１０回やってダメならホントになさそう
				*pLength = 0;
				return NULL;
			}
		}
		else
		{
			break;
		}
	}


	auto loadTask = ReadDataAsync(strfileName , STORAGE_LOCATION_MEMCARD);

	auto readTask = loadTask.then( [&pBuffer,&uSize,&bReadEnd]( const std::vector<byte>& fileData)
	{
		size_t sz;

		sz = fileData.size();

		uSize = 0;
		pBuffer = NULL;

		if( sz > 0 )
		{
			pBuffer = new Uint8[sz];
			const Uint8* p = &fileData[0];
			gxUtil::MemCpy(pBuffer, p, sz);
			uSize = sz;
		}

		bReadEnd = gxTrue;
	});

	//読み込み完了まで待つ

	while ( gxTrue )
	{
		CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents( CoreProcessEventsOption::ProcessAllIfPresent );
		Sleep(1);

		if( readTask.is_done() )
		{
			break;
		}
	}

	if (pBuffer == NULL)
	{
		uSize = 0;
	}
	else
	{
		while( !bReadEnd )
		{
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents( CoreProcessEventsOption::ProcessAllIfPresent );
			Sleep(1);
		}
	}

	*pLength = uSize;

	return pBuffer;
}
*/


gxBool CDeviceManager::SaveFile( const gxChar* pFileName , Uint8* pWriteBuf , size_t uSize , Uint32 location )
{

	Uint8* pBuffer = NULL;
	gxBool bReadEnd = gxFalse;

	gxChar fileName[1024];
	gxChar fileName0[1024];

	size_t len = sprintf_s(fileName0, 1024, "%s", pFileName );

	gxBool bDirect = gxFalse;

	for (int ii = 0; ii < len; ii++)
	{
		if (fileName0[ii] == '/')
		{
			fileName0[ii] = '\\';
		}

		if (fileName0[ii] == ':')
		{
			bDirect = gxTrue;
		}
	}

	if (bDirect)
	{
		sprintf(fileName, "%s", fileName0);
	}
	else
	{
		switch (location) {
		case STORAGE_LOCATION_ROM:
		case STORAGE_LOCATION_EXTERNAL:
			return gxFalse;
		case STORAGE_LOCATION_INTERNAL:
			sprintf(fileName, "Assets\\sd\\%s", fileName0);
			break;
		//case STORAGE_LOCATION_SD:		//				//読み書き、取り出しOK
		//	sprintf(fileName, "%s", fileName0);
		//	break;
		default:
			break;
		}
	}

	const std::wstring strfileName = UTF8toUTF16( fileName );

	//---------------------------

	WriteDataAsync( strfileName , pWriteBuf , uSize );

	return gxTrue;
}


//ストレージへのファイルアクセス
/*
gxBool CDeviceManager::SaveStorageFile( const gxChar* pFileName , Uint8* pReadBuf , Uint32 uSize )
{
	//STORAGE_LOCATION_RESOURCE:

	Uint8* pBuffer = NULL;
	gxBool bReadEnd = gxFalse;

	Uint32 len = strlen(pFileName) + 32;

	gxChar *fileName = new gxChar[len];
	sprintf_s(fileName, len, "%s", pFileName);

	for (int ii = 0; ii<len; ii++)
	{
		if (fileName[ii] == '/')
		{
			fileName[ii] = '\\';
		}
	}

	const std::wstring strfileName = UTF8toUTF16(fileName);

	SAFE_DELETE(fileName);

	//---------------------------

	auto saveTask = WriteDataAsync(strfileName, pReadBuf , uSize );

	while (gxTrue)
	{
		if (saveTask.is_done())
		{
			break;
		}
		if (CoreWindow::GetForCurrentThread())
		{
			//Suspend時はThreadが生きていないのでここにはこれない
			//Runイベントを抜けるときはスレッドが生きているので問題ない
			CoreWindow::GetForCurrentThread()->Dispatcher->ProcessEvents(CoreProcessEventsOption::ProcessAllIfPresent);
		}
		Sleep(1);
	}

	return gxTrue;
}
*/


gxBool CDeviceManager::GamePause()
{
	return ::GamePause();
}

gxBool CDeviceManager::GameUpdate()
{
	return gxTrue;
}


void CDeviceManager::UpdateMemoryStatus(size_t* uNow, size_t* uTotal, size_t* uMax)
{
	size_t uNowByte;
	size_t uTotalByte;
	size_t uMaxByte;

	::UpdateMemoryStatus(&uNowByte, &uTotalByte, &uMaxByte);

	*uNow = (uNowByte >> 10) >> 10;
	*uTotal = (uTotalByte >> 10) >> 10;
	*uMax = (uMaxByte >> 10) >> 10;
}


Float32 getFrameTime()
{
	static Uint64 oldCount = 0;
	static Uint64 freq = 0;
	
	Uint64 current = 0;

	LARGE_INTEGER qt;

	if (oldCount == 0)
	{
		::QueryPerformanceFrequency(&qt);
		freq = qt.QuadPart;

		::QueryPerformanceCounter(&qt);
		oldCount = qt.QuadPart;
	}

	::QueryPerformanceCounter(&qt);
	current = qt.QuadPart;

	Float32 fElapsedTime = (float)(current - oldCount) / freq;

	//oldCount = current;

	return fElapsedTime;
}



/*
inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		// Win32 API エラーをキャッチするためのブレークポイントをこの行に設定します。
		throw Platform::Exception::CreateException(hr);
	}
}
*/

inline Concurrency::task<bool> IsFileExistAsync(const std::wstring& filename, Sint32 _location)
{
	//ファイルが存在するかチェック

	using namespace Windows::Storage;
	using namespace Concurrency;

	Windows::Storage::StorageFolder ^folder;

	if (_location == STORAGE_LOCATION_ROM)
	{
		folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	}
	else if (_location == STORAGE_LOCATION_INTERNAL)
	{
		folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	}
	else if (_location == STORAGE_LOCATION_EXTERNAL)
	{
		folder = ApplicationData::Current->LocalFolder;
	}
	//else if (_location == STORAGE_LOCATION_SD)
	//{
	//	//毒；ToDo:外部ファイルの取扱をどうするか？(UWPの苦手なところだけどとりあえずローカルフォルダから探す）
	//	folder = ApplicationData::Current->LocalFolder;
	//}

	return create_task( folder->TryGetItemAsync( Platform::StringReference(filename.c_str() ))).then([](task<IStorageItem^> taskResult)	{
		auto result = taskResult.get();
		return (result == nullptr) ? false : true;
	});
	//return create_task(folder->GetFileAsync( Platform::StringReference(filename.c_str()))).then([](task<IStorageItem^> taskResult)

}

inline Concurrency::task<std::vector<byte>> ReadDataAsync(const std::wstring& filename , Sint32 _location )
{
	// バイナリ ファイルから非同期に読み取る関数。

	using namespace Windows::Storage;
	using namespace Concurrency;

	Windows::Storage::StorageFolder ^folder;
	
	if (_location == STORAGE_LOCATION_ROM)
	{
		folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	}
	else if (_location == STORAGE_LOCATION_INTERNAL)
	{
		folder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	}
	else if (_location == STORAGE_LOCATION_EXTERNAL)
	{
		folder = ApplicationData::Current->LocalFolder;
	}
	//else if (_location == STORAGE_LOCATION_SD)
	//{
	//	folder = ApplicationData::Current->LocalFolder;
	//}


	return create_task( folder->GetFileAsync( Platform::StringReference(filename.c_str())) ).then
		([] (StorageFile^ file)
			{
				return FileIO::ReadBufferAsync( file );
			}
		).then(	[] (Streams::IBuffer^ fileBuffer) -> std::vector<byte> 
			{
				std::vector<byte> returnBuffer;

				returnBuffer.resize(fileBuffer->Length);

				Streams::DataReader::FromBuffer(fileBuffer)->ReadBytes( Platform::ArrayReference<byte>(returnBuffer.data(), fileBuffer->Length) );

				return returnBuffer;
			}
		);
}


//inline Concurrency::task<std::vector<byte>> WriteDataAsync(const std::wstring& filename)
//inline void WriteDataAsync(const std::wstring& filename , Uint8 *pData , Uint32 uSize )
inline Concurrency::task<void> WriteDataAsync(const std::wstring& filename, Uint8 *pData, Uint32 uSize)
{
	// バイナリ ファイルから非同期に読み取る関数。

	using namespace Windows::Storage;
	using namespace Concurrency;

	//フォルダをローカルストレージにする
	//auto storageFolder = Windows::ApplicationModel::Package::Current->InstalledLocation;
	auto storageFolder = ApplicationData::Current->LocalFolder;

	return create_task(
		storageFolder->CreateFileAsync( Platform::StringReference(filename.c_str() ), CreationCollisionOption::ReplaceExisting ))
		.then([pData, uSize](StorageFile^ sampleFile)
		{
			// Create the buffer
			auto byteArray = ArrayReference<byte>(reinterpret_cast<byte*>(pData), uSize);
			IBuffer^ buffer = CryptographicBuffer::CreateFromByteArray(byteArray);// ::ConvertStringToBinary("What fools these mortals be", BinaryStringEncoding::Utf8);
			return FileIO::WriteBufferAsync(sampleFile, buffer);
	});


	//ファイルを書き込む
/*
	create_task( storageFolder->GetFileAsync(Platform::StringReference(filename.c_str())) ).then([pData,uSize](StorageFile^ sampleFile)
	{
		// Process file

		// Create the buffer
		auto byteArray = ArrayReference<byte>(reinterpret_cast<byte*>(pData), uSize );
		IBuffer^ buffer = CryptographicBuffer::CreateFromByteArray(byteArray);// ::ConvertStringToBinary("What fools these mortals be", BinaryStringEncoding::Utf8);
		create_task(FileIO::WriteBufferAsync(sampleFile, buffer));
	});
	return create_task(storageFolder->GetFileAsync(Platform::StringReference(filename.c_str()))).then
	([pData, uSize](StorageFile^ sampleFile)
	{
		// Create the buffer
		auto byteArray = ArrayReference<byte>(reinterpret_cast<byte*>(pData), uSize);
		IBuffer^ buffer = CryptographicBuffer::CreateFromByteArray(byteArray);// ::ConvertStringToBinary("What fools these mortals be", BinaryStringEncoding::Utf8);
		return FileIO::WriteBufferAsync(sampleFile, buffer);
	});
	*/


}


/*
wchar_t* CharToWChar( char const* pString)
{
	static wchar_t *pWChar = NULL;

	if( pWChar )
	{
		SAFE_DELETE( pWChar );
	}

	const std::string _string = pString;

	size_t stringSize  = _string.size();
	size_t wcharSize   = 0;

	pWChar = new wchar_t[ stringSize + 2];

	mbstowcs_s( &wcharSize, pWChar, stringSize + 1, _string.c_str(), _TRUNCATE );

	return pWChar;

}
*/



static wchar_t WCharBuf[1024];
static wchar_t *pWChar = WCharBuf;// NULL;

wchar_t *CDeviceManager::SJIStoUTF16( gxChar* pString , size_t *pSize )
{
	//char から wcharに変換する

	size_t u8Len = strlen( pString );
	size_t maxSize = u8Len*4;

	wchar_t *pW1 = new wchar_t[maxSize];

	//UTF8 -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8 *pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy( pW2 , pW1 , sz );
	pW2[sz+0] = 0x00;
	pW2[sz+1] = 0x00;

	SAFE_DELETES(pW1);

	if( pSize ) *pSize = sz;

	return (wchar_t*)pW2;
}

wchar_t* CDeviceManager::UTF8toUTF16(gxChar* pString, size_t* pSize)
{
	//char から wcharに変換する

	size_t u8Len = strlen(pString);
	size_t maxSize = u8Len * 4;

	wchar_t* pW1 = new wchar_t[maxSize];

	//UTF8 -> WIDECHAR(UTF16)
	size_t sz = MultiByteToWideChar( CP_UTF8, 0, (char*)pString, u8Len, pW1, maxSize);

	//SJIS -> WIDECHAR(UTF16)
	//size_t sz = MultiByteToWideChar(CP_ACP, 0, (char*)pString, u8Len, pW1, maxSize);

	//ここのSZはWIDECHARの文字数なのでバイト数に変換する
	sz *= 2;

	Uint8* pW2 = new Uint8[sz + 2];

	gxUtil::MemCpy(pW2, pW1, sz);
	pW2[sz + 0] = 0x00;
	pW2[sz + 1] = 0x00;

	SAFE_DELETES(pW1);

	if (pSize)* pSize = sz;

	return (wchar_t*)pW2;
}

gxChar* CDeviceManager::UTF16toUTF8(wchar_t *pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;

	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	gxChar* pBuf = new gxChar[u16len*4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.UTF-8");
	//size_t length = wcstombs( pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte( CP_UTF8, 0, pUTF16buf, -1, &pBuf[0], u16len * 4, NULL, NULL);

	if (length > u16len * 4)
	{
		*pSize = 0;
		pBuf[0] = 0x00;
		return pBuf;
	}

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES( pBuf );

	if( pSize ) *pSize = length;

	return (gxChar*)pBuf2;
}


gxChar* CDeviceManager::UTF16toSJIS(wchar_t* pUTF16buf, size_t* pSize)
{
	Uint8* u8 = (Uint8*)pUTF16buf;
	if (u8[0] == 0xff && u8[1] == 0xfe)
	{
		//BOM付きだったのでオフセットする
		pUTF16buf++;
	}

	size_t u16len = wcslen(pUTF16buf);

	gxChar* pBuf = new gxChar[u16len * 4];

	//size_t sz;
	//wchar_t* pw = CDeviceManager::MultiByteToWChar(m_pBuf, &sz);
	//gxUtil::MemCpy( pBuf, m_pBuf , m_uStringSize );

	//setlocale(LC_ALL, "ja_JP.Shift_JIS");
	//size_t length = wcstombs(pBuf, pUTF16buf, u16len * 4);
	size_t length = WideCharToMultiByte(CP_ACP, 0, pUTF16buf, -1, pBuf, u16len*4, NULL, NULL);

	gxChar* pBuf2 = new gxChar[length + 1];
	gxUtil::MemCpy(pBuf2, pBuf, length);
	pBuf2[length] = 0x00;

	SAFE_DELETES(pBuf);

	if (pSize)* pSize = length;

	return (gxChar*)pBuf2;
}

gxChar* CDeviceManager::UTF8toSJIS( gxChar* pUTF8buf, size_t* pSize)
{
	wchar_t *p16  = UTF8toUTF16(pUTF8buf, pSize);
	gxChar* pSJIS = UTF16toSJIS(p16, pSize);

	return pSJIS;
}

gxChar* CDeviceManager::SJIStoUTF8( gxChar* pSJISbuf, size_t* pSize)
{
	wchar_t* p16  = SJIStoUTF16(pSJISbuf, pSize);
	gxChar* pUTF8 = UTF16toUTF8(p16, pSize);

	return pUTF8;

}


//#include <shellapi.h>
//#include "gxLibResource.h"

void CDeviceManager::ToastDisp( gxChar* pMessage )
{
/*
	NOTIFYICONDATA	tn = { NOTIFYICONDATA_V2_SIZE };
	tn.hWnd = CWindows::GetInstance()->m_hWindow;
	tn.uID = 100;
	tn.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	//tn.uCallbackMessage = WM_FASTCOPY_NOTIFY;
	tn.hIcon = LoadIcon(CWindows::GetInstance()->m_hInstance, (LPCSTR)(LONG_PTR)IDI_GXLIB_ICON);	//hIcon;
	sprintf(tn.szTip, "testtest");
	tn.uFlags |= NIF_INFO;

	strncpy(tn.szInfoTitle, APPLICATION_NAME, sizeof(tn.szInfoTitle));
	strncpy(tn.szInfo, pMessage, sizeof(tn.szInfo));

	tn.uTimeout		= 3 * 1000;
	tn.dwInfoFlags	= NIIF_INFO | NIIF_NOSOUND;
	tn.dwState = NIS_SHAREDICON;

	
	HRESULT ret;
	ret = ::Shell_NotifyIcon(NIM_DELETE, &tn);

	ret = ::Shell_NotifyIcon(NIM_ADD, &tn);
*/

}


gxBool CDeviceManager::GetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

gxBool CDeviceManager::SetAchievement( Uint32 achieveindex )
{
	return gxTrue;
}

