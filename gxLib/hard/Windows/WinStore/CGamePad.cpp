﻿
#include <gxLib.h>
#include <gxLib/gx.h>
#include <gxLib/gxPadManager.h>
#include "CGamePad.h"
#include "CStoreInput.h"

#ifdef GX_DEBUG
//#define _DEBUG_DEVICE_STATUS_DRAW
#endif

//*************************************************************************************************************
//デバイス状態を管理
//*************************************************************************************************************
SINGLETON_DECLARE_INSTANCE( CGamePad );

#define AXIS_RANGE (65535>>1)

CGameingDevice::CGameingDevice()
{
	_num_pov = 0;
	_num_slider = 0;

	memset( &_Axis , 0x00 , sizeof(PadStat_t) );
	memset( &_Rotation , 0x00 , sizeof(PadStat_t) );
	memset( &_caribrate , 0x00 , sizeof(PadStat_t) );
	sprintf(_hardwarename, "Unconnected");

	_Pov0 = 0;
	_Pov1 = 0;
	_Pov2 = 0;
	_Pov3 = 0;
	_Slider0 = 0;
	_Slider1 = 0;

	//サポート状況を初期化
	_bPov[0]    = gxFalse;			//ハットスイッチ0
	_bPov[1]    = gxFalse;			//ハットスイッチ1
	_bPov[2]    = gxFalse;			//ハットスイッチ2
	_bPov[3]    = gxFalse;			//ハットスイッチ3
	_num_slider = 0;				//スライダーコントロール数
	_bAxisX =
	_bAxisY =
	_bAxisZ = gxFalse;				//サポートされているAXIS
	_bAxisRX =
	_bAxisRY =
	_bAxisRZ = gxFalse;				//サポートされているAXIS
	_bRumble = gxFalse;				//振動可能か？


	//使用するかどうか(基本的には全部使用することにする)
	_bUseAxisX
	= _bUseAxisY
	= _bUseAxisZ
	= _bUseRotationX
	= _bUseRotationY
	= _bUseRotationZ
	= _bUsePOV0
	= _bUsePOV1
	= _bUsePOV2
	= _bUsePOV3
	= _bUseSlider0
	= _bUseSlider1
	= _bUseForceFeedBack = true;

	for(int i=0;i<128;i++) {
		_button[i] = gxFalse;
	}

	_bSlider[0] = _bSlider[1] = gxFalse;
	_ForceFeefBackAxis = 0;

}


CGameingDevice::~CGameingDevice()
{
	
	
}

gxBool CGameingDevice::IsPress(GAMINGDEVICE_BTNID id)
{
	//------------------------------------------------------
	//ボタンが押されているかどうかデジタル入力に変換して返す
	//------------------------------------------------------

	switch(id) {
	//BUTTON
	case GDBTN_BUTTON00:
	case GDBTN_BUTTON01:
	case GDBTN_BUTTON02:
	case GDBTN_BUTTON03:
	case GDBTN_BUTTON04:
	case GDBTN_BUTTON05:
	case GDBTN_BUTTON06:
	case GDBTN_BUTTON07:
	case GDBTN_BUTTON08:
	case GDBTN_BUTTON09:
	case GDBTN_BUTTON10:
	case GDBTN_BUTTON11:
	case GDBTN_BUTTON12:
	case GDBTN_BUTTON13:
	case GDBTN_BUTTON14:
	case GDBTN_BUTTON15:
		return IsPressButton(_button[id-GDBTN_BUTTON00]);

	//AXIS
	case GDBTN_AXIS_XA:
	case GDBTN_AXIS_XS:
		if(!_bUseAxisX) return gxFalse;
		return IsPressAxis(_Axis.x,id-GDBTN_AXIS_XA);
	case GDBTN_AXIS_YA:
	case GDBTN_AXIS_YS:
		if(!_bUseAxisY) return gxFalse;
		return IsPressAxis(_Axis.y,id-GDBTN_AXIS_YA);
	case GDBTN_AXIS_ZA:
	case GDBTN_AXIS_ZS:
		if(!_bUseAxisZ) return gxFalse;
		return IsPressAxis(_Axis.z,id-GDBTN_AXIS_ZA);

	//ROTATION
	case GDBTN_ROT_XA:
	case GDBTN_ROT_XS:
		if(!_bUseRotationX) return gxFalse;
		return IsPressAxis(_Rotation.x,id-GDBTN_ROT_XA);
	case GDBTN_ROT_YA:
	case GDBTN_ROT_YS:
		if(!_bUseRotationY) return gxFalse;
		return IsPressAxis(_Rotation.y,id-GDBTN_ROT_YA);
	case GDBTN_ROT_ZA:
	case GDBTN_ROT_ZS:
		if(!_bUseRotationZ) return gxFalse;
		return IsPressAxis(_Rotation.z,id-GDBTN_ROT_ZA);

	//POV
	case GDBTN_POV0_U:
	case GDBTN_POV0_R:
	case GDBTN_POV0_D:
	case GDBTN_POV0_L:
		if(!_bUsePOV0) return gxFalse;
		return IsPressPov(_Pov0,id-GDBTN_POV0_U);

	case GDBTN_POV1_U:
	case GDBTN_POV1_R:
	case GDBTN_POV1_D:
	case GDBTN_POV1_L:
		if(!_bUsePOV1) return gxFalse;
		return IsPressPov(_Pov1,id-GDBTN_POV1_U);

	case GDBTN_POV2_U:
	case GDBTN_POV2_R:
	case GDBTN_POV2_D:
	case GDBTN_POV2_L:
		if(!_bUsePOV2) return gxFalse;
		return IsPressPov(_Pov2,id-GDBTN_POV2_U);

	case GDBTN_POV3_U:
	case GDBTN_POV3_R:
	case GDBTN_POV3_D:
	case GDBTN_POV3_L:
		if(!_bUsePOV3) return gxFalse;
		return IsPressPov(_Pov3,id-GDBTN_POV3_U);

	//SLIDER
	case GDBTN_SLIDER0_U:
	case GDBTN_SLIDER0_D:
		if(!_bUseSlider0) return gxFalse;
		return IsPressSlider(_Slider0,id-GDBTN_SLIDER0_U);

	case GDBTN_SLIDER1_U:
	case GDBTN_SLIDER1_D:
		if(!_bUseSlider1) return gxFalse;
		return IsPressSlider(_Slider1,id-GDBTN_SLIDER1_U);
	}

	return gxFalse;
}


gxBool CGameingDevice::IsPressButton( gxBool& btn)
{
	//ボタンが押されているかどうかチェック

	return (btn)? gxTrue : gxFalse;
}

gxBool CGameingDevice::IsPressAxis( Float32& axis , Sint32 dir)
{
	//アナログスティックが倒されているかどうかチェック
	int d = 0;

	if(axis < -AXIS_RANGE/2) d = -1;
	if(axis >  AXIS_RANGE/2) d =  1;

	if(dir == 0 && 	d == 1)  return gxTrue;
	if(dir == 1 && 	d == -1) return gxTrue;

	return gxFalse;
}

gxBool CGameingDevice::IsPressPov( Sint32 &pov , Sint32 urdl)
{
	//---------------------------------------------------------------------
	//POVが入力されているかどうか
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//押されているボタンとリクエスト方向が一致すれば押されていることにする
	//---------------------------------------------------------------------
	switch(urdl){
	case 0:	//上
		if(pov & JOY_U) return gxTrue;
		break;
	case 1:	//右
		if(pov & JOY_R) return gxTrue;
		break;
	case 2:	//下
		if(pov & JOY_D) return gxTrue;
		break;
	case 3:	//左
		if(pov & JOY_L) return gxTrue;
		break;
	}

	return gxFalse;
}

gxBool CGameingDevice::IsPressSlider( Float32 &sldr , Sint32 dir)
{
	//０～２５５の値が返ってくる

	if( sldr > 200 ) return gxTrue;

	return gxFalse;
}



CGamePad::CGamePad()
{
	//キーボード
	for( Sint32 ii=0; ii<256; ii++ )
	{
		m_KeyBoardPush[ii] = 0x00;
	}

	//マウス
	for( Sint32 ii=0; ii<8; ii++ )
	{
		m_MouseClick[ii] = 0x00;
	}
	m_MouseX = m_MouseY = 0;
	m_Wheel = 0;

	m_DeviceCnt = 0;
}

void CGamePad::Init()
{
//	CDirectInput::GetInstance()->Init();
}

void CGamePad::Action()
{
//	CXInput::GetInstance()->Action();
//	CDirectInput::GetInstance()->Action();
	CStoreInput::GetInstance()->Action();

	//設定したコントローラー情報をgxLibに通知する

	//キーボード

//	if( (m_KeyBoardPush[ gxKey::RSHIFT ] == 0x01) && (GetKeyState( VK_RSHIFT )&0x8000) == 0 )   m_KeyBoardPush[ gxKey::RSHIFT ] = 0x02;

//	if( (m_KeyBoardPush[ gxKey::RCTRL ] == 0x01) && (GetKeyState( VK_RCONTROL )&0x8000) == 0 ) m_KeyBoardPush[ gxKey::RCTRL ]  = 0x02;

//	if( (m_KeyBoardPush[ gxKey::RALT ] == 0x01) && (GetKeyState( VK_RMENU )&0x8000) == 0 )    m_KeyBoardPush[ gxKey::RALT ]   = 0x02;


	for( Sint32 ii=0; ii<256; ii++ )
	{
		if( m_KeyBoardPush[ii] == 0x01 )
		{
			gxPadManager::GetInstance()->SetKeyDown(ii);
		}
		if( m_KeyBoardPush[ii] == 0x02 )
		{
			gxPadManager::GetInstance()->SetKeyUp(ii);
		}

		//m_KeyBoardPush[ii] = 0x00;
	}

	//マウス
	//画面中の位置にアジャストする
	Float32 mx = m_MouseX;
	Float32 my = m_MouseY;
	Sint32 gamew,gameh;
	Sint32 scrw,scrh;
	Float32 ofx,ofy;

	CGameGirl::GetInstance()->GetGameResolution( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &scrw , &scrh );
	ofx = (scrw - gamew)/2.0f;
	ofy = (scrh - gameh) /2.0f;

	mx -= ofx;
	my -= ofy;

	mx = WINDOW_W * mx / gamew;
	my = WINDOW_H * my / gameh;

	gxPadManager::GetInstance()->SetMousePosition( 0x00, mx , my );

	for( Sint32 ii=0; ii<3; ii++ )
	{
		if( m_MouseClick[ii] == 0x01 )
		{
			gxPadManager::GetInstance()->SetMouseButtonDown(ii);
		}
		if( m_MouseClick[ii] == 0x02 )
		{
			gxPadManager::GetInstance()->SetMouseButtonUp(ii);
		}
		m_MouseClick[ii] = 0x00;
	}

	//コントローラー入力を行わない


	//全コントローラー情報を1Pに集める

	AdjustDefaultInput();

	//カスタム入力

	//debug
#ifdef _DEBUG_DEVICE_STATUS_DRAW
	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		Sint32 ax,ay;
		ax = ii%4;
		ay = ii/4;
		drawDevice( ii , ax , ay );
	}
#endif

/*
	CDirectInput::SetAnalogInfo
	void SetSensorInfo( Sint32 type , Float32 *pAnalogArry );
	void SetKeyDown(Uint32 uKey);
	void SetKeyUp(Uint32 uKey);
*/

/*
	void SetMouseButtonUp( Uint32 uLR );
	void SetMouseButtonDown( Uint32 uLR );
	void SetMousePosition( Sint32 x , Sint32 y );
	void SetMouseWheel(Sint32 n);
*/


}


//Sint32 CGamePad::ConvertKeyNumber( WPARAM id )
//{
//	//キーと配列をマッチングさせる
//
//	switch( id ){
//	case VK_ESCAPE:		return gxKey::ESC      ;
//	case VK_BACK:       return gxKey::BS   ;
//	case VK_TAB:        return gxKey::TAB         ;
//	case VK_RETURN:     return gxKey::RETURN      ;
////	case VK_RETURN:     return gxKey::KEYBOARD_ENTER       ;
//	case VK_SHIFT:
//	case VK_LSHIFT:	// return gxKey::SHIFT;
//	case VK_RSHIFT:	// return gxKey::RSHIFT;
//	{
//		short input = GetKeyState(VK_RSHIFT);
//		if (input & 0x8000)
//		{
//			return gxKey::RSHIFT;
//		}
//		return gxKey::SHIFT;
//	}
//
//	case VK_CONTROL:	// return gxKey::CTRL		;
//	case VK_LCONTROL:	// return gxKey::CTRL        ;
//	case VK_RCONTROL:
//		if (GetKeyState(VK_RCONTROL) & 0x8000)
//		{
//			return gxKey::RCTRL;
//		}
//		return gxKey::CTRL;
//
//	case VK_LMENU:
//	case VK_RMENU://      return gxKey::RALT;
//		if (GetKeyState(VK_RMENU) & 0x8000)
//		{
//			return gxKey::RALT;
//		}
//		return gxKey::ALT;
//	case VK_UP:         return gxKey::UP    ;
//	case VK_DOWN:       return gxKey::DOWN  ;
//	case VK_LEFT:       return gxKey::LEFT  ;
//	case VK_RIGHT:      return gxKey::RIGHT ;
//	case VK_SPACE:      return gxKey::SPACE       ;
//	case VK_F1:         return gxKey::F1          ;
//	case VK_F2:         return gxKey::F2          ;
//	case VK_F3:         return gxKey::F3          ;
//	case VK_F4:         return gxKey::F4          ;
//	case VK_F5:         return gxKey::F5          ;
//	case VK_F6:         return gxKey::F6          ;
//	case VK_F7:         return gxKey::F7          ;
//	case VK_F8:         return gxKey::F8          ;
//	case VK_F9:         return gxKey::F9          ;
//	case VK_F10:        return gxKey::F10         ;
//	case VK_F11:        return gxKey::F11         ;
//	case VK_F12:        return gxKey::F12         ;
//	case VK_NUMPAD0:	return gxKey::PAD0			;
//	case VK_NUMPAD1:	return gxKey::PAD1			;
//	case VK_NUMPAD2:	return gxKey::PAD2			;
//	case VK_NUMPAD3:	return gxKey::PAD3			;
//	case VK_NUMPAD4:	return gxKey::PAD4			;
//	case VK_NUMPAD5:	return gxKey::PAD5			;
//	case VK_NUMPAD6:	return gxKey::PAD6			;
//	case VK_NUMPAD7:	return gxKey::PAD7			;
//	case VK_NUMPAD8:	return gxKey::PAD8			;
//	case VK_NUMPAD9:	return gxKey::PAD9			;
//	case VK_PRIOR: 		return gxKey::PAGEUP		;
//	case VK_NEXT:		return gxKey::PAGEDOWN	;
//	case VK_DELETE:		return gxKey::DEL		;
//	case VK_INSERT:		return gxKey::INS		;
//	case VK_END:		return gxKey::HOME;
//	case VK_HOME:		return gxKey::END;
//	default:
//		if (id >= '0' && id <= '9')
//		{
//			return gxKey::NUM0 + id-'0';
//		}
//		if (id >= 'A' && id <= 'Z')
//		{
//			return gxKey::A + id-'A';
//		}
//		return id;
//	}
//
//	return 0x00;
//}


//void CGamePad::InputKeyCheck( UINT iMsg , WPARAM wParam ,LPARAM lParam )
//{
//	//キー入力されたら記録する
//
//	switch(iMsg) {
//	case WM_KEYDOWN:
//		//キーボード押した
//		//gxPadManager::GetInstance()->SetKeyDown(wParam);
//		m_KeyBoardPush[ ConvertKeyNumber( wParam ) ] = 0x01;
//		return;
//
//	case WM_KEYUP:
//		//キーボード離した
//		m_KeyBoardPush[ ConvertKeyNumber( wParam ) ] = 0x02;
//		return;
//
//	case WM_LBUTTONDOWN:
//		//マウスボタン押した
//		m_MouseClick[0] = 0x01;
//		break;
//	case WM_LBUTTONUP:
//		//マウスボタン離した
//		m_MouseClick[0] = 0x02;
//		break;
//	case WM_RBUTTONDOWN:
//		//マウスボタン押した
//		m_MouseClick[1] = 0x01;
//		break;
//	case WM_RBUTTONUP:
//		//マウスボタン離した
//		m_MouseClick[1] = 0x02;
//		break;
//	case WM_MBUTTONDOWN:
//		//マウスボタン押した
//		m_MouseClick[2] = 0x01;
//		break;
//	case WM_MBUTTONUP:
//		//マウスボタン離した
//		m_MouseClick[2] = 0x02;
//		break;
//
//	case WM_MOUSEMOVE:
//		{
//			//マウスが移動した
//			POINT point={0,0};
//
//			point.x = GET_X_LPARAM( lParam );
//			point.y = GET_Y_LPARAM( lParam );
//
//			//if( ScreenToClient( CWindows::GetInstance()->m_hWindow , &point ) )
//			{
//				Sint32 w, h;
//				CWindows::GetInstance()->GetWindowsResolution(&w, &h);
//
//				if( w == 0 || h == 0 ) break;
//
//				if( GetActiveWindow() == CWindows::GetInstance()->m_hWindow )
//				{
//					m_MouseX = point.x;// WINDOW_W*point.x / w;
//					m_MouseY = point.y;// WINDOW_H*point.y / h;
//				}
//			}
//		}
//		break;
//
//	case WM_MOUSEWHEEL:
//		m_Wheel = (Sint16)HIWORD(wParam)/WHEEL_DELTA;
//		break;
//
//	case WM_TOUCH:
//		//タッチ対応
//		//InputTouchCheck( iMsg , wParam , lParam );
//		break;
//	default:
//		break;
//	}
//
//
//}


void CGamePad::AdjustDefaultInput()
{
	//全コントローラー情報を1Pに割り振る

	Uint32 padButtonPush = 0x00;
	Float32 padAnalog[6] ={0};

	for( Sint32 ii=0; ii<m_DeviceCnt; ii++ )
	{
		CGameingDevice *pDevice = &m_Device[ii];
		
		//povから十字レバー情報を更新する

		for( Sint32 jj=0; jj<4; jj++ )
		{
			int tbl[]={
				JOY_U,JOY_R,JOY_D,JOY_L
			};
			if( pDevice->_Pov0 & tbl[jj] )
			{
				padButtonPush |= (0x01 << jj);
			}
		}

		for( Sint32 jj=0; jj<16-0; jj++ )
		{
			if( pDevice->_button[jj] )
			{
				padButtonPush |= (0x01 << (4+jj) );
			}
		}

		padAnalog[0] += pDevice->_Axis.x;
		padAnalog[1] += pDevice->_Axis.y;
		padAnalog[2] += pDevice->_Rotation.x;
		padAnalog[3] += pDevice->_Rotation.y;
		padAnalog[4] += pDevice->_Axis.z;
		padAnalog[5] += pDevice->_Rotation.z;

		//analogはボタン入力にも対応させる

		{
			if (pDevice->_Axis.y < -0.25f) padButtonPush |= BTN_ANALOG1U;
			if (pDevice->_Axis.x >  0.25f) padButtonPush |= BTN_ANALOG1R;
			if (pDevice->_Axis.y >  0.25f) padButtonPush |= BTN_ANALOG1D;
			if (pDevice->_Axis.x < -0.25f) padButtonPush |= BTN_ANALOG1L;

			if (pDevice->_Rotation.y < -0.25f) padButtonPush |= BTN_ANALOG2U;
			if (pDevice->_Rotation.x >  0.25f) padButtonPush |= BTN_ANALOG2R;
			if (pDevice->_Rotation.y >  0.25f) padButtonPush |= BTN_ANALOG2D;
			if (pDevice->_Rotation.x < -0.25f) padButtonPush |= BTN_ANALOG2L;

			if (pDevice->_Axis.z < -0.25f)		padButtonPush |= BTN_ANALOG3L;
			if (pDevice->_Axis.z >  0.25f) 		padButtonPush |= BTN_ANALOG3R;
			//			if( pDevice->_Rotation.z < -0.5f )	padButtonPush |= BTN_ANALOG3U;
			//			if( pDevice->_Rotation.z >  0.5f )	padButtonPush |= BTN_ANALOG3D;
		}
	}

	padAnalog[0] = CLAMP( padAnalog[0] , -1.0f , 1.0f );
	padAnalog[1] = CLAMP( padAnalog[1] , -1.0f , 1.0f );
	padAnalog[2] = CLAMP( padAnalog[2] , -1.0f , 1.0f );
	padAnalog[3] = CLAMP( padAnalog[3] , -1.0f , 1.0f );
	padAnalog[4] = CLAMP( padAnalog[4], -1.0f, 1.0f);
	padAnalog[5] = CLAMP( padAnalog[5], -1.0f, 1.0f);

	gxPadManager::GetInstance()->SetPadInfo( 0 , padButtonPush );
	gxPadManager::GetInstance()->SetAnalogInfo( 0 , padAnalog );
}


void CGamePad::drawDevice( Sint32 id , Sint32 ax , Sint32 ay)
{
	ax = 32+ax*128+32*ax;
	ay = 32+ay*128+32*ay;
	Sint32 az = 100;

	CGameingDevice *pDevice = &m_Device[id];

	gxLib::DrawBox( ax , ay , ax+128 , ay+128 , az , gxFalse , ATR_DFLT , 0xff00ff00 );
	gxLib::DrawLine( ax , ay+64 , ax+128 , ay+64 , az , ATR_DFLT , 0x4000ff00 );
	gxLib::DrawLine( ax+64 , ay , ax+64 , ay+128 , az , ATR_DFLT , 0x4000ff00 );

	//axy

	if( pDevice->_bAxisX && pDevice->_bAxisY )
	{
		Sint32 lx = ax+64 + pDevice->_Axis.x*64;
		Sint32 ly = ay+64 + pDevice->_Axis.y*64;

		gxLib::DrawBox( lx -4 , ly-4 , lx+4 , ly+4 , az , gxFalse , ATR_DFLT , 0xffff0000 );
	}

	//rxy

	if( pDevice->_bAxisRX && pDevice->_bAxisRY )
	{
		Sint32 rx = ax+64 + pDevice->_Rotation.x*64;
		Sint32 ry = ay+64 + pDevice->_Rotation.y*64;
		gxLib::DrawBox( rx -6 , ry-6 , rx+6 , ry+6 , az , gxFalse , ATR_DFLT , 0xff0000ff );
	}

	//z

	if( pDevice->_bAxisZ || pDevice->_bAxisRZ )
	{
		Sint32 zx = ax+64 + pDevice->_Axis.z*64;
		Sint32 zy = ay+64 + pDevice->_Rotation.z*64;
		gxLib::DrawBox( zx -8 , zy-8 , zx+8 , zy+8 , az , gxFalse , ATR_DFLT , 0xffffff00 );
	}

	//pov

	if( pDevice->_bPov[0] )
	{
		for( Sint32 ii=0; ii<4; ii++ )
		{
			Sint32 px = ax + ii*20;
			Sint32 py = ay;
			int tbl[]={
				JOY_U,JOY_R,JOY_D,JOY_L
			};
			gxLib::DrawBox( px , py , px+16 , py+16 , az , (pDevice->_Pov0 & tbl[ii]) , ATR_DFLT , 0xffff0000 );
		}
	}

	{
		for( Sint32 ii=0; ii<16; ii++ )
		{
			Sint32 tx,ty;

			tx = ax+ii%4*16;
			ty = ay+32+ii/4*16;

			gxLib::DrawBox( tx , ty , tx+14 , ty+14 , az , (pDevice->_button[ii])? true : false , ATR_DFLT , 0x8000ffff );
		}
	}

}
