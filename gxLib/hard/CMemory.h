﻿#ifndef _CMEMORY_H_
#define _CMEMORY_H_

#define _USE_MEMORY_H_

#define GAME_HEAP_SIZE (1024ull*1024*MAX_RAM_MB)

struct StRAMHead
{
	size_t size;			//8
	Uint32 index;			//4
	Uint8  layer;			//1
	Uint8  id[2];			//2
	Uint8  dummy[1];		//1
	Uint8  memo[16]={0};	//16
};

struct StRAMFoot
{
	Uint8  memo[16]={0};	//8
};

enum {
	enMaxRam = 1024*8,//1024*1024,
};

void MemoryInit();
void MemoryDestroy( Sint8 level = -1 );

void UpdateMemoryStatus( size_t* uNow , size_t* uTotal , size_t* uMax );

#ifdef _USE_MEMORY_H_
void* operator new( size_t size );
void* operator new[]( size_t size );
void  operator delete( void* ptr );
void  operator delete[]( void* ptr );
void* REALLOC( void* p , size_t sz );
#endif

#endif
