//-----------------------------------------------------------------------
//
// 
//
//-----------------------------------------------------------------------

#include <gxLib.h>
#include "CMemory.h"

static size_t m_UseMemory;
static size_t m_MaxUseMemory;
Uint8 m_AllocateLevel = 0;

Uint32 m_RamIndex = 0;

//#define LEAK_CHECK


#ifdef LEAK_CHECK
	struct debugRAM
	{
		void* pData;
	};
	#define RAM_ARRAY_MAX (1024*1024)
	debugRAM RAMArray[RAM_ARRAY_MAX] = {0};
#endif

Sint32 m_bInitilizedMemoryUnit = 0;

std::atomic<gxBool> s_BlockNew;
std::atomic<gxBool> s_BlockDel;

void SetMemoryMemo( void *adr , char* name )
{
	uint8_t* p = (uint8_t*)adr;
	p -= sizeof(StRAMHead);
	StRAMHead *q = (StRAMHead*)(p);

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
		memcpy( q->memo , name , 16 );
		q->memo[15] = 0x00;
	}
}

void SetAllocateLevel( Sint8 level = -1 )
{
	m_AllocateLevel = level;
}


void MemoryInit()
{
	s_BlockNew.store( gxFalse );
	s_BlockDel.store( gxFalse );

	m_bInitilizedMemoryUnit = 1;
	m_RamIndex = 0;

#ifdef LEAK_CHECK
	gxUtil::MemSet( RAMArray , 0x00 , sizeof(RAMArray) );
#endif
}


#ifdef LEAK_CHECK
void checkMemoryWarning()
{
	Sint32 nnn = 0;
	switch (m_RamIndex) {
	case 171:
		nnn++;
		break;
	}

}
#endif
void MemoryDestroy( Sint8 level)
{
#ifdef LEAK_CHECK
	for(Sint32 ii=0; ii< RAM_ARRAY_MAX; ii++ )
	{
		if(RAMArray[ii].pData )
		{
			StRAMHead *p = (StRAMHead*)&RAMArray[ii].pData;

			if ( ( level == -1 ) || ( p->level == level ) ) 
			{
				//リークを発見
				StRAMHead *p = (StRAMHead*)&RAMArray[ii].pData;

				delete p;
			}
		}
	}
#endif
	m_bInitilizedMemoryUnit = 2;
}


void UpdateMemoryStatus(size_t* uNow , size_t* uTotal , size_t* uMax )
{
	//MBで返す

	*uNow   = m_UseMemory;
	*uTotal = GAME_HEAP_SIZE;
	*uMax   = m_MaxUseMemory;
}

#ifdef _USE_MEMORY_H_

void* operator new( size_t size )
{
	while( s_BlockNew.load() )
	{
		gxLib::Sleep(1);
	}

	if (m_bInitilizedMemoryUnit != 1 )
	{
		return malloc(size);
	}

	s_BlockNew.store(gxTrue);

	Uint8 *p;

	p = (Uint8*)malloc( size + sizeof(StRAMHead) +  sizeof(StRAMFoot));

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->layer = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';


#ifdef LEAK_CHECK
	checkMemoryWarning();
	RAMArray[m_RamIndex].pData = p;
	m_RamIndex++;
#endif
	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

	s_BlockNew.store(gxFalse);

	return (void*)&p[ sizeof(StRAMHead) ];
}


void* operator new[]( size_t size )
{
	while( s_BlockNew.load() )
	{
		gxLib::Sleep(1);
	}

	Uint8 *p;

	if (m_bInitilizedMemoryUnit != 1)
	{
		return malloc(size);
	}

	s_BlockNew.store(gxTrue);

	p = (Uint8*)malloc( size + sizeof(StRAMHead) +  sizeof(StRAMFoot));

	StRAMHead *q = (StRAMHead*)p;
	q->index = m_RamIndex;
	q->layer = m_AllocateLevel;
	q->size  = size;
	q->id[0] = 'G';
	q->id[1] = 'X';


#ifdef LEAK_CHECK
	checkMemoryWarning();
	if (RAMArray[m_RamIndex].pData)
	{
		int n = 0;
		n++;
	}
	RAMArray[m_RamIndex].pData = p;
	m_RamIndex++;
#endif
	m_UseMemory += size;

	if( m_UseMemory >= m_MaxUseMemory )
	{
		m_MaxUseMemory = m_UseMemory;
	}

	s_BlockNew.store(gxFalse);

	return (void*)&p[ sizeof(StRAMHead) ];
}


void operator delete( void* ptr )
{
	if (ptr == nullptr) return;

	while(s_BlockDel.load())
	{
		gxLib::Sleep(1);
	}

	s_BlockDel.store(gxTrue);

	Uint8 *p = (Uint8 *)ptr;

#if defined(GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	if (m_bInitilizedMemoryUnit == 0)
	{
		//初期化完了前ならGXを確認しただけで落ちるのでここで開放する
		//しかし、アプリ終了時もこっちを通るので考慮が必要かも
		free(ptr);
		s_BlockDel.store(gxFalse);
		return;
	}

	p -= sizeof(StRAMHead);

	StRAMHead* q = (StRAMHead*)p;
	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
#ifdef LEAK_CHECK
			RAMArray[q->index].pData = NULL;
#endif
		m_UseMemory -= q->size;
		free(p);
	}
	else
	{
		free(ptr);
	}

	s_BlockDel.store(gxFalse);

}

void operator delete[]( void* ptr )
{
	if (ptr == nullptr) return;

	while(s_BlockDel.load())
	{
		gxLib::Sleep(1);
	}
	s_BlockDel.store(gxTrue);

	Uint8 *p = (Uint8 *)ptr;

#if defined (GX_RELEASE) || defined(GX_MASTER)
	if( ptr == NULL ) return;
#endif

	p -= sizeof(StRAMHead);

	StRAMHead *q = (StRAMHead*)p;

	if (m_bInitilizedMemoryUnit == 0)
	{
		//初期化完了前ならGXを確認しただけで落ちるのでここで開放する
		//しかし、アプリ終了時もこっちを通るので考慮が必要かも
		free(ptr);
		s_BlockDel.store(gxFalse);
		return;
	}

	//if(strcmp((const char*)&q->memo[0],"TEST0123456") == 0 )
	//{
	//	Sint32 nnn = 0;
	//	nnn ++;
	//}

	if (q->id[0] == 'G' && q->id[1] == 'X')
	{
#ifdef LEAK_CHECK
			RAMArray[q->index].pData = NULL;
#endif
		m_UseMemory -= q->size;
		free(p);
	}
	else
	{
		free(ptr);
	}

	s_BlockDel.store(gxFalse);
}

void* REALLOC( void* p , size_t sz )
{
	size_t copySZ = sz;
	if (p)
	{
		StRAMHead* q = (StRAMHead*)(((Uint8*)p) - sizeof(StRAMHead));

		copySZ = sz;

		if (sz > q->size) copySZ = q->size;
	}

	Uint8* pRet = new Uint8[sz];

	if (p)
	{
		gxUtil::MemCpy(pRet, p, copySZ);
	}

	return pRet;
}

#else
void* REALLOC( void* p , size_t sz )
{
	return realloc( p , sz );
}

#endif
