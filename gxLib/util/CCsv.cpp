﻿//------------------------------------------
//
// CCSV ver.2.0
//
//------------------------------------------

#include <gxLib.h>
#include "CCsv.h"
#include<algorithm>

//------------------------------------------------------------------------
CCsv::CCsv()
	: m_pTempBuf(CSV_CELL_BUF_MAX)
{
	//コンストラクタ
	m_RangeW = 0;
	m_RangeH = 0;

	m_bIgnoreComment = true;
	m_bCleanDquote   = true;
	m_pCsvReadOnlyBuffer = NULL;
	m_pCsvArray = NULL;
}


CCsv::~CCsv()
{
	SAFE_DELETES(m_pCsvArray);
	SAFE_DELETES(m_pCsvReadOnlyBuffer);
}


bool CCsv::LoadFile(char const* filename, bool bCommentOut)
{
	//----------------------------------------------------
	//CSVファイルをロードする
	//----------------------------------------------------

	m_bIgnoreComment = bCommentOut;

	size_t uSize;
	Uint8 *pData = gxLib::LoadFile((char*)filename, &uSize);
	if (!pData) return false;

	ReadFile(pData, uSize);

	SAFE_DELETES(pData);

	return true;
}


bool CCsv::ReadFile(uint8_t *pMem, size_t sz, bool bCommentOut)
{
	//----------------------------------------------------
	//CSVファイルをメモリから読み込む
	//----------------------------------------------------
	m_bIgnoreComment = bCommentOut;

	SAFE_DELETES(m_pCsvReadOnlyBuffer);

	m_pCsvReadOnlyBuffer = new gxChar[sz + 1024];	//改行、EOFなどで拡張される場合があるので少しバッファサイズを増やしておく

	gxUtil::MemCpy(&m_pCsvReadOnlyBuffer[0], pMem, sz);

	m_pCsvReadOnlyBuffer[sz] = 0x00; //念のため

	unsigned char* p = (unsigned char*)&m_pCsvReadOnlyBuffer[0];

	if (p[0] == 0xEF && p[1] == 0xBB && p[2] == 0xBF)
	{
		//BOM付きUTF-8文書だったのでオフセットする
		p += 3;
		sz -= 3;
	}

	analysingCsv((char*)p, sz);

	return true;

}


bool CCsv::analysingCsv(char *p, size_t sz)
{
	//----------------------------------------------------
	//CSVをパースする
	//----------------------------------------------------

	gxBool bDQ = gxFalse;
	Uint32 x = 0, y = 0;
	Uint32 y_max = 0;
	Uint32 x_max = 0;
	size_t cnt = 0;
	Sint32 bline = 0;
	for (uint32_t ii = 0; ii < sz+1; ii++)
	{
		switch (p[ii]) {
		case '\n':
			if (bDQ) {
				continue;
			}
			// ↓

		case 0x00:
			if (cnt > 0) x++;
			if (cnt == 0 && x == 0 ) bline++;
			if (x > x_max) x_max = x;
			y_max++;
			x = 0;
			cnt = 0;
			break;

		case '\"':
			bDQ = !bDQ;
			cnt++;
			break;

		case ',':
			if (!bDQ)
			{
				x++;
				cnt = 0;
			}
			break;

		case '\r':
			break;

		default:
			bline = 0;
			cnt++;
			break;
		}
	}

	//-------------------------------------------------
	//改行単位でスキャンしてセルのポインタを確定する
	//-------------------------------------------------

	m_RangeW = x_max;
	m_RangeH = y_max - bline;

	SAFE_DELETES(m_pCsvArray);
	m_pCsvArray = new gxChar*[x_max*y_max];
	gxUtil::MemSet(m_pCsvArray, 0x00, sizeof(uint8_t*)*x_max*y_max);

	x = 0;
	y = 0;
	cnt = 0;
	bDQ = gxFalse;
	uint32_t s1 = 0;

	for (uint32_t ii = 0; ii < sz+1; ii++)
	{
		switch (p[ii]) {

		case '\n':
			if (bDQ) {
				continue;
			}

			//↓

		case 0x00:

			if (cnt > 0)
			{
				m_pCsvArray[x + x_max * y] = &p[s1];
				x++;
			}

			p[ii] = 0x00;

			x = 0;
			cnt = 0;
			y++;
			s1 = ii + 1;
			break;

		case '\"':
			bDQ = !bDQ;
			cnt++;
			break;

		case ',':
			if (!bDQ)
			{
				m_pCsvArray[x + x_max * y] = &p[s1];
				x++;
				p[ii] = 0x00;
				s1 = ii + 1;
				cnt = 0;
			}
			break;

		case '\r':
			if (!bDQ) p[ii] = 0x00;
			break;

		default:
			cnt++;
			break;
		}
	}

	return gxTrue;
}


gxBool CCsv::initWritableSheet()
{
	gxChar *p = NULL;

	char buf[1024];

	for (int y = 0; y < m_RangeH;y++)
	{
		for (int x = 0; x < m_RangeW;x++)
		{
			p = m_pCsvArray[x + m_RangeW * y];
			if (p)
			{
				sprintf(buf, "(%d,%d)", x, y);

				m_pCellMap[buf] = p;
			}
		}
	}

	SAFE_DELETES(m_pCsvArray);
	SAFE_DELETES(m_pCsvReadOnlyBuffer);

	return true;
}


char* CCsv::getCellBuf(int x, int y)
{

	m_pTempBuf[0] = 0x00;
	if( x < 0 || x >= m_RangeW ) return &m_pTempBuf[0];
	if( y < 0 || y >= m_RangeH ) return &m_pTempBuf[0];

	if (m_pCsvReadOnlyBuffer)
	{
		//readOnly

		gxChar *pCell = m_pCsvArray[x + m_RangeW * y];

		if (pCell)
		{
			sprintf( &m_pTempBuf[0], "%s", pCell);
			cleanTab( &m_pTempBuf[0]);

			if (m_bCleanDquote)
			{
				cleanDQuote( &m_pTempBuf[0] );
			}
		}
		else
		{
			m_pTempBuf[0] = 0x00;
		}

		return &m_pTempBuf[0];

	}
		
		
	//sprintf(&m_pTempBuf[0], "%d,%d", x, y);
	//if (m_pCellMap.find(&m_pTempBuf[0]) == m_pCellMap.end())

	char buf[1024];
	sprintf(buf, "(%d,%d)", x, y);
	if (m_pCellMap.find( buf ) == m_pCellMap.end() )
	{
		return NULL;
	}

	//return &m_pCellMap[&m_pTempBuf[0]][0];
	//return (char*)m_pCellMap[ buf ].c_str();

	gxChar* pCell = (char*)m_pCellMap[buf].c_str();

	if (pCell)
	{
		sprintf(&m_pTempBuf[0], "%s", pCell);
		cleanTab(&m_pTempBuf[0]);

		if (m_bCleanDquote)
		{
			cleanDQuote(&m_pTempBuf[0]);
		}
	}
	else
	{
		m_pTempBuf[0] = 0x00;
	}

	return &m_pTempBuf[0];
}


char* CCsv::GetCell(int x, int y)
{
	//セルのデータを取得する
	char *p = getCellBuf(x, y);

	if (p)
	{
		return p;
	}
	else
	{
		m_pTempBuf[0] = CSV_DATA_NONE;
	}

	return &m_pTempBuf[0];
}


bool CCsv::DelCell( int x,int y )
{
	//セルバッファを削除する

	if (m_pCsvReadOnlyBuffer) initWritableSheet();


	//char buf[256];
	//sprintf( buf, "%d,%d", x, y);

	char buf[1024];
	sprintf(buf, "(%d,%d)", x, y);
	//if ( m_pCellMap.find(buf) == m_pCellMap.end() )
	if ( m_pCellMap.find( buf ) == m_pCellMap.end() )
	{
		//見つからなかった
		return false;
	}

	//あったので以前のバッファを削除しておく
	//delete[] m_pCellMap[buf];
	//m_pCellMap[buf].clear();
	//m_pCellMap[buf][0] = 0x00;	//毒
	if (m_pCellMap.find( buf ) == m_pCellMap.end()) return false;
	//さらに配列から削除
//	m_pCellMap.erase( buf );
	m_pCellMap.erase( buf );

	return true;
}

bool CCsv::SetCellFix(int x, int y, char const* msg)
{
	//セルにデータを書き込む

	if (x < 0 || y < 0) return false;

	if (m_pCsvReadOnlyBuffer) initWritableSheet();

	DelCell(x, y);

	char buf[1024];
	sprintf(buf, "(%d,%d)", x, y);

	m_pCellMap[buf] = msg;

	if (x + 1 > m_RangeW) m_RangeW = x + 1;
	if (y + 1 > m_RangeH) m_RangeH = y + 1;

	return true;
}

bool CCsv::SetCell( int x , int y , char const *msg , ...)
{
	//セルにデータを書き込む

	if( x < 0 || y < 0 ) return false;

	if (m_pCsvReadOnlyBuffer) initWritableSheet();

	size_t msglen = strlen(msg);

	if (msglen >= CSV_CELL_BUF_MAX )
	{
		m_pTempBuf.resize(CSV_CELL_BUF_MAX * 10);
	}

	va_list app;
	va_start(app, msg);
	vsprintf( &m_pTempBuf[0], msg, app );
	va_end(app);

	auto ret = SetCellFix(x, y, &m_pTempBuf[0]);

/*
	DelCell(x, y);

	char buf[1024];
	sprintf(buf, "(%d,%d)", x, y);

	m_pCellMap[ buf ] = &m_pTempBuf[0];

	if( x+1 > m_RangeW ) m_RangeW = x+1;
	if( y+1 > m_RangeH ) m_RangeH = y+1;
*/

	return ret;
}


bool CCsv::clearComment( char *p )
{
	//コメントを無視するか？

	if( m_bIgnoreComment == false ) return false;

	for( size_t i=0; i<strlen(p); i++ )
	{
		if(strlen(p)==1) break;

		if(p[i]=='/' && p[i+1]=='/')
		{
			p[i] = CSV_DATA_NONE;		//以降コメントなので無視する
			return true;
		}
	}

	return false;
}


bool CCsv::cleanDQuote( char* pString )
{
	//ダブルコーテーションを削除する

	int len = int( strlen( pString ) );

	//std::vector<char> buf(len+1);
	char buf[CSV_CELL_BUF_MAX];
	
	snprintf( &buf[0] , len+1, "%s" , pString );

	char *p2 = &buf[0];

	if( buf[0] == '"' )
	{
		p2 = &buf[1];

		if( buf[len-1] == '"' )
		{
			buf[len-1] = 0x00;
		}
		sprintf( pString , "%s" , p2 );

		return true;
	}

	return false;
}


bool CCsv::cleanTab( char *pString )
{
	//空白とかを破棄する

	//std::vector<char> src(CSV_CELL_BUF_MAX);
	//std::vector<char> dst(CSV_CELL_BUF_MAX);

	char src[CSV_CELL_BUF_MAX];
	char dst[CSV_CELL_BUF_MAX];

	size_t len;
	size_t cntSrc, cntDst = 0;
	bool dquote = false;

	//破棄します
	if (pString == NULL) return false;

	sprintf( &src[0], "%s", pString );
	sprintf( &dst[0], "%s", pString );
	len = strlen(&src[0]);

	cntSrc = 0;
	cntDst = 0;

	for (int ii = 0; ii < len; ii++)
	{
		switch (src[cntSrc]) {
		case '\"':
			dquote = !dquote;
			break;

		case '\t':
			//cntSrc ++;
			if (!dquote)
			{
				cntSrc++;
				continue;
			}
			break;

		default:
			break;
		}

		dst[cntDst] = src[cntSrc];
		cntSrc++;
		cntDst++;
	}

	dst[cntDst] = 0x00;
	sprintf(pString, "%s", &dst[0]);

	return true;

}


int CCsv::COUNTA( int x1 , int y1 , int x2 , int y2 )
{
	//指定範囲のデータ数を数える
	int cnt = 0;
	char *p;

	for( int y=y1; y<=y2; y++ )
	{
		if( y2 >= GetHeight() ) continue;

		for( int x=x1; x<=x2; x++ )
		{
			p = GetCell( x , y );

			if( p[0] == CSV_DATA_NONE ) continue;

			cnt ++;
		}
	}

	return cnt;
}


Sint32 CCsv::getLineWidth(Sint32 y)
{
	//行ごとの最大範囲を取得する

	Sint32 max = 0;

	for(Sint32 xx=0; xx<m_RangeW; xx++ )
	{
		if( GetCell(xx,y)[0] != CSV_DATA_NONE)
		{
			max = xx;
		}
	}

	return max;
}


void CCsv::UpdateCellRange()
{
	//セルの範囲を最小に絞る

	Sint32 w = GetWidth();
	Sint32 h = GetHeight();

	Sint32 max_x = 0, max_y = 0;

	for (int y = 0; y < h; y++)
	{
		Sint32 cnt = 0;

		for (int x = 0; x < w; x++)
		{
			if (GetCell(x, y)[0] != CSV_DATA_NONE)
			{
				cnt++;
				if (x + 1 > max_x)
				{
					max_x = x + 1;
				}
			}
		}

		if (cnt > 0)
		{
			//X方向に１データでもあればY最大値を更新する
			max_y = y + 1;
		}
	}

	m_RangeW = max_x;
	m_RangeH = max_y;
}

bool CCsv::SaveTSVFile(char const* pFileName)
{
	return saveFile(1, pFileName);
}

bool CCsv::SaveFile(char const* pFileName)
{
	return saveFile(0, pFileName);
}

bool CCsv::saveFile( int mode , char const* pFileName )
{
	//ＣＳＶファイルとして保存する
	std::string cmma = ",";

	if (mode == 1)
	{
		cmma = "\t";
	}


	UpdateCellRange();

	Sint32 w = GetWidth();
	Sint32 h = GetHeight();

	std::vector<char>buf(CSV_LINE_BUF_MAX );	//行単位で処理されるので大きめに
	std::vector<std::vector<char>> buf2;

	buf[0] = CSV_DATA_NONE;

	Uint32 n = 0;
	size_t total = 0;

	buf2.resize( h );

	for( int y=0; y<h; y++ )
	{
		n = 0;
		Sint32 wMax = getLineWidth(y);

		for(int x=0; x<w; x++ )
		{
			gxChar *p = GetCell( x , y );

			if( p )
			{
				n += sprintf( &buf[n] , "%s" , p );
			}


			if( x < wMax )
			{
				n += sprintf( &buf[n] , "%s",cmma.c_str() );
			}

			//if (x == w - 1)
			//{
			//	//最後のセルには「,」を付与しない
			//
			//}
			//else
			//{
			//}
		}
		n += sprintf( &buf[n] , "%s" , KAIGYO_CODE );
		size_t len = strlen( &buf[0] );

		buf2[y].resize( len+1 );

		total += len;

		sprintf( &buf2[y][0],"%s" , &buf[0] );
	}

	std::vector<char> pData( total + 1 );
	n = 0;

	for( int y=0; y<h; y++ )
	{
		n += sprintf( &pData[n] , "%s" , &buf2[y][0] );
	}

	gxLib::SaveStorageFile( (char*)pFileName , (Uint8*)&pData[0] , total );


	return true;
}


bool CCsv::SearchWord( char *msg , int &x , int &y )
{
	//特定の文字列を探して最初にであったものを返す

	char buf[255];

	for(int iy=0;iy<m_RangeH;iy++)
	{
		for( int ix=0;ix<m_RangeW; ix++ )
		{
			sprintf( buf,"%s",GetCell(ix,iy) );
			size_t l1 = strlen( buf );
			size_t l2 = strlen(msg);
			size_t len = ( l1 > l2 )? l1 : l2;

			if( strncmp( buf , msg , len) == 0 )
			{
				x = ix;
				y = iy;
				return true;
			}
		}
	}

	return false;
}




char* CCsv::VLOOKUP( char *name , int x1 , int x2 )
{
	//X1列目から指定された名前のセルを探して、X2列目のデータを取る。

	for( int i=0; i<GetHeight(); i++ )
	{
		if( strcmp(name,GetCell( x1 , i ) ) == 0 )
		{
			return GetCell( x2 , i );
		}
	}

	return NULL;
}

//------------------------ ここまで ↑↑↑ -------------------------------

void CCsv::GetCell( int x , int y , int& param )
{
	char* p = GetCell( x , y );

	param = atoi(p);
}


void CCsv::GetCell( int x , int y , float& param )
{
	char* p = GetCell( x , y );

	param = atof(p);
}


#ifdef _WIN32
	#ifdef PLATFORM_WINSTORE
		#define strcasecmp _stricmp
	#else
		#define strcasecmp stricmp
	#endif
#endif

void CCsv::GetCell(int x, int y, bool& param)
{
	char* p = GetCell(x, y);

	if (strcasecmp(p, "FALSE") == 0)
	{
		param = false;
	}
	else if (strcasecmp(p, "0") == 0 )
	{
		param = false;
	}
	else
	{
		param = true;
	}
}


void CCsv::GetCell( int x, int y, std::string& param )
{
	char* p = GetCell( x, y );

	param = p;
}

void CCsv::ChangeSJIStoUTF8()
{
	Sint32 w = GetWidth();
	Sint32 h = GetHeight();

	std::string str,str2;

	for (Sint32 y = 0; y < h; y++)
	{
		for (Sint32 x = 0; x < w; x++)
		{
			GetCell(x, y, str);

			//auto *p = CDeviceManager::SJIStoUTF8((gxChar*)str.c_str());
			//SAFE_DELETES(p);
			str2 = gxUtil::SJIStoUTF8( str );
			SetCellFix(x, y, str2.c_str());
		}
	}
}

std::vector<size_t> CCsv::Grep(std::string word , gxBool bUpper )
{
	Sint32 w = GetWidth();
	Sint32 h = GetHeight();
	std::vector<size_t> grepList;

	std::string str, str2;

	if (bUpper)
	{
		gxUtil::StrUpper(word);
	}
		
	for (Sint32 y = 0; y < h; y++)
	{
		for (Sint32 x = 0; x < w; x++)
		{
			GetCell(x, y, str);
			if (bUpper)
			{
				gxUtil::StrUpper(str);
			}

			if (str.find(word) != std::string::npos)
			{
				grepList.push_back(y);
				break;
			}
		}
	}

	return grepList;
}

std::vector<CCsv::POS> CCsv::GrepPos(std::string word, gxBool bUpper)
{
	Sint32 w = GetWidth();
	Sint32 h = GetHeight();
	std::vector<POS> grepList;

	std::string str, str2;

	if (bUpper)
	{
		gxUtil::StrUpper(word);
	}

	for (Sint32 y = 0; y < h; y++)
	{
		for (Sint32 x = 0; x < w; x++)
		{
			GetCell(x, y, str);
			if (bUpper)
			{
				gxUtil::StrUpper(str);
			}

			if (str.find(word) != std::string::npos)
			{
				POS pos;
				pos.x = x;
				pos.y = y;
				grepList.push_back(pos);
			}
		}
	}

	return grepList;
}

std::string CCsv::GetLine(size_t y)
{
	Sint32 w = GetWidth();
	Sint32 h = GetHeight();

	std::string str;
	std::string ret;

	for (Sint32 x = 0; x < w; x++)
	{
		GetCell(x, (Sint32)y, str);
		ret += str;
		ret += ",";
	}

	return ret;
}

void CCsvTest()
{
	//テスト用

#if 1

	CCsv *pCsv1 = new CCsv();
	pCsv1->LoadFile("testA.txt");

	CCsv *pCsv2 = new CCsv();
	pCsv2->LoadFile("testB.txt");

	if( *pCsv1 == *pCsv2 )
	{
		gxLib::DebugLog("SameCSV");
	}
	else
	{
		gxLib::DebugLog("AnotherCSV");
		CCsv csv = *pCsv2 |= *pCsv1;
		csv.SaveFile("merge.txt");
	}

	SAFE_DELETE( pCsv1 );
	SAFE_DELETE( pCsv2 );
#endif

}


