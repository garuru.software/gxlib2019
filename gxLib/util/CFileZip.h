#ifndef _CFILEZIP_H_
#define _CFILEZIP_H_

#include "miniz/miniz.h"
#include<algorithm>

class CFileZip
{
	struct StSaveInfo
	{
		std::vector<Uint8> m_pSaveBuffer;
		size_t m_uSaveSize = 0;
	};

public:

	struct InfoArchive
	{
		size_t  fileSize   = 0x00;
		std::string FileName1;
		std::string FileName2;
		uint8_t    *pFileData = NULL;

		~InfoArchive()
		{
		}

		void Release()
		{

		}

		void SetName(gxChar *pName)
		{
			FileName1 = "";
			FileName2 = "";

			size_t len = strlen(pName);

			//そのまんまのファイル名を記録
			FileName1 = pName;

			//大文字ファイル名を記録
			gxChar *pTxt = new gxChar[ len+1 ];
			sprintf(pTxt, "%s", pName );
			gxUtil::StrUpper(pTxt);
			FileName2 = pTxt;
			SAFE_DELETES(pTxt);
		}

	};

	CFileZip()
	{
		m_uFileSize   = 0;
		m_uFileNum    = 0;
	}

	~CFileZip()
	{
		if (m_pZArcLoad)
		{
			mz_zip_reader_end(m_pZArcLoad);
		}

		if (m_pZArcSave)
		{
			mz_zip_writer_end(m_pZArcSave);
		}

		SAFE_DELETES( m_pZArcLoad );
		SAFE_DELETES( m_pZArcSave );
		
		for (auto itr = m_FileInfo.begin(); itr != m_FileInfo.begin(); ++itr)
		{
			itr->Release();
		}

	}

	gxBool Load( gxChar *pFileName )
	{
		//zipを読み込んでファイルリストを作成する

		Uint8 *pData = NULL;
		size_t uSize = 0;

		pData = gxLib::LoadFile( pFileName , &uSize );

		if (pData == nullptr)
		{
			ASSERT(0);
		}

		return Read( pData , uSize );
	}

	gxBool Read( Uint8 *Data , size_t uSize )
	{
		//zipを読み込んでファイルリストを作成する
		//pDataはmemcpyされないのでDecodeするまで呼び出し側でメモリを保持しておくこと

		if (m_pZArcLoad == NULL)
		{
			m_pZArcLoad = new mz_zip_archive;
			memset(m_pZArcLoad, 0, sizeof(mz_zip_archive));
		}

		if( mz_zip_reader_init_mem( m_pZArcLoad, Data , uSize , 0 ) )
		{
			m_uFileNum = mz_zip_reader_get_num_files(m_pZArcLoad);

			m_FileInfo.clear();

			for ( Uint32 ii = 0; ii < m_uFileNum; ii++)
			{
				mz_zip_archive_file_stat stat;
				if (mz_zip_reader_file_stat(m_pZArcLoad, ii, &stat))
				{
					InfoArchive info;
					info.SetName( stat.m_filename );
					info.fileSize = stat.m_uncomp_size;
					m_FileInfo.push_back(info);
				}
			}
		}
		else
		{
			mz_zip_error err = mz_zip_get_last_error(m_pZArcSave);
			const char* pErr = mz_zip_get_error_string(err);
			printf("%s", pErr);
			return gxFalse;
		}
		return gxTrue;
	}

	Uint8* Decode( Uint32 index, size_t* pLength)
	{
		//pFileNameはdecodeされるファイル名

		if (index >= m_FileInfo.size())
		{
			*pLength = 0x00;
			return NULL;
		}

		Uint8 *pData = Decode( m_FileInfo[index].FileName1, pLength);

		return pData;
	}

	Uint8* Decode( std::string fileName , size_t* pLength )
	{
		//読み込んだZIPからファイルを取り出す

		size_t extracted_size = 0;

		void *p;
		p = mz_zip_reader_extract_file_to_heap( m_pZArcLoad, fileName.c_str(), &extracted_size, 0);

		*pLength = extracted_size;

		Uint8 *p1 = new Uint8[extracted_size];
		gxUtil::MemCpy(p1 , p, extracted_size);
		free(p);

		return p1;
	}


	const gxChar* AddFile(gxChar *pFileName, void *pData, size_t sz)
	{
		if (pData == nullptr)
		{
			pData = nullptr;
			printf("CFileZip...[%s]File not found.", pFileName);
			return nullptr;
		}

		if (m_pZArcSave == NULL)
		{
			m_pZArcSave = new mz_zip_archive;
			memset(m_pZArcSave, 0, sizeof(mz_zip_archive));

			m_pZArcSave->m_pIO_opaque = this;
			m_pZArcSave->m_pWrite = &CFileZip::saveFunc;
			m_pZArcSave->m_pFree = &CFileZip::freeFunc;
			m_pZArcSave->m_pNeeds_keepalive = NULL;

			if (!mz_zip_writer_init_v2(m_pZArcSave, 0, MZ_ZIP_FLAG_CASE_SENSITIVE))
			{
				mz_zip_error err = mz_zip_get_last_error(m_pZArcSave);
				return mz_zip_get_error_string(err);
			}
		}

		mz_bool bSuccess = mz_zip_writer_add_mem(m_pZArcSave, pFileName, (const void *)pData, sz, MZ_DEFAULT_LEVEL);
		//printf("size=%d", sz);
		if (!bSuccess)
		{
			printf("zip add error");
			mz_zip_error err = mz_zip_get_last_error(m_pZArcSave);
			return mz_zip_get_error_string(err);
		}
		return nullptr;
	}

	gxBool Save(gxChar *pFileName)
	{
		if (!Close()) return gxFalse;

		size_t uSize = 0;
		Uint8* pData = GetSaveImage(&uSize);
		gxBool bResult = gxTrue;

		if (gxLib::SaveFile(pFileName, pData, uSize))
		{
			bResult = gxTrue;
		}

		for (auto itr = m_FileInfo.begin(); itr != m_FileInfo.begin(); ++itr)
		{
			SAFE_DELETES(itr->pFileData);
		}

		return bResult;
	}


	Uint32  GetFileNum()
	{
		return m_uFileNum;
	}

	std::vector<InfoArchive>& GetFileList()
	{
		return m_FileInfo;
	}

	std::string GetFileName( Sint32 index , size_t *pSize = NULL )
	{
		InfoArchive *p = &m_FileInfo[ index ];

		if( p->FileName1[0] != 0x00 )
		{
			*pSize = p->fileSize;
		}

		return p->FileName1;
	}

	size_t GetFileSize( Sint32 index )
	{
		InfoArchive *p = &m_FileInfo[ index ];
		return p->fileSize;
	}


	Uint8* GetSaveImage(size_t *uSize)
	{
		*uSize = GetSaveInfo()->m_uSaveSize;

		return &GetSaveInfo()->m_pSaveBuffer[0];
	}

	StSaveInfo* GetSaveInfo()
	{
		return &SaveInfo;
	}

	gxBool Close()
	{
		if (m_pZArcSave == NULL) return gxFalse;

		if (m_bClosed) return gxTrue;

		if (!mz_zip_writer_finalize_archive(m_pZArcSave))
		{
			mz_zip_error err = mz_zip_get_last_error(m_pZArcSave);
			const char* pErr = mz_zip_get_error_string(err);
			printf("zip close error:%s", pErr);
			return false;
		}
		m_bClosed = gxTrue;

		return gxTrue;
	}

private:

	static void freeFunc(void *opaque, void *address)
	{
		SAFE_DELETES(address);
	}

	static size_t saveFunc(void *pOpaque, mz_uint64 file_ofs, const void *pBuf, size_t n)
	{

		//static int nnn = 0;
		//Uint8* p1 = (Uint8*)pBuf;
		//for (Sint32 ii = 0; ii < n; ii++)
		//{
		//	printf("%02x",p1[ii]);
		//}
		//gxLib::Sleep(10);

		CFileZip *p = (CFileZip*)pOpaque;
		StSaveInfo *q = p->GetSaveInfo();

		if (q->m_pSaveBuffer.size() < q->m_uSaveSize + n)
		{
			q->m_pSaveBuffer.resize(p->GetSaveInfo()->m_uSaveSize + n);
		}
		if (n > 0)
		{
			memcpy((Uint8*)&q->m_pSaveBuffer[q->m_uSaveSize], pBuf, n);
		}

		q->m_uSaveSize += n;
		//printf("\r\ndatasize=%d\r\n", q->m_uSaveSize);
		return n;
	}

	size_t m_uFileSize   = 0;
	Uint32 m_uFileNum    = 0;

	std::vector<InfoArchive> m_FileInfo;
	mz_zip_archive* m_pZArcLoad = NULL;
	mz_zip_archive* m_pZArcSave = NULL;

	StSaveInfo SaveInfo;
	gxBool m_bClosed = gxFalse;
};

#endif
