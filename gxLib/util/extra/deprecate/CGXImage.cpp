﻿//---------------------------------------------------------------
// CGaruruImage.cpp
// 合体ファイルを扱うクラス
//
//---------------------------------------------------------------
#include <gxLib.h>
#include "CGXImage.h"

//#define _NO_GXI_IMAGE_
//#define ENABLE_WINDOWS_TOOL

#ifdef GX_DEBUG
	#define GXI_READ_SIZE (8192*32)	//32ではT4でガクガクだった
#else
	#define GXI_READ_SIZE (8192*16)	//32ではT4でガクガクだった
#endif

#define MAX_PATH_LENGTH (256)

gxChar g_NameBuf[MAX_PATH_LENGTH];
gxChar g_NameBuf2[MAX_PATH_LENGTH];
gxChar g_NameBuf3[MAX_PATH_LENGTH];
gxChar g_NameBuf4[MAX_PATH_LENGTH];

SINGLETON_DECLARE_INSTANCE( CGXImage );

CGXImage::CGXImage()
{
	//---------------------------
	//---------------------------

	m_pFileImage = NULL;
	m_sFileNum   = 0;
	m_sVersion   = 0;
	m_ZettaiPath[0] = 0x00;
	m_sImageSize = 0;

	for(Sint32 ii=0;ii<enFileNumMax;ii++)
	{
		memset( &m_stFile[ii] ,0x00, sizeof(StUnitFile) );
		m_pStFileInfo[ ii ] = NULL;
	}

	//---------------------------
	m_sAddFileNum = 0;

	m_fh   = -999;
	m_sPos = 0;
	m_LoadSeq = 0;
	m_bLoading = gxFalse;
	m_bLoadComplete = gxTrue;

	m_sErrorCode = 0;
	m_sAllWork = 0;
	m_NowWork  = 0;

	m_root[0] = 0x00;

	m_sFileListNum = 0;
	m_pFileList = new StFileList[ enFileNumMax ];

	m_uMask = GXI_DECODE_KEY;
}


CGXImage::~CGXImage()
{
	//---------------------------
	//---------------------------

	if( m_pFileImage ) delete[] m_pFileImage;

	//SAFE_DELETES( m_pFileList );

	clearAll();


}


void CGXImage::Set(Uint8* pFileImage)
{
	//---------------------------------------------
	//外部からのイメージセット
	//---------------------------------------------

	if( m_pFileImage )
	{
		delete[] m_pFileImage;
	}

	m_pFileImage = pFileImage;

	clearAll();

	m_pFileList = new StFileList[enFileNumMax];
}


Sint32 CGXImage::getIndex( gxChar* pFileName )
{
	//---------------------------------------------
	//ファイルのインデックスを返す
	//---------------------------------------------

	Sint32 l1,l2,len;
	gxChar buf1[255];
	gxChar buf2[255];

	l1 = strlen(pFileName);

	sprintf(buf1,"%s",pFileName );

    gxUtil::StrUpper(buf1);

	for(Sint32 ii=0;ii<m_sFileNum;ii++)
	{
		l2 = strlen( m_stFile[ii].pFileName );
		len = ( l2 > l1 )? l2 : l1;

		sprintf(buf2,"%s",m_stFile[ii].pFileName);
		gxUtil::StrUpper( buf2 );

		if( strncmp( buf2 , buf1 , len) == 0 )
		{
			return ii;
		}
	}

	return -1;

}


gxBool CGXImage::Add (gxChar* pUnitFileName)
{
	//-----------------------------------------------
	//イメージにファイルを追加する
	//-----------------------------------------------

	StUnitFileInfo *p;
	p = m_pStFileInfo[m_sAddFileNum] = new StUnitFileInfo;

	Sint32 len = strlen(m_ZettaiPath)+1;
	sprintf(p->FileName,"%s",&pUnitFileName[len] );
	p->NameSize   = strlen(p->FileName)+1;
	p->NameSize   += (4-p->NameSize%4);	//奇数アドレスにならないように

	p->FileSize   = 0;
	p->pFileImage = NULL;
	m_sAddFileNum ++;

	return gxTrue;
}


gxBool CGXImage::Load(gxChar* pFileName, Sint32 version , Uint32 mask)
{
	//-----------------------------------------------
	//イメージファイルを読み込む(ブロック読み込み)
	//-----------------------------------------------
	Sint32 sRet = 1;
	Uint32 sPos = 0;

	m_uMask = mask;

#ifdef _NO_GXI_IMAGE_
	clearAll();
	m_pFileList = new StFileList[enFileNumMax];

	gxChar temp[FILENAMEBUF_LENGTH];
	gxUtil::GetFileNameWithoutExt(pFileName, temp);
	SetPath(temp);
	return gxTrue;
#endif

	//-----------------------------------------------------------

	m_bLoadComplete = gxFalse;

	if( m_pFileImage )
	{
		delete[] m_pFileImage;
	}

	{
		// Blowfish復号化
		size_t uSize = 0;
		Uint8* pTemp = gxLib::LoadFile( pFileName, &uSize );

        if( pTemp == NULL )
        {
            GX_DEBUGLOG("[GXI Error]FileNotFound %s" , pFileName );
        }
		m_pFileImage = pTemp;//毒 gxUtil::FileDecryption(pTemp, uSize, &uReadSize);

		Decode( m_pFileImage, uSize);

		//delete[] pTemp;
	}

	Analyse(version );

	m_bLoadComplete = gxTrue;
    sprintf( m_ZettaiPath , "%s" , pFileName );

	return gxTrue;

}



gxBool CGXImage::Analyse( Sint32 version )
{
	//-----------------------------------------------------------
	//情報解析
	//-----------------------------------------------------------

	Sint32 sRet = 1;
	Uint32 sPos = 0;
	Uint32 sImgPos = 0;

	//ヘッダー情報を登録
	StPackHeader hdr;

	if ( version == enVersion_AutoDetect )
	{
		//Auto Detect
		if (m_pFileImage[3] != 2 )
		{
			//DRQ1
			version = enVersion1_0_1;

			if (m_pFileImage[0] == 'D' && m_pFileImage[1] == 'R' && m_pFileImage[2] == 'Q' && m_pFileImage[3] == 0x00 )
			{
				//DRQがそのまま入っていれば解除キーは３バイト目になるはず
				//m_uMask = m_pFileImage[3];
				//LX形式
				version = enVersion1_0_1;

			}
		}
		else
		{
//			version = m_pFileImage[4]-1;// enVersion1_0_2;
			version = enVersion1_0_2;
		}
	}

	if(version == enVersion1_0_1 )
	{
		//ver1をver2仕様に変換する
		StPackHeader1 *phdrSrc;
		phdrSrc = (StPackHeader1*)&m_pFileImage[0];
		hdr.name[0] = 'D';
		hdr.name[1] = 'R';
		hdr.name[2] = 'Q';
		hdr.name[3] = '2';
		hdr.version   = phdrSrc->version;
		hdr.filenum   = phdrSrc->filenum;
		hdr.imagesize = phdrSrc->imagesize;
		hdr.Offset    = phdrSrc->Offset;
	}
	else if( version == enVersion1_0_2 )
	{
		//ver2
		StPackHeader2 *phdrSrc;
		phdrSrc = (StPackHeader2*)&m_pFileImage[0];
		hdr.name[0] = 'D';
		hdr.name[1] = 'R';
		hdr.name[2] = 'Q';
		hdr.name[3] = '2';
		hdr.version = phdrSrc->version;
		hdr.filenum = phdrSrc->filenum;
		hdr.imagesize = phdrSrc->imagesize;
		hdr.Offset = phdrSrc->Offset;
	}

	m_pStHeader = &hdr;//(StPackHeader*)&m_pFileImage[0];

	//ファイル数、バージョンを記録
	m_sFileNum = m_pStHeader->filenum;
	m_sVersion = m_pStHeader->version;

	//ディスクイメージの先頭アドレス
	Uint8 *pImage = &m_pFileImage[m_pStHeader->Offset];

	//ファイルのアドレスを登録
	sPos = 16; 
	sImgPos = 0;

	for(Sint32 ii=0;ii<m_sFileNum;ii++)
	{
		m_stFile[ii].pFileAddr = &pImage[sImgPos];
		m_stFile[ii].pFileName = (gxChar*)&m_pFileImage[sPos+16];
		m_stFile[ii].Size      = *((Uint32*)(&m_pFileImage[sPos+4]));

		sImgPos += *((Uint32*)&m_pFileImage[sPos+0]);

		sPos    += (16+   (*(Uint32*)&m_pFileImage[sPos+8]));	//リスト位置を更新
	}

	return gxTrue;
}


void CGXImage::Decode( Uint8 *p ,Uint32 sz)
{
	//-------------------------------------
	//XOR暗号化
	//-------------------------------------
	
	for(Uint32 ii=0;ii<sz;ii++)
	{
		p[ii] ^= m_uMask;
	}

}

Uint32 CGXImage::DecodeAsync( Uint8 *p ,Uint32 sz,Uint32 sPos)
{
	//-------------------------------------
	//XOR暗号化
	//-------------------------------------
	
	for(Uint32 ii=0;ii<GXI_READ_SIZE*8;ii++)
	{
		if(sPos >= sz)
		{
			return sz;
		}

		p[sPos] ^= m_uMask;
		sPos ++;
		m_NowWork ++;
	}

	return sPos;
}



Uint8* CGXImage::GetFile( gxChar* pFileName , Uint32 *pSize )
{

#ifdef _NO_GXI_IMAGE_

	if( isFileExist(pFileName) )
	{
		Sint32 id = getFileID(pFileName);

		*pSize = m_pFileList[ id ].uSize;

		return m_pFileList[ id ].pData;
	}
	else
	{
		sprintf( m_pFileList[ m_sFileListNum ].Name , "%s", pFileName );

	    gxUtil::StrUpr( m_pFileList[ m_sFileListNum ].Name );

		Uint32 uSize;
		Uint8 *pData;

		gxChar *pName = GetFullPathFileName(m_pFileList[m_sFileListNum].Name);

        gxUtil::StrUpr( pName );

		pData = gxLib::LoadFile(pName, &uSize );

		if( pData )
		{
			*pSize = uSize;

			m_pFileList[ m_sFileListNum ].pData = pData;
			m_pFileList[ m_sFileListNum ].uSize = uSize;
			m_sFileListNum ++;

			return pData;
		}
	}

	return NULL;
#endif


	Sint32 n;

	n = getIndex( pFileName );

	if( n < 0 )
	{
		*pSize = 0;
#ifdef GX_DEBUG
		GX_DEBUGLOG( "[CGXImage]File not Found 「%s」がないようです",pFileName);
#endif

		//「\\」に変えて再チャレンジ
		size_t len = strlen( pFileName );
		gxChar *pBuf = new gxChar[ len+1 ];
		sprintf( pBuf ,"%s" , pFileName );

		for( Sint32 ii=0; ii<len; ii++ )
		{
			if( pBuf[ii] == '/' )
			{
				pBuf[ii] = '\\';
			}
		}
		n = getIndex( pBuf );

		SAFE_DELETES( pBuf );

		if( n < 0 )
		{
			return NULL;
		}
	}
	if( pSize)
	{
		*pSize = m_stFile[n].Size;
	}
#ifdef GX_DEBUG
		GX_DEBUGLOG( "[CGXImage]File Found「%s」を読み込みました",pFileName );
#endif

	return (Uint8*)(m_stFile[n].pFileAddr);
}


gxChar *CGXImage::GetFullPathFileName(gxChar *fileName)
{
	gxChar temp[FILENAMEBUF_LENGTH];

	if (m_ZettaiPath[0])
	{
		sprintf(temp, "%s\\%s", m_ZettaiPath, fileName);
	}
	else
	{
		sprintf(temp, "%s", fileName);
	}

	Sint32 len = strlen(temp);
	static gxChar name[FILENAMEBUF_LENGTH];
	Sint32 num = 0;

	for (Uint32 ii = 0; ii<len; ii++)
	{
		if (temp[num] == '\\')
		{
			name[ii] = '/';
			name[ii + 1] = 0x00;
			num += 1;
			continue;
		}
		name[ii] = temp[num];
		name[ii + 1] = 0x00;
		num++;
	}


	return name;
}


Sint32 CGXImage::getFileID( gxChar *pFileName )
{
	gxChar temp[FILENAMEBUF_LENGTH];

	sprintf( temp , "%s" , pFileName );
	gxUtil::StrUpper( temp );

	for( Sint32 ii=0; ii<m_sFileListNum; ii++ )
	{
		if( m_pFileList[ ii ].Name[0] == 0x00 ) continue;

		if( strcmp( m_pFileList[ ii ].Name , temp ) == 0 )
		{
			return ii;
		}
	}

	return -1;
}

gxBool CGXImage::SetTexture( Sint32 pg , char* pName )
{
	Uint32 uSize;

	Uint8 *p = GetFile( pName , &uSize );

	gxLib::ReadTexture( pg , p , uSize , 0xff00ff00 );

	return gxTrue;
}


gxBool CGXImage::DeleteFile( gxChar *pFileName )
{
	Sint32 id = getFileID( pFileName );

	if( m_pFileList[ id ].Clear() )
	{
//		if( m_sFileListNum > 0 ) m_sFileListNum --;
		return gxTrue;
	}
	return gxFalse;
}


void CGXImage::InfoMake(gxChar *filename)
{
	//-------------------------------------
	//ディスクイメージ情報を書き出す
	//-------------------------------------
	Load( filename , enVersion_AutoDetect , m_uMask );

//	sprintf(g_NameBuf,"%s", filename );
	gxUtil::GetFileNameWithoutExt( filename , g_NameBuf);
	sprintf(g_NameBuf2,"%s.txt", g_NameBuf );

	Analyse();

	FILE *fp = fopen( g_NameBuf2,"wt");

	fprintf(fp,"ファイル名[%s]\n",filename);
	fprintf(fp,"ファイル数[%d]\n",m_sFileNum);

	for(Sint32 ii=0;ii<m_sFileNum;ii++)
	{
		fprintf(fp,"ID%03d : [%8d(%5dKB)] / [%s]\n",ii,m_stFile[ii].Size,m_stFile[ii].Size/1024,m_stFile[ii].pFileName);
	}

	fprintf(fp,"[EOF]");

	fclose(fp);
}

gxBool CGXImage::Save(gxChar* pSaveName)
{
#ifdef ENABLE_WINDOWS_TOOL
	//-----------------------------------------------
	//イメージファイルを作成する
	//-----------------------------------------------

	//---------------------------------------
	//ヘッダ情報作成
	//---------------------------------------

	StPackHeader2 stHead;
	stHead.name[0]  = 'D';
	stHead.name[1]  = 'R';
	stHead.name[2]  = 'Q';
	stHead.name[3]  = '2';
	stHead.version  = enVersionSaisin;
	stHead.filenum  = m_sAddFileNum;
	stHead.imagesize = 0;

	//---------------------------------------
	//ファイル情報書き込み
	//---------------------------------------

	for(Sint32 ii=0;ii<m_sAddFileNum;ii++)
	{
		int fh;
		struct stat filestat;

		sprintf( g_NameBuf,"%s\\%s",m_ZettaiPath,m_pStFileInfo[ii]->FileName );
		fh = open( g_NameBuf , O_RDONLY|O_BINARY );
		if( fh < 0 )
		{
			//---------------------------------------
			//ファイル読み込みの失敗
			//---------------------------------------

			return gxFalse;
		}
		else
		{
			//---------------------------------------
			//ファイルイメージの読み込み
			//---------------------------------------
			Sint32 ret = 1;
			Sint32 pos = 0;

			fstat( fh , &filestat );

			m_pStFileInfo[ii]->FileSize   = filestat.st_size;
			m_pStFileInfo[ii]->DiskSize   = filestat.st_size+( 4-(m_pStFileInfo[ii]->FileSize%4) );
			m_pStFileInfo[ii]->pFileImage = (gxChar*)malloc( m_pStFileInfo[ii]->DiskSize );	//�m�A

			ret = read( fh , &m_pStFileInfo[ii]->pFileImage[ pos ] , filestat.st_size );
			close( fh );
		}

		stHead.imagesize += m_pStFileInfo[ii]->DiskSize;
	}

    //-------------------------------------------
	//実際のファイル書き込み
	//-------------------------------------------
	int fh;

	//-------------------------------------------
	//リストファイルの作成
	//-------------------------------------------
	Uint32 sOffset= 0;
	Sint32 sDummy = 0;

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempList.bin" );
	fh = open( g_NameBuf4,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

	for( Sint32 ii=0; ii<m_sAddFileNum; ii++ )
	{
		write( fh , &m_pStFileInfo[ii]->DiskSize , 4);
		write( fh , &m_pStFileInfo[ii]->FileSize , 4);
		write( fh , &m_pStFileInfo[ii]->NameSize , 4);
		write( fh , &sDummy , 4);
		write( fh , &m_pStFileInfo[ii]->FileName , m_pStFileInfo[ii]->NameSize );

		sOffset += ( 4+4+4+4+m_pStFileInfo[ii]->NameSize );
	}

	close(fh);

	//-------------------------------------------
	//データファイルの作成
	//-------------------------------------------

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempData.bin" );
	fh = open( g_NameBuf4 ,O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE);

	for( Sint32 ii=0; ii<m_sAddFileNum; ii++ )
	{
		write( fh , m_pStFileInfo[ii]->pFileImage , m_pStFileInfo[ii]->DiskSize );

		delete[] m_pStFileInfo[ii]->pFileImage;	//解放A
	}

	close(fh);

	//-------------------------------------------
	//ヘッダファイルの作成
	//-------------------------------------------

	stHead.Offset = sizeof(stHead)+sOffset;

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempHead.bin" );
	fh = open( g_NameBuf4 , O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE );

	write( fh , &stHead , sizeof(stHead) );

	close(fh);


	Uint8 *pBuf;

	//-----------------------------------------------------
	//結合ファイル作成
	//-----------------------------------------------------
	Sint32 fh2;
	struct stat FileStat2;
	fh2 = open( pSaveName , O_WRONLY|O_BINARY|O_TRUNC|O_CREAT,S_IREAD|S_IWRITE );

	Uint32 sPos = 0;
	Uint32 sRet = 1;

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempHead.bin" );
	fh = open( g_NameBuf4 , O_RDONLY|O_BINARY );
	fstat( fh , &FileStat2 );

	pBuf = new Uint8[ FileStat2.st_size ];

	sRet = read( fh , &pBuf[ sPos ] , FileStat2.st_size );
	close( fh );

	Decode(pBuf,FileStat2.st_size);
	write( fh2 , pBuf , FileStat2.st_size );

	delete[] pBuf;

	//---------------------------------------------------

	sPos = 0;
	sRet = 1;

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempList.bin" );
	fh = open( g_NameBuf4 , O_RDONLY|O_BINARY );
	fstat( fh , &FileStat2 );

	pBuf = new Uint8[ FileStat2.st_size ];

	sRet = read( fh , &pBuf[ sPos ] , FileStat2.st_size );
	close( fh );

	Decode(pBuf,FileStat2.st_size);
	write( fh2 , pBuf , FileStat2.st_size );

	delete[] pBuf;

	//---------------------------------------------------

	sPos = 0;
	sRet = 1;

	sprintf(g_NameBuf4,"%s\\..\\%s",m_ZettaiPath,"TempData.bin" );
	fh = open( g_NameBuf4 , O_RDONLY|O_BINARY );
	fstat( fh , &FileStat2 );

	pBuf = new Uint8[ FileStat2.st_size ];//(gxChar*)malloc( FileStat2.st_size );

	sRet = read( fh , &pBuf[ sPos ] , FileStat2.st_size );
	close( fh );

	//暗号化
	Decode(pBuf,FileStat2.st_size);
	write( fh2 , pBuf , FileStat2.st_size );

	delete[] pBuf;

	close( fh2 );

	if( 0 )
	{
/*
		// Blowfish暗号化
		Uint8* pTemp = NULL;
		Uint32 uSize = 0;
		Uint8* pWrite = NULL;
		Uint32 uWriteSize = 0;

		pTemp = gxLib::LoadFile( pSaveName, &uSize );
		//pWrite = pTemp;//毒 gxUtil::FileEncryption(pTemp, uSize, &uWriteSize);

		gxLib::SaveFile( pSaveName, pWrite, uWriteSize );

		delete[] pTemp;
		//delete[] pWrite;
*/
	}
    

#endif
	return gxTrue;

}


void CGXImage::Directory(gxChar *path)
{
	//-------------------------------------
	//ディレクトリ捜索
	//-------------------------------------
#ifdef ENABLE_WINDOWS_TOOL

#endif

}


#ifdef ENABLE_WINDOWS_TOOL

#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <imagehlp.h>
#pragma comment(lib, "imagehlp.lib")

gxBool decompressGXI(gxChar *pFileName);
gxBool compress( gxChar *pFileName );

void DirectoryExplore( WCHAR *path );

gxChar g_pStatus[1024]={0};

CGXImage *g_pImg = NULL;

gxBool decompressGXI(gxChar *pFileName)
{
	//GXIを展開する
	Uint32 uSize = 0;
	Uint8* pData = NULL;

	pData = gxLib::LoadFile(pFileName, &uSize);

	if (pData == NULL )
	{
		//ファイルが読み取れない
		return gxFalse;
	}

	gxChar KeyCode[4] = { 0 };
	Uint32 Pass = 0x00;
	gxBool bFound = gxFalse;
	for (Uint32 ii = 0; ii < 255; ii++)
	{
		//解除キーを探す
		Pass = ii;
		KeyCode[0] = pData[0] ^ Pass;
		KeyCode[1] = pData[1] ^ Pass;
		KeyCode[2] = pData[2] ^ Pass;
		KeyCode[3] = 0x00;

		if (strcmp(KeyCode, "DRQ") == 0)
		{
			bFound = gxTrue;
			break;
		}
		if (strcmp(KeyCode, "GXI") == 0)
		{
			bFound = gxTrue;
			break;
		}
	}

	if( !bFound )
	{
		//解除キーが見つからなかった？
		return gxFalse;
	}


	CGXImage *pImg = new CGXImage();
	pImg->Load( pFileName , CGXImage::enVersion_AutoDetect , Pass);


	//解析情報を作成する
	pImg->InfoMake(pFileName);

	//実ファイルを展開する
	//※フォルダは自動作成される

	gxChar CurrentPath[512] = { 0 };
	gxChar TargetPath[512] = { 0 };
	gxChar TempPath[512] = { 0 };

	gxUtil::GetFileNameWithoutExt(pFileName , CurrentPath );

	Uint32 max = pImg->m_sFileNum;

	for (Sint32 ii = 0; ii < max; ii++)
	{
		gxUtil::GetPath( pImg->m_stFile[ii].pFileName , TargetPath );

		//該当のフォルダ構成を再現する
		sprintf(TempPath, "%s\\%s\\", CurrentPath, TargetPath);
		MakeSureDirectoryPathExists(TempPath);

		//ファイルを配置する
		sprintf(TargetPath, "%s", pImg->m_stFile[ii].pFileName);
		sprintf(TempPath, "%s\\%s", CurrentPath, TargetPath);

		gxLib::SaveFile(TempPath, pImg->m_stFile[ii].pFileAddr, pImg->m_stFile[ii].Size);
	}

	SAFE_DELETE( pImg );

	return gxTrue;
}


gxBool compressGXI( gxChar *pFileName )
{
	setlocale(LC_ALL, "ja_JP.Shift_JIS");

	g_pImg = new CGXImage();
	g_pImg->SetPath(pFileName);

	WCHAR fileName[512];
	mbstowcs(fileName, pFileName, 512);
	DirectoryExplore(fileName);

	gxChar OutFileName[512];
	gxChar OutName[512];
	gxChar OutPath[512];

	sprintf(OutFileName , "%s.GXI" , pFileName );
	g_pImg->Save(OutFileName);

	SAFE_DELETE( g_pImg );

	return gxTrue;
}


void DirectoryExplore( WCHAR *path )
{
	//-------------------------------------
	//ディレクトリ捜索
	//-------------------------------------
	WCHAR subpath[_MAX_PATH];
    WIN32_FIND_DATA lp;
	WCHAR filepath[_MAX_PATH];
	gxChar addpath[_MAX_PATH];

	wsprintf(filepath,L"%s\\*",path);

    HANDLE h = FindFirstFile(filepath,&lp);

	if(h == INVALID_HANDLE_VALUE)
	{
		printf("検索失敗\n");
		return; 
	}

	do{
		if((lp.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& strcmp((const gxChar*)lp.cFileName,"..")!=0 && strcmp((const gxChar*)lp.cFileName,".")!=0) {
		    wsprintf(subpath,L"%s\\%s",path,lp.cFileName);
		    DirectoryExplore(subpath);
		}

		if((lp.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)!=FILE_ATTRIBUTE_DIRECTORY)
		{
			//ファイルだった場合

			wsprintf(filepath,L"%s\\%s",path,lp.cFileName);
			wcstombs(addpath , filepath , _MAX_PATH );
			g_pImg->Add( addpath );
		}
	} while(FindNextFile(h,&lp));
 
    FindClose(h);
}

//-----------------------------------------------------------
//サンプルコード
//-----------------------------------------------------------

void GXISample(gxChar *pFileName)
{
	gxChar ext[512];

	gxUtil::GetExt(pFileName, ext);
	gxUtil::StrUpr(ext);
	if( strcmp( ext , ".DRQ" ) == 0 || (strcmp(ext, ".GXI") == 0) )
	{
		sprintf( g_pStatus , "DRQ Decode" );

		if( decompressGXI(pFileName ) )
		{
			sprintf( g_pStatus , "DRQ Decode Success" );
		}
		else
		{
			sprintf( g_pStatus , "DRQ Decode Failed!" );
		}
	}
	else
	{
		compressGXI(pFileName);
	}

}

#endif
