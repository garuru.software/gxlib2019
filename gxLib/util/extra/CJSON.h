﻿#ifndef CJSON_H_
#define CJSON_H_

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/pointer.h"
#include "rapidjson/stringbuffer.h"
#include "CTxt.h"

class CJson
{
public:

	struct DirectoryInfo
	{
		enum ValueType {
			eTypeNoData,
			eTypeNull,
			eTypeArrayIndex,
			eTypeObjName,
			eTypeObject,
			eTypeArray,
			eTypeTrue,
			eTypeFalse,
			eTypeInt,
			eTypeUint,
			eTypeInt64,
			eTypeUint64,
			eTypeFloat,
			eTypeDouble,
			eTypeString,
		};

		std::vector<DirectoryInfo> child;
		std::string name;
		ValueType type = eTypeNoData;
		void Clear()
		{
			type = eTypeNoData;
			name = "";
			child.clear();
		}

		void AddStringValue(std::string name, std::string value)
		{
			DirectoryInfo info;
			DirectoryInfo dat;
			info.type = eTypeObjName;
			info.name = name;
			dat.type = eTypeString;
			dat.name = "\""+value+"\"";
			info.child.push_back(dat);
			child.push_back(info);
		}

		void AddIntValue(std::string name, std::string value)
		{
			DirectoryInfo info;
			DirectoryInfo dat;
			info.type = eTypeObjName;
			info.name = name;
			dat.type = eTypeInt;
			dat.name = value;
			info.child.push_back(dat);
			child.push_back(info);
		}
	};

	CJson()
	{
	}

	~CJson()
	{
	}
	
	void Test();

	gxBool Load(std::string fileName)
	{
		size_t uSize = 0;
		Uint8 *pData = gxLib::LoadFile( fileName.c_str() , &uSize );

		char*pData2 = new char[uSize+1];
		gxUtil::MemCpy(pData2 , pData , uSize );
		pData2[uSize] = 0x00;

		Read(pData2,uSize);

		SAFE_DELETES(pData);
		SAFE_DELETES(pData2);

		return gxTrue;
	}

	gxBool Read( char *pData , size_t uSize )
	{
		std::string src = "";
		src = (char*)pData;
		m_ParsedDoc.Parse(src.c_str());

		if (m_ParsedDoc.HasParseError())
		{
			auto err = m_ParsedDoc.GetParseError();

			size_t line = m_ParsedDoc.GetErrorOffset();

			switch (err) {
			case rapidjson::kParseErrorNone:	                        //!< No error.
				break;
			case rapidjson::kParseErrorDocumentEmpty:
				gxLib::DebugLog("The document is empty.(%d)",line);
				break;
			case rapidjson::kParseErrorDocumentRootNotSingular:
				gxLib::DebugLog("The document root must not follow by other values.(%d)", line); 
				break;

			case rapidjson::kParseErrorValueInvalid:
				gxLib::DebugLog("Invalid value.(%d)", line); 
				break;

			case rapidjson::kParseErrorObjectMissName:
				gxLib::DebugLog("Missing a name for object member.(%d)", line);
				break;
			case rapidjson::kParseErrorObjectMissColon:
				gxLib::DebugLog("Missing a colon after a name of object member.(%d)", line); break;
			case rapidjson::kParseErrorObjectMissCommaOrCurlyBracket:
				gxLib::DebugLog("Missing a comma or '}' after an object member.(%d)", line); break;

			case rapidjson::kParseErrorArrayMissCommaOrSquareBracket:
				gxLib::DebugLog("Missing a comma or ']' after an array element.(%d)", line); break;

			case rapidjson::kParseErrorStringUnicodeEscapeInvalidHex:
				gxLib::DebugLog("Incorrect hex digit after \\u escape in string.(%d)", line); break;
			case rapidjson::kParseErrorStringUnicodeSurrogateInvalid:
				gxLib::DebugLog("The surrogate pair in string is invalid.(%d)", line); break;
			case rapidjson::kParseErrorStringEscapeInvalid:
				gxLib::DebugLog("Invalid escape character in string.(%d)", line); break;
			case rapidjson::kParseErrorStringMissQuotationMark:
				gxLib::DebugLog("Missing a closing quotation mark in string.(%d)", line); break;
			case rapidjson::kParseErrorStringInvalidEncoding:
				gxLib::DebugLog("Invalid encoding in string.(%d)", line); break;

			case rapidjson::kParseErrorNumberTooBig:
				gxLib::DebugLog("Number too big to be stored in double.(%d)", line); break;
			case rapidjson::kParseErrorNumberMissFraction:
				gxLib::DebugLog("Miss fraction part in number.(%d)", line); break;
			case rapidjson::kParseErrorNumberMissExponent:
				gxLib::DebugLog("Miss exponent in number.(%d)", line); break;

			case rapidjson::kParseErrorTermination:
				gxLib::DebugLog("Parsing was terminated.(%d)", line); break;
			case rapidjson::kParseErrorUnspecificSyntaxError:
				gxLib::DebugLog("Unspecific syntax error.(%d)", line); break;
			default:
				break;
			}
		}
		else
		{
			m_Dir.Clear();
			rapidjson::Type type = m_ParsedDoc.GetType();

			if (type == rapidjson::kArrayType)
			{
				size_t max = m_ParsedDoc.Size();
				m_Dir.type = DirectoryInfo::eTypeArray;
				for (rapidjson::SizeType i = 0; i < max; i++)
				{
					rapidjson::Value& val = m_ParsedDoc[i];
					DirectoryInfo d;
					m_Dir.child.push_back(d);
					listup(m_Dir.child[i], val);
				}
			}
			else
			{
				rapidjson::Value& val = m_ParsedDoc;
				listup(m_Dir, val);
			}
		}

		return gxTrue;
	}

	void DispDebugInfo();
	void Print();

	void Save(std::string fileName)
	{
		outputJSONText(m_Dir);
		m_Txt.Save(fileName);
	}

	void SaveInfo(std::string fileName);

	//void SetObject( DirectoryInfo info , )
	//{
	//
	//}

	gxBool AddStringValue(DirectoryInfo& info, std::string name , std::string string)
	{
		if (info.type == DirectoryInfo::eTypeObject)
		{
			DirectoryInfo d2,d3;
			d3.type = DirectoryInfo::eTypeString;
			d3.name = "\"";
			d3.name += string;
			d3.name += "\"";

			d2.type = DirectoryInfo::eTypeObjName;
			d2.name = "\"";
			d2.name += name;
			d2.name += "\"";
			d2.child.push_back(d3);

			info.child.push_back(d2);

			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetValue(DirectoryInfo &info,std::string string )
	{
		if (info.type == DirectoryInfo::eTypeObjName)
		{
			DirectoryInfo &dir2 = info.child[0];
			dir2.type = DirectoryInfo::eTypeString;
			dir2.name = "\"";
			dir2.name += string;
			dir2.name += "\"";
			return gxTrue;
		}
		return gxFalse;

	}

	gxBool SetValue(DirectoryInfo &info, Float32 value)
	{
		if (info.type == DirectoryInfo::eTypeObjName)
		{
			DirectoryInfo &dir2 = info.child[0];
			dir2.type = DirectoryInfo::eTypeFloat;
			char buf[256];
			sprintf(buf, "%.8f", value);
			dir2.name = buf;
			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetValue(DirectoryInfo &info, Sint32 value)
	{
		if (info.type == DirectoryInfo::eTypeObjName)
		{
			DirectoryInfo &dir2 = info.child[0];
			dir2.type = DirectoryInfo::eTypeInt;
			char buf[256];
			sprintf(buf, "%d", value);
			dir2.name = buf;
			return gxTrue;
		}
		return gxFalse;
	}

	gxBool SetObjectValue(DirectoryInfo& info, std::string name , Sint32 value)
	{
		std::string srcName = "\"" + name + "\"";

		if (info.type == DirectoryInfo::eTypeObject)
		{
			for (size_t ii = 0; ii < info.child.size(); ii++)
			{
				DirectoryInfo& dir2 = info.child[ii];
				if (dir2.name == srcName)
				{
					SetValue(dir2, value);
					break;
				}
			}

			return gxTrue;
		}
		return gxFalse;
	}

	gxBool GetObjectValue(DirectoryInfo& info, std::string name , std::string &ret )
	{
		std::string srcName = "\"" + name + "\"";

		if (info.type == DirectoryInfo::eTypeObject)
		{
			for (size_t ii = 0; ii < info.child.size(); ii++)
			{
				DirectoryInfo& dir2 = info.child[ii];
				if (dir2.name == srcName)
				{
					if(dir2.type == DirectoryInfo::eTypeString) 
					ret = dir2.child[0].name;
					return gxTrue;
				}
			}

		}
		return gxFalse;
	}

	DirectoryInfo& Get()
	{
		return m_Dir;
	}

private:

	void listup(DirectoryInfo& d, rapidjson::Value& val);

	DirectoryInfo& addDir(DirectoryInfo& d, std::string str, DirectoryInfo::ValueType type)
	{
		DirectoryInfo d2;
		d2.type = type;
		d2.name = str;
		d.child.push_back(d2);

		return d.child.back();
	}

	//JSON情報出力
	void outputJSONText(DirectoryInfo& d);
	std::string getString(rapidjson::Value& s, DirectoryInfo::ValueType* pType = nullptr);
	rapidjson::Value& parseObjectName(std::string key);

	//Debug情報出力
	void outputDirInfo(DirectoryInfo& info , gxBool NoTab = gxFalse);

	std::vector<std::string> splitstring(std::string string);
	std::string getPaths(std::string path);

	rapidjson::Document m_ParsedDoc;
	DirectoryInfo m_Dir;
	CTxt m_Txt;
};

#endif
