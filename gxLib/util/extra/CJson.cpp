//------------------------------------------------------------------
//
//JSONデータをVECTOR配列に詰めなおす
//
//------------------------------------------------------------------
#include <gxLib.h>
#include "CJson.h"
#include "CTxt.h"

Sint32 m_Indent = 0;

void CJson::listup(DirectoryInfo& d, rapidjson::Value& val)
{
	static char buf[256];

	if (val.IsArray())
	{
		size_t max = val.Size();
		size_t ii = 0;
		d.type = DirectoryInfo::eTypeArray;

		for (size_t ii = 0; ii < max; ii++)
		{

			std::string temp = "";// d.name;
			temp += "[";
			temp += itoa(ii, buf, 10);
			temp += "]";
			DirectoryInfo& d2 = addDir(d, temp, DirectoryInfo::eTypeArrayIndex);

			listup(d2, val[ii]);
		}
	}
	else if (val.IsObject())
	{
		size_t num = val.MemberCount();
		Sint32 index = 0;

		DirectoryInfo& d2 = addDir(d, "<object>", DirectoryInfo::eTypeObject);

		for (auto itr = val.MemberBegin(); itr != val.MemberEnd(); ++itr)
		{
			if (val.HasMember(itr->name))
			{
				const char* name = itr->name.GetString();

				std::string temp = "\"";
				temp += name;
				temp += "\"";

				if (num == 1)
				{
					std::string temp = "\"";
					temp += name;
					temp += "\"";
					DirectoryInfo& d3 = addDir(d2, temp, DirectoryInfo::eTypeObjName);
					listup(d3, val[name]);
				}
				else
				{
					std::string temp = "\"";
					temp += name;// itoa(index, buf, 10);
					temp += "\"";
					DirectoryInfo& d3 = addDir(d2, temp, DirectoryInfo::eTypeObjName);
					listup(d3, val[name]);
				}
			}
			index++;
		}
	}
	else
	{
		DirectoryInfo::ValueType type = DirectoryInfo::eTypeNull;
		std::string str = getString(val, &type).c_str();
		addDir(d, str.c_str(), type);
	}
}


std::string CJson::getString(rapidjson::Value& s, DirectoryInfo::ValueType* pType)
{
	char buf[256];

	std::string ret;

	DirectoryInfo::ValueType type = DirectoryInfo::eTypeNull;

	if (s.IsNumber())
	{
		if (s.IsInt())
		{
			type = DirectoryInfo::eTypeInt;
			sprintf(buf, "%d", s.GetInt());
			ret = buf;
		}
		else if (s.IsUint())
		{
			type = DirectoryInfo::eTypeUint;
			sprintf(buf, "%ud", s.GetUint());
			ret = buf;
		}
		else if (s.IsInt64())
		{
			type = DirectoryInfo::eTypeInt64;
			sprintf(buf, "%d", s.GetInt64());
			ret = buf;
		}
		else if (s.IsUint64())
		{
			type = DirectoryInfo::eTypeUint64;
			sprintf(buf, "%ud", s.GetUint64());
			ret = buf;
		}
		else if (s.IsFloat())
		{
			type = DirectoryInfo::eTypeFloat;
			sprintf(buf, "%f", s.GetFloat());
			ret = buf;
		}
		else if (s.IsDouble())
		{
			type = DirectoryInfo::eTypeDouble;
			sprintf(buf, "%lf", s.GetDouble());
			ret = buf;
		}
	}
	else
	{
		if (s.IsString())
		{
			type = DirectoryInfo::eTypeString;
			ret = '\"';
			ret += s.GetString();
			ret += '\"';
		}
		else if (s.IsTrue())
		{
			type = DirectoryInfo::eTypeTrue;
			ret = "true";
		}
		else if (s.IsFalse())
		{
			type = DirectoryInfo::eTypeFalse;
			ret = "false";
		}
		else if (s.IsNull())
		{
			type = DirectoryInfo::eTypeNull;
			ret = "null";
		}
	}
	if (pType) *pType = type;

	return ret;
}

//--------------------------------------------------------------------
//ディレクトリ構造可視化用
//--------------------------------------------------------------------

void CJson::DispDebugInfo()
{
	m_Txt.Clear();
	m_Indent = 0;
	outputDirInfo(m_Dir);
	m_Txt.OutPutText();
	//m_Txt.Save("test.txt");
}

void CJson::SaveInfo( std::string fileName )
{
	m_Txt.Clear();
	m_Indent = 0;
	outputDirInfo(m_Dir);
	m_Txt.Save(fileName);
}

void CJson::Print()
{
	outputJSONText(m_Dir);
	m_Txt.OutPutText();
}

void CJson::outputDirInfo(DirectoryInfo &info, gxBool bNoTab)
{
	//デバッグ用（解析情報を表示）
	static const char* TypeName[] = {
		"eTypeNoData",
		"eTypeNull",
		"eTypeArrayIndex",
		"eTypeObjName",
		"eTypeObject",
		"eTypeArray",
		"eTypeTrue",
		"eTypeFalse",
		"eTypeInt",
		"eTypeUint",
		"eTypeInt64",
		"eTypeUint64",
		"eTypeFloat",
		"eTypeDouble",
		"eTypeString",
	};
	//printf("[%s:%s]\r\n", TypeName[info.type],info.name.c_str());

	static std::vector<Sint32> arrayIndex;

	if( !bNoTab )
	{
		if( arrayIndex.size() > 0 )
		{
			for (Sint32 ii=0; ii < m_Indent; ii++)
			{
				bool bDraw = false;

				for (Sint32 jj=0; jj < arrayIndex.size(); jj++)
				{
					if( ii == abs(arrayIndex[jj]) )
					{
						bDraw = true;
						if (info.type == DirectoryInfo::eTypeArrayIndex)
						{
							if (arrayIndex[jj] < 0)
							{
								m_Txt.Add("-");
								arrayIndex.pop_back();

							}
							else if (jj+1 == arrayIndex.size())
							{
								m_Txt.Add("+");
							}
							else
							{
								m_Txt.Add("|");
							}
						}
						else
						{
							m_Txt.Add("|");
						}

					}
				}

				if(!bDraw)
				{
					m_Txt.Add("  ");
				}
			}
		}
		else
		{
			for (Sint32 ii = 0; ii < m_Indent; ii++)
			{
				m_Txt.Add("  ");
			}
		}
	}

	bNoTab = gxFalse;

	if (info.child.size() > 1)
	{
		if (info.type == DirectoryInfo::eTypeArray)
			arrayIndex.push_back( m_Indent );
	}

	//if (info.type == DirectoryInfo::eTypeArrayIndex)
	//{
	//	printf("|");
	//}

	if (info.child.size() == 1)
	{
		m_Txt.Add("%s:", info.name.c_str());
		bNoTab = gxTrue;
	}
	else
	{
		m_Txt.Add(" %s" LF, info.name.c_str());
	}

	m_Indent++;
	for (Sint32 ii = 0; ii < info.child.size(); ii++)
	{
		if (info.child.size() > 1)
		{
			if (info.type == DirectoryInfo::eTypeArray)
			{
				if (info.child.size()-1 == ii )
				{
					arrayIndex.back() = arrayIndex.back()*-1;
				}
			}
		}
		outputDirInfo(info.child[ii], bNoTab);
	}

	m_Indent--;


}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

void CJson::outputJSONText(DirectoryInfo &d)
{
	size_t num = d.child.size();

	switch(d.type){
	case DirectoryInfo::eTypeNoData:
		for (size_t ii = 0; ii < num; ii++)
		{
			outputJSONText(d.child[ii]);
		}
		break;

	case DirectoryInfo::eTypeObject:
	{
		if (num > 0)
		{
			m_Txt.AppendLine();
			m_Txt += "{";
			m_Txt.AppendLine();
			for (size_t ii = 0; ii < num; ii++)
			{
				outputJSONText(d.child[ii]);
				if (ii != (num - 1)) m_Txt += ",";
			}
			m_Txt += "}";
			m_Txt.AppendLine();
		}
	}
	break;

	case DirectoryInfo::eTypeObjName:
	{
			m_Txt += d.name;
			m_Txt += ":";
			if (num > 1)
			{
				m_Txt += "{";
				m_Txt.AppendLine();
			}
			for (size_t ii = 0; ii < num; ii++)
			{
				outputJSONText( d.child[ii] );
			}
			if (num > 1)
			{
				m_Txt += "}";
				m_Txt.AppendLine();
			}
	}
	break;

	case DirectoryInfo::eTypeArrayIndex:
	{
		for (size_t ii = 0; ii < num; ii++)
		{
			outputJSONText(d.child[ii]);
		}
	}
	break;

	case DirectoryInfo::eTypeArray:
	{
		m_Txt += d.name;
		m_Txt += ":[";
		for (size_t ii = 0; ii < num; ii++)
		{
			outputJSONText( d.child[ii] );
			if (ii != (num - 1)) m_Txt += ",";
		}
		m_Txt += "]";
		m_Txt.AppendLine();
	}
	break;

	case DirectoryInfo::eTypeNull:
	case DirectoryInfo::eTypeTrue:
	case DirectoryInfo::eTypeFalse:
	case DirectoryInfo::eTypeInt:
	case DirectoryInfo::eTypeUint:
	case DirectoryInfo::eTypeInt64:
	case DirectoryInfo::eTypeUint64:
	case DirectoryInfo::eTypeFloat:
	case DirectoryInfo::eTypeDouble:
	case DirectoryInfo::eTypeString:
		m_Txt += d.name;
		for (size_t ii = 0; ii < num; ii++)
		{
			outputJSONText(d.child[ii]);
		}
		break;
	}
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

std::vector<std::string> CJson::splitstring(std::string string)
{
	auto separator = std::string("/");          // 区切り文字
	auto separator_length = separator.length(); // 区切り文字の長さ

	std::vector<std::string> list;

	if (separator_length == 0)
	{
		list.push_back(string);
	}
	else
	{
		auto offset = std::string::size_type(0);
		while (true) {
			auto pos = string.find(separator, offset);
			if (pos == std::string::npos) {
				list.push_back(string.substr(offset));
				break;
			}
			list.push_back(string.substr(offset, pos - offset));
			offset = pos + separator_length;
		}
	}

	return list; // list == {"a", "b", "c"}
}

std::string CJson::getPaths(std::string path)
{
	path = path.erase(0, 1);
	path = path + "/";
	return path;
}

rapidjson::Value& CJson::parseObjectName(std::string key)
{
	rapidjson::Value s;


	auto list = splitstring(key);

	s = m_ParsedDoc[list[0].c_str()];
	for (Sint32 ii = 1; ii < list.size(); ii++)
	{
		if (s.IsArray())
		{
			s = s[0];
			s = s[list[ii].c_str()];
		}
		else if (s.IsObject())
		{
			s = s[list[ii].c_str()];
		}

	}

	auto r = getString(s);

	return s;
}


//--------------------------------------------------------------------
//--------------------------------------------------------------------

void CJson::Test()
{
	Load("sample.json");

	DispDebugInfo();

	DirectoryInfo &info = Get();
	AddStringValue(info.child[0], "n_noMember3", "stringData");

	DispDebugInfo();

	Print();

	Save("test.txt");
}
