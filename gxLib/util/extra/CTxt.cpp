
#include <gxLib.h>
#include <gxLib/util/CCsv.h>
#include "gxLib/util/CFileZip.h"
#include "CTxt.h"

void CTxt::InsertLine( Sint32 line , std::string text)
{
	gxBool bLFExist = gxFalse;
	size_t start = 0;
	size_t end = 0;

	if (text.size() == 0)
	{
		//空文字を渡された場合は行の追加のみ
		addLine(nullptr,0,line);
		return;
	}

	for (size_t ii = 0; ii < text.size(); ii++)
	{
		//改行がある場合は複数の行に分ける
		switch (text[ii]) {
		case '\n':
			bLFExist = gxTrue;
			break;
		case '\r':
			break;
		default:
			end++;
			break;
		}

		gxBool bStrEnd = (ii == (text.size() - 1)) ? true : false;
		if (bLFExist || bStrEnd)
		{
			line = (Sint32)addLine( (Uint8*)(&text[start]), end - start, line );
			bLFExist = gxFalse;
			start = ii + 1;
			end = start;
		}
	}
}


void CTxt::RemoveLine(size_t line)
{
	if (m_List.size() == 0) return;
	if (line < 0) return;
	if (line > m_List.size()) return;

	auto itr = std::next(m_List.begin(), line);
	if (itr != m_List.end())
	{
		m_List.erase(itr);
	}
}

void CTxt::ReplaceLine(size_t line, std::string str)
{
	RemoveLine(line);
	InsertLine((Sint32)line, str);
}

gxBool CTxt::Load(std::string fileName)
{
	Clear();

	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile((const char*)fileName.c_str(), &uSize);

	if (Read(pData, uSize))
	{
		m_FileName = fileName;
	}
	else
	{
		SAFE_DELETES(pData);
		return gxFalse;
	}

	SAFE_DELETES(pData);

	return gxTrue;
}


gxBool CTxt::Read( Uint8 *pData , size_t uSize)
{
	if (pData == nullptr) return gxFalse;

	printf("Now Loading...\r\n");

	if (!analysis(pData, uSize))
	{
		return gxFalse;
	}

	return gxTrue;
}

gxBool CTxt::Save(std::string fileName)
{
	size_t uSize = 0;
	std::string output;

	std::vector<Uint32> u32s;

	char buf[32] = {0};
	Sint32 len = sprintf(buf, "%s", LF);

	std::vector<Uint32> lf;
	for (Sint32 ii = 0; ii < len; ii++)
	{
		lf.push_back(buf[ii]);
	}

	for (auto itr = m_List.begin(); itr != m_List.end(); ++itr)
	{
		auto &ref = (*itr);
		u32s.insert(u32s.end(), ref.begin(), ref.end());
		u32s.insert(u32s.end(), lf.begin(), lf.end());
	}

	if (u32s.size() > 0)
	{
		output += gxUtil::UTF32toUTF8(u32s);
	}

	gxLib::SaveFile(fileName.c_str(), (void*)output.c_str(), output.size());

	return gxTrue;
}

void CTxt::GrepReplace(std::string src, std::string dst, size_t x, size_t y)
{

	for( size_t ii=y; ii< m_List.size(); ii++)
	{
		EditGrepReplace(0, ii, src , dst );
		printf("GrepReplace:%zd/%zd           \r", ii, m_List.size());
	}
	printf("\r\n");
}

void CTxt::EditGrepReplace(size_t x, size_t y, std::string src, std::string dst)
{
	CTxt::POS pos;
	pos.x = (Sint32)x;
	pos.y = (Sint32)y;
	while (true)
	{
		if (Search( src, &pos, pos.x, pos.y))
		{
			if (y != pos.y) break;
			EditReplace(pos.x, pos.y, src, dst);
			pos.x += (Sint32)(dst.size()+0);
		}
		else
		{
			break;
		}
	}
}

void CTxt::EditReplace(size_t x, size_t y, std::string words, std::string str)
{
	auto itr = std::next(m_List.begin(), y);

	size_t uSize = 0;
	std::vector<Uint32> words32 = gxUtil::UTF8toUTF32(words);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32line = (*itr);

		std::vector<Uint32> tmp;
		for (Sint32 ii = 0; ii < x; ii++)
		{
			tmp.push_back(u32line[ii]);
		}

		for (size_t ii = 0; ii < words32.size(); ii++)
		{
			if (u32line[x+ii] != words32[ii])
			{
				return;
			}
		}

		for (size_t ii=0; ii<str.size(); ii++)
		{
			tmp.push_back(str[ii]);
		}

		for (size_t ii = x+words32.size(); ii < u32line.size(); ii++)
		{
			tmp.push_back(u32line[ii]);
		}
		*itr = tmp;
	}
}

std::vector<Uint32> CTxt::getLine32(size_t y)
{
	auto itr = std::next(m_List.begin(), y);
	if (itr != m_List.end())
	{
		return *itr;
	}
	std::vector<Uint32> ret;

	return ret;
}

void CTxt::setLine32(size_t y , std::vector<Uint32> line)
{
	auto itr = std::next(m_List.begin(), y);

	if (itr != m_List.end())
	{
		*itr = line;
		return;
	}

	AppendLine();
	itr = std::next(m_List.begin(), y);
	if (itr != m_List.end())
	{
		*itr = line;
	}
}

void CTxt::EditDelete(size_t x, size_t y, size_t num)
{
	auto linestr = getLine32(y);

	std::vector<Uint32> dst;

	for (Sint32 ii = 0; ii < linestr.size(); ii++)
	{
		if (ii >= x && num == 0) break;
		if (ii >= x && ii < x + num) continue;
		dst.push_back(linestr[ii]);
	}

	setLine32(y, dst);
}


void CTxt::Add(char* buf, ...)
{
	gxChar temp[eTempBufMax];

	va_list app;
	va_start(app, buf);
	vsprintf(temp, buf, app);
	va_end(app);

	std::string tmpstr = temp;

	Add(tmpstr);
}

void CTxt::Add(std::string str)
{
	size_t y = GetLineNum();

	if (y > 0) y--;
	auto linestr = getLine32(y);

	std::vector<Uint32> str32 = gxUtil::UTF8toUTF32(str);

	for (Sint32 ii = 0; ii < str32.size(); ii++)
	{
		if (str32[ii] == '\r') continue;
		if (str32[ii] == '\n')
		{
			setLine32(y, linestr);
			linestr.clear();
			y++;
			continue;
		}
		linestr.push_back(str32[ii]);
	}

	setLine32(y, linestr);
}

void CTxt::EditInsert(size_t x, size_t y, std::string str)
{
	auto itr = std::next(m_List.begin(), y);

	size_t uSize = 0;
	std::vector<Uint32> dst = gxUtil::UTF8toUTF32(str);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32 = (*itr);

		std::vector<Uint32> tmp;
		for (Sint32 ii = 0; ii < x; ii++)
		{
			tmp.push_back(u32[ii]);
		}

		for (size_t ii = 0; ii < dst.size(); ii++)
		{
			tmp.push_back(dst[ii]);
		}

		for (size_t ii = x; ii < u32.size(); ii++)
		{
			tmp.push_back(u32[ii]);
		}
		*itr = tmp;
	}
}

std::string CTxt::GetLine(size_t line)
{
	auto itr = std::next(m_List.begin(), line);

	if (itr != m_List.end())
	{
		std::vector<Uint32> u32 = (*itr);

		if (u32.size() == 0) return "";

//テスト		u32.push_back(0);

		return gxUtil::UTF32toUTF8(u32);
	}

	return "";
}

gxBool CTxt::Search(std::string text, POS *pPos , size_t x, size_t y)
{
	POS pos;
	pos.x = (Sint32)x;
	pos.y = (Sint32)y;

	auto u32findStr = gxUtil::UTF8toUTF32(text);

	std::vector<Uint32> src32;
	Sint32 findIndex = -1;
	for (auto itr = std::next(m_List.begin(), pos.y); itr != m_List.end(); ++itr)
	{
		size_t sz = (*itr).size();
		src32.clear();

		for (Sint32 ii=pos.x; ii<sz ; ii++)
		{
			src32.push_back((*itr)[ii]);
		}

		findIndex = CTxtUtil::findString32(src32, u32findStr);
		if (findIndex >= 0)
		{
			pos.x = findIndex+pos.x;
			if (pPos) *pPos = pos;
			return gxTrue;
		}
		pos.x = 0;
		pos.y++;
	}
	pos.x = -1;
	pos.y = -1;
	if (pPos) *pPos = pos;

	return gxFalse;
}


std::vector<CTxt::POS> CTxt::Grep(std::string text, std::function<void(POS pos)>func)
{
	std::vector<POS> grepList;

	POS pos;

	while(gxTrue)
	{
		if (!Search(text, &pos, 0, pos.y))
		{
			break;
		}

		if (func)
		{
			func( pos );
		}

		grepList.push_back(pos);
		pos.y++;
	}

	return grepList;
}

gxBool CTxt::analysis(Uint8* pData, size_t size)
{
	char* p = new char[size + 1];

	if (p == nullptr)
	{
		return gxFalse;
	}
	else
	{
		if (size == 0)
		{
			return gxTrue;
		}
	}

	gxUtil::MemCpy(p, pData, size);
	p[size] = 0;


	m_List.clear();

	InsertLine(0, p );

	SAFE_DELETES(p);

	return gxTrue;
}

size_t CTxt::addLine(Uint8* pData, size_t size, Sint32 line)
{
	if (line < 0)
	{
		//末尾に追加する
		if (m_List.size() == 0)
		{
			//１行もなければ最初に追加
			line = 0;
		}
		else
		{
			//行が存在すれば最終行
			line = m_List.size();
		}
	}
	else
	{
		//指定行
		if (line > m_List.size())
		{
			line = m_List.size();
		}
	}

	if (size == 0 || pData == nullptr)
	{
		//空文字を受け渡された場合
		std::vector<Uint32> str;
		//str.emplace_back(0);
		//m_List.emplace_back(str);
		//return line+1;
		m_List.push_back(str);
		return m_List.size();
	}


	size_t firstLine = line;

	char* p = new char[size+1];
	memcpy(p, pData, size);
	p[size] = 0;

	std::string temp = p;

	SAFE_DELETES(p);

	if (temp.size() >= size)
	{
		temp.push_back('\0');
	}

	std::vector<Uint32> dst  = gxUtil::UTF8toUTF32(temp);

	auto itr = std::next(m_List.begin(), firstLine);
	m_List.insert(itr, dst);

//	std::vector<Uint32> append;
//
//	if (m_List.size() > firstLine)
//	{
//		for (size_t ii = 0; ii < m_List[firstLine].size(); ii++)
//		{
//			append.emplace_back(m_List[firstLine][ii]);
//		}
//	}
//		
//	for (size_t ii = 0; ii < dst.size(); ii++)
//	{
//		switch (dst[ii]) {
//		case '\r':
//		case '\n':
//			continue;
//		}
//		append.emplace_back(dst[ii]);
//	}
//
//	auto itr = std::next(m_List.begin(), firstLine);
//	m_List.insert(itr,append);
//
	
//#if 0
//	m_List.push_back(append);
//#else
//	if (firstLine > m_List.size()) return gxFalse;
//
//	RemoveLine(firstLine);
//	auto itr = std::next(m_List.begin(), firstLine);
//	m_List.insert(itr,append);
//
//#endif

	return m_List.size();
}


gxBool CTxt::LoadBin(std::string fileName)
{
	Clear();

	std::string text, comment;
	size_t uSize = 0;
	Uint8* pData = gxLib::LoadFile(fileName.c_str(), &uSize);

	char buf[1024] = { 0x00 };
	size_t addr = 0x00;
	if (pData)
	{
		Uint8 data = 0;
		char moji = 0x00;
		char mojibuf[3] = { 0x00 };
		char* pMoji = nullptr;
		for (size_t ii = 0; ii < uSize; ii += 16)
		{
			text = "";
			comment = "";
			text += "	";

			sprintf(buf, "	//%08zX", ii);
			comment += buf;

			for (Sint32 jj = 0; jj < 16; jj++)
			{
				sprintf(buf, "0x%02x,", pData[ii + jj]);
				text += buf;

				mojibuf[0] = '.';
				mojibuf[1] = 0x00;

				comment += mojibuf;
			}

			sprintf(buf, "%s%s", text.c_str(), comment.c_str());
			std::string binstr = buf;
			addLine((Uint8*)binstr.c_str(), binstr.size());

			addr += 16;

			if (addr + 16 >= uSize) break;
		}

		//端数を処理する

		text = "";
		comment = "";
		text += "	";

		sprintf(buf, "	//%08zX", addr);
		comment += buf;

		for (size_t jj = 0; jj < 16; jj++)
		{
			if (jj >= (uSize % 16))
			{
				sprintf(buf, "     ");
			}
			else
			{
				sprintf(buf, "0x%02x,", pData[addr + jj]);
			}

			text += buf;

			mojibuf[0] = moji;
			comment += mojibuf;
		}
		sprintf(buf, "%s%s", text.c_str(), comment.c_str());
		std::string binstr = buf;
		addLine((Uint8*)binstr.c_str(), binstr.size());
	}

	return gxTrue;
}

void CTxt::OutPutText()
{
	size_t uSize = 0;

	for (auto itr = m_List.begin(); itr != m_List.end(); ++itr)
	{
		std::vector<Uint32> u32s = *itr;
		std::string output;

		u32s.push_back(0);

		if (u32s.size() > 0)
		{
			output += gxUtil::UTF32toUTF8(u32s);
		}

		output += m_LF;
		printf( "%s",output.c_str() );
	}

}

std::string CTxt::ConvertString()
{
	std::string alltext;

	for (size_t ii = 0; ii < GetLineNum(); ii++)
	{
		alltext += GetLine(ii);
		alltext += m_LF;
	}

	return alltext;
}

void  CTxt::EditDeleteBefore(Sint32 x, Sint32 y)
{
	std::string line = GetLine(y);
	line = CTxtUtil::DeleteBefore(line, x);
	ReplaceLine(y, line);
}





void CTxt::AppendLine(char* buf, ...)
{
	gxChar temp[eTempBufMax];

	va_list app;
	va_start(app, buf);
	vsprintf(temp, buf, app);
	va_end(app);

	std::string tmpstr = temp;
	AppendLine(tmpstr);
}

void CTxt::AppendLine(std::string text)
{
	InsertLine(-1, text);
}

void CTxt::EditDeleteAfter(Sint32 x, Sint32 y)
{
	std::string line = GetLine(y);
	line = CTxtUtil::DeleteAfter(line, x);
	ReplaceLine(y, line);
}


//----------------------------------------------------------------------
//static
//----------------------------------------------------------------------

std::string CTxtUtil::DecodeURL(std::string url)
{
	size_t uSize = 0;
	std::vector<Uint32> words32 = gxUtil::UTF8toUTF32(url);
	std::vector<Uint32> buf32;

	for (Sint32 ii = 0; ii < words32.size(); ii++)
	{
		if (words32[ii] == '%')
		{
			char num[5];
			num[0] = '0';
			num[1] = 'x';
			num[2] = (char)words32[ii + 1];
			num[3] = (char)words32[ii + 2];
			num[4] = 0x00;
			double dn = atof(num);
			int n = (int)dn;
			buf32.push_back(n);
			ii += 2;
			continue;
		}
		buf32.push_back(words32[ii]);
	}
//テスト	buf32.push_back(0);
	std::string ret = gxUtil::UTF32toUTF8(buf32);

	return ret;
}


Sint32 CTxtUtil::FindFront(std::string src, std::string find, Sint32 x)
{
	std::vector<Uint32> src32  = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(find);

	return CTxtUtil::findString32(src32, find32, 1 , x );
}


Sint32 CTxtUtil::FindBack(std::string src, std::string find, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(find);

	return CTxtUtil::findString32(src32, find32, -1 , x);
}

std::string CTxtUtil::DeleteAfter(std::string src, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	gxBool bModeDelete = gxTrue;
	for (size_t ii = 0; ii <src32.size(); ii++)
	{
		if (ii >= x)
		{
			if (src[ii] == '\r' || src[ii] == '\n') bModeDelete = gxFalse;
			if( bModeDelete) continue;
		}

		dst32.push_back(src32[ii]);
	}

	//ここでターミネータを入れておかないとUTF32toUTF8で終了を判定できない
//テスト	dst32.push_back(0x00);

	printf("DeleteAfter\r\n");
	std::string ret8 = gxUtil::UTF32toUTF8(dst32);

	printf("DeleteAfter2=%s\r\n",ret8.c_str());
	return ret8;
}

std::string CTxtUtil::DeleteBefore(std::string src, Sint32 x)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	size_t start = 0;
	for (size_t ii = x; ii > 0; ii--)
	{
		if (ii > 0)
		{
			if (src[ii-1] == '\r' || src[ii-1] == '\n') start = ii;
		}
	}

	for (size_t ii=0; ii<start; ii++)
	{
		dst32.push_back(src32[ii]);
	}

	gxBool bModeDelete = gxTrue;
	for (size_t ii=x; ii<src32.size(); ii++)
	{
		dst32.push_back(src32[ii]);
	}

	std::string ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}


std::string CTxtUtil::InsertText(std::string src, Sint32 x, std::string addText)
{
	//挿入する

	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> add32 = gxUtil::UTF8toUTF32(addText);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < x; ii++)
	{
		dst32.push_back(src32[ii]);
	}

	for (size_t ii = 0; ii < add32.size(); ii++)
	{
		dst32.push_back(add32[ii]);
	}

	for (size_t ii = x; ii < src32.size(); ii++)
	{
		dst32.push_back(src32[ii]);
	}

	dst32.push_back(0);
	std::string ret8 = gxUtil::UTF32toUTF8(dst32);

	return ret8;
}


std::string CTxtUtil::DeleteText(std::string src, Sint32 x, Sint32 num)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < src32.size(); ii++)
	{
		if (ii >= x && ii < x+num )
		{
			continue;
		}

		dst32.push_back(src32[ii]);
	}

	std::string ret32;

	if (dst32.size() == 0)
	{
		return ret32;
	}
//テスト	dst32.push_back(0);
	ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}

std::string CTxtUtil::BackSpace(std::string src, Sint32 x, Sint32 num)
{
	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> dst32;

	for (size_t ii = 0; ii < src32.size(); ii++)
	{
		if (ii >= (x-num) && ii<x)
		{
			continue;
		}
		dst32.push_back(src32[ii]);
	}

	std::string ret32 = gxUtil::UTF32toUTF8(dst32);

	return ret32;
}


std::vector<std::string> CTxtUtil::Separate(std::string src, std::string svalue)
{
	std::vector<std::string> ret;

	std::vector<Uint32> src32 = gxUtil::UTF8toUTF32(src);
	std::vector<Uint32> find32 = gxUtil::UTF8toUTF32(svalue);

	size_t x = 0;;
	Sint32 s1=0, s2=0;
	Sint32 pos;

	do {
		pos = CTxtUtil::findString32(src32, find32, 1, x);
		if (pos != -1)
		{
			s2 = pos;
			if (s1 == s2)
			{
				//空データ
				ret.push_back("");
				s1 += (Sint32)svalue.size();
				x = s1;
				continue;
			}
			else
			{
				auto str = getPartText32toUTF8(src32, s1, s2);
				str += '\0';
				ret.push_back(str);
				s1 = pos + (Sint32)svalue.size();
				x = s1;
				continue;
			}

		}
		else
		{
			//みつからなかったので最終データか確認
			if (s1 < src32.size())
			{
				//最後尾未満なら何か文字があったと判断する
				s2 = (Sint32)src32.size();
				auto str = getPartText32toUTF8(src32, s1, s2);
				if (str != "")
				{
					//何か入っていた
					str += '\0';
					ret.push_back(str);
				}
			}
			break;
		}
		x = pos + 1;

	} while ( pos >= 0);

	return ret;
}

std::string CTxtUtil::GetStringinText(std::string text, size_t x1, size_t x2)
{
	auto u32Str = gxUtil::UTF8toUTF32(text);
	std::vector<Uint32> str32;

	for (size_t ii = x1; ii < x2; ii++)
	{
		str32.push_back(u32Str[ii]);
	}

//テスト	str32.push_back(0);
	auto u8Str = gxUtil::UTF32toUTF8(str32);

	return u8Str;
}


std::string CTxtUtil::Sprintf( char* pBuf, ... )
{
	gxChar buf[eTempBufMax];

	va_list app;
	va_start(app, pBuf);
	vsprintf(buf, pBuf, app);
	va_end(app);

	std::string tmpstr = buf;

	return tmpstr;
}


Sint32 CTxtUtil::findString32(std::vector<Uint32> src32, std::vector<Uint32> findStr32, Sint32 dir, size_t x )
{
	Sint32 max = (Sint32)src32.size();
	Sint32 num = (Sint32)findStr32.size();
	gxBool bFound = false;
	Sint32 loopnum = 0;
	Sint32 loopcnt = 0;

	max -= (Sint32)x;
	max -= num;

	if (max < 0) return -1;

	loopnum = max;

	if (loopnum == 0)
	{
		loopnum = 1;
	}
	else
	{
		if (num > max) return -1;
	}

	Sint32 ii = 0;

	if (dir > 0)
	{
		ii = (Sint32)x;
	}
	else
	{
		ii = (Sint32)src32.size() - 1;
		if (x > num)
		{
			ii -= (Sint32)x;
		}
		else
		{
			ii -= num;
		}
	}

	do {
		gxBool bFound = true;
		for (Sint32 jj = 0; jj < num; jj++)
		{
			if ((ii + jj) >= src32.size())
			{
				bFound = false;
				break;
			}

			if (src32[ii + jj] != findStr32[jj])
			{
				bFound = false;
				break;
			}
		}
		if (bFound)
		{
			return ii;
		}
		ii += dir;
		loopcnt++;

	} while (loopcnt <= loopnum);

	return -1;
}


std::string CTxtUtil::getPartText32toUTF8(std::vector<Uint32> src32, size_t s1, size_t s2)
{
	std::vector<Uint32> dst32;

	if (s1 >= src32.size()) return "";
	//if ((s2+1) >= src32.size()) return "";
	if (s1 == s2)
	{
		dst32.push_back(src32[s1]);
	}
	else
	{
		for (size_t x = s1; x < s2; x++)
		{
			if (x >= src32.size()) return "";
			dst32.push_back(src32[x]);
		}
	}
	//テスト	dst32.push_back(0);
	std::string ret = gxUtil::UTF32toUTF8(dst32);

	return ret;
}
