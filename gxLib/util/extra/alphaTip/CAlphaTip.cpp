﻿#include <gxLib.h>
#include <gxLib/Util/CFileTarga.h>
#include "CAlphaTip.h"

#define _AMP_VERSION_NUM_ (0x0000)

enum {
	ScreenW = WINDOW_W,//512,
	ScreenH = WINDOW_H,//512,
};

static const Uint32 ThresholdColor	= 20;						// 色の無効と判断する閾値
static const Uint32 TotalThresholdColor	= ThresholdColor * 3;	// RGB 色の無効と判断する閾値

gxPos m_Camera = { 0,0 };
Float32 m_CameraScale = 1.0f;

#define TIP_SIZE (32)	//デフォルト64 / 32 16

// コンストラクタ
CAlphaTip::CAlphaTip()
{
	m_TipRect.w = TIP_SIZE;
	m_TipRect.h = TIP_SIZE;
	m_SrcRect.w = TIP_SIZE;
	m_SrcRect.h = TIP_SIZE;

	m_eMode = enModeARGB;

	m_sTipNum = 0;
	m_sMapNum = 0;

	m_sPageNum = 0;

	m_pTga = NULL;

	m_pMapInfo = NULL;

	m_sStartPage = 0;
	m_sEndPage   = 0;

	m_Pos.x = 0;
	m_Pos.y = 0;
	m_Pos.z = 0;

	m_Center.x = 0;
	m_Center.y = 0;

	m_Rect.x1 = 0;
	m_Rect.y1 = 0;
	m_Rect.x2 = 32000;
	m_Rect.y2 = 32000;

	m_fScale = 1.f;
	m_fAlpha = 1.f;

	m_fScroll = 1.f;
	m_uARGB = 0xffffffff;

	m_bRepeatAMP = gxFalse;

	m_pFileName[0] = 0x00;
	m_pOriginal = NULL;

	m_Clip.Set( 0, 0 , ScreenW , ScreenH );
}


CAlphaTip::~CAlphaTip()
{
	SAFE_DELETE(m_pTga);

	if(!m_bRepeatAMP)
	{
		SAFE_DELETES(m_pMapInfo);
	}
	else
	{
		m_pMapInfo = NULL;
	}
}


gxBool CAlphaTip::LoadAMP( Uint32 page , gxChar *pFileName )
{
	//AMPファイルを読み込みます

	Uint8* pData;
	size_t uSize;

	m_pFileName[0] = 0x00;

	pData = gxLib::LoadFile( pFileName , &uSize );

	if( pData == NULL ) return gxFalse;

	ReadAMP( page , pFileName , pData , uSize );

	SAFE_DELETES( pData );

	return gxTrue;
}


	
gxBool CAlphaTip::ReadAMP( Uint32 page , gxChar *pFileName  , Uint8 *pData , size_t uSize )
{
	sprintf( m_pFileName , "%s" , pFileName );

	m_sStartPage = page;

	readAMP( page , pFileName , pData );

	return gxTrue;
}


//-------------------------------------------------------------------------
/*!
 @brief	重複 AMP 設定（指定された AlphaTip の内容を使用）

 @param [in]	pOrgAlphaTip	オリジナルの AlphaTip

 @retval 		gxFalse:失敗、gxTrue:成功
 */
//-------------------------------------------------------------------------
gxBool CAlphaTip::RepeatAMP( CAlphaTip* pOrgAlphaTip )
{
	m_bRepeatAMP = gxTrue;

	m_pOriginal = pOrgAlphaTip;

	m_pMapInfo = pOrgAlphaTip->GetMapData();

	m_sStartPage = pOrgAlphaTip->GetStartPage();

	m_TipRect.w = pOrgAlphaTip->GetTipWidth();
	m_TipRect.h = pOrgAlphaTip->GetTipHeight();
	m_SrcRect.w = pOrgAlphaTip->GetSrcWidth();
	m_SrcRect.h = pOrgAlphaTip->GetSrcHeight();

	m_Rect.x1 = 0;
	m_Rect.y1 = 0;
	m_Rect.x2 = m_SrcRect.w + m_TipRect.w;
	m_Rect.y2 = m_SrcRect.h + m_TipRect.h;

	m_sMapNum = pOrgAlphaTip->GetMapNum();

	//元画像の大きさをチップサイズで割り切れるサイズに丸める
	Sint32 w = m_SrcRect.w;
	Sint32 h = m_SrcRect.h;

	m_SrcRect.w = ((w / m_TipRect.w) + ((w % m_TipRect.w) ? 1 : 0)) * m_TipRect.w;
	m_SrcRect.h = ((h / m_TipRect.h) + ((h % m_TipRect.h) ? 1 : 0)) * m_TipRect.h;

	return gxTrue;
}


void CAlphaTip::Copy( Sint32 _x , Sint32 _y , Sint32 _z , gxRect* _pRect , gxPos *pCenter , Uint32 _atr/*=ATR_DFLT*/, Uint32 _argb/*=ARGB_DFLT*/, Float32 _fRotation/* = 0.f*/, Float32 _fScale/* = 1.f*/ )
{
	//矩形コピー

	Sint32 mw = m_SrcRect.w;
	Sint32 mh = m_SrcRect.h;
	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	Sint32 tipMax  = (enSingleTexWidth*enSingleTexHeight) / (tw*th);

	Sint32 numX = enSingleTexWidth  / tw;
	Sint32 numY = enSingleTexHeight / th;

	Sint32 celNumX = mw / tw;
	Sint32 celNumY = mh / th;

	Sint32 x1,y1,x2,y2,mx,my,tcx,tcy;

	gxPos center;

	center.x = 0;
	center.y = 0;

	if( pCenter )
	{
		Sint32 wx1=_pRect->x1,hy1=_pRect->y1;
		center.x = pCenter->x;
		center.y = pCenter->y;

		center.x += wx1%tw;
		center.y += hy1%th;
	}

	mx = _x ;//- pCenter->x;
	my = _y ;//- pCenter->y;

	x1 = _pRect->x1 / tw;
	y1 = _pRect->y1 / th;
	x2 = _pRect->x2 / tw;
	y2 = _pRect->y2 / th;
	x2 = _pRect->x2 / tw;
	y2 = _pRect->y2 / th;

	if( x1 < 0 ) x1 = 0;
	if( y1 < 0 ) y1 = 0;
	if( x2 >= celNumX ) x2 = celNumX-1;
	if( y2 >= celNumY ) y2 = celNumY-1;

	tcx = tw>>1;
	tcy = th>>1;

	Float32 fScale = _fScale;
	Float32 fRot = _fRotation;
	Float32 fCos = gxUtil::Cos( fRot );
	Float32 fSin = gxUtil::Sin( fRot );

	Sint32 ax,ay,az,rx,ry;
	Sint32 page,u,v,w,h;
	Uint32 n;
	Uint32 atr,atribute;
	Uint32 argb = _argb;
	Float32 rot = 0.f;

	az = _z;

	Float32 fx,fy;

	fx = +( tcx - center.x )*_fScale;
	fy = +( tcy - center.y )*_fScale;

	w = tw;
	h = th;


	for( Sint32 y=y1; y<y2+1; y++ )
	{
		for( Sint32 x=x1; x<x2+1; x++)
		{
			n   = m_pMapInfo[y*celNumX+x].index;
			atr = m_pMapInfo[y*celNumX+x].atr;

			atribute = 0;
			rot      = fRot;

			if( atr&enAtrRot90  ) { rot += 90*1; }
			if( atr&enAtrRot180 ) { rot += 90*2; }
			if( atr&enAtrRot270 ) { rot += 90*3; }
			if( atr&enAtrFlipX  ) { atribute |= ATR_FLIP_X; }
			if( atr&enAtrFlipY  ) { atribute |= ATR_FLIP_Y; }
			if( atr&(enAtrFlipX|enAtrFlipY)  ) { rot = -rot; }

			page = m_sStartPage + n/tipMax;

			u = (n%numX)*tw;
			v = (n/numX);
			v = (v%numY)*th;

			rx = (Sint32) ((x-x1)*( tw*_fScale ) + fx);
			ry = (Sint32) ((y-y1)*( th*_fScale ) + fy);

			//x' = x cosθ - y sinθ
			//y' = x sinθ + y cosθ
			ax = (Sint32) (fCos*rx - fSin*ry);
			ay = (Sint32) (fSin*rx + fCos*ry);

			ax += mx;
			ay += my;

			if( ax+tcx+(tw*fScale) < m_Clip.x1 || ax >= m_Clip.x2 ) continue;
			if( ay+tcy+(th*fScale) < m_Clip.y1 || ay >= m_Clip.y2 ) continue;

			PutBgTip( (Float32) ax , (Float32) ay , az , page , u, v , (Float32) w , (Float32) h , (Float32) tcx , (Float32) tcy , atribute , argb , rot , fScale, fScale );

		}

		gxLib::Printf( gxLib::Joy(0)->mx , gxLib::Joy(0)->my-32 , 255 , ATR_DFLT , ARGB_DFLT , (gxChar*) "%d,%d",gxLib::Joy(0)->mx,gxLib::Joy(0)->my );

	}
}

gxBool CAlphaTip::readAMP( Uint32 page , gxChar *pFileName , Uint8 *pData )
{
	//AMPデータを読み込む

	StAmpHeader head;

	memcpy( &head , pData , sizeof(head) );

	m_TipRect.w = head.tw;
	m_TipRect.h = head.th;
	m_SrcRect.w = head.mw;
	m_SrcRect.h = head.mh;


	m_Rect.x1 = 0;
	m_Rect.y1 = 0;
	m_Rect.x2 = m_SrcRect.w+m_TipRect.w;
	m_Rect.y2 = m_SrcRect.h+m_TipRect.h;

	m_sPageNum = head.texNum;
	m_sMapNum  = head.mapNum;

	m_pMapInfo = new StMapInfo[ m_sMapNum ];

	memcpy( m_pMapInfo , &pData[sizeof(head)] , sizeof(StMapInfo)*m_sMapNum );

	//元画像の大きさをチップサイズで割り切れるサイズに丸める
	Sint32 w = m_SrcRect.w;
	Sint32 h = m_SrcRect.h;

	m_SrcRect.w = ((w /m_TipRect.w) + ((w%m_TipRect.w)? 1 : 0 ))*m_TipRect.w;
	m_SrcRect.h = ((h /m_TipRect.h) + ((h%m_TipRect.h)? 1 : 0 ))*m_TipRect.h;

	for( Sint32 ii=0;ii<m_sPageNum; ii++ )
	{
		m_sEndPage = m_sStartPage + ii;

		if( !gxLib::LoadTexture( m_sEndPage , getAmpDivFileName( pFileName , ii) ) )
		{
			return gxFalse;
		}
	}

	gxLib::UploadTexture();

	return gxTrue;
}


//------------------------------------------------------
//表示用
//------------------------------------------------------
void CAlphaTip::Draw()
{
	if( m_sMapNum == 0 ) return;

	disp();
}


void CAlphaTip::disp()
{
	//全表示
	//-----------------------------------------------------------------------------

	Sint32 mw = m_SrcRect.w;
	Sint32 mh = m_SrcRect.h;
	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	Sint32 tipMax = (enSingleTexWidth * enSingleTexHeight) / (tw * th);	//横長テクスチャに入る最大のセル枚数

	Sint32 numX = enSingleTexWidth  / tw;								//分割テクスチャに入る横の数
	Sint32 numY = enSingleTexHeight / th;								//分割テクスチャに入る縦の数

	Sint32 celNumX = mw / tw;
	Sint32 celNumY = mh / th;

	Sint32 x1 = 0;
	Sint32 y1 = 0;
	Sint32 x2 = celNumX-1;
	Sint32 y2 = celNumY-1;

	Float32 pos_x1 = -m_Pos.x / m_fScale;
	Float32 pos_y1 = -m_Pos.y / m_fScale;
	Float32 pos_x2 = pos_x1 + (ScreenW / m_fScale);
	Float32 pos_y2 = pos_y1 + (ScreenH / m_fScale);

	x1 = pos_x1/tw;
	y1 = pos_y1/th;
	x2 = pos_x2/tw;
	y2 = pos_y2/th;

	x1 --;
	y1 --;
	x2 ++;
	y2 ++;
	x1 = CLAMP( x1 , 0 , celNumX-1);
	y1 = CLAMP( y1 , 0 , celNumY-1);
	x2 = CLAMP( x2 , 0 , celNumX-1);
	y2 = CLAMP( y2 , 0 , celNumY-1);

	//-----------------------------------------------------------------------------

	Float32 cx_pos = (tw / 2) * m_fScale;
	Float32 cy_pos = (th / 2) * m_fScale;

	Float32 fWidth  = tw * m_fScale;	//表示時のセルのサイズ
	Float32 fHeight = th * m_fScale;

	Float32 startX  = (x1 * fWidth) + cx_pos + m_Pos.x;
	Float32 startY  = (y1 * fHeight);

	Float32 ay = startY + cy_pos + m_Pos.y;

	Uint32  uARGB = SET_ALPHA( m_fAlpha , m_uARGB );

	for( Sint32 y = y1; y < y2 + 1; y++ )
	{
		Float32 ax = startX;

		for( Sint32 x = x1; x < x2 + 1; x++)
		{
			StMapInfo* p = NULL;
			Sint32 tmpX = x;

			if(tmpX >= celNumX)
			{
				tmpX -= (6000 / tw) + 1;
				tmpX %= (2400 + 2400) / tw;
				tmpX += (6000 / tw) + 1;
			}
			p = &m_pMapInfo[y * celNumX + tmpX];

			Uint32 n = p->index;

			if( n > 0 )
			{
				Uint32 atr = p->atr;
				Sint32 page = m_sStartPage + n / tipMax;
				Sint32 u = (n % numX) * tw;
				Sint32 v = (n / numX);
				v = (v % numY) * th;

				PutBgTip ( ax-16, ay-16 , m_Pos.z , page , u, v , tw , tw , -(tw>>1) , -(tw>>1) , ATR_DFLT , uARGB , 0.0f , m_fScale, m_fScale );

			}
			ax += fWidth;
		}
		ay += fHeight;
	}

}

//------------------------------------------------------
//(開発用)AMPデータ作成用
//------------------------------------------------------
gxBool CAlphaTip::Make( gxChar *pFileName )
{
	//AMPファイルの作成、再結合を行います

	gxChar ext[256];
	gxUtil::GetExt( pFileName , ext );

	if( strcmp(".AMP" , ext ) == 0 )
	{
		//再結合
		rebuild( pFileName );
	}
	else
	{
		//分割
		m_pFileName[0] = 0x00;

		SAFE_DELETE(m_pTga);

		m_pTga = new CFileTarga();

		sprintf( m_pFileName , "%s",pFileName );

		if( !m_pTga->LoadFile( pFileName ) )
		{
			SAFE_DELETE(m_pTga);
			return gxFalse;
		}

		m_SrcRect.w = m_pTga->GetWidth();
		m_SrcRect.h = m_pTga->GetHeight();

		make();

		SAFE_DELETE(m_pTga);
	}

	return gxTrue;
}


gxBool CAlphaTip::make(void)
{
	if( m_pTga == NULL ) return gxFalse;

	makeChips();

	return gxTrue;
}

void CAlphaTip::makeChips(void)
{
	Sint32 w = m_SrcRect.w;
	Sint32 h = m_SrcRect.h;

	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;

	Sint32 total;
	Sint32 ax,ay;

	Sint32 xNum = w/tw + ( (w%tw)? 1 : 0);
	Sint32 yNum = h/th + ( (h%th)? 1 : 0);

	total = xNum * yNum;

	//テンポラリの１６ｘ１６ARGBバッファ
	Uint32    *pTip  = new Uint32[ tw*th ];

	//ユニークなチップを貯めこむバッファ
	StTipBank *pBank = new StTipBank[ enMaxTipNum ];

	//マップ情報を貯めこむバッファ
	StMapInfo *pMap  = new StMapInfo[ total ];

	m_sTipNum = 0;

	ax = 0;
	ay = 0;

	//最初にブランクのチップを１つ作っておく
	memset( pTip, 0x00000000, sizeof(Uint32) * tw*th );
	addTip( pTip , pBank );

	m_sMapNum = 0;

	for( Sint32 x=0;x<w; x+=tw )
	{
		for( Sint32 y=0;y<h; y+=th )
		{
			makeTip( pTip , x , y );

			Uint32 atr = enAtrDefault;
			Sint32 index = getSameTip( pTip , pBank , &atr );

			if( index == -1 )
			{
				atr = enAtrDefault;
				index = addTip( pTip , pBank );
			}

#ifdef GX_DEBUG
if((ax + (ay * xNum)) >= total) {
	gxLib::DebugLog("StMapInfo[%d] をオーバーしました。配列を増加してください", total);
}
#endif
			addMap( ax , ay , index , 0x00000000 , xNum , yNum , pMap , &atr );

			ay ++;
		}

		ax ++;
		ay = 0;
	}

	saveAMP( pMap , pBank );

	delete[] pTip;
	delete[] pBank;
	delete[] pMap;
}

void CAlphaTip::makeTip( Uint32 *pTip , Sint32 tx , Sint32 ty )
{
	//16x16のマップチップ情報を作る
	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	Sint32 w = m_SrcRect.w;
	Sint32 h = m_SrcRect.h;

	Uint32 argb = 0xffffffff;
//		Sint32 n = 0;

	for( Sint32 y=0;y<th; y++ )
	{
		for( Sint32 x=0;x<tw; x++ )
		{

			if (tx + x < 0 || tx + x >= w)
			{
				argb = 0x00000000;
			}
			else if (ty + y < 0 || ty + y >= h)
			{
				argb = 0x00000000;
			}
			else
			{
				argb = m_pTga->GetARGB(tx + x, ty + y);
			}

			pTip[y*tw+x] = argb;
		}
	}

}

Sint32 CAlphaTip::getSameTip( Uint32 *pTip , StTipBank *pBank , Uint32 *pAtr )
{
	//既に同じチップがあるか調査する

	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;

	gxBool bTransparent = gxTrue;

	for( Sint32 i = 0; i < th * tw; ++i )
	{
		Sint32 argb = pTip[i];
		if((argb & 0xff000000) != 0)
		{
			// αが 0 じゃない
			bTransparent = gxFalse;
			break;
		}
	}
	if(bTransparent)
	{
		// 透明チップなのでここで終了
		*pAtr = enAtrDefault;
		return 0;
	}

	//通常状態で同じかどうか検査する
	for( Sint32 ii=0; ii<m_sTipNum; ii++ )
	{
		if( memcmp( pBank[ii].pData , &pTip[0] , tw*th*sizeof(Uint32) ) == 0 )
		{
			*pAtr = enAtrDefault;
//GX_LOG("同じ%d", ii);
			return ii;
		}
	}

	// このチップは登録しなければならないチップだった
	return -1;
}

Sint32 CAlphaTip::addTip( Uint32* pTip , StTipBank *pBank )
{
	//チップ情報を追加する

	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	Sint32 n = m_sTipNum;

	if(n >= enMaxTipNum)
	{
		gxLib::DebugLog("StTipBank[%d] をオーバーしました。enMaxTipNum を増加してください", enMaxTipNum);
	}

	pBank[n].pData = new Uint32[tw*th];

	memcpy( pBank[n].pData , &pTip[0] , (tw*th)*sizeof(Uint32) );

	m_sTipNum ++;

	return n;
}


void CAlphaTip::addMap( Sint32 ax , Sint32 ay , Sint32 index , Uint32 atr , Sint32 xmax , Sint32 ymax , StMapInfo *pMap , Uint32 *pAtr )
{
	//マップ情報を追加する

	pMap[ ax + ay*xmax ].index = index;
	pMap[ ax + ay*xmax ].atr = *pAtr;

	m_sMapNum ++;
}


gxBool CAlphaTip::saveAMP( StMapInfo *pMap , StTipBank *pBank )
{
	outputTGA( pBank );
	outputAMP( pMap );

	return gxTrue;
}


gxBool CAlphaTip::outputAMP( StMapInfo *pMap )
{
	//MAPファイルを出力する

	StAmpHeader head;
	head.format[0] ='A';
	head.format[1] ='M';

	head.version   = _AMP_VERSION_NUM_;
	head.texNum    = m_sPageNum;
	head.mapNum    = m_sMapNum;
	head.mw        = m_SrcRect.w;
	head.mh        = m_SrcRect.h;
	head.tw        = m_TipRect.w;
	head.th        = m_TipRect.h;

	Uint32 uSize = sizeof(StMapInfo)*m_sMapNum + sizeof( head );

	Uint8 *pData = new Uint8[sizeof(StMapInfo) * m_sMapNum + sizeof(head)];

	memcpy( &pData[ 0 ]            , &head    , sizeof(head) );
	memcpy( &pData[ sizeof(head) ] , &pMap[0] , sizeof(StMapInfo)*m_sMapNum );

	gxChar nameFull[256];
	gxChar nameBase[256];

	gxUtil::GetFileNameWithoutExt ( m_pFileName , nameBase );

	sprintf( nameFull , "%s.amp" , nameBase )  ;

	gxLib::SaveFile( nameFull , pData , uSize );

	return gxTrue;
}


gxBool CAlphaTip::outputTGA( StTipBank *pBank )
{
	//分解されたTGAを出力する

	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	CFileTarga* pTga = NULL;

	m_sPageNum = 0;

	Sint32 numX = enSingleTexWidth  / tw;
	Sint32 numY = enSingleTexHeight / th;

	for( Sint32 ii=0; ii<m_sTipNum; ii++ )
	{
		Sint32 ax = (ii%numX)*tw;
		Sint32 ay = (ii/numX)*th;

		ay = ay%(numY*th);


		if( ii%(numX*numY) == 0 )
		{
			if( pTga )
			{
				pTga->SaveFile( getAmpDivFileName( m_pFileName , m_sPageNum -1 ) );
				delete pTga;
			}
			pTga = new CFileTarga();
			pTga->Create( enSingleTexWidth , enSingleTexHeight , 32 );
			m_sPageNum ++;
		}

		//----------------------------------------------------------
		Uint32 *pTip = pBank[ii].pData;

		for( Sint32 y=0;y<th; y++ )
		{
			for( Sint32 x=0;x<tw; x++ )
			{
				Uint32 argb = pTip[ y*tw+x ];
				argb = pTga->SetARGB( ax+x , ay+y , argb );
			}
		}
		//----------------------------------------------------------
	}

	if( pTga )
	{
		pTga->SaveFile( getAmpDivFileName( m_pFileName , m_sPageNum -1 ) );
		delete pTga;
	}

	return gxTrue;
}


//------------------------------------------------------
//(開発用)AMPデータから元データを復元する
//------------------------------------------------------

gxBool CAlphaTip::rebuild( gxChar *pFileName )
{
	//AMPファイルから再構築します
	size_t uSize = 0;
	Uint8* pData = NULL;

	pData = gxLib::LoadFile( pFileName , &uSize );

	if( pData == NULL )
	{
		return gxFalse;
	}

	sprintf( m_pFileName , "%s",pFileName );

	//ヘッダー情報の構築

	StAmpHeader head;
	memcpy( &head , pData , sizeof(head) );
	m_pMapInfo = (StMapInfo*)&pData[ sizeof(head) ];

	m_TipRect.w = head.tw;
	m_TipRect.h = head.th;
	m_SrcRect.w = head.mw;
	m_SrcRect.h = head.mh;

	m_sPageNum = head.texNum;
	m_sMapNum  = head.mapNum;

	//元画像の大きさをチップサイズで割り切れるサイズに丸める
	Sint32 w = m_SrcRect.w;
	Sint32 h = m_SrcRect.h;

	m_SrcRect.w = ((w /m_TipRect.w) + ((w%m_TipRect.w)? 1 : 0 ))*m_TipRect.w;
	m_SrcRect.h = ((h /m_TipRect.h) + ((h%m_TipRect.h)? 1 : 0 ))*m_TipRect.h;

	//復元tgaファイルの作成
	CFileTarga *pTga;
	CFileTarga *pTgaMaster = new CFileTarga();

	pTgaMaster->Create( m_SrcRect.w , m_SrcRect.h , 32 );

	pTga = new CFileTarga[m_sPageNum];

	for( Sint32 ii=0;ii<m_sPageNum; ii++ )
	{
		pTga[ii].LoadFile( getAmpDivFileName( pFileName , ii) );
	}

	rebuildTga( pTgaMaster , pTga );

	delete[] pTga;

	//結合ファイルを保存する（デバッグ用）

	gxChar saveName[256];
	sprintf( saveName , "%s.tga" , pFileName );

	pTgaMaster->SaveFile( saveName );

	SAFE_DELETE( pTgaMaster );

	SAFE_DELETES( pData );

	m_pMapInfo = NULL;

	return gxTrue;
}

void CAlphaTip::rebuildTga( CFileTarga *pTgaMaster , CFileTarga* pTga )
{
	//AMPからTGAを再構築する

	Sint32 tw = m_TipRect.w;
	Sint32 th = m_TipRect.h;
	Sint32 mw = m_SrcRect.w;
	Sint32 numX   = mw / tw;
	Sint32 tipXNum = enSingleTexWidth  / tw;
	Sint32 tipYNum = enSingleTexHeight / th;
	Sint32 tipMax  = (enSingleTexWidth*enSingleTexHeight) / (tw*th);

	Sint32 atrCnt = 0;

	for( Sint32 ii=0;ii<m_sMapNum; ii++ )
	{
		Sint32 ax = (ii%numX)*tw;
		Sint32 ay = (ii/numX)*th;

		Uint32 n = m_pMapInfo[ii].index;

		Sint32 page,u,v,w,h;
		page = n/tipMax;

		u = (n%tipXNum)*tw;
		v = (n/tipXNum);
		v = (v%tipYNum)*th;
		w = tw;
		h = th;

		Uint32 argb;

		if( m_pMapInfo[ii].atr ) atrCnt ++;	//DEBUG用回転オブジェクトの数を数えてみる

		for( Sint32 y=0;y<th;y++)
		{
			for( Sint32 x=0;x<tw;x++)
			{
				argb = pTga[page].GetARGB( u+x,v+y );

				Sint32 vx,vy;

				vx = x;
				vy = y;

				switch( m_pMapInfo[ii].atr ) {
				case enAtrRot90:
					vx = tw-y-1;
					vy = x;
					break;
				case enAtrRot180:
				case enAtrFlipY|enAtrFlipX:
					vx = tw-x-1;
					vy = th-y-1;
					break;
				case enAtrRot270:
					vx = y;
					vy = th-x-1;
					break;
				case enAtrFlipX:
					vx = tw-x-1;
					vy = y;
					break;

				case enAtrFlipY:
					vx = x;
					vy = th-y-1;
					break;

				case enAtrRot90|enAtrFlipY:
					vx = th-y-1;
					vy = tw-x-1;
					break;
				case enAtrRot90|enAtrFlipX:
					vx = y;
					vy = x;
					break;
				default:
					vx = x;
					vy = y;
				}

				pTgaMaster->SetARGB( ax+vx,ay+vy , argb );
			}
		}
	}

	gxLib::DebugLog((gxChar*) "[%s]回転オブジェクトの数は%d" , m_pFileName , atrCnt );
}


gxChar *CAlphaTip::getAmpDivFileName( gxChar *pFileName , Sint32 index )
{
	//入出力用のファイル名を作成する

	static gxChar fileName[256];

	gxChar namePath[256];
	gxChar nameFull[256];
	gxChar nameBase[256];

	namePath[0] = 0x00;
	nameFull[0] = 0x00;
	nameBase[0] = 0x00;

	gxUtil::GetPath( pFileName , namePath );
	gxUtil::GetFileNameWithoutPath( pFileName , nameFull );
	gxUtil::GetFileNameWithoutExt ( nameFull , nameBase );

	if( namePath[0] )
	{
		sprintf( fileName , "%s/@%s_%03d.tga", namePath , nameBase , index );
	}
	else
	{
		sprintf( fileName , "@%s_%03d.tga", nameBase , index );
	}

	return fileName;
}

//-------------------------------------------------------------------------
/*!
 @brief	αカウント初期化
 */
//-------------------------------------------------------------------------
void CAlphaTip::initializeAlphaCount(void)
{
	for(Sint32 i = 0; i < enMaxAlphaNum; ++i)
	{
		m_alphaCheck[i] = 0xffffffff;
	}
	m_opaqueCount = 0;
}

//-------------------------------------------------------------------------
/*!
 @brief	αカウントチェック

 @param [in]	argb	ARGB 値

 @return		gxFalse:まだまだいける、gxTrue:チェック個数オーバーした
 */
//-------------------------------------------------------------------------
gxBool CAlphaTip::checkAlphaCount(Uint32 argb)
{
	Uint32 alpha = argb & 0xff000000;
//		if(alpha == 0)
	if(alpha < ARGB(10, 0, 0, 0))
	{
		// αが 0 に近いのでこれは透明と判断する
		return gxFalse;
	}
	if(argb == 0xff000000)
	{
		// 真っ黒は登録しない
		return gxFalse;
	}

	// 何らかの色が付いていても黒に近い場合は透明として扱う
	Uint32 total = getTotalAlphaRGB(argb);
	if(0 < total && total <= TotalThresholdColor)
	{
			return gxFalse;
	}

	// αチェック

	for(Sint32 i = 0; i < enMaxAlphaNum; ++i)
	{
		if(m_alphaCheck[i] == alpha)
		{
			return gxFalse;
		}
		else if(m_alphaCheck[i] == 0xffffffff)
		{
			m_alphaCheck[i] = alpha;
			return gxFalse;
		}
	}

	return gxTrue;
}

#ifdef GX_DEBUG
//-------------------------------------------------------------------------
/*!
 @brief	テーブルに登録されているα値出力
 */
//-------------------------------------------------------------------------
void CAlphaTip::outputAlphaCount(void)
{
	for(Sint32 i = 0; i < enMaxAlphaNum; ++i)
	{
		if(m_alphaCheck[i] == 0xffffffff)
		{
			return;
		}
		gxLib::DebugLog("%3d", m_alphaCheck[i] >> 24);
	}
}
#endif

//-------------------------------------------------------------------------
/*!
 @brief	指定色が指定された閾値より大きいかチェック

 @param [in]	argb		チェックする色
 @param [in]	threshold	閾値（0 ～ 255）

 @return		gxFalse:閾値以下、gxTrue:閾値より大きい（黒は無条件にこちら）
 */
//-------------------------------------------------------------------------
gxBool CAlphaTip::checkColor(Uint32 argb, Uint32 threshold)
{
	if(argb == 0xff000000 )
	{
		return gxTrue;
	}

	Uint32 a = (argb & 0xff000000) >> 24;
	Uint32 r = (argb & 0x00ff0000) >> 16;
	Uint32 g = (argb & 0x0000ff00) >> 8;
	Uint32 b = (argb & 0x000000ff);

	if(a > threshold && (r > threshold || g > threshold || b > threshold))
	{
		return gxTrue;
	}
	return gxFalse;
}

//-------------------------------------------------------------------------
/*!
 @brief	αを加味した RGB 値の合計を取得

 @param [in]	argb	色

 @return		αを加味した RGB 値の合計
 */
//-------------------------------------------------------------------------

Uint32 CAlphaTip::getTotalAlphaRGB(Uint32 argb)
{
	Uint32 alpha = argb & 0xff000000;
	alpha >>= 24;
	Uint32 r = (argb & 0x00ff0000) >> 16;
	Uint32 g = (argb & 0x0000ff00) >> 8;
	Uint32 b = (argb & 0x000000ff);
	r = (r * alpha) / 255;
	g = (g * alpha) / 255;
	b = (b * alpha) / 255;
	Uint32 color = r + g + b;
	return color;
}

//-------------------------------------------------------------------------
/*!
 @brief	αを加味した RGB 値を取得

 @param [in]	argb	色

 @return		αを加味した RGB 値
 */
//-------------------------------------------------------------------------
Uint32 CAlphaTip::getAlphaRGB(Uint32 argb)
{
	Uint32 alpha = argb & 0xff000000;
	alpha >>= 24;
	Uint32 r = (argb & 0x00ff0000) >> 16;
	Uint32 g = (argb & 0x0000ff00) >> 8;
	Uint32 b = (argb & 0x000000ff);
	r = (r * alpha) / 255;
	g = (g * alpha) / 255;
	b = (b * alpha) / 255;
	Uint32 color = ARGB(0, r, g, b);
	return color;
}

void CAlphaTip::PutBgTip(
	Float32 x, Float32 y, Sint32 prio,
	Sint32 page, Sint32 u, Sint32 v, Float32 w, Float32 h,
	Float32 cx, Float32 cy,
	Uint32 atr, Uint32 col, Float32 r,
	Float32 sx, Float32 sy)
{
	//-------------------------------------------------------
	//BG チップ描画専用のスプライトリクエスト
	//-------------------------------------------------------
	Uint32 argb = col;

	argb = 0x00000000;// CStageManager::GetInstance()->GetAnbientARGB();
	if (argb) col = (col & 0xff000000) | (argb & 0x00ffffff);

//	col = 0xFFFFFFFF;// GetBackLight(col);

	Sint32 off = 0;

	//	gxLib::PutSprite(x, y, prio, page, u, v, w, h, cx, cy, atr, col, r, sx, sy);
	gxLib::PutTriangle(
		off+x          , off+y, u, v + 0,
		off+x + w * sx , off+y, u + w, v + 0,
		off+x          , off+y + h * sy, u, v + h,
		prio, page, atr, col);

	gxLib::PutTriangle(
		off+x         , off+y + h * sy, u, v + h,
		off+x + w * sx, off+y, u + w, v + 0,
		off+x + w * sx, off+y + h * sy, u + w, v + h,
		prio, page, atr, col);
}


void CAlphaTip::debugControl(void)
{
	if(gxLib::Joy(0)->psh&JOY_L ) m_Pos.x -=64;
	if(gxLib::Joy(0)->psh&JOY_R ) m_Pos.x +=64;
	if(gxLib::Joy(0)->psh&JOY_U ) m_Pos.y -=64;
	if(gxLib::Joy(0)->psh&JOY_D ) m_Pos.y +=64;

	if(gxLib::Joy(0)->whl < -0.5f ) m_fScale += ( 2.0f-m_fScale)/100.0f;
	if(gxLib::Joy(0)->whl >  0.5f ) m_fScale += ( 0.1f-m_fScale)/100.0f;
	if(gxLib::Joy(0)->trg&MOUSE_M ) m_fScale = 1.0f;

	static gxPos mouse = {0,0};

	if( gxLib::Joy(0)->psh&MOUSE_L )
	{
		if( gxLib::Joy(0)->trg&MOUSE_L )
		{
			mouse.x = gxLib::Joy(0)->mx;
			mouse.y = gxLib::Joy(0)->my;
		}

		m_Pos.x += gxLib::Joy(0)->mx - mouse.x;
		m_Pos.y += gxLib::Joy(0)->my - mouse.y;

		mouse.x = gxLib::Joy(0)->mx;
		mouse.y = gxLib::Joy(0)->my;
	}

//		if(gxLib::Joy(0)->psh&JOY_U ) m_fRotation -= 3.f;
//		if(gxLib::Joy(0)->psh&JOY_D ) m_fRotation += 3.f;

	gxLib::DrawBox( 128,128,128+ScreenW , 128+ScreenH , 255 , gxFalse , ATR_DFLT , 0xff00ff00 ,3 );
}

static CAlphaTip *m_pAlphaTip = NULL;

void AlphaTipLoad(gxChar* pFileName)
{

	if (m_pAlphaTip)
	{
		SAFE_DELETE(m_pAlphaTip);
	}

	m_pAlphaTip = new CAlphaTip();

	//		m_pAlphaTip->LoadAMP( 0 , "amp/bg01_0010.amp");//bg01_0500.amp" );
	//		m_pAlphaTip->LoadAMP( 0 , "amp/bg01_0405.amp" );
	//		m_pAlphaTip->LoadAMP( 0 , "amp/bg01_0890.amp" );

	m_pAlphaTip->LoadAMP(0, pFileName);
}

void AlphaTipTest()
{
	if( m_pAlphaTip == NULL ) return;

	gxPos pos = { 0,0 };

	m_pAlphaTip->debugControl();
	m_pAlphaTip->Draw();

	
}


void AlphaTipMake( gxChar *pFileName )
{
	CAlphaTip *m_pAlphaTip = NULL;

	m_pAlphaTip = new CAlphaTip();
	m_pAlphaTip->CAlphaTip::Make(pFileName);

	SAFE_DELETE( m_pAlphaTip );
}

