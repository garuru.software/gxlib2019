﻿//-----------------------------------------------
//大きな画像をマップチップに分解する
//アルファチャンネルに対応したBMP,TGA画像を
//指定した矩形に切り抜いた圧縮画像を作成します
//※圧縮を回転、反転に対応させた
//-----------------------------------------------
#ifndef _CALPHAMAP_H_
#define _CALPHAMAP_H_

#pragma pack (push, 1)

class CFileTarga;
class CAlphaTip
{
public:
	enum {
		enAtrDefault = 0x00,
		enAtrFlipX   = 0x01,
		enAtrFlipY   = 0x02,
		enAtrRot90   = 0x04,
		enAtrRot180  = 0x08,
		enAtrRot270  = 0x10,
		enAtrNONEw   = 0x20,	//コレ以降水平方向にオブジェクトなし
		enAtrNONEh   = 0x40,	//コレ以降垂直方向にオブジェクトなし
	};

	enum {
		//分解できるテクスチャのマックスサイズ
		enMaxWidth  = (1280*10),
		enMaxHeight = (640*10),

		//分割されるテクスチャのサイズ
		enSingleTexWidth  = 256,
		enSingleTexHeight = 256,
		enMaxTipNum = (enMaxWidth*enMaxHeight) / (16*16),
	};

	enum eModeCompress {
		enModeARGB,
		enModeRGB,
		enMode8bit,
	};

	struct StRect {
		Sint32 w;
		Sint32 h;
	};

	struct StMapInfo {
		Uint32 index;
		Uint32 atr;
	};

	CAlphaTip();	// コンストラクタ
	~CAlphaTip();	// デストラクタ

	gxBool LoadAMP( Uint32 page , gxChar *pFileName );
	gxBool ReadAMP( Uint32 page , gxChar *pFileName , Uint8 *pData , size_t uSize );
	gxBool RepeatAMP( CAlphaTip* pOrgAlphaTip );
	gxBool Make( gxChar *pFileName );
	void Draw();

	void Copy( Sint32 _x , Sint32 _y , Sint32 _z , gxRect* _pRect , gxPos *pCenter , Uint32 _atr=ATR_DFLT, Uint32 _argb=ARGB_DFLT , Float32 _fRotation = 0.f , Float32 _fScale = 1.f );

	void SetRect( Sint32 wh )
	{
		//縦横比が同じMAPチップに分解します
		m_TipRect.w = wh;
		m_TipRect.h = wh;
	}

	void SetClip( Sint32 x1 , Sint32 y1 , Sint32 w , Sint32 h )
	{
		m_Clip.Set( x1 , y1 , w , h );
	}

	void debugControl(void);

	void SetPos( Sint32 x , Sint32 y , Sint32 z = -1 )
	{
		m_Pos.x = x;
		m_Pos.y = y;
		if( z != -1 ) m_Pos.z = z;
	}

	void SetScale( Float32 fScale )
	{
		m_fScale = fScale;
	}

	void SetAlpha( Float32 fAlpha )
	{
		m_fAlpha = fAlpha;
	}

	void SetScroll( Float32 _fScroll)
	{
		m_fScroll = _fScroll;
	}

	Sint32 GetEndTexturePage(void)
	{
		return m_sEndPage;
	}

	// 現在読み込んでいるマップのチップサイズ取得
	Sint32 GetTipWidth(void)
	{
		return m_TipRect.w;
	}
	Sint32 GetTipHeight(void)
	{
		return m_TipRect.h;
	}

	Sint32 GetSrcWidth(void)
	{
		return m_SrcRect.w;
	}
	Sint32 GetSrcHeight(void)
	{
		return m_SrcRect.h;
	}

	Sint32 GetStartPage(void)
	{
		return m_sStartPage;
	}

	Sint32 GetMapNum(void)
	{
		return m_sMapNum;
	}

	StMapInfo* GetMapData(void)
	{
		return m_pMapInfo;
	}


	gxChar* GetFileName()
	{
		return m_pFileName;
	}

	gxBool IsRepeat()
	{
		return m_bRepeatAMP;
	}

	void GetInfo( Sint32 *pTip , Sint32 *pMap , Sint32 *pPage , Sint32 *ww , Sint32 *hh )
	{
		*pTip  = m_sTipNum;
		*pMap  = m_sMapNum;
		*pPage = m_sPageNum;
		*ww = m_SrcRect.w;
		*hh = m_SrcRect.h;
	}


/*
	void SetDispRect( Sint32 _x1 , Sint32 _y1 , Sint32 _w1 , Sint32 _h1 , Float32 fScale , gxRect *pRect )
	{
		//クリッピング領域から表示するべき矩形を割り出す
		
	}
*/

	CAlphaTip* GetOriginal()
	{
		return m_pOriginal;
	}

	void SetARGB( Uint32 argb )
	{
		m_uARGB = argb;
	}


//------------------------------------------------------
//表示用
//------------------------------------------------------
private:
	gxBool readAMP( Uint32 page , gxChar *pFileName , Uint8 *pData );
	void disp();

//------------------------------------------------------
//(開発用)AMPデータ作成用
//------------------------------------------------------
private:
	struct StTipBank {
		Uint32 *pData;

		StTipBank()
		{
			pData = NULL;
		}

		~StTipBank()
		{
			if( pData )
			{
				delete pData;
			}
		}
	};

	struct StAmpHeader {
		Uint8  format[2];	//2:am
		Uint32  mapNum;		//4:
		Uint16 version;		//2:0~65535
		Uint16  texNum;		//2:
		Uint16  mw,mh;		//4:
		Uint8   tw,th;		//2:
		Uint8   dummy[16];	//16
	};

	enum {	// 各種定数定義
//		enMaxAlphaNum	= 4 + 1,	// α種類チェック用（+1 = α:0 の分）もし必要なチップまではじかれてしまう場合はこの数値を小さくしてください
		enMaxAlphaNum	= 3,		// α種類チェック用（もし必要なチップまではじかれてしまう場合はこの数値を小さくしてください）
	};

private:
	gxBool make(void);
	void makeChips(void);
	void makeTip( Uint32 *pTip , Sint32 tx , Sint32 ty );

	Sint32 getSameTip( Uint32 *pTip , StTipBank *pBank , Uint32 *pAtr );
	Sint32 addTip( Uint32* pTip , StTipBank *pBank );
	void addMap( Sint32 ax , Sint32 ay , Sint32 index , Uint32 atr , Sint32 xmax , Sint32 ymax , StMapInfo *pMap , Uint32 *pAtr );

	gxBool saveAMP( StMapInfo *pMap , StTipBank *pBank );
	gxBool outputAMP( StMapInfo *pMap );
	gxBool outputTGA( StTipBank *pBank );

//------------------------------------------------------
//(開発用)AMPデータから元データを復元する
//------------------------------------------------------
	gxBool rebuild( gxChar *pFileName );
	void rebuildTga( CFileTarga *pTgaMaster , CFileTarga* pTga );
	gxChar *getAmpDivFileName( gxChar *pFileName , Sint32 index );

	// αカウント
	// αにアンチエイリアスがかかっているので、それの検出用処理
	void initializeAlphaCount(void);											// 初期化
	gxBool checkAlphaCount(Uint32 argb);										// チェック
#ifdef GX_DEBUG
	void outputAlphaCount(void);												// テーブルに登録されているα値出力
#endif
	gxBool checkColor(Uint32 argb, Uint32 threshold);							// 指定色が指定された閾値より大きいかチェック
	Uint32 getTotalAlphaRGB(Uint32 argb);										// αを加味した RGB 値の合計を取得
	Uint32 getAlphaRGB(Uint32 argb);											// αを加味した RGB 値を取得

	static void PutBgTip(
		Float32 x, Float32 y, Sint32 prio,
		Sint32 page, Sint32 u, Sint32 v, Float32 w, Float32 h, Float32 cx = 0.0f, Float32 cy = 0.0f,
		Uint32 atr = ATR_DFLT, Uint32 col = ARGB_DFLT,
		Float32 r = 0.0f, Float32 sx = 1.0f, Float32 sy = 1.0f);


private:
	//表示用
	gxPos  m_Pos;
	gxRect m_Rect;
	gxPos  m_Center;
	gxRect m_Clip;

	Float32 m_fScale;
	Float32 m_fAlpha;
//	Float32 m_fRotation;
	Float32 m_fScroll;		// レイヤーの奥行きを出すためのスクロールスピード
	Uint32  m_uARGB;

	StRect m_TipRect;
	StRect m_SrcRect;
	StMapInfo *m_pMapInfo;

	Sint32 m_sTipNum;
	Sint32 m_sMapNum;
	Sint32 m_sPageNum;

	Sint32 m_sStartPage;
	Sint32 m_sEndPage;

	gxBool m_bRepeatAMP;	// gxFalse:通常 AMP（データが存在する）、gxTrue:重複 AMP（データは存在しない）

	//開発用
	gxChar m_pFileName[256];
	CAlphaTip* m_pOriginal;
	eModeCompress m_eMode;
	CFileTarga *m_pTga;

	Uint32 m_alphaCheck[enMaxAlphaNum];
	Sint32 m_opaqueCount;	// 不透明カウント用
};
#pragma pack (pop)
#endif
