﻿#include <gxLib.h>
#include "CSpriteStudioManager.h"
#include <gxLib/util/extra/CJSON.h>
#include <gxLib/util/CCsv.h>
#include <gxLib/util/CFileZip.h>

#ifdef PLATFORM_WINDESKTOP
	#define ENABLE_WINDOWS_TOOL	//(Windowsデスクトップ開発のみ対応)
#endif

using namespace rapidjson;

//--------------------
typedef struct StHeaderInfo {
	Sint32 AnimationNum;
	gxChar name[256][FILENAMEBUF_LENGTH];
} StHeaderInfo;

typedef struct StImageList {
	//画像データのリスト
	Sint32 num;
	gxChar Name[1024][FILENAMEBUF_LENGTH];
} StImageList;

typedef struct StPartsList {
	//パーツのリスト
	Sint32 num;
	gxChar Name[1024][FILENAMEBUF_LENGTH];
} StPartsList;

typedef struct StSSAMotions {
	//モーションデータ配列のリスト（１フレーム分）
	Sint32 partsNum;
	double partsData[256][64];
} SSAMotions;

typedef struct StSSAList
{
	Sint32 frmMax;
	SSAMotions ssaMotions[1024];
	Sint32 index[1024];
	Sint32 num[1024];
} StSSAList;

typedef struct StSSAEList
{
	Sint32 ssaeMax;
	gxChar name[256][1024];
} StSSAEList;

typedef struct StSSParts {
	Sint32	partID;	//	Int		part ID　パーツ名に対応するパーツID
	Sint32	texID;	//	Int		reference image ID　テクスチャ名に対応するID
	Sint32	u;		//	Int		source rect-Left　テクスチャ内のセルの開始X座標
	Sint32	v;		//	Int		source rect-Top　テクスチャ内のセルの開始Y座標
	Sint32	w;		//	Int		source rect-Width　テクスチャ内のセルの幅
	Sint32	h;		//	Int		source rect-Height　テクスチャ内のセルの高さ
	Float32	x;		//	Float	position-X　表示X座標
	Float32	y;		//	Float	position-Y　表示Y座標
	Float32	r;		//	Float	angle (=Z axis rotation)　Z回転角度
	Float32	sx;		//	Float	scale-X　X拡大率
	Float32	sy;		//	Float	scale-Y　Y拡大率
	Sint32	cx;		//	Int		pivot offset-X		default=0　原点位置X
	Sint32	cy;		//	Int		pivot offset-Y		default=0　原点位置Y
	Sint32	flipX;	//	Int		flip-H				defalut=false　X反転フラグ
	Sint32	flipY;	//	Int		flip-V				defalut=false　Y反転フラグ
	Float32	alpha;	//	Float	opacity				default=1.0　不透明度
	Sint32	aType;	//	Int		alpha blend type	default=mix　パーツのαブレンド方法
	Sint32	ox1;	//	Int		vertex deformation-LeftTop-X　左上X頂点変形オフセット
	Sint32	oy1;	//	Int		vertex deformation-LeftTop-Y　左上Y頂点変形オフセット
	Sint32	ox2;	//	Int		vertex deformation-RightTop-X　右上X頂点変形オフセット
	Sint32	oy2;	//	Int		vertex deformation-RightTop-Y　右上Y頂点変形オフセット
	Sint32	ox3;	//	Int		vertex deformation-LeftBottom-X　左下X頂点変形オフセット
	Sint32	oy3;	//	Int		vertex deformation-LeftBottom-Y　左下Y頂点変形オフセット
	Sint32	ox4;	//	Int		vertex deformation-RightBottom-X　右下X頂点変形オフセット
	Sint32	oy4;	//	Int		vertex deformation-RightBottom-Y　右下Y頂点変形オフセット
	Sint32	label;	//	Int		color Label		default=0　パーツのカラーラベル
	Sint32	bType;	//	Int		color blend type	default=mix　カラーブレンド方法
	Sint32	rgba1;	//	Int		vertex color-LeftTop-RGBA　左上頂点カラー値（※１）
	Sint32	rgba2;	//	Int		vertex color-RightTop-RGBA　右上頂点カラー値
	Sint32	rgba3;	//	Int		vertex color-LeftBottom-RGBA　左下頂点カラー値
	Sint32	rgba4;	//	Int		vertex color-RightBottom-RGBA　右下頂点カラー値
	Float32	vtx1;	//	float	vertex position-LeftTop-X　左上頂点バッファデータX
	Float32	vty1;	//	float	vertex position-LeftTop-Y　左上頂点バッファデータY
	Float32	vtx2;	//	float	vertex position-RightTop-X　右上頂点バッファデータX
	Float32	vty2;	//	float	vertex position-RightTop-Y　右上頂点バッファデータY
	Float32	vtx3;	//	float	vertex position-LeftBottom-X　左下頂点バッファデータX
	Float32	vty3;	//	float	vertex position-LeftBottom-Y　左下頂点バッファデータY
	Float32	vtx4;	//	float	vertex position-RightBottom-X　右下頂点バッファデータX
	Float32	vty4;	//	float	vertex position-RightBottom-Y　右下頂点バッファデータY

	void assignData( double *pData )
	{
		partID	= (Sint32	)pData[0];
		texID	= (Sint32	)pData[1];
		u		= (Sint32	)pData[2];
		v		= (Sint32	)pData[3];
		w		= (Sint32	)pData[4];
		h		= (Sint32	)pData[5];
		x		= (Float32	)pData[6];
		y		= (Float32	)pData[7];
		r		= (Float32	)pData[8];
		sx		= (Float32	)pData[9];
		sy		= (Float32	)pData[10];
		cx		= (Sint32	)pData[11];
		cy		= (Sint32	)pData[12];
		flipX	= (Sint32	)pData[13];
		flipY	= (Sint32	)pData[14];
		alpha	= (Float32	)pData[15];
		aType	= (Sint32	)pData[16];
		ox1		= (Sint32	)pData[17];
		oy1		= (Sint32	)pData[18];
		ox2		= (Sint32	)pData[19];
		oy2		= (Sint32	)pData[20];
		ox3		= (Sint32	)pData[21];
		oy3		= (Sint32	)pData[22];
		ox4		= (Sint32	)pData[23];
		oy4		= (Sint32	)pData[24];
		label	= (Sint32	)pData[25];
		bType	= (Sint32	)pData[26];
		rgba1	= (Sint32	)pData[27];
		rgba2	= (Sint32	)pData[28];
		rgba3	= (Sint32	)pData[29];
		rgba4	= (Sint32	)pData[30];
		vtx1	= (Float32	)pData[31];
		vty1	= (Float32	)pData[32];
		vtx2	= (Float32	)pData[33];
		vty2	= (Float32	)pData[34];
		vtx3	= (Float32	)pData[35];
		vty3	= (Float32	)pData[36];
		vtx4	= (Float32	)pData[37];
		vty4	= (Float32	)pData[38];
	}
} StSSParts;

struct StAnim1Frm {

	//１フレームに含まれるanimデータ
	Sint32 partNum = 0;
	//StSSParts partData[128]={0};
	std::vector<StSSParts> partData;// [128] = { 0 };

	void add( double *pData )
	{
		/*
		if (partNum >= 128)
		{
			partNum = 128;
		}
		*/
		StSSParts temp;
		partData.push_back(temp);
		partData[partNum].assignData(pData);
		partNum ++;
	}

};

//--------------------
//ToDo:ここらへんの野良変数をちゃんと片付ける

StHeaderInfo HeaderInfo;
StImageList ImageList;
StPartsList PartsList;
StSSAList   SSAList;
StSSAEList  SSAEList;

//--------------------

gxBool makeCSV( gxChar *pFileName , Sint32 index ,Value &o );
gxBool analysisSectionHeader(Value &doc );
gxBool analysisSectionAnimation(Value &doc);

gxBool JSONAnalysis(gxChar *pFileName , Uint8 *pData2 );
gxBool analysisSectionHeader(Value &o);
void MakeZipFile(gxChar* pFileName);


gxBool MakeSSPK( gxChar *pFileName )
{
	//------------------------------------------------------------------------
	//
	//JSONファイルがドラッグ＆ドロップされたときに呼ばれてSSPKを作成する
	//
	//------------------------------------------------------------------------

	size_t uSize = 0;
	Uint8 *pData;

	//JSONファイルを取得
	pData = gxLib::LoadFile(pFileName, &uSize);

	if ( pData == NULL )
	{
		gxLib::SetToast("JSONファイルが読み込めませんでした");
		return gxFalse;
	}

	gxUtil::MemSet(&HeaderInfo, 0x00, sizeof(StHeaderInfo));

	Uint8 *pData2 = new Uint8[uSize + 8];

	gxUtil::MemCpy(pData2, &pData[0], uSize);

	SAFE_DELETES(pData);

	pData2[uSize] = 0x00;

	//JSONの解析

	if (!JSONAnalysis(pFileName, pData2) )
	{
		gxLib::SetToast("JSONファイルが解析できませんでした");
		return gxFalse;
	}

	SAFE_DELETES(pData2);

	return gxTrue;
}


void ResetCache()
{
	//------------------------------------------------------------------------
	//解析データをリセットする
	//------------------------------------------------------------------------

	gxUtil::MemSet( &ImageList  , 0x00 , sizeof(ImageList) );
	gxUtil::MemSet( &PartsList  , 0x00 , sizeof(PartsList) );
	gxUtil::MemSet( &SSAList    , 0x00 , sizeof(SSAList) );

}

gxBool JSONAnalysis(gxChar *pFileName , Uint8 *pData2 )
{
	//------------------------------------------------------------------------
	//JSONファイルを解析して要素を分解する
	//------------------------------------------------------------------------
	//MakeZipFile(pFileName);
	//return gxTrue;

	gxUtil::MemSet( &SSAEList   , 0x00 , sizeof(SSAEList) );

	ResetCache();

	Document doc;

	doc.Parse((gxChar*)pData2);

	bool error = doc.HasParseError();

	if (error)
	{
		gxLib::SetToast("解析失敗-JSONファイルのフォーマットがあわない");
		exit(gxTrue);
	}

	if (error)
	{
		//JSONが解析できなかった
		SAFE_DELETES(pData2);
		return NULL;
	}

	//JSONの分解

	rapidjson::Type type = doc.GetType();

	if ( type == kArrayType)
	{
		//しょっぱなから配列だった場合

		SizeType num = doc.Size();

		for (SizeType i = 0; i < num; i++)
		{
			ResetCache();

			Value& obj = doc[i];
			analysisSectionHeader(obj);;

			analysisSectionAnimation( obj["animation"] );

			//Uint32 uSize = 0;
			//gxChar* pText = dispList( &uSize );
			//SaveListFile( i , pFileName, pText , uSize);
			//SAFE_DELETES(pText);
			makeCSV( pFileName , i , obj["animation"] );

		}
		//gxLib::SetToast("[%s.txt]を保存しました", pFileName );
	}
	else
	{
		analysisSectionHeader(doc);

		Value& obj = doc["animation"];

		analysisSectionAnimation(obj);

		//Uint32 uSize = 0;
		//gxChar* pText = dispList(&uSize);
		//SaveListFile( -1 , pFileName, pText, uSize);
		//gxLib::SetToast("[%s.txt]を保存しました", pFileName);
		//SAFE_DELETES(pText);
	}

	//SSPKにまとめる

	CCsv *pCsv = new CCsv();

	Sint32 yy = 0;

	pCsv->SetCell(0,yy,"max,%d" , SSAEList.ssaeMax );
	yy ++;

	for( Sint32 ii=0; ii<SSAEList.ssaeMax; ii++ )
	{
		pCsv->SetCell(0,yy,"csv,%d,%s.csv" , ii , SSAEList.name[ii] );
		yy ++;
	}

	gxChar path[FILENAMEBUF_LENGTH];
	gxChar name[FILENAMEBUF_LENGTH];
	gxChar noext[FILENAMEBUF_LENGTH];
	gxChar buf[FILENAMEBUF_LENGTH];

	gxUtil::GetPath(pFileName, path);
	gxUtil::GetFileNameWithoutPath(pFileName, name);
	gxUtil::GetFileNameWithoutExt(name, noext);

	sprintf(buf, "%s\\%s\\index.csv", path, noext );

	pCsv->SaveFile( buf );

	SAFE_DELETE( pCsv );

	MakeZipFile(pFileName);

	return gxTrue;


}

gxBool analysisSectionHeader(Value &o)
{
	//------------------------------------------------------------------------
	//ヘッダ情報の取得
	//------------------------------------------------------------------------

	Value& obj = o["images"];

	SizeType num = obj.Size();

	for (SizeType i = 0; i < num; i++) {
		const char* v = obj[i].GetString();
		gxLib::DebugLog("images[%d] = %s", i, v);
		sprintf(ImageList.Name[i], "%s", v);
		ImageList.num = i + 1;
	}

	obj = o["name"];
	const char* v = obj.GetString();
	gxLib::DebugLog("name[%s]", v);

	sprintf(&HeaderInfo.name[HeaderInfo.AnimationNum][0], "%s", v);
	HeaderInfo.AnimationNum ++;

	return gxTrue;
}


gxBool analysisSectionAnimation( Value &o )
{
	//------------------------------------------------------------------------
	//アニメーションデータの解析
	//------------------------------------------------------------------------

	for (Value::ConstMemberIterator itr = o.MemberBegin(); itr != o.MemberEnd(); itr++)
	{
		const char* name = itr->name.GetString();
		const Value& value = itr->value;
		rapidjson::Type type = value.GetType();


		switch (type) {
		case kNullType:     //!< null
		case kFalseType:	// = 1,     //!< false
		case kTrueType:		// = 2,      //!< true
			break;
		case kObjectType:	// = 3,    //!< object
			break;
		case kArrayType:	// = 4,     //!< array 
		{
			Sint32 nowFrm = -1;
			Sint32 dataCnt = 0;

			if (strcmp(name, "ssa") == 0)
			{
				//フレーム数
				SizeType num2 = value.Size();
				rapidjson::Type type2 = value.GetType();

				SSAList.frmMax = num2;

				for (SizeType i = 0; i < num2; i++)
				{
					//パーツ数
					SSAMotions motions;

					rapidjson::Type type3 = value[i].GetType();
					SizeType num3 = value[i].Size();

					motions.partsNum = num3;

					SSAList.num[ i ] = 0;

					for (SizeType j = 0; j < num3; j++)
					{
						if (nowFrm != i)
						{
							nowFrm = i;

							if (i > 0)
							{
								if (SSAList.index[nowFrm - 1] > 0)
								{
									//1つもなかった場合は計算しない
									SSAList.num[nowFrm - 1] = dataCnt - SSAList.index[nowFrm - 1];
								}
							}

							SSAList.index[nowFrm] = dataCnt;
						}

						SSAList.num[nowFrm] ++;

						//２８個のSSモーションデータ
						//motions.frm = i;

						SizeType num4 = value[i][j].Size();
						rapidjson::Type type4 = value[i][j].GetType();
						for (SizeType k = 0; k < num4; k++)
						{
							rapidjson::Type type5 = value[i][j][k].GetType();
							const double val = value[i][j][k].GetDouble();

							motions.partsData[j][k] = val;
						}
						dataCnt++;
					}


					SSAList.ssaMotions[i] = motions;
				}
			}
			else if (strcmp(name, "parts") == 0)
			{
				//Partsリストの作成
				SizeType num = value.Size();
				for (SizeType i = 0; i < num; i++) {
					const char* v = value[i].GetString();
					sprintf(PartsList.Name[i], "%s", v);
					PartsList.num = i + 1;
				}
			}
		}
		break;
		case kStringType:
			gxLib::DebugLog("%s", value.GetString());
			break;

		case kNumberType:
			if (value.IsDouble())
				gxLib::DebugLog("%f", value.GetDouble());
			else
				gxLib::DebugLog("%d", value.GetInt());
			break;

		default:
			gxLib::DebugLog("(unknown type)");
			break;
		}
	}

	return gxTrue;
}


















#ifdef ENABLE_WINDOWS_TOOL

//-------------------------------------------------------------------------------------------------------------------
//Windows専用
//-------------------------------------------------------------------------------------------------------------------

#include <sys/types.h>
#include <sys/stat.h>
//#include <io.h>   //for ios
#include <fcntl.h>
//#include <imagehlp.h> //for ioS
#pragma comment(lib, "imagehlp.lib")


gxChar tempBuf[1024*1024];

gxBool makeCSV( gxChar *pFileName , Sint32 index ,Value &o )
{
	//--------------------------------------------
	//CSV形式のSSPK形式を作成する
	//--------------------------------------------

	gxChar path[FILENAMEBUF_LENGTH];
	gxChar name[FILENAMEBUF_LENGTH];
	gxChar noext[FILENAMEBUF_LENGTH];
	gxChar buf[FILENAMEBUF_LENGTH];

	gxUtil::GetPath(pFileName, path);
	gxUtil::GetFileNameWithoutPath(pFileName, name);
	gxUtil::GetFileNameWithoutExt(name, noext);

	sprintf(buf, "%s\\%s", path, noext);
	CreateDirectory(buf, NULL);

	CCsv *pCsv = new CCsv();
	Sint32 yy = 0;

	for (Sint32 ii = 0; ii< ImageList.num; ii++)
	{
		pCsv->SetCell(0, yy, "images,%d,%s", ii, ImageList.Name[ii]);

		sprintf(buf, "%s\\%s", path, ImageList.Name[ii] );
		Uint8 *pData;
		size_t uSize = 0;
		pData = gxLib::LoadFile(buf, &uSize);

		sprintf(buf, "%s\\%s\\%s", path, noext , ImageList.Name[ii] );
		gxLib::SaveFile( buf, pData , uSize );
		SAFE_DELETES(pData);
		yy++;
	}

	for (Value::ConstMemberIterator itr = o.MemberBegin(); itr != o.MemberEnd(); itr++)
	{
		const char* name = itr->name.GetString();
		const Value& value = itr->value;
		rapidjson::Type type = value.GetType();

		switch (type) {
		case kNullType:     //!< null
		case kFalseType:	// = 1,     //!< false
		case kTrueType:		// = 2,      //!< true
			break;
		case kObjectType:	// = 3,    //!< object
			break;
		case kArrayType:	// = 4,     //!< array 
		{
			if (strcmp(name, "ssa") == 0)
			{
				//フレーム数
				SizeType frmMax = value.Size();

				StAnim1Frm *pAnmAllData = new StAnim1Frm[frmMax];
				for (SizeType frm = 0; frm < frmMax; frm++)
				{
					//パーツ数

					SizeType partsMax = value[frm].Size();

					StAnim1Frm anm1frm;

					for (SizeType prt = 0; prt < partsMax; prt++)
					{
						//39個のSSモーションデータ
						//motions.frm = i;

						SizeType dataNum = value[frm][prt].Size();

						double data[64];
						for (SizeType k = 0; k < dataNum; k++)
						{
							const double val = value[frm][prt][k].GetDouble();
							data[k] = val;
						}
						anm1frm.add( data );
					}
					pAnmAllData[frm] = anm1frm;
				}


				pCsv->SetCell(0, yy, "//cmd,frm, ID, texID, u, v, w, h,vx1,vy1,argb1,vx2,vy2,argb2,vx3,vy3,argb3,vx4,vy4,argb4,aType,alpja,bType,blend,flip_x,flip_y");
				yy++;

				for (Sint32 ii = 0; ii < (Sint32)frmMax; ii++)
				{
					Sint32 max = pAnmAllData[ii].partNum;

					for (Sint32 jj = 0; jj < max; jj++)
					{
						StSSParts* parts = &pAnmAllData[ii].partData[jj];

						Uint32 n = 0;
						n += sprintf( &tempBuf[n], "frm,");
						n += sprintf( &tempBuf[n], "%d,", ii);
						n += sprintf( &tempBuf[n], "%d,", parts->partID);
						n += sprintf( &tempBuf[n], "%d,", parts->texID);
						n += sprintf( &tempBuf[n], "%d,", parts->u);
						n += sprintf( &tempBuf[n], "%d,", parts->v);
						n += sprintf( &tempBuf[n], "%d,", parts->w);
						n += sprintf( &tempBuf[n], "%d,", parts->h);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vtx1);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vty1);
						n += sprintf( &tempBuf[n], "%d,", parts->rgba1);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vtx2);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vty2);
						n += sprintf( &tempBuf[n], "%d,", parts->rgba2);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vtx3);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vty3);
						n += sprintf( &tempBuf[n], "%d,", parts->rgba3);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vtx4);
						n += sprintf( &tempBuf[n], "%.2f,", parts->vty4);
						n += sprintf( &tempBuf[n], "%d,", parts->rgba4);
						n += sprintf( &tempBuf[n], "%d,", parts->aType);
						n += sprintf( &tempBuf[n], "%.2f,", parts->alpha);
						n += sprintf( &tempBuf[n], "%d,", parts->bType);
						n += sprintf( &tempBuf[n], "%.2f,", parts->alpha);	//bRate
						n += sprintf( &tempBuf[n], "%d,", parts->flipX);
						n += sprintf( &tempBuf[n], "%d,", parts->flipY);
						n += sprintf( &tempBuf[n], "\"resetved1/6\",");
						n += sprintf( &tempBuf[n], "\"resetved2/6\",");
						n += sprintf( &tempBuf[n], "\"resetved3/6\",");
						n += sprintf( &tempBuf[n], "\"resetved4/6\",");
						n += sprintf( &tempBuf[n], "\"resetved5/6\",");
						n += sprintf( &tempBuf[n], "\"resetved6/6\",");
						pCsv->SetCell(0, yy, "%s" , tempBuf );
						yy++;
					}
				}

				SAFE_DELETES(pAnmAllData);

				
				//sprintf(buf, "test%02d.txname", index);
				//gxLib::SaveFile(buf, (Uint8*)&ImageList, sizeof(ImageList));

			}
			else if (strcmp(name, "parts") == 0)
			{
				////Partsリスト
				//pCsv->SetCell(0, yy, "parts,partsName" );
				//yy++;
			}
		}
		break;
		case kStringType:
			break;

		case kNumberType:
			if (strcmp(name, "fps") == 0)
			{
				pCsv->SetCell(0, yy, "fps,%d",value.GetInt() );
				yy++;
			}
			else if (strcmp(name, "CanvasWidth") == 0)
			{
				pCsv->SetCell(0, yy, "CanvasWidth,%d", value.GetInt());
				yy++;
			}
			else if (strcmp(name, "CanvasHeight") == 0)
			{
				pCsv->SetCell(0, yy, "CanvasHeight,%d", value.GetInt());
				yy++;
			}
			else if (strcmp(name, "MarginWidth") == 0)
			{
				pCsv->SetCell(0, yy, "MarginWidth,%d", value.GetInt());
				yy++;
			}
			else if (strcmp(name, "MarginHeight") == 0)
			{
				pCsv->SetCell(0, yy, "MarginHeight,%d", value.GetInt());
				yy++;
			}
			break;

		default:
			gxLib::DebugLog("(unknown type)");
			break;
		}
	}

	for( Sint32 ii=0;ii<SSAList.frmMax; ii++ )
	{
		pCsv->SetCell(0, yy, "index,%d,%d,%d", ii , SSAList.index[ii] , SSAList.num[ii] );
		yy ++;
	}

	//モーション解析データを出力する

	sprintf(buf, "%s\\%s\\%s.csv", path, noext, HeaderInfo.name[index] );
	pCsv->SaveFile(buf);
	SAFE_DELETE(pCsv);

	//SSAEリストに加える

	sprintf( SSAEList.name[SSAEList.ssaeMax] , "%s" , HeaderInfo.name[index] );
	SSAEList.ssaeMax ++;

	return gxTrue;
}

struct DirList
{
	std::vector<std::string> m_DirectoryList;
	std::map<std::string,gxBool> m_TempDirectoryList;
};

DirList *s_pDirList = nullptr;


void searchDir(std::string dirName, std::string filter)
{
	//zipのフォルダ構造を再帰的に取得

	if (s_pDirList->m_TempDirectoryList.count(dirName) > 0)
	{
		//既に調査済みのフォルダは再調査しない
		return;
	}

	HANDLE hFind;
	WIN32_FIND_DATA win32fd;

	if (dirName == "")
	{
		hFind = FindFirstFile((dirName + ".\\" + filter/**.*"*/).c_str(), &win32fd);
	}
	else
	{
		hFind = FindFirstFile((dirName + "\\" + filter/**.*"*/).c_str(), &win32fd);
	}

	if (hFind == INVALID_HANDLE_VALUE)
	{
		return;
	}

	do {
		if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (win32fd.cFileName[0] != '.')
			{
				std::string directory = "";
				if (dirName == "")
				{
					directory = win32fd.cFileName;
				}
				else
				{
					directory = dirName + "\\" + win32fd.cFileName;
				}
				searchDir(directory, filter);
			}
		}
		else
		{
			std::string fileName = "";

			if (dirName == "")
			{
				fileName = win32fd.cFileName;
			}
			else
			{
				fileName = dirName + "\\" + win32fd.cFileName;
			}

			std::replace(fileName.begin(), fileName.end(), '\\', '/');
			s_pDirList->m_TempDirectoryList[fileName] = true;
		}

	} while (FindNextFile(hFind, &win32fd));

	FindClose(hFind);
}

std::vector<std::string> MakeDirectoryList(std::string path)
{
	s_pDirList = new DirList();

	s_pDirList->m_DirectoryList.clear();
	s_pDirList->m_TempDirectoryList.clear();

	gxUtil::FileNames fileNames = path;
	std::string filter = "*.*";
	if (fileNames.FileName[0])
	{
		filter = fileNames.FileName;
		path   = fileNames.FilePath;
	}

	searchDir(path, filter);

	for (auto itr = s_pDirList->m_TempDirectoryList.begin(); itr != s_pDirList->m_TempDirectoryList.end(); ++itr)
	{
		s_pDirList->m_DirectoryList.push_back(itr->first);
	}

	std::vector<std::string> list = s_pDirList->m_DirectoryList;
	SAFE_DELETE(s_pDirList);

	return list;
}


void MakeZipFile(gxChar* pFileName)
{
	std::vector<std::string> MakeDirectoryList(std::string path);

	gxUtil::FileNames fNames = pFileName;

	std::string dirPath = fNames.FilePath + fNames.FileNameWithoutExt + "\\";
	std::vector<std::string> list = MakeDirectoryList(dirPath);

	CFileZip zip;

	for (Sint32 ii = 0; ii < list.size(); ii++)
	{
		size_t uSize = 0;
		Uint8* pData = gxLib::LoadFile(list[ii].c_str(), &uSize);
		gxUtil::FileNames fn = list[ii].c_str();

		std::string relPath = fNames.FilePath + fNames.FileNameWithoutExt;
		std::string fn3 = fn.GetRelativeFileName(relPath.c_str());
		zip.AddFile((gxChar*)fn3.c_str(), pData, uSize);
		SAFE_DELETES(pData);
	}

	fNames.ChangeDirUp();
	fNames.ChangeDirDown("sspk_zip");
	dirPath = fNames.FilePath + fNames.FileNameWithoutExt + ".zip";

	zip.Save((gxChar*)dirPath.c_str());
}

#else

gxBool makeCSV( gxChar *pFileName , Sint32 index ,Value &o )
{
	return gxTrue;
}

void MakeZipFile(gxChar* pFileName)
{

}

#endif
