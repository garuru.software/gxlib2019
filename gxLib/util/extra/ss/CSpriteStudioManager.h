﻿#ifndef _CSPRITESTUDIOMANAGER_H_
#define _CSPRITESTUDIOMANAGER_H_

//https://github.com/SpriteStudio/Ss5ConverterToSSAJSON/wiki/SSAJSON-Format

#define SS_PARTS_DATA_AMX (256)

//--------------------------------
//--------------------------------


typedef struct SSImageList
{
	gxChar name[FILENAMEBUF_LENGTH];
} SSImageList;

typedef struct SSIndexList
{
	Sint32 start;
	Sint32 partsNum;

} SSIndexList;

struct SSAnimeList
{
	Sint32 frm;
	Sint32 ID;
	Sint32 texID;
	Float32 u;
	Float32 v;
	Float32 w;
	Float32 h;
	Float32 vx1;
	Float32 vy1;
	Uint32 argb1;
	Float32 vx2;
	Float32 vy2;
	Uint32 argb2;
	Float32 vx3;
	Float32 vy3;
	Uint32 argb3;
	Float32 vx4;
	Float32 vy4;
	Uint32 argb4;
	Sint32 aType;
	Float32 alpha;
	Sint32 bType;
	Float32 blend;
	Sint32 flip_x;
	Sint32 flip_y;

	Sint32 dummy[6];
};

typedef struct SSHeader
{
	Sint32 w;
	Sint32 h;
	Sint32 merginW;
	Sint32 merginH;
	Float32 fps;
	Sint32 animeNum;
	Sint32 imageNum;
	Sint32 indexNum;
	SSImageList *pImageList;
	SSAnimeList *pAnimList;
	SSIndexList *pIndexList;
	Sint32      *pTexturePageIndex;
	Float32     m_NowFrame = 0.0f;
	gxBool      m_bLoop = gxFalse;

	~SSHeader()
	{
		SAFE_DELETES( pImageList );
		SAFE_DELETES( pAnimList );
		SAFE_DELETES( pIndexList );
		SAFE_DELETES(pTexturePageIndex);
	}

} SSHeader;

class CCsv;
class CSpriteStudioManager
{
public:

	enum {
		enPatrsDataMax = SS_PARTS_DATA_AMX,
	};

	CSpriteStudioManager()
	{
		m_Animes = nullptr;
		m_MaxAnimNum = 0;
	}

	~CSpriteStudioManager()
	{
		SAFE_DELETES(m_Animes);
	}

	//SS5Converterから出力したJSONファイルを読み込む
	//void LoadJSON( Sint32 tpg , gxChar *pJSON );

	//JSONからコンバートした独自形式のSSAE相当のCSVを読み込む
	void LoadSSAE_CSV( Sint32 tpg , gxChar *pFileName );

	gxBool LoadSSPK( Sint32 tpg , gxChar *pFileName);

	//SS-JSONからSSPKを作成する
	gxBool MMakeSSPK( gxChar *pFileName );


	Uint32 GetSSAENum()
	{
		return m_MaxAnimNum;
	}

	Uint32 GetMaxFrameNum(Uint32 ssaeIndex);
	Uint32 GetCurrentFrameNum(Uint32 ssaeIndex);

	void OverrideUVWH( Sint32 n , Sint32 tpg , Sint32 u , Sint32 v , Sint32 w , Sint32 h )
	{
		if(tpg!=-1)
		{
			m_SSAnimeOverRide[n].texID = tpg;
		}

		m_SSAnimeOverRide[n].u = u;
		m_SSAnimeOverRide[n].v = v;
		m_SSAnimeOverRide[n].w = w;
		m_SSAnimeOverRide[n].h = h;
	}

	void SetSec( Sint32 ssaeIndex , Float32 sec);
	void SaveCSV(gxChar *pFileName) {};

	gxBool GetLoop(Sint32 ssaeIndex);
	void SetLoop( Sint32 ssaeIndex, gxBool bLoopOn = gxTrue );
	void PutAnime( Sint32 ssaeIndex  , Sint32 x , Sint32 y , Sint32 prio, Float32 sec=-1, Uint32 attribute=ATR_DFLT , Uint32 argb=0 , Uint32 blend=0x00 );

	void Action();
	void Draw();

	SINGLETON_DECLARE( CSpriteStudioManager );

private:

	gxBool cnvCSVtoAnimData( CCsv* pCsv , SSHeader *pHead );

	SSHeader* m_Animes = nullptr;
	Sint32 m_MaxAnimNum = 0;

	std::map<int, SSAnimeList> m_SSAnimeOverRide;

};
#endif
