//ネットワーク管理
#include <gxLib.h>
#include "gxNetworkManager.h"
#include "util/gxUIManager.h"

SINGLETON_DECLARE_INSTANCE( gxNetworkManager );

Sint32 gxNetworkManager::OpenURL( const gxChar *pOpenURL , const gxChar *pPostData, size_t PostSize , std::function<void(gxBool bSuccess , uint8_t *p , size_t size)> func )
{
	Sint32 index = m_sHttpRequestMax;// %enHttpOpenMax;

	sprintf( m_pHttp[ index ].m_URL ,"%s" , pOpenURL );
	//m_pHttp[index].pPostData = pPostData;

	m_pHttp[ index ].uFileSize    = 0;
	m_pHttp[ index ].uReadFileSize = 0;
	m_pHttp[ index ].bClose = gxFalse;

	//SAFE_DELETES( m_pHttp[ index ].pData );

	m_pHttp[ index ].pData    = NULL;
	m_pHttp[ index ].bRequest = gxTrue;
	m_pHttp[ index ].SetSeq(1);
	m_pHttp[ index ].index    = index;
	m_pHttp[ index ].func     = func;

	if( PostSize > 0 )
	{
		SAFE_DELETES(m_pHttp[index].PostData);
		m_pHttp[ index ].PostData = new gxChar[ PostSize ];
	}

	m_sHttpRequestMax ++;

	return index;
}

gxBool gxNetworkManager::CloseURL(Uint32 uIndex)
{
	m_pHttp[uIndex].bClose = gxTrue;
	m_pHttp[uIndex].pData  = nullptr;	//Closeまでしてくれているということは外部で処理されたはず

	return gxTrue;
}


void gxNetworkManager::Action()
{
	std::lock_guard<std::mutex> lock(m_UpdateLock);

	if( m_pHttp.size() > 0 )
	{
		gxUIManager::GetInstance()->NowNetworking();

		for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); )
		{
			if (itr->second.GetSeq() == 100 )
			{
				if( itr->second.func )
				{
					itr->second.func(  (itr->second.Error==0)? gxTrue : gxFalse,  itr->second.pData , itr->second.uFileSize );
				}

				itr->second.SetSeq(999);
			}

			if (itr->second.bClose)
			{
				SAFE_DELETES(itr->second.PostData);
				SAFE_DELETES(itr->second.pData);

				itr = m_pHttp.erase(itr);
                continue;
			}
			++itr;
		}
	}
}


gxNetworkManager::StHTTP * gxNetworkManager::GetNextReq()
{
	std::lock_guard<std::mutex> lock(m_UpdateLock);

	if (m_pHttp.size() == 0) return NULL;


	for (auto itr = m_pHttp.begin(); itr != m_pHttp.end(); itr++ )
	{
		//gxNetworkManager::StHTTP *httpInfo;
		//httpInfo = gxNetworkManager::GetInstance()->GetHTTPInfo( ii );
		if(itr->second.GetSeq()==999 ) continue;

        if(itr->second.bRequest )
		{
            itr->second.bRequest = false;
			return &itr->second;
		}
	}


	return NULL;
}

