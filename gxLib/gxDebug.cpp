// -----------------------------------------------------------
//
// デバッグ用ルーチン色々
// （処理棒など）
// -----------------------------------------------------------

#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include "gxTexManager.h"
#include "gxRender.h"
#include "gxPadManager.h"
#include "gxSoundManager.h"
#include "gxBlueTooth.h"
#include "gxDebug.h"
#include "util/CVirtualPad.h"
#include "util/Font/CFontManager.h"

#define PRIORITY_DEBUG_MAX ( MAX_PRIORITY_NUM+10 )

extern const gxChar* JoyStringTbl[];
extern Sint32 GxPadBitIndexTbl[];

SINGLETON_DECLARE_INSTANCE( gxDebug );

#ifdef GX_DEBUG
Sint32 gxDebug::m_DebugSwitch[8]={1,1,1,1,1,1,1,1};
#else
Sint32 gxDebug::m_DebugSwitch[8]={0,0,0,0,0,0,0,0};
#endif

gxBool gxDebug::m_bMasterDebugSwitch = gxFalse;

static gxChar *s_errString[]={
	"Vertex Buffer OverFlow."	//	ErrVertexBufferOver,
};

gxDebug::gxDebug()
{
	m_sPage = enPageTop;

	m_stInfo.drawCnt   = 0;
	m_stInfo.actionCnt = 0;

	m_sDebugNum = 0;
	m_sDebugCnt = 0;

	//for( Sint32 ii=0; ii<enLogMax; ii++ )
	//{
	//	m_pLineString[ii] = new gxChar[ enStringMaxSize ];
	//}
	m_LineString.resize(enLogMax);

	m_sActionCount = 0;
	m_sDrawCount   = 0;

	m_fTimeOld = gxLib::GetTime();

	m_Cursor    = 0;
	m_ConfigSeq = 0;

	CGameGirl::GetInstance()->GetWindowsResolution( &m_WinW, &m_WinH );

}


gxDebug::~gxDebug()
{
	//for( Sint32 ii=0; ii<enLogMax; ii++ )
	//{
	//	SAFE_DELETES( m_pLineString[ii] );
	//}

}


void gxDebug::SetError( EnErr errNo )
{
#ifdef GX_DEBUG
	if( m_Error[errNo] == 0 )
	{
		gxLib::SetToast("gxLibError=%s" , s_errString[(Sint32)errNo] );
		m_Error[errNo] = 1;
	}
#endif

}

void gxDebug::SetDebugSwitch( gxBool bOn )
{
	m_bMasterDebugSwitch = bOn;
}


void gxDebug::LogDisp( const gxChar *pFormat )
{
#ifdef GX_MASTER
	return;
#endif

	//sprintf( m_pLineString[m_sDebugNum%enLogMax], "[%d]%s", gxLib::GetGameCounter(), _buf );

	m_LineString[m_sDebugNum%enLogMax] = pFormat;

	CDeviceManager::GetInstance()->LogDisp(
		(char*)m_LineString[m_sDebugNum%enLogMax].c_str() );

	m_sDebugNum ++;

}


void gxDebug::Action()
{
	Float32 fTimeNow = gxLib::GetTime();

	CGameGirl::GetInstance()->GetWindowsResolution( &m_WinW, &m_WinH );

	if( fTimeNow  >= m_fTimeOld+1.0f )
	{
		m_stInfo.actionCnt = m_sActionCount;
		m_stInfo.drawCnt   = m_sDrawCount;

		m_sActionCount = 0;
		m_sDrawCount   = 0;

		m_fTimeOld = gxLib::GetTime();
	}
}


void gxDebug::Draw()
{
	//Float32 fScale = CGameGirl::GetInstance()->GetFontManager()->GetScale();
	//CGameGirl::GetInstance()->GetFontManager()->SetScale(0.5f);
	m_FontWidth = CGameGirl::GetInstance()->GetFontManager()->GetFontSize();


	if( !CGameGirl::GetInstance()->IsDeviceMode() )
	{
		m_ConfigSeq = 0;
		if( m_pVPadButton )  SAFE_DELETE( m_pVPadButton );
		if(m_pManualButton[0])  SAFE_DELETE(m_pManualButton[0]);
		if (m_pManualButton[1])  SAFE_DELETE(m_pManualButton[1]);


		//CGameGirl::GetInstance()->GetFontManager()->SetScale(fScale);
		return;
	}

	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	gxLib::DrawColorBox(
		0,0,	0xC00000ff,
		sw,0,	0xC00000ff,
		sw,sh,	0xff000000,
		0,sh,	0xff000000,
		PRIORITY_DEBUG_MAX,ATR_DFLT
	);

	drawDefault();

	//m_sPage = enPageSound;
	//m_sPage = enPageBench;

	switch( m_sPage ){
	case enPageTop:
		drawMenu();
		break;

	case enPageLog:
		debugLogDisp();
		break;

	case enPagePad:
		controllerConfig();
		break;

	case enPageTex:
		textureView();
		break;

	case enPageSound:
		soundView();
		break;

	case enPageRAM:
		memView();
		break;

	case enPageHTTP:
		httpView();
		break;

	case enPageFile:
		fileView();
		break;

	case enPageCheck:
		renderView();
		break;

	case enPageBench:
		benchView();
		break;
	default:
		break;
	}

	m_sDrawCallCount = 0;
}

void gxDebug::drawDefault()
{
	gxChar* tbl[] = {
		"DEBUG INFORMATION",
		"CONSOLE LOG",
		"GAMEPAD",
		"TEXTURE",
		"AUDIO",
		"MEMORY",
		"FILE",
		"HTTP",
		"RENDER",
		"BENCHMARK",
		"BENCHMARK TEST",
	};

	gxLib::Printf(32, 8, PRIORITY_DEBUG_MAX + 1, ATR_DFLT, 0xffffffff, "[ %s ]", tbl[m_sPage]);

	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_LEFT))
	{
		changePage(m_sPage - 1);
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_RIGHT))
	{
		changePage(m_sPage + 1);
	}

	Sint32 ax, ay, mx, my;
	Sint32 az = PRIORITY_DEBUG_MAX;

	ax = m_WinW - 128;
	ay = 0;

	mx = gxLib::Joy(0)->mx;
	my = gxLib::Joy(0)->my;

	gxLib::GetNativePosition( &mx , &my );

	gxRect rect;

	Sint32 bPush = gxFalse;
	rect.Set(ax, ay, 128, 128);

	if (gxLib::Joy(0)->psh & MOUSE_L)
	{
		if (rect.IsHit(mx, my))
		{
			bPush = gxTrue;
		}
	}
	else if (gxLib::Joy(0)->rls & MOUSE_L)
	{
		if (rect.IsHit(mx, my))
		{
			changePage(m_sPage + 1);
		}
	}

	gxLib::DrawBox(rect.x1, rect.y1, rect.x2, rect.y2, az, gxTrue, ATR_DFLT, (bPush) ? 0x80ffff00 : 0x80ff8000);
}

void gxDebug::drawMenu()
{
	Sint32 hh = m_FontWidth;

	Sint32 ax, ay, az;

	ax = 16;
	ay = 64;
	az = PRIORITY_DEBUG_MAX;

	size_t uNow, uTotal, uMax;

	CGameGirl::GetInstance()->GetMemoryRemain(&uNow, &uTotal, &uMax);

	Uint32 glo = CGameGirl::GetInstance()->GetIPAddressV4(gxFalse);
	Uint32 loc = CGameGirl::GetInstance()->GetIPAddressV4(gxTrue);
	Uint32 gID = CGameGirl::GetInstance()->GetUID();

	Sint32 gamew = WINDOW_W, gameh = WINDOW_H;
	Sint32 winw = WINDOW_W, winh = WINDOW_H;

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);


#ifdef _USE_OPENGL
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "OPENGL");
#else
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "DIRECT-X");
#endif

	ay += hh;

#ifdef _USE_MULTITHREAD_
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "Multi Thread Mode");
#else
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "Single Thread Mode");
#endif

	ay += hh;

#ifdef GX_DEBUG
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "Debug Mode");
#else
	gxLib::Printf(16, ay, az, ATR_DFLT, 0xffffffff, "Release Mode");
#endif

	ay = 5*hh;

	//	gxLib::Printf( 16    , ay+32 , az , ATR_DFLT , 0xffffffff , "ScreenResolution" );
	//	gxLib::Printf( 16+128, ay+32 , az , ATR_DFLT , 0xffffffff , "%d , %d / %d , %d" );

	uTotal = MAX_RAM_MB;
	Sint32 ww = 400;
	Float32 fTimeNow = gxLib::GetTime();

	Sint32 ln = 0;
	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "IP(GLOBAL)");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d.%d.%d.%d", (glo & 0xff000000) >> 24, (glo & 0x00ff0000) >> 16, (glo & 0x0000ff00) >> 8, (glo & 0x000000ff) >> 0);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "FPS %d/%d", m_stInfo.drawCnt , FRAME_PER_SECOND);
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "U%d / D%d", m_stInfo.actionCnt, m_stInfo.drawCnt );//, FRAME_PER_SECOND , m_sDrawCallCount );
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "BenchScore");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%.4f pts.", gxLib::GetBenchmarkScore() );
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Reso App");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d x %d", WINDOW_W, WINDOW_H);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Reso Device");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d x %d", winw, winh);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Reso Scaling" );
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d x %d (%.2f%%)", gamew, gameh , 100.0f*gamew/WINDOW_W );
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "RAM");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d / %dMB (%d%%)", uNow, uTotal , 100*uNow/uTotal);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "USE RAM MAX");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%dMB", uMax);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Order");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d / %d", m_stInfo.OrderNum + m_stInfo.SubOrderNum, MAX_ORDER_NUM);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Sub");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d (%d％)", m_stInfo.SubOrderNum, 100 * (m_stInfo.SubOrderNum + 1) / (m_stInfo.OrderNum + m_stInfo.SubOrderNum + 1));
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "RCmd");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d", m_stInfo.cmd_max);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "VTX");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d", m_stInfo.vtx_max);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Index");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d", m_stInfo.idx_max);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "Time");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%f ", fTimeNow);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "ID");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d", gID);
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "IP(LOCAL)");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%d.%d.%d.%d", (loc & 0xff000000) >> 24, (loc & 0x00ff0000) >> 16, (loc & 0x0000ff00) >> 8, (loc & 0x000000ff) >> 0);
	ln++;

	//bt
	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "BT");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%s", CBlueToothManager::GetInstance()->IsBlueToothExist() ? "Enable" : "Disable");
	ln++;

	gxLib::Printf(ax, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "RenderTime");
	gxLib::Printf(ax + ww, ay + hh * ln, az, ATR_DFLT, 0xffffffff, "%.4f msec %.2f％", m_stInfo.RenderTime, 100.0f * m_stInfo.RenderTime / (1.0f / FRAME_PER_SECOND));
	ln++;

	//sound

/*
    Sint32 num = 0;

	for (Sint32 ii = 0; ii < MAX_SOUND_NUM; ii++)
	{
		if (gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow)
			//		if( gxLib::IsPlayAudio( ii ) )
		{
			gxLib::Printf(WINDOW_W, ay + 12 * num, az, ATR_STR_RIGHT, 0xffffffff, "snd(%03d) %04x/%d/%.2f",
				ii,
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->uStatus,
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->bPlayNow,
				gxSoundManager::GetInstance()->GetPlayInfo(ii)->fVolume
			);

			num++;
		}
	}
*/
	gxLib::Printf( m_WinW, m_WinH - hh, az, ATR_STR_RIGHT | ATR_DFLT, 0xffffffff, "gxLib ver.%d.%d.%d (C) garuru software labs.", VERSION_MAJOR, VERSION_MINOR, VERSION_RELEASE);
}




void gxDebug::debugLogDisp()
{
	//debugLog表示
	drawConsole();
}


void gxDebug::drawCircleRader()
{
	//ファイル読み込みで止まっていないときに動作

	static Float32 m_fRotArray[360];
	static Sint32  m_sRotationCnt = 0;
	static Float32 m_fRotation = 0.0f;

	m_fRotArray[ m_sRotationCnt%360 ] = m_fRotation;

	m_fRotation += 3.2f;
	m_sRotationCnt += 1;

	Sint32 ax,ay,ax1,ay1,ax2,ay2;
	ax = 32;
	ay = 32;

	Sint32 max = 64;

	for( Sint32 ii=0; ii<max;ii++ )
	{
		if( ii >= m_sRotationCnt ) continue;

		Sint32 n1 = ( m_sRotationCnt + 360 - 1 - ii )%360;
		Sint32 n2 = ( m_sRotationCnt + 360 - 2 - ii )%360;

		ax1 = ax+gxUtil::Cos( m_fRotArray[n1] )*32;
		ay1 = ay+gxUtil::Sin( m_fRotArray[n1] )*32;

		ax2 = ax+ gxUtil::Cos( m_fRotArray[n2] )*32;
		ay2 = ay+ gxUtil::Sin( m_fRotArray[n2] )*32;

		Sint32 alpha = 255 * (max-1-ii) / (max-1);

		gxLib::DrawTriangle( ax  , ax  , ax1 , ay1 , ax2 , ay2 , 120 , gxTrue , ATR_DFLT , ARGB( alpha , 0x00 , 0xff , 0x00 ) );
	}
}

void gxDebug::drawConsole()
{
	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

    Sint32 ax,ay,az,ah = m_FontWidth;

	ax = 16;
	ay = m_WinH - ((enLogMax+3)* ah);
	az = PRIORITY_DEBUG_MAX;

	Sint32 max = enLogMax;

	Sint32 yy = m_sDebugNum;
	if( yy >= enLogMax ) yy = enLogMax;

	Sint32 y1 ,y2;
	y1 = ay;
	y2 = m_WinH - ah;

	gxLib::DrawBox( 0, y1, sw, y2   , PRIORITY_DEBUG_MAX, gxTrue , ATR_DFLT , 0x80000080 );
	gxLib::DrawBox( 0, y1-ah, sw, y1, PRIORITY_DEBUG_MAX, gxTrue, ATR_DFLT, 0x80008080);
	gxLib::DrawBox( 0, y2, sw, y2+ah, PRIORITY_DEBUG_MAX, gxTrue , ATR_DFLT , 0x80008080 );

	ay += yy*16;

	for( Sint32 ii=0; ii<max;ii++ )
	{
		if( ii >= m_sDebugNum ) continue;

		Sint32 n = ( m_sDebugNum + enLogMax - 1 - ii )%enLogMax;

		gxLib::Printf( ax , y2 - ah - ah *ii , PRIORITY_DEBUG_MAX , ATR_DFLT , ARGB_DFLT , "%s", m_LineString[n].c_str() );
	}

	gxChar *funcName[]={
		"pause",
		"sound",
		"step",
		"pad",
		"screen",

		"reset",
		"filter",
		" ",
		"fullspeed",
		"debugWindow",

		"none",
		"sshot",
		"none",
		"none",
		"none",
	};


	CGameGirl::GetInstance()->GetFontManager()->SetScale(0.5f);
	for( Sint32 ii=0; ii<5; ii++ )
	{
		Sint32 n = ii;
		if (gxLib::KeyBoard(gxKey::SHIFT)&enStatPush)
		{
			n += 5;

			if (gxLib::KeyBoard(gxKey::CTRL)&enStatPush)
			{
				n += 5;
			}
		}

		gxLib::Printf( ax + (sw/5)*ii , sh-26 , PRIORITY_DEBUG_MAX , ATR_DFLT , ARGB_DFLT , funcName[n] );
	}

    for(Sint32 ii=0; ii<1390; ii+= 32)
    {
        gxLib::Printf( 0 , ii , PRIORITY_DEBUG_MAX , ATR_DFLT , ARGB_DFLT , "Y1=%d",ii );
    }

    CGameGirl::GetInstance()->GetFontManager()->SetScale(1.0f);

    for(Sint32 ii=0; ii<1390; ii+= 32)
    {
        gxLib::Printf( 128 , ii , PRIORITY_DEBUG_MAX , ATR_DFLT , ARGB_DFLT , "Y2=%d",ii );
    }

}


void gxDebug::controllerConfig()
{
	Sint32 prio = PRIORITY_DEBUG_MAX + 1;
	Sint32 gamew = WINDOW_W, gameh = WINDOW_H;
	Sint32 winw = WINDOW_W, winh = WINDOW_H;

	//プレイヤーデバイスをコントロールする
	for (Sint32 ii = 0; ii < PLAYER_MAX; ii++)
	{
		drawgxKeyInfo(ii);
	}

	//バーチャルパッドのON・OFFボタン

	CGameGirl::GetInstance()->GetGameResolution(&gamew, &gameh);
	CGameGirl::GetInstance()->GetWindowsResolution(&winw, &winh);

	if( m_pVPadButton == nullptr )
	{
		m_pVPadButton = new gxUtil::RoundButton( winw-64 , 256 , prio , 0xFFFF0000 , 64 );
		m_pVPadButton->SetText("VPad");
	}

	m_pVPadButton->Update();

	m_pVPadButton->SetPos(winw - 64, 256, prio);

	if( m_pVPadButton->IsTrigger() )
	{
		gxBool bDisp = CVirtualStick::GetInstance()->IsDisp();

		gxLib::SetVirtualPad( !bDisp );
	}

	m_pVPadButton->Draw();

	Sint32 gw,gh,sw,sh;
	gxLib::GetDeviceResolution( &gw , &gh , &sw , &sh );

	for( Sint32 ii=0; ii<3;ii++)
	{
		gxVector2 pos = gxUtil::Touch(ii)->GetPosition();

		gxLib::Printf(sw, m_FontWidth *ii,PRIORITY_DEBUG_MAX,ATR_STR_RIGHT , 0xffffffff , "%.2f,%.2f",
			pos.x,pos.y );
			//gxLib::Joy(0)->mx , gxLib::Joy(0)->my );
	}


#ifdef PLATFORM_WINDESKTOP
	void ControllerDeviceConfig();
	ControllerDeviceConfig();
#endif

}

void gxDebug::drawgxKeyInfo( Sint32 id )
{
	//gxLibで受け付けた入力内容を表示する

	gxVector2 analogL;
	gxVector2 analogR;
	gxVector2 analogN;
	Uint32  key = gxLib::Joy(id)->psh;
	gxVector3 gyro,accel,orientation,magneField;

	analogL.x = gxLib::Joy(id)->lx;		// 左アナログＸ
	analogL.y = gxLib::Joy(id)->ly;		// 左アナログＹ
	analogR.x = gxLib::Joy(id)->rx;		// 右アナログ
	analogR.y = gxLib::Joy(id)->ry;		// 右アナログ
	analogN.x = gxLib::Joy(id)->lt;		// Lトリガー
	analogN.y = gxLib::Joy(id)->rt;		// Rトリガー

	//各種センサー値
	gyro  = gxLib::Joy(id)->gyro;		//ジャイロ
	accel = gxLib::Joy(id)->accel;		//加速度
	orientation = gxLib::Joy(id)->orientation;	//方向
	magneField  = gxLib::Joy(id)->magneField;	//地磁気

	Sint32 ax=32+id*(WINDOW_W/2),ay=128,az=PRIORITY_DEBUG_MAX;

	Sint32 yy = 0;

	ay += 196;

	Sint32 wx = ax + 96;
	Sint32 wy = ay + 64;

	gxLib::Printf(wx , wy-128, az, ATR_STR_CENTER, ARGB_DFLT, "Player %d", id + 1);
	gxLib::DrawBox(wx - 64, wy - 64, wx + 64, wy + 64, az, gxTrue, ATR_DFLT, 0xff808080);
	gxLib::DrawPoint(wx + analogL.x * 64, wy + analogL.y * 64, az, ATR_DFLT, 0xffff0000, 32.0f);
	gxLib::DrawPoint(wx + analogR.x * 64, wy + analogR.y * 64, az, ATR_DFLT, 0xfffff00f, 24.0f);

	gxLib::DrawPoint(wx -64, wy-64 + analogN.x * 128, az, ATR_DFLT, 0xff00ffff, 16.0f);
	gxLib::DrawPoint(wx +64, wy-64 + analogN.y * 128, az, ATR_DFLT, 0xff00ffff, 16.0f);

	yy = 0;

	for (Sint32 ii = 0; ii < 32; ii++)
	{
		if (gxLib::Joy(id)->psh & (0x01 << ii))
		{
			gxLib::Printf(ax, ay + yy*24, az, ATR_DFLT, 0xffffffff, "%s", JoyStringTbl[ GxPadBitIndexTbl[ii] ]);
			yy++;
		}
	}

    if( id != 0 ) return;
    
    //センサー類
    static gxPadManager::StSensor sensor;
    if( gxUtil::Touch()->IsPush())
    {
        
    }
    else
    {
        sensor.gyro = gxLib::Joy(0)->gyro;
        sensor.accel = gxLib::Joy(0)->accel;
        sensor.magne = gxLib::Joy(0)->magneField;
        sensor.orient = gxLib::Joy(0)->orientation;
    }

    //----------------------------------------------------
    Sint32 w = 128;
	Sint32 hh = m_FontWidth;
	Sint32 bx,by;
    Uint32 argb = 0xff00ff00;
    gxVector3 *p;
    ax += 128;
    ay += 256;

    bx = ax;
    by = ay;

    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.accel.x*w/2 , by+sensor.accel.y*w/2,az,ATR_DFLT , argb,16.0f);
    
    
    bx += 256;
    by += 0;
    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.accel.z*w/2 , by+sensor.accel.y*w/2,az,ATR_DFLT , argb,16.0f);
    p = &sensor.accel;
	gxLib::Printf(bx + 256, by-hh*2, az, ATR_DFLT, 0xff00ff00, "accel");
	gxLib::Printf(bx+256, by-hh, az, ATR_DFLT, 0xffffffff, "x:%.2f" ,p->x);
    gxLib::Printf(bx+256, by-0, az, ATR_DFLT, 0xffffffff, "y:%.2f" ,p->y);
    gxLib::Printf(bx+256, by+hh, az, ATR_DFLT, 0xffffffff, "z:%.2f" ,p->z);

    
    //----------------------------------------------------
    argb = 0xffff0000;
    ax += 0;
    ay += 256;
    
    bx = ax;
    by = ay;
	gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.gyro.x*w/2 , by+sensor.gyro.y*w/2,az,ATR_DFLT , argb,16.0f);
    
    bx += 256;
    by += 0;
    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.gyro.z*w/2 , by+sensor.gyro.y*w/2,az,ATR_DFLT , argb,16.0f);
    p = &sensor.gyro;
	gxLib::Printf(bx + 256, by - hh * 2, az, ATR_DFLT, 0xff00ff00, "gyro");
	gxLib::Printf(bx+256, by-hh, az, ATR_DFLT, 0xffffffff, "x:%.2f" ,p->x);
    gxLib::Printf(bx+256, by-0, az, ATR_DFLT, 0xffffffff, "y:%.2f" ,p->y);
    gxLib::Printf(bx+256, by+hh, az, ATR_DFLT, 0xffffffff, "z:%.2f" ,p->z);

    //----------------------------------------------------
    argb = 0xffff00ff;
    ax += 0;
    ay += 256;
    
    bx = ax;
    by = ay;
	gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.magne.x*w/2 , by+sensor.magne.y*w/2,az,ATR_DFLT , argb,16.0f);
    
    bx += 256;
    by += 0;
    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
//    gxLib::DrawPoint(bx + sensor.magne.fz*w/2 , by+sensor.magne.fy*w/2,az,ATR_DFLT , argb,16.0f);
    gxLib::DrawCircle(bx,by,az,ATR_DFLT , 0xff808080,w , 3.0f );
    Float32 _fx = gxUtil::Cos(sensor.magne.y) * (w/2);
    Float32 _fy = gxUtil::Sin(sensor.magne.y) * (w/2);
    gxLib::DrawPoint(bx + _fx , by+_fy,az,ATR_DFLT , argb,16.0f);
    p = &sensor.magne;
	gxLib::Printf(bx + 256, by - hh * 2, az, ATR_DFLT, 0xff00ff00, "magne");
	gxLib::Printf(bx+256, by-hh, az, ATR_DFLT, 0xffffffff, "x:%.2f" ,p->x);
    gxLib::Printf(bx+256, by-0, az, ATR_DFLT, 0xffffffff, "y:%.2f" ,p->y);
    gxLib::Printf(bx+256, by+hh, az, ATR_DFLT, 0xffffffff, "z:%.2f" ,p->z);

	//----------------------------------------------------
    argb = 0xffffff00;
    ax += 0;
    ay += 256;
    bx = ax;
    by = ay;

    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.orient.x*w/2 , by+sensor.orient.y*w/2,az,ATR_DFLT , argb,16.0f);
    
    bx += 256;
    by += 0;
    gxLib::DrawLine(bx-w/2,by,bx+w/2,by,az,ATR_DFLT , argb);
    gxLib::DrawLine(bx,by-w/2,bx,by+w/2,az,ATR_DFLT , argb);
    gxLib::DrawPoint(bx + sensor.orient.z*w/2 , by+sensor.orient.y*w/2,az,ATR_DFLT , argb,16.0f);
    p = &sensor.orient;
	gxLib::Printf(bx + 256, by - hh * 2, az, ATR_DFLT, 0xff00ff00, "orientation");
	gxLib::Printf(bx+256, by-32, az, ATR_DFLT, 0xffffffff, "x:%.2f" ,p->x);
    gxLib::Printf(bx+256, by-0, az, ATR_DFLT, 0xffffffff, "y:%.2f" ,p->y);
    gxLib::Printf(bx+256, by+32, az, ATR_DFLT, 0xffffffff, "z:%.2f" ,p->z);
    

}

void gxDebug::textureView()
{
	///----------------------------------------------------------
	// texture確認画面
	///----------------------------------------------------------
	Sint32 az = PRIORITY_DEBUG_MAX;
	Sint32 ax = 32;
	//強制的にアクションを止める
	CGameGirl::GetInstance()->StopGameMain();

	if( m_ConfigSeq == 0 )
	{
		m_Cursor = 0;
		m_ConfigSeq = 100;
	}

	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		m_Cursor --;
	}
	else 	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		m_Cursor ++;
	}

	if( gxLib::KeyBoard( gxKey::RETURN )&enStatTrig )
	{
		gxLib::UploadTexture( gxTrue );
		gxLib::SetToast("UploadTexture()");
	}


	Sint32 maxTex = (MAX_MASTERTEX_NUM+6);

	m_Cursor = ( maxTex + m_Cursor)%(maxTex);

	Sint32 WidthHeight = m_WinH;

	if( m_WinW < m_WinH ) WidthHeight = m_WinW;

	Float32 fx = 1.0f * WidthHeight / 2048.0f;
	Float32 fy = 1.0f * WidthHeight / 2048.0f;

	gxLib::PutSprite( 0, 0, az ,m_Cursor*64, 0,0,2048,2048,0,0,ATR_DFLT , ARGB_DFLT , 0 , fx, fy  );
	gxLib::Printf( ax   , 64   , az , ATR_DFLT , 0xff01FF01 , "[ PAGE %d / %d ]" , m_Cursor , MAX_MASTERTEX_NUM );


	gxLib::DrawLine( 2048*fx/2 , 0 , 2048*fx/2 , 2048*fy , az , ATR_DFLT , 0xff00ff00 );
	gxLib::DrawLine( 0 , 2048*fy/2 , 2048*fx   , 2048*fy/2 , az , ATR_DFLT , 0xff00ff00 );

}

void gxDebug::soundView()
{
	///----------------------------------------------------------
	// sound確認画面
	///----------------------------------------------------------
	Sint32 ax = 32;
	Sint32 ay = 64;
	Sint32 az = PRIORITY_DEBUG_MAX;
	Sint32 hh = m_FontWidth;

	if( m_ConfigSeq == 0 )
	{
		m_Cursor = 0;
		m_ConfigSeq = 100;
	}

	if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_UP))
	{
		m_Cursor --;
	}
	else if (gxUtil::KeyBoard()->IsRepeat(gxKey::NUMPAD_DOWN))
	{
		m_Cursor ++;
	}
	m_Cursor = ( MAX_SOUND_NUM + m_Cursor)%MAX_SOUND_NUM;

	if (gxLib::KeyBoard(gxKey::RETURN)&enStatTrig )
	{
		gxLib::PlayAudio( m_Cursor );
	}
	if (gxLib::KeyBoard(gxKey::SPACE)&enStatTrig )
	{
		if( gxSoundManager::GetInstance()->GetPlayInfo( m_Cursor )->bReverb )
		{
			gxLib::SetAudioReverb( m_Cursor , gxFalse );
		}
		else
		{
			gxLib::SetAudioReverb( m_Cursor , gxTrue );
		}
	}

	gxLib::Printf( ax,ay  ,az , ATR_DFLT , 0xff00ff00 , "MASTER VOLUME %f" , gxLib::GetAudioMasterVolume() );

	ay += 64;

	//static gxChar fileName[MAX_SOUND_NUM][1024];
	gxChar fileName[FILENAMEBUF_LENGTH];

	Uint32 argb = 0xffffffff;
	Sint32 cnt = 0;
	for ( Sint32 ii=0; ii<MAX_SOUND_NUM; ii++ )
	{
		Sint32 n = m_Cursor + ii;

		if( n >= MAX_SOUND_NUM ) break;

		argb = 0xffffffff;

		StPlayInfo* pInfo = gxSoundManager::GetInstance()->GetPlayInfo( n );

		if( n == m_Cursor )
		{
			gxLib::Printf( ax-24 , ay+ii*hh  ,az , ATR_DFLT , argb , ">" );
		}

		if (!pInfo->bUse)
		{
			gxLib::Printf( ax,ay+ii*hh  ,az , ATR_DFLT , argb , "%02d.--------" , n );
			continue;
		}

		gxUtil::GetFileNameWithoutPath( pInfo->fileName , fileName );
		if( gxLib::IsPlayAudio( n ) ) argb = 0xffff0000;

		gxLib::Printf( ax,ay+ii*hh  ,az , ATR_DFLT , argb , "%02d.%-20s" , n , fileName);
		gxLib::Printf( ax+640, ay + ii * hh, az, ATR_DFLT, argb, "%.2f %.2f", pInfo->fPanning, pInfo->fFreqRatio);

		if (pInfo->bReverb)
		{
			gxLib::Printf(ax + 480, ay + ii * hh, az, ATR_DFLT, 0xffffff00, "Reverb");
		}

		cnt ++;
		if( cnt >= 16 ) break;
	}

}


void gxDebug::memView()
{
	//強制的にアクションを止める
	//CGameGirl::GetInstance()->StopGameMain();

	Sint32 ax =32-0;
	Sint32 ay =128;
	Sint32 az = PRIORITY_DEBUG_MAX;

	size_t free,uTotal,uNow,uMax;

	CGameGirl::GetInstance()->GetMemoryRemain( &uNow , &uTotal , &uMax );

	uTotal = MAX_RAM_MB;

	free = uTotal - uNow;

	Float32 fRatio    = 1.0f * uNow / uTotal;
	Float32 fRatioMax = 1.0f * uMax / uTotal;

	Sint32 y = ay;
	Sint32 h = m_FontWidth;
	Sint32 w = WINDOW_W*0.9;

	gxLib::DrawBox( ax , y , ax + w           , y + 32 , az , gxTrue  , ATR_DFLT , 0xffffff00 );
	gxLib::DrawBox( ax , y , ax + w*fRatioMax , y + 32 , az , gxTrue  , ATR_DFLT , 0xff00ff00 );
	gxLib::DrawBox( ax , y , ax + w*fRatio    , y + 32 , az , gxTrue  , ATR_DFLT , 0xffff0000 );
	gxLib::DrawBox( ax , y , ax + w           , y + 32 , az , gxFalse , ATR_DFLT , 0xffffffff );

	y += 64;

	gxLib::Printf( 16    , y+h*0 , az , ATR_DFLT , 0xffffffff , "USE");
	gxLib::Printf( 16    , y+h*1 , az , ATR_DFLT , 0xffffffff , "MAX");
	gxLib::Printf( 16    , y+h*2 , az , ATR_DFLT , 0xffffffff , "FREE");

	gxLib::Printf( 320   , y+h*0 , az , ATR_DFLT , 0xffffffff , "%dMB/ %dMB (%.2f％)" , uNow , uTotal , fRatio*100.0f);
	gxLib::Printf( 320   , y+h*1 , az , ATR_DFLT , 0xffffffff , "%dMB" ,uMax);
	gxLib::Printf( 320   , y+h*2 , az , ATR_DFLT , 0xffffffff , "%dMB" ,free);


	y += 260;

	//SysRAM以降に使用したメモリ

	fRatio = 1.0f * (uNow- m_uSystemUseRAM) / (uTotal- m_uSystemUseRAM);
	fRatioMax = 1.0f * (uMax- m_uSystemUseRAM) / (uTotal- m_uSystemUseRAM);

	gxLib::Printf(16, y + h * 0, az, ATR_DFLT, 0xffffffff, "APP USE Memory Ratio (%.2f％)" , fRatio*100.0f);

	y += 64;
	gxLib::DrawBox(ax, y, ax + w,            y + 24, az, gxTrue,  ATR_DFLT, 0xffffff00 );
	gxLib::DrawBox(ax, y, ax + w*fRatioMax , y + 24, az, gxTrue,  ATR_DFLT, 0xff00ff00 );
	gxLib::DrawBox(ax, y, ax + w*fRatio,     y + 24, az, gxTrue,  ATR_DFLT, 0xffff0000 );
	gxLib::DrawBox(ax, y, ax + w,            y + 24, az, gxFalse, ATR_DFLT, 0xffffffff );

	y += 64;

	gxLib::Printf( 16    , y+h*0 , az , ATR_DFLT , 0xffffffff , "USE");
	gxLib::Printf( 16    , y+h*1 , az , ATR_DFLT , 0xffffffff , "MAX");
	gxLib::Printf( 16    , y+h*2 , az , ATR_DFLT , 0xffffffff , "FREE");

	gxLib::Printf( 320   , y+h*0 , az , ATR_DFLT , 0xffffffff , "%dMB/ %dMB (%.2f％)" , uNow-m_uSystemUseRAM , uTotal-m_uSystemUseRAM , fRatio*100.0f);
	gxLib::Printf( 320   , y+h*1 , az , ATR_DFLT , 0xffffffff , "%dMB" ,uMax-m_uSystemUseRAM );
	gxLib::Printf( 320   , y+h*2 , az , ATR_DFLT , 0xffffffff , "%dMB" ,free- (uNow- m_uSystemUseRAM));
}

void gxDebug::fileView()
{
	//強制的にアクションを止める
	CGameGirl::GetInstance()->StopGameMain();

	Sint32 az = PRIORITY_DEBUG_MAX;
}

gxBool drawPot( Float32 x , Float32 y );

void gxDebug::benchView()
{
#if 0
	Sint32 ax = 0;
	Sint32 ay = 32;
	Sint32 az = PRIORITY_DEBUG_MAX;

	//強制的にアクションを止める
	CGameGirl::GetInstance()->StopGameMain();

	//2320

	if( m_ConfigSeq == 0 )
	{
		m_Cursor = 0;
		m_ConfigSeq = 100;
		m_sPolygonNum = 0;
		m_sMaxPolygonNum = 0;
		m_fOldTime = gxLib::GetTime();
		return;
	}


	Float32 fOld = m_fOldTime;

	m_fOldTime = gxLib::GetTime();

	if( (m_fOldTime - fOld) < (1.0f / 60.0f) )
	{
		m_sPolygonNum += 10;
		if( m_sPolygonNum > m_sMaxPolygonNum ) m_sMaxPolygonNum = m_sPolygonNum;
	}
	else
	{
		m_sPolygonNum -= 10;
	}

	for( Sint32 ii=0; ii<m_sPolygonNum; ii++ )
	{
		Sint32 ax = gxLib::Rand()%WINDOW_W;
		Sint32 ay = gxLib::Rand()%WINDOW_H;
		gxLib::DrawBox( ax-64,ay-64,ax+64,ay+64 ,100, gxTrue, ATR_DFLT , gxLib::Rand() );
		gxLib::DrawBox( ax-64,ay-64,ax+64,ay+64 ,100, gxTrue, ATR_ALPHA_PLUS , gxLib::Rand() );
	}
	
	gxLib::Printf( 16    , ay+0  , az , ATR_DFLT , 0xffffffff , "POLYGON %d , %f msec" , m_sPolygonNum ,(m_fOldTime - fOld) );
	gxLib::Printf( 16    , ay+16 , az , ATR_DFLT , 0xffffffff , "SCORE = %d " , m_sMaxPolygonNum  );

	Sint32 Polygons = (m_sMaxPolygonNum*FRAME_PER_SECOND) / 10000.0f;
	gxLib::Printf( 16    , ay+48 , az , ATR_DFLT , 0xffffffff , "%d万polygon /sec " , Polygons  );
#endif

}


void gxDebug::renderView()
{
	static Float32 fRot = 0.0f;

/*
	if( fRot == 0.0f )
	{
		gxLib::LoadTexture( 0 , "visor/gameboy.tga" );
		gxLib::UploadTexture();
	}

	gxLib::SetBgColor( 0xff008070 );

	gxLib::DrawColorBox(
		0       , 0       , 0xffff0000,
		WINDOW_W, 0       , 0xff00ff00,
		WINDOW_W, WINDOW_H, 0xff0000ff,
		0       , WINDOW_H, 0xffffffff,
		PRIORITY_DEBUG_MAX,
		ATR_DFLT);

*/

	Sint32 x1,y1,x2,y2,x3,y3,x4,y4,prio;
	prio = PRIORITY_DEBUG_MAX + 1;

	x1 = 32;
	y1 = 32;
	gxLib::DrawPoint( x1,y1,prio, ATR_DFLT, 0xFFFFFF00, 10.0f );

	for(Sint32 ii=0; ii < 1024; ii++)
	{
		x1 = 256+ gxLib::Rand()%128;
		y1 = 16 + gxLib::Rand()%32;
		gxLib::DrawPoint( x1, y1, prio, ATR_DFLT, (gxLib::Rand() % 0xFFFFFFFF) | 0xff000000, 1.0f);
	}

	//線
	x1 = 64;
	y1 = 32;
	x2 = 220;
	y2 = 32;
	gxLib::DrawLine( x1,y1,x2,y2,prio, ATR_DFLT, 0xffff0000 , 3 );

	//三角形（ノンテクスチャ＆グラデーションカラー）

	x1 = 64;
	y1 = 64;
	x2 = x1 -32;
	y2 = y1 +32;
	x3 = x1 +32;
	y3 = y1 +32;

	gxLib::DrawColorTriangle(
			x1 , y1 ,0xFF0000ff,
			x2 , y2 ,0xFF00ff00,
			x3 , y3 ,0xFFff0000,
			prio,
			ATR_DFLT );

	x1 = x1 + 24;
	y1 = y1 + 16;
	x2 = x1 -32;
	y2 = y1 +32;
	x3 = x1 +32;
	y3 = y1 +32;

	gxLib::DrawTriangle(
			x1 , y1 ,
			x2 , y2 ,
			x3 , y3 ,
			prio,
			gxTrue,
			ATR_DFLT , 0x80FFFFFF );

	gxLib::PutTriangle(
			x1+128 , y1 , 0,0,
			x2+128 , y2 , 0,728,
			x3+128 , y3 , 1,728,
			prio,
			0,
			ATR_DFLT , 0xFFFFFFFF , 0x8000ff00 );

	x1 = 32;
	y1 = 128;

	x2 = x1 + 64;
	y2 = y1 + 64;

	gxLib::DrawBox(
			x1 , y1 ,
			x2 , y2 ,
			prio,
			gxTrue,
			ATR_DFLT , 0x80FFFFFF );

	x1 = x1 + 32;
	y1 = y1 + 32;
	x2 = x1 + 64;
	y2 = y1 + 64;

	gxLib::DrawBox(
			x1 , y1 ,
			x2 , y2 ,
			prio,
			gxFalse,
			ATR_DFLT , 0xFF00FF00 );

	x1 = 128;
	y1 = 128;
	x2 = x1+256;
	y2 = y1-16;
	x3 = x2+32;
	y3 = y2 + 64;
	x4 = x1-16;
	y4 = y1+24;

	gxLib::DrawColorBox(
		x1, y1, 0xFF0000ff,
		x2, y2, 0xFF00ff00,
		x3, y3, 0xFFff0000,
		x4, y4, 0xFF808080,
		prio,
		ATR_DFLT);

	x1 = 256;
	y1 = 256;

	fRot += 1.0f;

	gxLib::PutSprite(
			x1+32 , y1+32 , prio,
			0 , 0,0,1024,768,512,768/2,
			ATR_DFLT , 0x80FFFFFF , fRot , 0.25f , 0.25f , 0xF0010101 );


	gxLib::PutSprite(
			x1 , y1 , prio,
			0 , 0,0,1024,768,512,768/2,
			ATR_DFLT , 0x80FFFFFF , fRot , 0.25f , 0.25f );


	x1 += 320;
	y1 -= 200;

	gxLib::PutSprite(
			x1 , y1 , prio,
			0 , 0,0,1024,768,512,768/2,
			ATR_DFLT|ATR_FLIP_X , 0x80FFFFFF , fRot , 0.25f , 0.25f );

	y1 += 160;

	gxLib::PutSprite(
			x1 , y1 , prio,
			0 , 0,0,1024,768,512,768/2,
			ATR_DFLT|ATR_FLIP_Y , 0x80FFFFFF , fRot , gxUtil::Cos(fRot)*0.25f , gxUtil::Cos(fRot)*0.25f );


	drawPot( 0,0 );

}


void gxDebug::httpView()
{
	CGameGirl::GetInstance()->GetWindowsResolution(&m_WinW, &m_WinH);
	Sint32 prio = PRIORITY_DEBUG_MAX;

	if (m_pManualButton[0] == nullptr)
	{
		gxUtil::OpenWebFile("http://www.garuru.co.jp/garuru.png", nullptr, [this](gxBool bSuccess, uint8_t* p, size_t size) {
			if (bSuccess)
			{
				gxLib::ReadTexture( MASTERTEXTURE_MAX*(64)-16, p, size);
				gxLib::UploadTexture();
			}
			
		});
	}
	gxSprite spr = { MASTERTEXTURE_MAX*(64)-16, 0, 0, 600, 300, 300, 150 };
	gxLib::PutSprite(&spr, WINDOW_W / 2, WINDOW_W / 2, prio);

	for (Sint32 n = 0; n < 2; n++)
	{
		if (m_pManualButton[n] == nullptr)
		{
			m_pManualButton[n] = new gxUtil::RoundButton(128, 256, prio, 0xFFFF0000, 128);
		}

		m_pManualButton[n]->Update();

		m_pManualButton[n]->SetPos(128, 128+200*n, prio);

		m_pManualButton[n]->Draw();
	}
	m_pManualButton[0]->SetText("Manual");
	m_pManualButton[1]->SetText("License");

	if (m_pManualButton[0]->IsTrigger())
	{
		gxLib::OpenAppManual();
	}

	if (m_pManualButton[1]->IsTrigger())
	{
		gxLib::OpenEULADocument();
	}




}


void gxDebug::SetAlreadyUseRAM()
{
	size_t uNow, uTotal, uMax;
	CGameGirl::GetInstance()->GetMemoryRemain(&uNow, &uTotal, &uMax);
	m_uSystemUseRAM = uNow;
}


#include <gxLib/util/gxMatrix.h>
VECTOR3 teapot[] = {
  {0.700000f, -1.200000f, 0.000000f},
  {0.605600f, -1.200000f, -0.355700f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.700000f, -1.200000f, 0.000000f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.605600f, -1.200000f, -0.355700f},
  {0.355700f, -1.200000f, -0.605600f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.605600f, -1.200000f, -0.355700f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.598800f, -1.243700f, -0.351700f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.619600f, -1.243700f, -0.363900f},
  {0.355700f, -1.200000f, -0.605600f},
  {0.000000f, -1.200000f, -0.700000f},
  {0.000000f, -1.243700f, -0.692100f},
  {0.000000f, -1.243700f, -0.692100f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.355700f, -1.200000f, -0.605600f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.000000f, -1.243700f, -0.692100f},
  {0.000000f, -1.243800f, -0.716200f},
  {0.000000f, -1.243800f, -0.716200f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.351700f, -1.243700f, -0.598800f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.000000f, -1.243800f, -0.716200f},
  {0.000000f, -1.200000f, -0.750000f},
  {0.000000f, -1.200000f, -0.750000f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.363900f, -1.243800f, -0.619600f},
  {0.000000f, -1.200000f, -0.700000f},
  {-0.375700f, -1.200000f, -0.605600f},
  {-0.357600f, -1.243700f, -0.598800f},
  {-0.357600f, -1.243700f, -0.598800f},
  {0.000000f, -1.243700f, -0.692100f},
  {0.000000f, -1.200000f, -0.700000f},
  {0.000000f, -1.243700f, -0.692100f},
  {-0.357600f, -1.243700f, -0.598800f},
  {-0.364700f, -1.243700f, -0.619600f},
  {-0.364700f, -1.243700f, -0.619600f},
  {0.000000f, -1.243800f, -0.716200f},
  {0.000000f, -1.243700f, -0.692100f},
  {0.000000f, -1.243800f, -0.716200f},
  {-0.364700f, -1.243700f, -0.619600f},
  {-0.381100f, -1.200000f, -0.648900f},
  {-0.381100f, -1.200000f, -0.648900f},
  {0.000000f, -1.200000f, -0.750000f},
  {0.000000f, -1.243800f, -0.716200f},
  {-0.375700f, -1.200000f, -0.605600f},
  {-0.615600f, -1.200000f, -0.355700f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.357600f, -1.243700f, -0.598800f},
  {-0.375700f, -1.200000f, -0.605600f},
  {-0.357600f, -1.243700f, -0.598800f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.364700f, -1.243700f, -0.619600f},
  {-0.357600f, -1.243700f, -0.598800f},
  {-0.364700f, -1.243700f, -0.619600f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.381100f, -1.200000f, -0.648900f},
  {-0.364700f, -1.243700f, -0.619600f},
  {-0.615600f, -1.200000f, -0.355700f},
  {-0.700000f, -1.200000f, 0.000000f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.615600f, -1.200000f, -0.355700f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.601800f, -1.243700f, -0.351700f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.620000f, -1.243700f, -0.363900f},
  {-0.700000f, -1.200000f, 0.000000f},
  {-0.605600f, -1.200000f, 0.355700f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.700000f, -1.200000f, 0.000000f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.692100f, -1.243700f, 0.000000f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.716200f, -1.243700f, 0.000000f},
  {-0.605600f, -1.200000f, 0.355700f},
  {-0.355700f, -1.200000f, 0.605600f},
  {-0.351700f, -1.243700f, 0.598800f},
  {-0.351700f, -1.243700f, 0.598800f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.605600f, -1.200000f, 0.355700f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.351700f, -1.243700f, 0.598800f},
  {-0.363900f, -1.243700f, 0.619600f},
  {-0.363900f, -1.243700f, 0.619600f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.598800f, -1.243700f, 0.351700f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.363900f, -1.243700f, 0.619600f},
  {-0.381100f, -1.200000f, 0.648900f},
  {-0.381100f, -1.200000f, 0.648900f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.619600f, -1.243700f, 0.363900f},
  {-0.355700f, -1.200000f, 0.605600f},
  {0.000000f, -1.200000f, 0.700000f},
  {0.000000f, -1.243700f, 0.692100f},
  {0.000000f, -1.243700f, 0.692100f},
  {-0.351700f, -1.243700f, 0.598800f},
  {-0.355700f, -1.200000f, 0.605600f},
  {-0.351700f, -1.243700f, 0.598800f},
  {0.000000f, -1.243700f, 0.692100f},
  {0.000000f, -1.243700f, 0.716200f},
  {0.000000f, -1.243700f, 0.716200f},
  {-0.363900f, -1.243700f, 0.619600f},
  {-0.351700f, -1.243700f, 0.598800f},
  {-0.363900f, -1.243700f, 0.619600f},
  {0.000000f, -1.243700f, 0.716200f},
  {0.000000f, -1.200000f, 0.750000f},
  {0.000000f, -1.200000f, 0.750000f},
  {-0.381100f, -1.200000f, 0.648900f},
  {-0.363900f, -1.243700f, 0.619600f},
  {0.000000f, -1.200000f, 0.700000f},
  {0.355700f, -1.200000f, 0.605600f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.000000f, -1.243700f, 0.692100f},
  {0.000000f, -1.200000f, 0.700000f},
  {0.000000f, -1.243700f, 0.692100f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.000000f, -1.243700f, 0.716200f},
  {0.000000f, -1.243700f, 0.692100f},
  {0.000000f, -1.243700f, 0.716200f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.000000f, -1.200000f, 0.750000f},
  {0.000000f, -1.243700f, 0.716200f},
  {0.355700f, -1.200000f, 0.605600f},
  {0.605600f, -1.200000f, 0.355700f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.355700f, -1.200000f, 0.605600f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.351700f, -1.243700f, 0.598800f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.363900f, -1.243700f, 0.619600f},
  {0.605600f, -1.200000f, 0.355700f},
  {0.700000f, -1.200000f, 0.000000f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.605600f, -1.200000f, 0.355700f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.692100f, -1.243700f, 0.000000f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.598800f, -1.243700f, 0.351700f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.716200f, -1.243700f, 0.000000f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.619600f, -1.243700f, 0.363900f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.963000f, -0.686100f, 0.000000f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.963000f, -0.686100f, 0.000000f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.865200f, -0.450000f, -0.508100f},
  {0.865200f, -0.450000f, -0.508100f},
  {1.000000f, -0.450000f, 0.000000f},
  {0.963000f, -0.686100f, 0.000000f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.648900f, -1.200000f, -0.381100f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.753000f, -0.938900f, -0.442300f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.865200f, -0.450000f, -0.508100f},
  {0.833100f, -0.686100f, -0.489300f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.000000f, -1.200000f, -0.750000f},
  {0.000000f, -0.938900f, -0.870400f},
  {0.000000f, -0.938900f, -0.870400f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.381100f, -1.200000f, -0.648900f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.000000f, -0.938900f, -0.870400f},
  {0.000000f, -0.686100f, -0.963000f},
  {0.000000f, -0.686100f, -0.963000f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.442300f, -0.938900f, -0.753000f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.000000f, -0.686100f, -0.963000f},
  {0.000000f, -0.450000f, -1.000000f},
  {0.000000f, -0.450000f, -1.000000f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.489300f, -0.686100f, -0.833100f},
  {0.000000f, -1.200000f, -0.750000f},
  {-0.381100f, -1.200000f, -0.648900f},
  {-0.442300f, -0.938900f, -0.753000f},
  {-0.442300f, -0.938900f, -0.753000f},
  {0.000000f, -0.938900f, -0.870400f},
  {0.000000f, -1.200000f, -0.750000f},
  {0.000000f, -0.938900f, -0.870400f},
  {-0.442300f, -0.938900f, -0.753000f},
  {-0.489300f, -0.686100f, -0.833100f},
  {-0.489300f, -0.686100f, -0.833100f},
  {0.000000f, -0.686100f, -0.963000f},
  {0.000000f, -0.938900f, -0.870400f},
  {0.000000f, -0.686100f, -0.963000f},
  {-0.489300f, -0.686100f, -0.833100f},
  {-0.508100f, -0.450000f, -0.865200f},
  {-0.508100f, -0.450000f, -0.865200f},
  {0.000000f, -0.450000f, -1.000000f},
  {0.000000f, -0.686100f, -0.963000f},
  {-0.381100f, -1.200000f, -0.648900f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.442300f, -0.938900f, -0.753000f},
  {-0.381100f, -1.200000f, -0.648900f},
  {-0.442300f, -0.938900f, -0.753000f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.489300f, -0.686100f, -0.833100f},
  {-0.442300f, -0.938900f, -0.753000f},
  {-0.489300f, -0.686100f, -0.833100f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-0.508100f, -0.450000f, -0.865200f},
  {-0.489300f, -0.686100f, -0.833100f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.648900f, -1.200000f, -0.381100f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.753000f, -0.938900f, -0.442300f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-0.833100f, -0.686100f, -0.489300f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.750000f, -1.200000f, 0.000000f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-0.870400f, -0.938900f, 0.000000f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.963000f, -0.686100f, 0.000000f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.381100f, -1.200000f, 0.648900f},
  {-0.442300f, -0.938900f, 0.753000f},
  {-0.442300f, -0.938900f, 0.753000f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.648900f, -1.200000f, 0.381100f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.442300f, -0.938900f, 0.753000f},
  {-0.489300f, -0.686100f, 0.833100f},
  {-0.489300f, -0.686100f, 0.833100f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.753000f, -0.938900f, 0.442300f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.489300f, -0.686100f, 0.833100f},
  {-0.508100f, -0.450000f, 0.865200f},
  {-0.508100f, -0.450000f, 0.865200f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-0.833100f, -0.686100f, 0.489300f},
  {-0.381100f, -1.200000f, 0.648900f},
  {0.000000f, -1.200000f, 0.750000f},
  {0.000000f, -0.938900f, 0.870400f},
  {0.000000f, -0.938900f, 0.870400f},
  {-0.442300f, -0.938900f, 0.753000f},
  {-0.381100f, -1.200000f, 0.648900f},
  {-0.442300f, -0.938900f, 0.753000f},
  {0.000000f, -0.938900f, 0.870400f},
  {0.000000f, -0.686100f, 0.963000f},
  {0.000000f, -0.686100f, 0.963000f},
  {-0.489300f, -0.686100f, 0.833100f},
  {-0.442300f, -0.938900f, 0.753000f},
  {-0.489300f, -0.686100f, 0.833100f},
  {0.000000f, -0.686100f, 0.963000f},
  {0.000000f, -0.450000f, 1.000000f},
  {0.000000f, -0.450000f, 1.000000f},
  {-0.508100f, -0.450000f, 0.865200f},
  {-0.489300f, -0.686100f, 0.833100f},
  {0.000000f, -1.200000f, 0.750000f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.000000f, -0.938900f, 0.870400f},
  {0.000000f, -1.200000f, 0.750000f},
  {0.000000f, -0.938900f, 0.870400f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.000000f, -0.686100f, 0.963000f},
  {0.000000f, -0.938900f, 0.870400f},
  {0.000000f, -0.686100f, 0.963000f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.000000f, -0.450000f, 1.000000f},
  {0.000000f, -0.686100f, 0.963000f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.381100f, -1.200000f, 0.648900f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.833100f, -0.686100f, 0.489300f},
  {0.833100f, -0.686100f, 0.489300f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.442300f, -0.938900f, 0.753000f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.833100f, -0.686100f, 0.489300f},
  {0.865200f, -0.450000f, 0.508100f},
  {0.865200f, -0.450000f, 0.508100f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.489300f, -0.686100f, 0.833100f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.750000f, -1.200000f, 0.000000f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.648900f, -1.200000f, 0.381100f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.870400f, -0.938900f, 0.000000f},
  {0.963000f, -0.686100f, 0.000000f},
  {0.963000f, -0.686100f, 0.000000f},
  {0.833100f, -0.686100f, 0.489300f},
  {0.753000f, -0.938900f, 0.442300f},
  {0.833100f, -0.686100f, 0.489300f},
  {0.963000f, -0.686100f, 0.000000f},
  {1.000000f, -0.450000f, 0.000000f},
  {1.000000f, -0.450000f, 0.000000f},
  {0.865200f, -0.450000f, 0.508100f},
  {0.833100f, -0.686100f, 0.489300f},
  {1.000000f, -0.450000f, 0.000000f},
  {0.865200f, -0.450000f, -0.508100f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.935200f, -0.261100f, 0.000000f},
  {1.000000f, -0.450000f, 0.000000f},
  {0.935200f, -0.261100f, 0.000000f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.935200f, -0.261100f, 0.000000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.865200f, -0.450000f, -0.508100f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.865200f, -0.450000f, -0.508100f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.809100f, -0.261100f, -0.475200f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.705000f, -0.138900f, -0.414000f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.000000f, -0.450000f, -1.000000f},
  {0.000000f, -0.261100f, -0.935200f},
  {0.000000f, -0.261100f, -0.935200f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.508100f, -0.450000f, -0.865200f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.000000f, -0.261100f, -0.935200f},
  {0.000000f, -0.138900f, -0.814800f},
  {0.000000f, -0.138900f, -0.814800f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.475200f, -0.261100f, -0.809100f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.000000f, -0.138900f, -0.814800f},
  {0.000000f, -0.075000f, -0.750000f},
  {0.000000f, -0.075000f, -0.750000f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.414000f, -0.138900f, -0.705000f},
  {0.000000f, -0.450000f, -1.000000f},
  {-0.508100f, -0.450000f, -0.865200f},
  {-0.475200f, -0.261100f, -0.809100f},
  {-0.475200f, -0.261100f, -0.809100f},
  {0.000000f, -0.261100f, -0.935200f},
  {0.000000f, -0.450000f, -1.000000f},
  {0.000000f, -0.261100f, -0.935200f},
  {-0.475200f, -0.261100f, -0.809100f},
  {-0.414000f, -0.138900f, -0.705000f},
  {-0.414000f, -0.138900f, -0.705000f},
  {0.000000f, -0.138900f, -0.814800f},
  {0.000000f, -0.261100f, -0.935200f},
  {0.000000f, -0.138900f, -0.814800f},
  {-0.414000f, -0.138900f, -0.705000f},
  {-0.381100f, -0.075000f, -0.648900f},
  {-0.381100f, -0.075000f, -0.648900f},
  {0.000000f, -0.075000f, -0.750000f},
  {0.000000f, -0.138900f, -0.814800f},
  {-0.508100f, -0.450000f, -0.865200f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.475200f, -0.261100f, -0.809100f},
  {-0.508100f, -0.450000f, -0.865200f},
  {-0.475200f, -0.261100f, -0.809100f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-0.414000f, -0.138900f, -0.705000f},
  {-0.475200f, -0.261100f, -0.809100f},
  {-0.414000f, -0.138900f, -0.705000f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.381100f, -0.075000f, -0.648900f},
  {-0.414000f, -0.138900f, -0.705000f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.865200f, -0.450000f, -0.508100f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-0.809100f, -0.261100f, -0.475200f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.705000f, -0.138900f, -0.414000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.935200f, -0.261100f, 0.000000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.814800f, -0.138900f, 0.000000f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-0.508100f, -0.450000f, 0.865200f},
  {-0.475200f, -0.261100f, 0.809100f},
  {-0.475200f, -0.261100f, 0.809100f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.865200f, -0.450000f, 0.508100f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.475200f, -0.261100f, 0.809100f},
  {-0.414000f, -0.138900f, 0.705000f},
  {-0.414000f, -0.138900f, 0.705000f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.809100f, -0.261100f, 0.475200f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.414000f, -0.138900f, 0.705000f},
  {-0.381100f, -0.075000f, 0.648900f},
  {-0.381100f, -0.075000f, 0.648900f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.705000f, -0.138900f, 0.414000f},
  {-0.508100f, -0.450000f, 0.865200f},
  {0.000000f, -0.450000f, 1.000000f},
  {0.000000f, -0.261100f, 0.935200f},
  {0.000000f, -0.261100f, 0.935200f},
  {-0.475200f, -0.261100f, 0.809100f},
  {-0.508100f, -0.450000f, 0.865200f},
  {-0.475200f, -0.261100f, 0.809100f},
  {0.000000f, -0.261100f, 0.935200f},
  {0.000000f, -0.138900f, 0.814800f},
  {0.000000f, -0.138900f, 0.814800f},
  {-0.414000f, -0.138900f, 0.705000f},
  {-0.475200f, -0.261100f, 0.809100f},
  {-0.414000f, -0.138900f, 0.705000f},
  {0.000000f, -0.138900f, 0.814800f},
  {0.000000f, -0.075000f, 0.750000f},
  {0.000000f, -0.075000f, 0.750000f},
  {-0.381100f, -0.075000f, 0.648900f},
  {-0.414000f, -0.138900f, 0.705000f},
  {0.000000f, -0.450000f, 1.000000f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.000000f, -0.261100f, 0.935200f},
  {0.000000f, -0.450000f, 1.000000f},
  {0.000000f, -0.261100f, 0.935200f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.000000f, -0.138900f, 0.814800f},
  {0.000000f, -0.261100f, 0.935200f},
  {0.000000f, -0.138900f, 0.814800f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.000000f, -0.075000f, 0.750000f},
  {0.000000f, -0.138900f, 0.814800f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.865200f, -0.450000f, 0.508100f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.508100f, -0.450000f, 0.865200f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.475200f, -0.261100f, 0.809100f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.414000f, -0.138900f, 0.705000f},
  {0.865200f, -0.450000f, 0.508100f},
  {1.000000f, -0.450000f, 0.000000f},
  {0.935200f, -0.261100f, 0.000000f},
  {0.935200f, -0.261100f, 0.000000f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.865200f, -0.450000f, 0.508100f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.935200f, -0.261100f, 0.000000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.809100f, -0.261100f, 0.475200f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.814800f, -0.138900f, 0.000000f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.705000f, -0.138900f, 0.414000f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.648900f, -0.075000f, -0.381100f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.617600f, -0.038900f, -0.362800f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.442200f, -0.011100f, -0.259700f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.000000f, -0.075000f, -0.750000f},
  {0.000000f, -0.038900f, -0.713900f},
  {0.000000f, -0.038900f, -0.713900f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.381100f, -0.075000f, -0.648900f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.000000f, -0.038900f, -0.713900f},
  {0.000000f, -0.011100f, -0.511100f},
  {0.000000f, -0.011100f, -0.511100f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.362800f, -0.038900f, -0.617600f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.000000f, -0.011100f, -0.511100f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.259700f, -0.011100f, -0.442200f},
  {0.000000f, -0.075000f, -0.750000f},
  {-0.381100f, -0.075000f, -0.648900f},
  {-0.362800f, -0.038900f, -0.617600f},
  {-0.362800f, -0.038900f, -0.617600f},
  {0.000000f, -0.038900f, -0.713900f},
  {0.000000f, -0.075000f, -0.750000f},
  {0.000000f, -0.038900f, -0.713900f},
  {-0.362800f, -0.038900f, -0.617600f},
  {-0.259700f, -0.011100f, -0.442200f},
  {-0.259700f, -0.011100f, -0.442200f},
  {0.000000f, -0.011100f, -0.511100f},
  {0.000000f, -0.038900f, -0.713900f},
  {0.000000f, -0.011100f, -0.511100f},
  {-0.259700f, -0.011100f, -0.442200f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, -0.011100f, -0.511100f},
  {-0.381100f, -0.075000f, -0.648900f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.362800f, -0.038900f, -0.617600f},
  {-0.381100f, -0.075000f, -0.648900f},
  {-0.362800f, -0.038900f, -0.617600f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.442200f, -0.011100f, -0.259700f},
  {-0.442200f, -0.011100f, -0.259700f},
  {-0.259700f, -0.011100f, -0.442200f},
  {-0.362800f, -0.038900f, -0.617600f},
  {-0.259700f, -0.011100f, -0.442200f},
  {-0.442200f, -0.011100f, -0.259700f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {-0.259700f, -0.011100f, -0.442200f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.648900f, -0.075000f, -0.381100f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.511100f, -0.011100f, 0.000000f},
  {-0.511100f, -0.011100f, 0.000000f},
  {-0.442200f, -0.011100f, -0.259700f},
  {-0.617600f, -0.038900f, -0.362800f},
  {-0.442200f, -0.011100f, -0.259700f},
  {-0.511100f, -0.011100f, 0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {-0.442200f, -0.011100f, -0.259700f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.750000f, -0.075000f, 0.000000f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.442200f, -0.011100f, 0.259700f},
  {-0.442200f, -0.011100f, 0.259700f},
  {-0.511100f, -0.011100f, 0.000000f},
  {-0.713900f, -0.038900f, 0.000000f},
  {-0.511100f, -0.011100f, 0.000000f},
  {-0.442200f, -0.011100f, 0.259700f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {-0.511100f, -0.011100f, 0.000000f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.381100f, -0.075000f, 0.648900f},
  {-0.362800f, -0.038900f, 0.617600f},
  {-0.362800f, -0.038900f, 0.617600f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.648900f, -0.075000f, 0.381100f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.362800f, -0.038900f, 0.617600f},
  {-0.259700f, -0.011100f, 0.442200f},
  {-0.259700f, -0.011100f, 0.442200f},
  {-0.442200f, -0.011100f, 0.259700f},
  {-0.617600f, -0.038900f, 0.362800f},
  {-0.442200f, -0.011100f, 0.259700f},
  {-0.259700f, -0.011100f, 0.442200f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {-0.442200f, -0.011100f, 0.259700f},
  {-0.381100f, -0.075000f, 0.648900f},
  {0.000000f, -0.075000f, 0.750000f},
  {0.000000f, -0.038900f, 0.713900f},
  {0.000000f, -0.038900f, 0.713900f},
  {-0.362800f, -0.038900f, 0.617600f},
  {-0.381100f, -0.075000f, 0.648900f},
  {-0.362800f, -0.038900f, 0.617600f},
  {0.000000f, -0.038900f, 0.713900f},
  {0.000000f, -0.011100f, 0.511100f},
  {0.000000f, -0.011100f, 0.511100f},
  {-0.259700f, -0.011100f, 0.442200f},
  {-0.362800f, -0.038900f, 0.617600f},
  {-0.259700f, -0.011100f, 0.442200f},
  {0.000000f, -0.011100f, 0.511100f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {-0.259700f, -0.011100f, 0.442200f},
  {0.000000f, -0.075000f, 0.750000f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.000000f, -0.038900f, 0.713900f},
  {0.000000f, -0.075000f, 0.750000f},
  {0.000000f, -0.038900f, 0.713900f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.000000f, -0.011100f, 0.511100f},
  {0.000000f, -0.038900f, 0.713900f},
  {0.000000f, -0.011100f, 0.511100f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, -0.011100f, 0.511100f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.381100f, -0.075000f, 0.648900f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.442200f, -0.011100f, 0.259700f},
  {0.442200f, -0.011100f, 0.259700f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.362800f, -0.038900f, 0.617600f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.442200f, -0.011100f, 0.259700f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.259700f, -0.011100f, 0.442200f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.750000f, -0.075000f, 0.000000f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.648900f, -0.075000f, 0.381100f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.713900f, -0.038900f, 0.000000f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.442200f, -0.011100f, 0.259700f},
  {0.617600f, -0.038900f, 0.362800f},
  {0.442200f, -0.011100f, 0.259700f},
  {0.511100f, -0.011100f, 0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.000000f, 0.000000f, -0.000000f},
  {0.442200f, -0.011100f, 0.259700f},
  {-0.800000f, -1.012500f, 0.000000f},
  {-0.787000f, -1.041700f, -0.100000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-0.800000f, -1.012500f, 0.000000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-0.787000f, -1.041700f, -0.100000f},
  {-0.763000f, -1.095800f, -0.100000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-0.787000f, -1.041700f, -0.100000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-1.115900f, -1.036400f, -0.100000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.319300f, -0.999700f, -0.100000f},
  {-0.763000f, -1.095800f, -0.100000f},
  {-0.750000f, -1.125000f, 0.000000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-0.763000f, -1.095800f, -0.100000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-1.148900f, -1.088600f, -0.100000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.382600f, -1.037800f, -0.100000f},
  {-0.750000f, -1.125000f, 0.000000f},
  {-0.763000f, -1.095800f, 0.100000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-0.750000f, -1.125000f, 0.000000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-1.166700f, -1.116700f, 0.000000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.416700f, -1.058300f, 0.000000f},
  {-0.763000f, -1.095800f, 0.100000f},
  {-0.787000f, -1.041700f, 0.100000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-0.763000f, -1.095800f, 0.100000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-1.148900f, -1.088600f, 0.100000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.382600f, -1.037800f, 0.100000f},
  {-0.787000f, -1.041700f, 0.100000f},
  {-0.800000f, -1.012500f, 0.000000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-0.787000f, -1.041700f, 0.100000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-1.098100f, -1.008300f, 0.000000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.115900f, -1.036400f, 0.100000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.285200f, -0.979200f, 0.000000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.319300f, -0.999700f, 0.100000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-0.987000f, -0.411100f, -0.100000f},
  {-0.987000f, -0.411100f, -0.100000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.388900f, -0.900000f, -0.100000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-1.347500f, -0.738500f, -0.100000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-0.963000f, -0.338900f, -0.100000f},
  {-0.963000f, -0.338900f, -0.100000f},
  {-0.987000f, -0.411100f, -0.100000f},
  {-1.216700f, -0.562900f, -0.100000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.461100f, -0.900000f, -0.100000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-1.408100f, -0.701700f, -0.100000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-0.950000f, -0.300000f, 0.000000f},
  {-0.950000f, -0.300000f, 0.000000f},
  {-0.963000f, -0.338900f, -0.100000f},
  {-1.244400f, -0.509400f, -0.100000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.500000f, -0.900000f, 0.000000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-1.440700f, -0.681900f, 0.000000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-0.963000f, -0.338900f, 0.100000f},
  {-0.963000f, -0.338900f, 0.100000f},
  {-0.950000f, -0.300000f, 0.000000f},
  {-1.259300f, -0.480600f, 0.000000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.461100f, -0.900000f, 0.100000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-1.408100f, -0.701700f, 0.100000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {-0.987000f, -0.411100f, 0.100000f},
  {-0.987000f, -0.411100f, 0.100000f},
  {-0.963000f, -0.338900f, 0.100000f},
  {-1.244400f, -0.509400f, 0.100000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.350000f, -0.900000f, 0.000000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.388900f, -0.900000f, 0.100000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.314800f, -0.758300f, 0.000000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {-1.347500f, -0.738500f, 0.100000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {-1.201900f, -0.591700f, 0.000000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-1.000000f, -0.450000f, 0.000000f},
  {-0.987000f, -0.411100f, 0.100000f},
  {-1.216700f, -0.562900f, 0.100000f},
  {0.850000f, -0.712500f, 0.000000f},
  {0.850000f, -0.605600f, -0.220000f},
  {1.169800f, -0.737100f, -0.184600f},
  {1.169800f, -0.737100f, -0.184600f},
  {1.135200f, -0.805600f, 0.000000f},
  {0.850000f, -0.712500f, 0.000000f},
  {1.135200f, -0.805600f, 0.000000f},
  {1.169800f, -0.737100f, -0.184600f},
  {1.274700f, -0.981400f, -0.118800f},
  {1.274700f, -0.981400f, -0.118800f},
  {1.231500f, -1.006900f, 0.000000f},
  {1.135200f, -0.805600f, 0.000000f},
  {1.231500f, -1.006900f, 0.000000f},
  {1.274700f, -0.981400f, -0.118800f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.231500f, -1.006900f, 0.000000f},
  {0.850000f, -0.605600f, -0.220000f},
  {0.850000f, -0.406900f, -0.220000f},
  {1.234000f, -0.610100f, -0.184600f},
  {1.234000f, -0.610100f, -0.184600f},
  {1.169800f, -0.737100f, -0.184600f},
  {0.850000f, -0.605600f, -0.220000f},
  {1.169800f, -0.737100f, -0.184600f},
  {1.234000f, -0.610100f, -0.184600f},
  {1.354900f, -0.933900f, -0.118800f},
  {1.354900f, -0.933900f, -0.118800f},
  {1.274700f, -0.981400f, -0.118800f},
  {1.169800f, -0.737100f, -0.184600f},
  {1.274700f, -0.981400f, -0.118800f},
  {1.354900f, -0.933900f, -0.118800f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.274700f, -0.981400f, -0.118800f},
  {0.850000f, -0.406900f, -0.220000f},
  {0.850000f, -0.300000f, 0.000000f},
  {1.268500f, -0.541700f, 0.000000f},
  {1.268500f, -0.541700f, 0.000000f},
  {1.234000f, -0.610100f, -0.184600f},
  {0.850000f, -0.406900f, -0.220000f},
  {1.234000f, -0.610100f, -0.184600f},
  {1.268500f, -0.541700f, 0.000000f},
  {1.398100f, -0.908300f, 0.000000f},
  {1.398100f, -0.908300f, 0.000000f},
  {1.354900f, -0.933900f, -0.118800f},
  {1.234000f, -0.610100f, -0.184600f},
  {1.354900f, -0.933900f, -0.118800f},
  {1.398100f, -0.908300f, 0.000000f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.354900f, -0.933900f, -0.118800f},
  {0.850000f, -0.300000f, 0.000000f},
  {0.850000f, -0.406900f, 0.220000f},
  {1.234000f, -0.610100f, 0.184600f},
  {1.234000f, -0.610100f, 0.184600f},
  {1.268500f, -0.541700f, 0.000000f},
  {0.850000f, -0.300000f, 0.000000f},
  {1.268500f, -0.541700f, 0.000000f},
  {1.234000f, -0.610100f, 0.184600f},
  {1.354900f, -0.933900f, 0.118800f},
  {1.354900f, -0.933900f, 0.118800f},
  {1.398100f, -0.908300f, 0.000000f},
  {1.268500f, -0.541700f, 0.000000f},
  {1.398100f, -0.908300f, 0.000000f},
  {1.354900f, -0.933900f, 0.118800f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.398100f, -0.908300f, 0.000000f},
  {0.850000f, -0.406900f, 0.220000f},
  {0.850000f, -0.605600f, 0.220000f},
  {1.169800f, -0.737100f, 0.184600f},
  {1.169800f, -0.737100f, 0.184600f},
  {1.234000f, -0.610100f, 0.184600f},
  {0.850000f, -0.406900f, 0.220000f},
  {1.234000f, -0.610100f, 0.184600f},
  {1.169800f, -0.737100f, 0.184600f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.354900f, -0.933900f, 0.118800f},
  {1.234000f, -0.610100f, 0.184600f},
  {1.354900f, -0.933900f, 0.118800f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.354900f, -0.933900f, 0.118800f},
  {0.850000f, -0.605600f, 0.220000f},
  {0.850000f, -0.712500f, 0.000000f},
  {1.135200f, -0.805600f, 0.000000f},
  {1.135200f, -0.805600f, 0.000000f},
  {1.169800f, -0.737100f, 0.184600f},
  {0.850000f, -0.605600f, 0.220000f},
  {1.169800f, -0.737100f, 0.184600f},
  {1.135200f, -0.805600f, 0.000000f},
  {1.231500f, -1.006900f, 0.000000f},
  {1.231500f, -1.006900f, 0.000000f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.169800f, -0.737100f, 0.184600f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.231500f, -1.006900f, 0.000000f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.274700f, -0.981400f, 0.118800f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.451900f, -1.200000f, -0.050000f},
  {1.451900f, -1.200000f, -0.050000f},
  {1.400000f, -1.200000f, 0.000000f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.427800f, -1.200000f, -0.083300f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.478900f, -1.227200f, -0.074700f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.548100f, -1.200000f, -0.050000f},
  {1.548100f, -1.200000f, -0.050000f},
  {1.451900f, -1.200000f, -0.050000f},
  {1.491200f, -1.227700f, -0.058600f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.572200f, -1.200000f, -0.083300f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.632200f, -1.231200f, -0.074700f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.600000f, -1.200000f, 0.000000f},
  {1.600000f, -1.200000f, 0.000000f},
  {1.548100f, -1.200000f, -0.050000f},
  {1.622700f, -1.232700f, -0.058600f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.650000f, -1.200000f, 0.000000f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.714800f, -1.233300f, 0.000000f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.548100f, -1.200000f, 0.050000f},
  {1.548100f, -1.200000f, 0.050000f},
  {1.600000f, -1.200000f, 0.000000f},
  {1.693500f, -1.235400f, 0.000000f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.572200f, -1.200000f, 0.083300f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.491200f, -1.227700f, 0.058600f},
  {1.491200f, -1.227700f, 0.058600f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.632200f, -1.231200f, 0.074700f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.491200f, -1.227700f, 0.058600f},
  {1.451900f, -1.200000f, 0.050000f},
  {1.451900f, -1.200000f, 0.050000f},
  {1.548100f, -1.200000f, 0.050000f},
  {1.622700f, -1.232700f, 0.058600f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.350000f, -1.200000f, 0.000000f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.427800f, -1.200000f, 0.083300f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.396300f, -1.225000f, 0.000000f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.491200f, -1.227700f, 0.058600f},
  {1.478900f, -1.227200f, 0.074700f},
  {1.491200f, -1.227700f, 0.058600f},
  {1.420400f, -1.225000f, 0.000000f},
  {1.400000f, -1.200000f, 0.000000f},
  {1.400000f, -1.200000f, 0.000000f},
  {1.451900f, -1.200000f, 0.050000f},
  {1.491200f, -1.227700f, 0.058600f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.157100f, -1.533300f, -0.092400f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.102600f, -1.441700f, -0.060300f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.533300f, -0.181500f},
  {0.000000f, -1.533300f, -0.181500f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.000000f, -1.533300f, -0.181500f},
  {0.000000f, -1.441700f, -0.118500f},
  {0.000000f, -1.441700f, -0.118500f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.092400f, -1.533300f, -0.157100f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.000000f, -1.441700f, -0.118500f},
  {0.000000f, -1.350000f, -0.100000f},
  {0.000000f, -1.350000f, -0.100000f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.060300f, -1.441700f, -0.102600f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.092400f, -1.533300f, -0.157100f},
  {-0.092400f, -1.533300f, -0.157100f},
  {0.000000f, -1.533300f, -0.181500f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.533300f, -0.181500f},
  {-0.092400f, -1.533300f, -0.157100f},
  {-0.060300f, -1.441700f, -0.102600f},
  {-0.060300f, -1.441700f, -0.102600f},
  {0.000000f, -1.441700f, -0.118500f},
  {0.000000f, -1.533300f, -0.181500f},
  {0.000000f, -1.441700f, -0.118500f},
  {-0.060300f, -1.441700f, -0.102600f},
  {-0.050800f, -1.350000f, -0.086500f},
  {-0.050800f, -1.350000f, -0.086500f},
  {0.000000f, -1.350000f, -0.100000f},
  {0.000000f, -1.441700f, -0.118500f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.157100f, -1.533300f, -0.092400f},
  {-0.157100f, -1.533300f, -0.092400f},
  {-0.092400f, -1.533300f, -0.157100f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.092400f, -1.533300f, -0.157100f},
  {-0.157100f, -1.533300f, -0.092400f},
  {-0.102600f, -1.441700f, -0.060300f},
  {-0.102600f, -1.441700f, -0.060300f},
  {-0.060300f, -1.441700f, -0.102600f},
  {-0.092400f, -1.533300f, -0.157100f},
  {-0.060300f, -1.441700f, -0.102600f},
  {-0.102600f, -1.441700f, -0.060300f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.050800f, -1.350000f, -0.086500f},
  {-0.060300f, -1.441700f, -0.102600f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.181500f, -1.533300f, 0.000000f},
  {-0.181500f, -1.533300f, 0.000000f},
  {-0.157100f, -1.533300f, -0.092400f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.157100f, -1.533300f, -0.092400f},
  {-0.181500f, -1.533300f, 0.000000f},
  {-0.118500f, -1.441700f, 0.000000f},
  {-0.118500f, -1.441700f, 0.000000f},
  {-0.102600f, -1.441700f, -0.060300f},
  {-0.157100f, -1.533300f, -0.092400f},
  {-0.102600f, -1.441700f, -0.060300f},
  {-0.118500f, -1.441700f, 0.000000f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.102600f, -1.441700f, -0.060300f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.157100f, -1.533300f, 0.092400f},
  {-0.157100f, -1.533300f, 0.092400f},
  {-0.181500f, -1.533300f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.181500f, -1.533300f, 0.000000f},
  {-0.157100f, -1.533300f, 0.092400f},
  {-0.102600f, -1.441700f, 0.060300f},
  {-0.102600f, -1.441700f, 0.060300f},
  {-0.118500f, -1.441700f, 0.000000f},
  {-0.181500f, -1.533300f, 0.000000f},
  {-0.118500f, -1.441700f, 0.000000f},
  {-0.102600f, -1.441700f, 0.060300f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.118500f, -1.441700f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.092400f, -1.533300f, 0.157100f},
  {-0.092400f, -1.533300f, 0.157100f},
  {-0.157100f, -1.533300f, 0.092400f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.157100f, -1.533300f, 0.092400f},
  {-0.092400f, -1.533300f, 0.157100f},
  {-0.060300f, -1.441700f, 0.102600f},
  {-0.060300f, -1.441700f, 0.102600f},
  {-0.102600f, -1.441700f, 0.060300f},
  {-0.157100f, -1.533300f, 0.092400f},
  {-0.102600f, -1.441700f, 0.060300f},
  {-0.060300f, -1.441700f, 0.102600f},
  {-0.050800f, -1.350000f, 0.086500f},
  {-0.050800f, -1.350000f, 0.086500f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.102600f, -1.441700f, 0.060300f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.533300f, 0.181500f},
  {0.000000f, -1.533300f, 0.181500f},
  {-0.092400f, -1.533300f, 0.157100f},
  {0.000000f, -1.575000f, 0.000000f},
  {-0.092400f, -1.533300f, 0.157100f},
  {0.000000f, -1.533300f, 0.181500f},
  {0.000000f, -1.441700f, 0.118500f},
  {0.000000f, -1.441700f, 0.118500f},
  {-0.060300f, -1.441700f, 0.102600f},
  {-0.092400f, -1.533300f, 0.157100f},
  {-0.060300f, -1.441700f, 0.102600f},
  {0.000000f, -1.441700f, 0.118500f},
  {0.000000f, -1.350000f, 0.100000f},
  {0.000000f, -1.350000f, 0.100000f},
  {-0.050800f, -1.350000f, 0.086500f},
  {-0.060300f, -1.441700f, 0.102600f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.000000f, -1.533300f, 0.181500f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.533300f, 0.181500f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.000000f, -1.441700f, 0.118500f},
  {0.000000f, -1.533300f, 0.181500f},
  {0.000000f, -1.441700f, 0.118500f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.000000f, -1.350000f, 0.100000f},
  {0.000000f, -1.441700f, 0.118500f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.092400f, -1.533300f, 0.157100f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.060300f, -1.441700f, 0.102600f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.000000f, -1.575000f, 0.000000f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.181500f, -1.533300f, 0.000000f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.157100f, -1.533300f, 0.092400f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.118500f, -1.441700f, 0.000000f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.102600f, -1.441700f, 0.060300f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.562400f, -1.200000f, -0.330300f},
  {0.562400f, -1.200000f, -0.330300f},
  {0.650000f, -1.200000f, 0.000000f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.086500f, -1.350000f, -0.050800f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.248300f, -1.294400f, -0.145900f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.330300f, -1.200000f, -0.562400f},
  {0.330300f, -1.200000f, -0.562400f},
  {0.562400f, -1.200000f, -0.330300f},
  {0.458200f, -1.255600f, -0.269100f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.000000f, -1.350000f, -0.100000f},
  {0.000000f, -1.294400f, -0.287000f},
  {0.000000f, -1.294400f, -0.287000f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.050800f, -1.350000f, -0.086500f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.000000f, -1.294400f, -0.287000f},
  {0.000000f, -1.255600f, -0.529600f},
  {0.000000f, -1.255600f, -0.529600f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.145900f, -1.294400f, -0.248300f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.000000f, -1.255600f, -0.529600f},
  {0.000000f, -1.200000f, -0.650000f},
  {0.000000f, -1.200000f, -0.650000f},
  {0.330300f, -1.200000f, -0.562400f},
  {0.269100f, -1.255600f, -0.458200f},
  {0.000000f, -1.350000f, -0.100000f},
  {-0.050800f, -1.350000f, -0.086500f},
  {-0.145900f, -1.294400f, -0.248300f},
  {-0.145900f, -1.294400f, -0.248300f},
  {0.000000f, -1.294400f, -0.287000f},
  {0.000000f, -1.350000f, -0.100000f},
  {0.000000f, -1.294400f, -0.287000f},
  {-0.145900f, -1.294400f, -0.248300f},
  {-0.269100f, -1.255600f, -0.458200f},
  {-0.269100f, -1.255600f, -0.458200f},
  {0.000000f, -1.255600f, -0.529600f},
  {0.000000f, -1.294400f, -0.287000f},
  {0.000000f, -1.255600f, -0.529600f},
  {-0.269100f, -1.255600f, -0.458200f},
  {-0.330300f, -1.200000f, -0.562400f},
  {-0.330300f, -1.200000f, -0.562400f},
  {0.000000f, -1.200000f, -0.650000f},
  {0.000000f, -1.255600f, -0.529600f},
  {-0.050800f, -1.350000f, -0.086500f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.145900f, -1.294400f, -0.248300f},
  {-0.050800f, -1.350000f, -0.086500f},
  {-0.145900f, -1.294400f, -0.248300f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.269100f, -1.255600f, -0.458200f},
  {-0.145900f, -1.294400f, -0.248300f},
  {-0.269100f, -1.255600f, -0.458200f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.562400f, -1.200000f, -0.330300f},
  {-0.562400f, -1.200000f, -0.330300f},
  {-0.330300f, -1.200000f, -0.562400f},
  {-0.269100f, -1.255600f, -0.458200f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.086500f, -1.350000f, -0.050800f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.248300f, -1.294400f, -0.145900f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.650000f, -1.200000f, 0.000000f},
  {-0.650000f, -1.200000f, 0.000000f},
  {-0.562400f, -1.200000f, -0.330300f},
  {-0.458200f, -1.255600f, -0.269100f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.100000f, -1.350000f, 0.000000f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.287000f, -1.294400f, 0.000000f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.562400f, -1.200000f, 0.330300f},
  {-0.562400f, -1.200000f, 0.330300f},
  {-0.650000f, -1.200000f, 0.000000f},
  {-0.529600f, -1.255600f, 0.000000f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.050800f, -1.350000f, 0.086500f},
  {-0.145900f, -1.294400f, 0.248300f},
  {-0.145900f, -1.294400f, 0.248300f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.086500f, -1.350000f, 0.050800f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.145900f, -1.294400f, 0.248300f},
  {-0.269100f, -1.255600f, 0.458200f},
  {-0.269100f, -1.255600f, 0.458200f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.248300f, -1.294400f, 0.145900f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.269100f, -1.255600f, 0.458200f},
  {-0.330300f, -1.200000f, 0.562400f},
  {-0.330300f, -1.200000f, 0.562400f},
  {-0.562400f, -1.200000f, 0.330300f},
  {-0.458200f, -1.255600f, 0.269100f},
  {-0.050800f, -1.350000f, 0.086500f},
  {0.000000f, -1.350000f, 0.100000f},
  {0.000000f, -1.294400f, 0.287000f},
  {0.000000f, -1.294400f, 0.287000f},
  {-0.145900f, -1.294400f, 0.248300f},
  {-0.050800f, -1.350000f, 0.086500f},
  {-0.145900f, -1.294400f, 0.248300f},
  {0.000000f, -1.294400f, 0.287000f},
  {0.000000f, -1.255600f, 0.529600f},
  {0.000000f, -1.255600f, 0.529600f},
  {-0.269100f, -1.255600f, 0.458200f},
  {-0.145900f, -1.294400f, 0.248300f},
  {-0.269100f, -1.255600f, 0.458200f},
  {0.000000f, -1.255600f, 0.529600f},
  {0.000000f, -1.200000f, 0.650000f},
  {0.000000f, -1.200000f, 0.650000f},
  {-0.330300f, -1.200000f, 0.562400f},
  {-0.269100f, -1.255600f, 0.458200f},
  {0.000000f, -1.350000f, 0.100000f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.000000f, -1.294400f, 0.287000f},
  {0.000000f, -1.350000f, 0.100000f},
  {0.000000f, -1.294400f, 0.287000f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.000000f, -1.255600f, 0.529600f},
  {0.000000f, -1.294400f, 0.287000f},
  {0.000000f, -1.255600f, 0.529600f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.330300f, -1.200000f, 0.562400f},
  {0.330300f, -1.200000f, 0.562400f},
  {0.000000f, -1.200000f, 0.650000f},
  {0.000000f, -1.255600f, 0.529600f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.050800f, -1.350000f, 0.086500f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.458200f, -1.255600f, 0.269100f},
  {0.458200f, -1.255600f, 0.269100f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.145900f, -1.294400f, 0.248300f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.458200f, -1.255600f, 0.269100f},
  {0.562400f, -1.200000f, 0.330300f},
  {0.562400f, -1.200000f, 0.330300f},
  {0.330300f, -1.200000f, 0.562400f},
  {0.269100f, -1.255600f, 0.458200f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.100000f, -1.350000f, 0.000000f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.086500f, -1.350000f, 0.050800f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.287000f, -1.294400f, 0.000000f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.458200f, -1.255600f, 0.269100f},
  {0.248300f, -1.294400f, 0.145900f},
  {0.458200f, -1.255600f, 0.269100f},
  {0.529600f, -1.255600f, 0.000000f},
  {0.650000f, -1.200000f, 0.000000f},
  {0.650000f, -1.200000f, 0.000000f},
  {0.562400f, -1.200000f, 0.330300f},
  {0.458200f, -1.255600f, 0.269100f}
};

gxBool drawPot( Float32 x , Float32 y )
{
	Uint32 num = ARRAY_LENGTH(teapot);
	Sint32 prio = PRIORITY_DEBUG_MAX;
	std::vector<VECTOR3> pot;

	for( Uint32 ii=0; ii<num; ii++ )
	{
		VECTOR3 *p = &teapot[ii];
		pot.push_back(*p);
	}

	VECTOR3 rot={ 0,0,0};
	std::vector<VECTOR3> vtx;

    Sint32 n = gxLib::GetGameCounter(); //810
	rot.x = n % 360;
	rot.y = n % 360;

	VECTOR3 _vtx;

	Sint32 winW,winH;
	CGameGirl::GetInstance()->GetWindowsResolution( &winW, &winH );

	for( Sint32 ii=0; ii<pot.size(); ii++ )
	{
		mtxSetUnit();

		mtxRotX( DEG2RAD( rot.x ) );
		mtxRotY( DEG2RAD( rot.y ) );
		mtxRotZ( DEG2RAD( rot.z ) );

		mtxAffinLocal( &_vtx, &pot[ii] );

		_vtx *= 100.0f;
		_vtx.x += winW/2 + x;
		_vtx.y += winH/2 + y;

		vtx.push_back(_vtx);

		gxLib::DrawPoint( _vtx.x , _vtx.y , prio , ATR_DFLT ,0xffffffff , 3.0f );
	}

	for( Sint32 ii=0; ii<vtx.size()-3; ii+=3 )
	{
		gxLib::DrawLine ( vtx[ii+0].x , vtx[ii+0].y , vtx[ii+1].x , vtx[ii+1].y , prio , ATR_DFLT ,0xff00ff00 ,8.0f);//, 1.0f );
		gxLib::DrawLine ( vtx[ii+1].x , vtx[ii+1].y , vtx[ii+2].x , vtx[ii+2].y , prio , ATR_DFLT ,0xff00ff00 ,8.0f);//, 1.0f );
		gxLib::DrawLine ( vtx[ii+2].x , vtx[ii+2].y , vtx[ii+0].x , vtx[ii+0].y , prio , ATR_DFLT ,0xff00ff00 ,8.0f);//, 1.0f );
		//gxLib::DrawLine ( vtx[ii+2].x , vtx[ii+2].y , vtx[ii+3].x , vtx[ii+3].y , prio , ATR_DFLT ,0xff00ff00 );//, 1.0f );
		//gxLib::DrawLine ( vtx[ii+3].x , vtx[ii+3].y , vtx[ii+0].x , vtx[ii+0].y , prio , ATR_DFLT ,0xff00ff00 );//, 1.0f );
	}
	

	struct Triangle {
		VECTOR3 vtx[3];
		Float32 Z=0.0f;

		void draw()
		{
			Sint32 prio = PRIORITY_DEBUG_MAX;
			gxLib::DrawTriangle(
				vtx[0].x, vtx[0].y,
				vtx[1].x, vtx[1].y,
				vtx[2].x, vtx[2].y,
				prio, gxTrue,ATR_DFLT, argb);
		}

		void update()
		{
			Z = 10000.0f + (vtx[0].z + vtx[1].z + vtx[2].z ) / -3.0f;
			normal();
		}

		void normal()
		{
			VECTOR3 ab = vtx[1] - vtx[0];
			VECTOR3 bc = vtx[2] - vtx[1];
			ab = ab.crossProduct(bc);
			VECTOR3 normal = ab.normalize();

			VECTOR3 dirLight = { 0.0f , 0.0f ,-1.0f };
			dirLight = dirLight.normalize();
			//Float32 fBrightness = ABS(dirLight.dotProduct( normal ));	//1.0f - diff.length();
			Float32 fBrightness = dirLight.dotProduct(normal);	//1.0f - diff.length();

			Sint32 lum = 255 * fBrightness;
			lum = CLAMP(lum, 32, 255);
			argb = ARGB(0xff , lum, lum , lum );
			//argb = ARGB(0xff , 0xff, 0xff , 0xff );
		}

		Uint32 argb = 0xffffffff;

	};

	Triangle _tri;
	std::map<Float32 , Triangle > zSort;

	Float32 f = 0.00000f;

	for( Sint32 ii=0; ii<=vtx.size()-3; ii+=3 )
	{
		_tri.vtx[0] = vtx[ii+0];
		_tri.vtx[1] = vtx[ii+1];
		_tri.vtx[2] = vtx[ii+2];
		_tri.update();

retry:
        if (zSort.find(_tri.Z + f) != zSort.end())
        {
            //同じZのやつ発見
            f += 0.001f;
            goto retry;
        }

		zSort[_tri.Z+f] = _tri;
		f += 0.001f;
	}

    //gxLib::DebugLog("-------------------------------");
    for( auto itr = zSort.begin(); itr != zSort.end(); ++itr )
	{
		itr->second.draw();
      //gxLib::DebugLog("%.2f , %.2f , %.2f", itr->second.vtx->x, itr->second.vtx->y, itr->second.vtx->z);
	}

	return gxTrue;
}
