//-------------------------------------------------------------------
//
// gxLib.cpp
//
// このラッピング関数群では絶対に機種依存のコードを書かないこと!!!!!
//
//-------------------------------------------------------------------
#include <gxLib.h>
#include "gx.h"
#include "gxOrderManager.h"
#include "gxTexManager.h"
#include "gxPadManager.h"
#include "gxRender.h"
#include "gxSoundManager.h"
#include "gxMovieManager.h"
#include "gxBlueTooth.h"
#include "gxFileManager.h"
//#include "gxNetworkManager.h"
#include "gxDebug.h"
#include "util/Font/CFontManager.h"
#include "util/CVirtualPad.h"
#include "util/gxUIManager.h"
/*
【裏ルール】

　PutSpriteのテクスチャページに負の値を設定すると特殊な効果がある。
　enNoneTexturePage・・・ノンテクスチャポリゴン
　enWiredPage　・・・外周部位を取り囲むライン

　通常のボックス処理と似ているが、回転/拡大/縮小が可能。

*/
#define INVALID_PAD_ID (0xff)
#define TOUCH_MAXIMUM  (16)

gxLib::StSaveData gxLib::SaveData;
static StOrder s_Order;

SINGLETON_DECLARE_INSTANCE(CBlueToothManager);

Uint32 gxLib::PutSprite(
		gxSprite *pSpr,
		Sint32 x,		Sint32 y,	Sint32 prio,
		Uint32 atr,	Uint32 col,	Float32 r,
		Float32 sx,		Float32 sy,
		Uint32 blendColor
	)
{
	if ((col & 0xff000000) == 0)  return 0;

	return PutSprite(x,y,prio,pSpr->page,pSpr->u,pSpr->v,pSpr->w,pSpr->h,pSpr->cx,pSpr->cy,atr,col,r,sx,sy, blendColor);
}
void gxLib::Destroy()
{
	CVirtualStick::DeleteInstance();

}

Uint32 gxLib::PutSprite(
		Sint32 x,		Sint32 y,	Sint32 prio,
		Sint32 page,	Sint32 u, 	Sint32 v,	Sint32 w,	Sint32 h,
		Sint32 cx,		Sint32 cy,	Uint32 atr,	Uint32 col,	Float32 r,
		Float32 sx,		Float32 sy,
		Uint32 blendColor )
{
	if ((col & 0xff000000) == 0)  return 0;

	s_Order.sType = enOrderTypeTexturePolygon;

	if( page == enWiredPage )
	{
		s_Order.sType = enOrderTypWired;
	}
	else if( page == enNoneTexturePage )
	{
		s_Order.sType = enOrderTypeNoneTexPolygon;
	}
	else if( page == enBackBuffer )
	{

	}

	s_Order.sAtribute = atr;
	s_Order.x  = x;
	s_Order.y  = y;
	s_Order.prio = prio;

	s_Order.sx = sx;
	s_Order.sy = sy;
	s_Order.sz = 0.f;

	s_Order.rx = 0.f;
	s_Order.ry = 0.f;
	s_Order.rz = r;

	s_Order.pg[0] = page;
	s_Order.u = u;
	s_Order.v = v;
	s_Order.w = w;
	s_Order.h = h;
	s_Order.cx = cx;
	s_Order.cy = cy;

	s_Order.color[0] = col;
	s_Order.color[1] = col;
	s_Order.color[2] = col;
	s_Order.color[3] = col;

	s_Order.blend = blendColor;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;
}

Uint32 gxLib::PutSpriteEx( StSprite *pSprDesc )
{
	s_Order.sType = enOrderTypeTexturePolygon;

	if( pSprDesc->albedo.page <= enWiredPage )
	{
		s_Order.sType = enOrderTypWired;
	}
	else if( pSprDesc->albedo.page <= enNoneTexturePage )
	{
		s_Order.sType = enOrderTypeNoneTexPolygon;
	}

	//if (pSprDesc->pos.x < 0 || pSprDesc->pos.x > WINDOW_W) return 0;
	//if (pSprDesc->pos.y < 0 || pSprDesc->pos.y > WINDOW_H) return 0;

	s_Order.sAtribute = pSprDesc->atr;
	s_Order.x  = pSprDesc->pos.x;
	s_Order.y  = pSprDesc->pos.y;
	s_Order.prio = pSprDesc->pos.z;

	s_Order.sx = pSprDesc->scl.x;
	s_Order.sy = pSprDesc->scl.y;
	s_Order.sz = 0.f;

	s_Order.rx = 0.f;
	s_Order.ry = 0.f;
	s_Order.rz = pSprDesc->rot.z;

	s_Order.pg[0] = pSprDesc->albedo.page;
	s_Order.map_u[0]  = pSprDesc->albedo.u;
	s_Order.map_v[0]  = pSprDesc->albedo.v;
	s_Order.u  = pSprDesc->albedo.u;
	s_Order.v  = pSprDesc->albedo.v;
	s_Order.w  = pSprDesc->albedo.w;
	s_Order.h  = pSprDesc->albedo.h;
	s_Order.cx = pSprDesc->albedo.cx;
	s_Order.cy = pSprDesc->albedo.cy;

	s_Order.color[0] = pSprDesc->argb;
	s_Order.color[1] = pSprDesc->argb;
	s_Order.color[2] = pSprDesc->argb;
	s_Order.color[3] = pSprDesc->argb;

	s_Order.blend    = pSprDesc->blend;

    s_Order.shader = pSprDesc->shader;
    s_Order.option[0] = pSprDesc->option[0];
    s_Order.option[1] = pSprDesc->option[1];
    s_Order.option[2] = pSprDesc->option[2];
    s_Order.option[3] = pSprDesc->option[3];
	s_Order.pString = NULL;

    if( s_Order.shader == gxShaderNormal )
	{
		//法線マップ用データ
		s_Order.pg[1] = pSprDesc->normal.page;
		s_Order.map_u[1] = pSprDesc->normal.u;
		s_Order.map_v[1] = pSprDesc->normal.v;

		//法線マップ用点光源データ
		s_Order.plight_pos[0] = pSprDesc->plight_pos[0];
		s_Order.plight_pos[1] = pSprDesc->plight_pos[1];
		s_Order.plight_pos[2] = pSprDesc->plight_pos[2];
		s_Order.plight_rgb[0] = pSprDesc->plight_rgb[0];
		s_Order.plight_rgb[1] = pSprDesc->plight_rgb[1];
		s_Order.plight_rgb[2] = pSprDesc->plight_rgb[2];
		s_Order.plight_intensity = pSprDesc->plight_intensity;
	}

/*
	if( s_Order.sAtribute & ATR_MAP_PALLET )
	{
		s_Order.pg[2] = pSprDesc->pallet.page;
		s_Order.map_u[2] = pSprDesc->pallet.u;
		s_Order.map_v[2] = pSprDesc->pallet.v;
	}
*/
	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;
}



Uint32 gxLib::DrawPoint( Sint32 x1,	Sint32 y1, Sint32 prio, Uint32 atr, Uint32 col , Float32 fSize )
{
	//ポイント
	if ((col & 0xff000000) == 0)  return 0;

	if (fSize > 1.0f)
	{
		//大きな点は三角形に分割して描画する

		Sint32 divNum = fSize;

		divNum = CLAMP(divNum, 4, 64);
		Float32 fDivRot = 360.0f / (divNum);
		Float32  fRot1 = 0.0f;
		Float32  fRot2 = fDivRot;

		gxVector2 pt1, pt2;
		for (Sint32 ii = 0; ii < divNum; ii++)
		{
			pt1.Set( 0 , -fSize/2.0f );
			pt2.Set( 0, -fSize / 2.0f);

			if (ii == divNum - 1)
			{
				fRot2 = 0.0f;
			}

			gxUtil::RotationPoint(&pt1, fRot1);
			gxUtil::RotationPoint(&pt2, fRot2);

			gxLib::DrawTriangle(
				x1, y1,
				x1 + pt1.x, y1 + pt1.y,
				x1 + pt2.x, y1 + pt2.y,
				prio,gxTrue,
				atr, col
			);

			fRot1 += fDivRot;
			fRot2 += fDivRot;

		}
	}

	s_Order.sType = enOrderTypePoint;

	s_Order.sAtribute = atr;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;


	s_Order.x1[0]  = (Float32)x1;
	s_Order.y1[0]  = (Float32)y1;
	s_Order.color[0] = col;

	s_Order.sx = 1.0f;	//width;
	s_Order.sy = 1.0f;	//width;
	s_Order.rz = 0;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;
}


Uint32 gxLib::DrawCircle( Sint32 x1, Sint32 y1, Sint32 prio, Uint32 atr, Uint32 argb , Float32 fRadius , Float32 fSize )
{
	//ポイント
	if ((argb & 0xff000000) == 0)  return 0;

	if (fRadius < 1.0f)
	{
		return 0;
	}

	Sint32 divNum = fRadius/4.0f;

	divNum = CLAMP(divNum, 4, 64);
	Float32 fDivRot = 360.0f / (divNum);
	Float32  fRot1 = 0.0f;
	Float32  fRot2 = fDivRot;

	gxVector2 pt1, pt2;

	for (Sint32 ii = 0; ii < divNum; ii++)
	{
		pt1.Set( 0 , -fRadius / 2.0f );
		pt2.Set( 0 , -fRadius / 2.0f);

		if (ii == divNum - 1)
		{
			fRot2 = 0.0f;
		}

		gxUtil::RotationPoint(&pt1, fRot1);
		gxUtil::RotationPoint(&pt2, fRot2);

		gxLib::DrawLine(
			x1 + pt1.x, y1 + pt1.y,
			x1 + pt2.x, y1 + pt2.y,
			prio,
			atr, argb,fSize
		);

		fRot1 += fDivRot;
		fRot2 += fDivRot;
	}

	return 0;
}


Uint32 gxLib::DrawLine(
		Sint32 x1,	Sint32 y1,
		Sint32 x2 , Sint32 y2 , 
		Sint32 prio,
		Uint32 atr, Uint32 col , Float32 fSize )
{
	//ライン
	if ((col & 0xff000000) == 0)  return 0;

	if( fSize > 1.0f )
	{
		gxVector2 pta,ptb,pt1, pt2;
		gxVector2 pt3, pt4;

		Float32 fRot = gxUtil::Atan(x2 - x1, y2 - y1);

		pta.Set( 0,-fSize/2.0f );
		ptb.Set( 0, fSize / 2.0f);
		gxUtil::RotationPoint( &pta , fRot );
		//gxUtil::RotationPoint( &ptb , fRot );

		pt1.x = x1 + pta.x;
		pt1.y = y1 + pta.y;
		pt4.x = x1 - pta.x;
		pt4.y = y1 - pta.y;

		pt2.x = x2 + pta.x;
		pt2.y = y2 + pta.y;
		pt3.x = x2 - pta.x;
		pt3.y = y2 - pta.y;

		gxLib::DrawTriangle(
			pt1.x,pt1.y,
			pt2.x,pt2.y,
			pt3.x,pt3.y,prio,gxTrue,atr, col);

		gxLib::DrawTriangle(
			pt1.x, pt1.y,
			pt3.x, pt3.y,
			pt4.x, pt4.y, prio, gxTrue, atr, col);

		return 0;
	}


	s_Order.sType = enOrderTypeLine;

	s_Order.sAtribute = atr;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;

	{
		s_Order.opt  = 0;

		s_Order.x1[0]  = (Float32)x1;
		s_Order.y1[0]  = (Float32)y1;

		s_Order.x1[1]  = (Float32)x2;
		s_Order.y1[1]  = (Float32)y2;
	}

	s_Order.color[0] = col;
	s_Order.color[1] = col;
	s_Order.color[2] = col;
	s_Order.color[3] = col;

	//太さに対応→やめる
	s_Order.sx = 1.0f;	//width;
	s_Order.sy = 1.0f;	//width;
	s_Order.rz = 0;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;

}


Uint32 gxLib::DrawTriangle(
		Sint32 x1 ,	Sint32 y1 ,
		Sint32 x2 , Sint32 y2 ,
		Sint32 x3 , Sint32 y3 ,
		Sint32 prio,
		gxBool bFill,
		Uint32 atr,	Uint32 argb )
{
	if ((argb & 0xff000000) == 0)  return 0;

	if( bFill )
	{
		//トライアングルフィル

		s_Order.sType = enOrderTypeTriangle;

		s_Order.sAtribute = atr;
		s_Order.x  = 0;
		s_Order.y  = 0;
		s_Order.prio = prio;

		s_Order.opt  = 1;

		s_Order.x1[0]  = (Float32)x1;
		s_Order.y1[0]  = (Float32)y1;

		s_Order.x1[1]  = (Float32)x2;
		s_Order.y1[1]  = (Float32)y2;

		s_Order.x1[2]  = (Float32)x3;
		s_Order.y1[2]  = (Float32)y3;

		s_Order.color[0] = argb;
		s_Order.color[1] = argb;
		s_Order.color[2] = argb;

		s_Order.sx = 1.0f;	//width;
		s_Order.sy = 1.0f;	//width;
		s_Order.rz = 0;

        s_Order.shader = 0;
        s_Order.option[0] = 0;
        s_Order.option[1] = 0;
        s_Order.option[2] = 0;
        s_Order.option[3] = 0;
		s_Order.pString = NULL;

		gxOrderManager::GetInstance()->set( &s_Order );
	}
	else
	{
		DrawLine( x1 , y1 , x2 , y2 , prio , atr , argb );
		DrawLine( x2 , y2 , x3 , y3 , prio , atr , argb );
		DrawLine( x3 , y3 , x1 , y1 , prio , atr , argb );
	}

	return 0;

}


Uint32 gxLib::DrawColorBox(
		Sint32 x1 ,	Sint32 y1 ,Uint32 argb1,
		Sint32 x2 , Sint32 y2 ,Uint32 argb2,
		Sint32 x3 , Sint32 y3 ,Uint32 argb3,
		Sint32 x4 , Sint32 y4 ,Uint32 argb4,
		Sint32 prio,
		Uint32 atr )
{
	
	Uint32 _argb[3];
	
	_argb[0] = argb1;
	_argb[1] = argb2;
	_argb[2] = argb4;

	DrawColorTriangle(
		x1,y1, _argb[0],
		x2,y2, _argb[1],
		x4,y4, _argb[2],
		prio,atr );

	_argb[0] = argb2;
	_argb[1] = argb4;
	_argb[2] = argb3;

	DrawColorTriangle(
		x2,y2, _argb[0],
		x4,y4, _argb[1],
		x3,y3, _argb[2],
		prio,atr );

	return 0;
}


Uint32 gxLib::DrawColorTriangle(
		Sint32 x1 ,	Sint32 y1 ,Uint32 argb1,
		Sint32 x2 , Sint32 y2 ,Uint32 argb2,
		Sint32 x3 , Sint32 y3 ,Uint32 argb3,
		Sint32 prio,
		Uint32 atr )
{
	//トライアングルフィル

	s_Order.sType = enOrderTypeTriangle;

	s_Order.sAtribute = atr;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;

	s_Order.opt  = 1;

	s_Order.x1[0]  = (Float32)x1;
	s_Order.y1[0]  = (Float32)y1;

	s_Order.x1[1]  = (Float32)x2;
	s_Order.y1[1]  = (Float32)y2;

	s_Order.x1[2]  = (Float32)x3;
	s_Order.y1[2]  = (Float32)y3;

	s_Order.color[0] = argb1;
	s_Order.color[1] = argb2;
	s_Order.color[2] = argb3;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	s_Order.sx = 1.0f;	//width;
	s_Order.sy = 1.0f;	//width;
	s_Order.rz = 0;

	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;
}


Uint32 gxLib::PutTriangle(
		Sint32 x1 ,	Sint32 y1 , Sint32 u1, Sint32 v1 ,
		Sint32 x2 , Sint32 y2 , Sint32 u2, Sint32 v2 ,
		Sint32 x3 , Sint32 y3 , Sint32 u3, Sint32 v3 ,
		Sint32 prio,
		Sint32 tpg,
		Uint32 atr,	Uint32 argb , Uint32 blendColor )
{
	//トライアングルフィル
	if ((argb & 0xff000000) == 0)  return 0;

	if( tpg == enNoneTexturePage )
	{
		Uint32 n =gxLib::DrawTriangle(	x1 ,y1 ,	x2 ,y2 ,	x3 ,y3 ,	prio,	gxTrue,	atr , argb );

		return n;
	}

	s_Order.sType = enOrderTypeTextureTriangle;

	s_Order.sAtribute = atr;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;

	s_Order.opt  = 0;

	s_Order.x1[0]  = (Float32)x1;
	s_Order.y1[0]  = (Float32)y1;
	s_Order.u1[0]   = (Float32)u1;
	s_Order.v1[0]   = (Float32)v1;

	s_Order.x1[1]  = (Float32)x2;
	s_Order.y1[1]  = (Float32)y2;
	s_Order.u1[1]   = (Float32)u2;
	s_Order.v1[1]   = (Float32)v2;

	s_Order.x1[2]  = (Float32)x3;
	s_Order.y1[2]  = (Float32)y3;
	s_Order.u1[2]   = (Float32)u3;
	s_Order.v1[2]   = (Float32)v3;

	s_Order.color[0] = argb;
	s_Order.color[1] = argb;
	s_Order.color[2] = argb;

	s_Order.blend = blendColor;

	s_Order.sx = 1.0f;	//width;
	s_Order.sy = 1.0f;	//width;
	s_Order.rz = 0;

	s_Order.pg[0] = tpg;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	gxOrderManager::GetInstance()->set( &s_Order );

	return 0;

}


Uint32 gxLib::DrawBox(
		Sint32 x1,		Sint32 y1,	Sint32 x2 , Sint32 y2 , Sint32 prio,
		gxBool bFill,
		Uint32 atr,	Uint32 argb , Float32 fSizef )
{
	if ((argb & 0xff000000) == 0)  return 0;

	//ボックス
	Sint32 cx=0,cy=0;
	Float32 r = 0.0f;
	Float32 sx = 1.0f;
	Float32 sy = 1.0f;

	if( x1 > x2 )	SWAP( x1 , x2 );
	if( y1 > y2 )	SWAP( y1 , y2 );

	if( bFill )
	{
		PutSprite(	x1,	y1,	prio,	enNoneTexturePage,0,0,x2-x1,y2-y1,	cx,cy,atr,argb,r,sx,sy );
		return 0;
	}
	else
	{
		gxLib::DrawLine( x1 , y1 , x2 , y1 , prio , atr , argb , fSizef );
		gxLib::DrawLine( x2 , y1 , x2 , y2 , prio , atr , argb , fSizef );
		gxLib::DrawLine( x1 , y2 , x2 , y2 , prio , atr , argb , fSizef );
		gxLib::DrawLine( x1 , y1 , x1 , y2 , prio , atr , argb , fSizef );
	}

	return 0;
}


gxBool gxLib::LoadTexture( Uint32 texPage , const gxChar* fileName , Uint32 colorKey , Uint32 ox , Uint32 oy , Sint32 *w , Sint32 *h )
{
#ifdef USE_TEXTURE_CACHE
	if (gxTexManager::GetInstance()->GetFileName(texPage))
	{
		if (strcmp(gxTexManager::GetInstance()->GetFileName(texPage), fileName) == 0)
		{
			GX_DEBUGLOG("テクスチャキャッシュにヒット");
			return gxTrue;
		}
	}
	sprintf(gxTexManager::GetInstance()->GetFileName(texPage), "%s", fileName);
#endif

	GX_DEBUGLOG("gxLib::LoadTexture( %d[%d] , \"%s\");",texPage , texPage/gxTexManager::enPageTexNum , fileName );

	size_t uSize = 0;
	Uint8 *pData = gxLib::LoadStorageFile( fileName , &uSize );

	if( pData )
	{
        //毒
        //gxLib::Sleep(16);
		gxBool bReturn = ReadTexture(texPage, pData, uSize, colorKey, ox, oy, w, h);

		SAFE_DELETES(pData);
        //gxLib::Sleep(16);

		return bReturn;
	}
	SAFE_DELETES(pData);

	return gxFalse;
}

gxBool gxLib::ReadTexture( Uint32 texPage , Uint8* pBuffer , size_t Size , Uint32 colorKey , Uint32 ox , Uint32 oy , Sint32 *w , Sint32 *h )
{
	CFileTarga tga;
	tga.ReadFile( pBuffer , Size , colorKey );
//	tga.ApplyTgaImage( pBuffer , Size );

	if( w ) *w = tga.GetWidth();
	if( h ) *h = tga.GetHeight();

	gxTexManager::GetInstance()->addTexture( texPage , &tga ,colorKey , ox , oy );

	return gxTrue;
}


gxBool gxLib::UploadTexture(gxBool bForce , Sint32 tpg )
{
	//テクスチャをアップロード

	if( bForce )
	{
		gxTexManager::GetInstance()->SetForceUpdate( tpg , gxTrue );
	}
	gxTexManager::GetInstance()->SetUploadTextureRequest();

	return gxTrue;
}

StJoyStat* gxLib::Joy(Uint32 n)
{
	//コントローラーの取得

	return gxPadManager::GetInstance()->GetJoyStatus( n );
}

Uint8 gxLib::KeyBoard( Uint32 n )
{
	return gxPadManager::GetInstance()->GetKeyBoardStatus( n );
}


//void gxLib::Printf( Sint32 x , Sint32 y , Sint32 prio , Uint32 atr , Uint32 argb , gxChar* pFormat , ... )
//{
//	//文字列の格納
//
//	gxChar _buf[ enStringMaxSize ];
//	va_list app;
//
//	va_start( app, pFormat );
//	vsprintf( _buf , pFormat, app );
//	va_end( app );
//
///*
//	s_Order.sType = enOrderTypeFont;
//
//	if( atr&ATR_STR_CENTER )
//	{
//		Sint32 len = strlen(_buf);
//		x -= (len*6)/2;
//	}
//	else if( atr&ATR_STR_RIGHT )
//	{
//		Sint32 len = strlen(_buf);
//		x -= (len*6);
//	}
//
//	s_Order.sAtribute = atr;
//	s_Order.x  = x;
//	s_Order.y  = y;
//	s_Order.pr = prio;
//
//
//	s_Order.color[0] = argb;
//	s_Order.color[1] = argb;
//	s_Order.color[2] = argb;
//	s_Order.color[3] = argb;
//
//	s_Order.pString = _buf;
//
//	gxOrderManager::GetInstance()->set( &s_Order );
//*/
//
//	CGameGirl::GetInstance()->GetFontManager()->Print( x ,y , prio , atr , argb, _buf );
//
//}
//

void gxLib::Printf(Sint32 x, Sint32 y, Sint32 prio, gxChar* pFormat, ...)
{
	static gxChar _buf[enStringMaxSize];
	va_list app;

	{
		size_t sz0 = 0;
		va_start(app, pFormat);
		sz0 = vsprintf(_buf, pFormat, app);
		va_end(app);

		_buf[enStringMaxSize - 1] = 0x00; //念の為

		CGameGirl::GetInstance()->GetFontManager()->Print(
			x,
			y,
			prio,
			ATR_DFLT,
			ARGB_DFLT,
			(char*)_buf);

	}
	return;
}


void gxLib::Printf(Sint32 x, Sint32 y, Sint32 prio, Uint32 atr, Uint32 argb, gxChar* pFormat, ...)
{
	//文字列の格納
	if ((argb & 0xff000000) == 0)  return;

	static gxChar _buf[enStringMaxSize];
	va_list app;

	{
		size_t sz0 = 0;
		va_start(app, pFormat);
		sz0 = vsprintf(_buf, pFormat, app);
		va_end(app);

		_buf[enStringMaxSize - 1] = 0x00; //念の為

		//---------------------
		//utf16に変換
		//---------------------
		//wchar_t* pWChar = CDeviceManager::UTF8toUTF16(_buf);

		CGameGirl::GetInstance()->GetFontManager()->Print(
			x,
			y,
			prio,
			atr,
			argb,
			(char*)_buf);

		//SAFE_DELETES(pWChar);
	}
	return;

#if 0

#ifdef GX_STRING_ENCODE_SJIS
	bool sjis = gxTrue;// False;// Windowsは必ずSJIS
	bool utf8 = gxTrue;
#else
	bool sjis = gxFalse;
	bool utf8 = gxTrue;
#endif

	if (sjis)
	{
		//sjis
		va_start(app, pFormat);
		vsprintf(_buf, pFormat, app);
		//vswprintf((wchar_t*)_buf, enStringMaxSize, (wchar_t*)pFormat, app);
		va_end(app);

		//---------------------
		//sjis -> utf16に変換
		//---------------------
		setlocale(LC_ALL, "JPN");// ja_JP.Shift_JIS");
		size_t sz = strlen(_buf);
		wchar_t destBuf[FILENAMEBUF_LENGTH];
		size_t sz2 = mbstowcs(destBuf, _buf, FILENAMEBUF_LENGTH );
		CGameGirl::GetInstance()->GetFontManager()->Print(x, y, prio, atr, argb, (char*)destBuf);
		return;
		//---------------------
	}
	else if (utf8)
	{
        size_t sz0 = 0;
		va_start(app, pFormat);
		sz0 = vsprintf(_buf, pFormat, app);
		//wchar型でフォーマットされた文字列を対処
		//vswprintf((wchar_t*)_buf, enStringMaxSize, (wchar_t*)pFormat, app);

		va_end(app);

        _buf[enStringMaxSize-1] = 0x00; //念の為

		//---------------------
		//utf16に変換
		//---------------------
		wchar_t *pWChar = CDeviceManager::UTF8toUTF16( _buf );
		CGameGirl::GetInstance()->GetFontManager()->Print(x, y, prio, atr, argb, (char*)pWChar);
		SAFE_DELETES( pWChar );   //毒：：staticな領域
		return;
	}
	else
	{
		va_start(app, pFormat);
		//vsprintf(_buf, pFormat, app);
		//wchar型でフォーマットされた文字列を対処
		vswprintf((wchar_t*)_buf, enStringMaxSize, (wchar_t*)pFormat, app);
		va_end(app);

		//---------------------
		//utf16に変換
		//---------------------
		//setlocale(LC_ALL, "JPN");// ja_JP.Shift_JIS");
		//size_t sz = wcslen((wchar_t*)_buf);
		//wchar_t destBuf[256];
		//size_t sz2 = mbstowcs(destBuf, _buf, 256);
		//size_t sz2 = wsprintf(destBuf, L"%s", _buf);
		//CGameGirl::GetInstance()->GetFontManager()->Print(x, y, prio, atr, argb, (char*)destBuf);
		CGameGirl::GetInstance()->GetFontManager()->Print(x, y, prio, atr, argb, (char*)_buf);
		return;

	}

/*
	s_Order.sType = enOrderTypeFont;

	if( atr&ATR_STR_CENTER )
	{
		Sint32 len = strlen(_buf);
		x -= (len*6)/2;
	}
	else if( atr&ATR_STR_RIGHT )
	{
		Sint32 len = strlen(_buf);
		x -= (len*6);
	}

	s_Order.sAtribute = atr;
	s_Order.x  = x;
	s_Order.y  = y;
	s_Order.pr = prio;


	s_Order.color[0] = argb;
	s_Order.color[1] = argb;
	s_Order.color[2] = argb;
	s_Order.color[3] = argb;

	s_Order.pString = _buf;

	gxOrderManager::GetInstance()->set( &s_Order );
	CGameGirl::GetInstance()->GetFontManager()->Print(x, y, prio, atr, argb, _buf);
*/
#endif

}



gxBool gxLib::LoadAudio( Uint32 uIndex , const gxChar* pFileName )
{
	//Audioファイルをロードする

	if (gxSoundManager::GetInstance() == nullptr) return gxFalse;

	GX_DEBUGLOG("gxLib::LoadAudio( %d , \"%s\");",uIndex , pFileName );

	if (!gxSoundManager::GetInstance()->LoadAudioFile(uIndex, pFileName))
	{
		return gxFalse;
	}

	return gxTrue;
}


gxBool gxLib::ReadAudio( Uint32 uIndex , Uint8* pMemory ,size_t uSize )
{

	gxSoundManager::GetInstance()->ReadAudioFile( uIndex , pMemory , uSize );

	return gxTrue;
}


gxBool gxLib::PlayAudio( Uint32 uIndex , gxBool bLoop , gxBool bReverb , Uint32 uFrm )
{
	if( uFrm )
	{
		gxSoundManager::GetInstance()->PlayAudio( uIndex , 1.0f , bReverb, bLoop );
		gxSoundManager::GetInstance()->SetFade( uIndex , 1.0f , uFrm );
	}
	else
	{
		gxSoundManager::GetInstance()->PlayAudio( uIndex , 1.0f , bReverb, bLoop );
	}

	//gxSoundManager::GetInstance()->SetVolume( uIndex , GetAudioMasterVolume() );

	return gxTrue;
}

gxBool gxLib::SetAudioPann( Uint32 uIndex , Float32 fPan )
{
	gxSoundManager::GetInstance()->SetPanning( uIndex , fPan );
	return gxTrue;
}


gxBool gxLib::SetAudioReverb( Uint32 uIndex , gxBool bEnable )
{
	gxSoundManager::GetInstance()->SetReverb( uIndex , bEnable );

	return gxTrue;
}

/*
gxBool gxLib::SetAudioFadeIn( Uint32 uIndex , gxBool bLoop , Float32 fTgtVolume ,Uint32 uFrm )
{
	gxSoundManager::GetInstance()->PlayAudio( uIndex , 0.f , gxFalse , bLoop );
	gxSoundManager::GetInstance()->SetFade( uIndex , fTgtVolume , uFrm );

	return gxTrue;
}


gxBool gxLib::SetAudioFadeOut( Uint32 uIndex , Uint32 uFrm )
{

	gxSoundManager::GetInstance()->SetFade( uIndex , 0.f , uFrm );

	return gxTrue;
}
*/


gxBool gxLib::SetAudioVolume( Uint32 uIndex , Float32 fVol )
{
	gxSoundManager::GetInstance()->SetVolume( uIndex , fVol );
	return gxTrue;
}

Float32 gxLib::GetAudioVolume ( Uint32 index )
{
	return gxSoundManager::GetInstance()->GetVolume(index);
}

gxBool gxLib::SetAudioPitch(Uint32 uIndex, Float32 fRatio)
{
	gxSoundManager::GetInstance()->SetFreq(uIndex, fRatio);

	return gxTrue;
}


gxBool gxLib::StopAudio( Uint32 uIndex , Uint32 uFrm )
{
	if( uFrm )
	{
		gxSoundManager::GetInstance()->SetFade( uIndex , 0.f , uFrm );
	}
	else
	{
		gxSoundManager::GetInstance()->StopAudio( uIndex );
	}

	return gxTrue;
}


gxBool gxLib::StopAllAudio( Uint32 uFrm )
{
	//すべてのサウンドを停止する

	for( Sint32 ii=0; ii<MAX_SOUND_NUM; ii++ )
	{
		if( gxSoundManager::GetInstance()->IsPlay(ii) )
		{
			StopAudio( ii , uFrm );
		}
	}

	return gxTrue;
}


gxBool gxLib::IsPlayAudio( Uint32 uIndex )
{
	return gxSoundManager::GetInstance()->IsPlay( uIndex );
}


gxBool gxLib::SetAudioMasterVolume( Float32 fVolume )
{
	fVolume = CLAMP(fVolume, 0.0f, 1.0f);

	if( gxSoundManager::GetInstance()->GetMasterVolume() != fVolume || fVolume >= 1.0f )
	{
		gxSoundManager::GetInstance()->SetMasterVolumeLevel( fVolume );
		gxUIManager::GetInstance()->DispVolumeMeter();
	}

	return gxTrue;
}


Float32 gxLib::GetAudioMasterVolume()
{
	return gxSoundManager::GetInstance()->GetMasterVolume();
}

void gxLib::DebugLog( const gxChar* pFormat , ... )
{
	static gxChar _buf[ enStringMaxSize ];
	va_list app;

	if( pFormat == NULL ) return;

	va_start( app, pFormat );

	if ( vsprintf(_buf, pFormat, app) >= enStringMaxSize )
	{
		va_end( app );
		return;
	}

	va_end( app );
	_buf[enStringMaxSize - 1] = 0x00;

	if( CGameGirl::GetInstance()->IsInitCompleted() )
	{
		gxDebug::GetInstance()->LogDisp( _buf );
	}
	else
	{
		CDeviceManager::GetInstance()->LogDisp( _buf );
	}
}


gxBool gxLib::SaveConfig()
{
	//コンフィグファイルをセーブする

	gxLib::SaveData.Version_no   = VERSION_NUMBER;
	gxLib::SaveData.MasterVolume = gxSoundManager::GetInstance()->GetMasterVolume();
//	gxLib::SaveData.RenderMode   = 0;
//	gxLib::SaveData.WindowFull   = 0;
	gxLib::SaveData.EnableSound  = ( gxLib::SaveData.MasterVolume == 0.0f )? 0 : 1;
	gxLib::SaveData.EnableVPad   = ( CVirtualStick::GetInstance()->IsDisp() )? 1 : 0;

	char buf[1024];
	sprintf( buf , "gxLib/%s", FILENAME_CONFIG );

	return CDeviceManager::GetInstance()->SaveFile( buf , (Uint8*)&gxLib::SaveData , sizeof(gxLib::StSaveData) , STORAGE_LOCATION_INTERNAL);

/*
	if( CDeviceManager::GetInstance()->SaveConfig() )
	{
		return gxTrue;
	}
	return gxFalse;
*/
}

gxBool gxLib::LoadConfig()
{
	//コンフィグファイルをロードする
	//※システムの初期化時には使わない、あとからコンフィグを読む必要がある時だけ

	gxUtil::MemSet( &gxLib::SaveData , 0x00 , sizeof(gxLib::SaveData) );

	char buf[1024];
	sprintf( buf , "gxLib/%s", FILENAME_CONFIG );

	//-------------------------------------------------------------------

	Uint8 *pData = NULL;
	size_t uSize = 0;

	pData = CDeviceManager::GetInstance()->LoadFile( buf , &uSize , STORAGE_LOCATION_INTERNAL);

	if( pData )
	{
		gxUtil::MemCpy( &gxLib::SaveData , pData , sizeof(gxLib::StSaveData) );
	}
	else
	{
		//ファイルがなかったのでデフォルトデータにする→と思ったけどやめた。gxLib側でそのあたりの処理をさせることにする。deviceの処理はできるだけシンプルに。。。
		return gxFalse;
	}

	SAFE_DELETES( pData );

	return gxTrue;
}

Uint8* gxLib::LoadFile( const gxChar* pFileName , size_t* pLength )
{
	//Directにファイルを探しに行く

	Uint8 *pData = nullptr;
	
	pData = LoadStorageFile( pFileName , pLength , STORAGE_LOCATION_ROM);

	if( pData == nullptr )
	{
		pData = LoadStorageFile( pFileName , pLength , STORAGE_LOCATION_EXTERNAL);
		if( pData == nullptr )
		{
			if( pData == nullptr )
			{
				pData = LoadStorageFile( pFileName , pLength , STORAGE_LOCATION_INTERNAL);
			}
		}
	}

	return pData;
}

Uint8* gxLib::LoadStorageFile( const gxChar* pFileName , size_t* pLength , ESTORAGE_LOCATION location )
{
	Sint32 id = gxFileManager::GetInstance()->LoadReq( pFileName ,location );

	GX_DEBUGLOG("gxLib::LoadFile(\"%s\");",pFileName );

	Uint8* pData = NULL;

	while( !gxFileManager::GetInstance()->IsLoadEnd( id ) )
	{
#ifndef _USE_MULTITHREAD_
		gxFileManager::GetInstance()->Action();
#endif
		gxLib::Sleep( 1 );

		if (!CGameGirl::GetInstance()->IsExist())
		{
			gxFileManager::GetInstance()->Action();
			//break;
		}
	}

	*pLength = gxFileManager::GetInstance()->GetFileSize(id);
	pData = gxFileManager::GetInstance()->GetFileAddr(id);

	if( pData == NULL )
	{
		GX_DEBUGLOG("[gxLib::Error]gxLib::LoadFile　読み込み失敗(\"%s\") %d;",pFileName , id );
	}

	return pData;
}

gxBool gxLib::SaveFile( const gxChar* pFileName, void* pData, size_t uSize )
{
	if( !SaveStorageFile(pFileName,pData,uSize , STORAGE_LOCATION_EXTERNAL) )
	{
		return SaveStorageFile(pFileName,pData,uSize , STORAGE_LOCATION_AUTO );
	}
	
	return gxTrue;
}

gxBool gxLib::SaveStorageFile( const gxChar* pFileName, void* pData, size_t uSize , ESTORAGE_LOCATION location )
{
	//セーブデータの保存

	switch( location ){
	case STORAGE_LOCATION_ROM:
	//case STORAGE_LOCATION_DISC:
		return gxFalse;
	default:
		break;
	}

	Sint32 id = gxFileManager::GetInstance()->SaveReq(pFileName, pData, uSize, location );

	while (!gxFileManager::GetInstance()->IsSaveEnd(id))
	{
#ifdef _USE_MULTITHREAD_

#else
		gxFileManager::GetInstance()->Action();
#endif
		gxLib::Sleep(1);
		if (!CGameGirl::GetInstance()->IsExist())
		{
			gxFileManager::GetInstance()->Action();
			//break;
		}
	}

	return gxTrue;
}


gxBool gxLib::RemoveFile( const gxChar* pFileName )
{
	return RemoveStorageFile( pFileName , STORAGE_LOCATION_EXTERNAL );
}

gxBool gxLib::RemoveStorageFile( const gxChar* pFileName, ESTORAGE_LOCATION location )
{
	return SaveStorageFile( pFileName , nullptr , 0 , location );
}

Sint32 gxLib::Rand( Uint32 n )
{
	//ランダム生成：：xorshift法

    static Uint32 x=123456789,y=362436069,z=521288629,w=88675123;
	Uint32 t;
	Uint32 r;

	if( n )
	{
		n = abs((int)n);
		//ランダムを初期化
		 x = 123456789 + n;
		 y = 362436069 + n;
		 z = 521288629 + n;
		 w = 88675123  + n;
	}

    t=(x^(x<<11));
    x=y;
    y=z;
    z=w;

	r = ( w=(w^(w>>19))^(t^(t>>8)) );

	Sint32 rnd = r;

	return abs(rnd);

}

void gxLib::SetBgColor(Uint32 argb)
{
	gxRender::GetInstance()->SetClearColor( argb );
}


Uint32 gxLib::GetGameCounter()
{
	//メインループを通るたびに加算されるゲームカウンターの値を得る
	return CGameGirl::GetInstance()->GetCounter();
}

void gxLib::CreateThread( void* (*pFunc)(void*) , void * pArg )
{
	//--------------------------------------------------
	//スレッドを作成します
	//作成できるスレッド開始関数は
	//void func(Sint32)
	//です
	//--------------------------------------------------

	CDeviceManager::GetInstance()->MakeThread( pFunc , pArg );

}

StTouch* gxLib::Touch( Sint32 n )
{
	if( n < 0 || n >= TOUCH_MAXIMUM )
	{
		return NULL;
	}

	return gxPadManager::GetInstance()->GetTouchStatus( n );
}



gxBool gxLib::LoadMovie( Sint32 uIndex , const gxChar *pFileName , Sint32 texPage , Sint32 uAudioIndex , gxChar *AudioFileName )
{
	if( AudioFileName )
	{
		gxLib::LoadAudio( uAudioIndex , AudioFileName );
		CMovieManager::GetInstance()->BindMusic( uIndex , uAudioIndex );
	}

	CMovieManager::GetInstance()->LoadMovie( uIndex , pFileName );
	CMovieManager::GetInstance()->BindTexturePage( uIndex , texPage );

	return gxTrue;
}


gxBool gxLib::PlayMovie( Sint32 uIndex , gxBool bLoop )
{
	Sint32 index = 0;

	CMovieFile *pMovie;

	pMovie = CMovieManager::GetInstance()->GetMovie( uIndex );

	if( pMovie )
	{
		index = pMovie->m_uMusicBindIndex;

		if ( index >= 0 )	gxLib::PlayAudio( index );

		CMovieManager::GetInstance()->PlayMovie( uIndex );

		if( bLoop )
		{
			CMovieManager::GetInstance()->SetLoop( uIndex , bLoop );
		}

		CMovieManager::GetInstance()->SetFrame(uIndex , 0 );

		CMovieManager::GetInstance()->SetAutoFrame(uIndex , gxTrue );

	}
	else
	{
		return gxFalse;
	}
	return gxTrue;

}


gxBool gxLib::StopMovie( Sint32 uIndex )
{
	Sint32 index = 0;

	CMovieFile *pMovie;

	pMovie = CMovieManager::GetInstance()->GetMovie( uIndex );

	if( pMovie )
	{
		index = pMovie->m_uMusicBindIndex;

		if ( index >= 0 )	gxLib::StopAudio( index );

		CMovieManager::GetInstance()->StopMovie( uIndex );
	}
	else
	{
		return gxFalse;
	}

	return gxTrue;
}


gxBool gxLib::StillMovie( Sint32 uIndex , Sint32 frmNo )
{
	//動画の特定フレームを再生する

	Sint32 index = 0;

	CMovieFile *pMovie;

	pMovie = CMovieManager::GetInstance()->GetMovie( uIndex );

	if( pMovie )
	{
		//index = pMovie->m_uMusicBindIndex;

		//if ( index >= 0 )	gxLib::StopAudio( index );

		CMovieManager::GetInstance()->SetLoop( uIndex , gxFalse );

		CMovieManager::GetInstance()->SetFrame( uIndex , frmNo );

		CMovieManager::GetInstance()->SetAutoFrame( uIndex , gxFalse );

	}
	else
	{
		return gxFalse;
	}
	return gxTrue;

}



Float32 gxLib::GetTime( gxClock *pInfo )
{
	//現在の時刻を得る

	gxClock time;

	Float32 fNowTime  = CGameGirl::GetInstance()->GetTime(&time);

	if (pInfo)
	{
		*pInfo = time;
	}

	return fNowTime;
}


//コントローラーを振動させる
void gxLib::SetRumble( Sint32 playerID , Uint32 frm, Float32 pann , Float32 vol  )
{
	//止めるときは両方ゼロで
	pann = CLAMP(pann, -1.0f, 1.0f);
	pann = CLAMP(vol, 0.0f, 1.0f);

	Float32 volR = 0.5f;
	Float32 volL = 0.5f;

	if( pann < 0.f )
	{
		volL += fabs( pann )*0.5f;
		volR = 1.0f - volL;
	}
	else if( pann > 0.f )
	{
		volR += fabs( pann )*0.5f;
		volL = 1.0f - volR;
	}
	else
	{
	}



//	gxPadManager::GetInstance()->SetRumble( playerID , 0 , volL , frm );
//	gxPadManager::GetInstance()->SetRumble( playerID , 1 , volR , frm );

	gxPadManager::GetInstance()->SetRumble( playerID , 0 , vol , frm );
//	gxPadManager::GetInstance()->SetRumble( playerID , 1 , volR , frm );

}


gxBool gxLib::IsDebugSwitchOn( Sint32 n )
{
	if( !gxDebug::GetInstance()->m_bMasterDebugSwitch ) return gxFalse;

	return (gxDebug::GetInstance()->m_DebugSwitch[ n ] > 0 )? gxTrue : gxFalse;
}

void gxLib::SetDebugSwitch( Sint32 n , gxBool bOn , gxBool bToggle )
{
	if( bToggle )
	{
		if( gxDebug::GetInstance()->m_DebugSwitch[ n ] > 0 )
		{
			gxDebug::GetInstance()->m_DebugSwitch[ n ] = 0;
		}
		else
		{
			gxDebug::GetInstance()->m_DebugSwitch[ n ] = 1;
		}
		return;
	}

	gxDebug::GetInstance()->m_DebugSwitch[ n ] = (bOn>0)? 1 : 0;
}


Uint32 gxLib::GetIPAddressV4( gxBool bLocalAddress )
{
	//IPアドレスを取得する
	
	if( bLocalAddress )
	{
		return CGameGirl::GetInstance()->GetIPAddressV4( gxTrue );
	}
	else
	{
		return CGameGirl::GetInstance()->GetIPAddressV4( gxFalse );
	}

	return 0x00000000;
}


Uint32 gxLib::GetUID()
{
	return CGameGirl::GetInstance()->GetUID();
}


gxBool gxLib::IsOnline()
{
	//オンラインでつながっているか？

	return CGameGirl::GetInstance()->IsOnline();
}


gxBool gxLib::BlueToothDataSend( Uint8* pData , size_t uSize)
{
	//BlueToothデータ送信

	Sint32 n = CBlueToothManager::GetInstance()->StoreSendCnt % CBlueToothManager::enBlueToothStoreMax;

    SAFE_DELETES( CBlueToothManager::GetInstance()->pStoreSendData[n] );

	CBlueToothManager::GetInstance()->pStoreSendData[n] = new Uint8[uSize];
	CBlueToothManager::GetInstance()->StoreSendDataSize[n] = Uint32(uSize);

	gxUtil::MemCpy( (void*)CBlueToothManager::GetInstance()->pStoreSendData[n] , pData , uSize);

	CBlueToothManager::GetInstance()->StoreSendCnt ++;

	return gxTrue;
}


//BlueToothデータ受信
Uint8* gxLib::BlueToothDataRecv(size_t* uSize )
{
	Sint32 max = CBlueToothManager::GetInstance()->StoreRecvCnt;
	Sint32 min = CBlueToothManager::GetInstance()->StoreReadCnt;

	if (min >= max)
	{
		*uSize = 0;
		return NULL;
	}

	Sint32 n = CBlueToothManager::GetInstance()->StoreReadCnt%CBlueToothManager::enBlueToothStoreMax;

	CBlueToothManager::GetInstance()->StoreReadCnt ++;
	CBlueToothManager *pBT = CBlueToothManager::GetInstance();

	*uSize = pBT->StoreRecvDataSize[n];
	return pBT->pStoreRecvData[n];
}


void gxLib::Sleep( Uint32 msec )
{
	CDeviceManager::GetInstance()->Sleep(msec);
}

gxBool gxLib::ChangeRenderTarget( Sint32 prio , Sint32 changePage )
{
	//レンダーターゲットを切り替える

	s_Order.sType = enOrderTypeChangeRenderTarget;

	s_Order.sAtribute = ATR_DFLT;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;

	gxOrderManager::GetInstance()->set( &s_Order , gxFalse );
	return gxTrue;
}

void gxLib::GetDeviceResolution( Sint32 *gw , Sint32 *gh , Sint32 *sw , Sint32 *sh )
{
	//----------------------------------------------------------
	//Deviceネイティブの画面サイズと実ゲーム画面サイズを得る
	//----------------------------------------------------------

	CGameGirl::GetInstance()->GetGameResolution   ( gw , gh );

	Sint32 _sw,_sh;
	CGameGirl::GetInstance()->GetWindowsResolution( &_sw , &_sh );

	if( sw ) *sw = _sw;
	if( sh ) *sh = _sh;

}


void gxLib::GetNativePosition( Sint32 *pCX , Sint32 *pCY )
{
	//----------------------------------------------
	//Deviceネイティブの座標に変換する
	//いろいろ面倒だったのでメモ多め
	//----------------------------------------------

	Sint32 mx,my;

	mx = *pCX;
	my = *pCY;

	Sint32 gamew=WINDOW_W,gameh=WINDOW_H;
	Sint32 winw =WINDOW_W,winh =WINDOW_H;

	CGameGirl::GetInstance()->GetGameResolution   ( &gamew , &gameh );
	CGameGirl::GetInstance()->GetWindowsResolution( &winw , &winh   );

	//変換開始

	Float32 fRatioX,fRatioY;

	//まずゲーム座標系のオフセットを得る
	Float32 ox = (winw - gamew) / 2;
	Float32 oy = (winh - gameh) / 2;

	//ゲーム座標系の拡大率を得る
	fRatioX = 1.0f*gamew / WINDOW_W;
	fRatioY = 1.0f*gameh / WINDOW_H;

	//画面外のオフセットサイズをゲーム座標系で得る
	ox /= fRatioX;
	oy /= fRatioY;

	//現在のスクリーンサイズをゲーム座標系で得る
	Float32 gw = ox + ox + WINDOW_W;
	Float32 gh = oy + oy + WINDOW_H;

	//左上を起点としたマウス座標に計算し直す

	Float32 fx, fy;

	fx = mx + ox;
	fy = my + oy;

	//ゲーム画面中の座標位置を比率に変換
	fx = fx / gw;
	fy = fy / gh;

	//実際のスクリーン座標系のどの位置か計算する

	mx = winw*fx;
	my = winh*fy;


	*pCX = mx;
	*pCY = my;
}


void gxLib::SetVirtualPad( gxBool bDispOn , Sint32 TypeFlag )
{
	//バーチャルパッドの表示ON/OFF切り替え

	CVirtualStick::GetInstance()->SetDisp( bDispOn );

}


Uint8* gxLib::LoadWebFile( gxChar* pURL , size_t* pLength , gxChar* pPostData )
{
	//ネットワーク上のファイルを取得する

	Sint32 index = 0;
	index = gxUtil::OpenWebFile( pURL , pPostData );

	while( gxTrue )
	{
#ifndef _USE_MULTITHREAD_
		CDeviceManager::GetInstance()->NetWork();
#endif

		if( gxUtil::IsDownloadWebFile( index ) )
		{
			Uint8 *pData = gxUtil::GetDownloadWebFile(index , pLength );

			if( pData )
			{
				//Uint8 *pData2 = new Uint8[ *pLength ];
				//gxUtil::MemCpy( pData2 , pData , *pLength );
				//return pData2;
				gxUtil::CloseWebFile( index );
				return pData;
			}

			break;
		}

		gxLib::Sleep( 3 );
	}

	return NULL;
}


void gxLib::SetToast( gxChar *pFormat , ... )
{
	//文字列の格納

	gxChar _buf[ enStringMaxSize ];
	va_list app;

	if( pFormat == NULL ) return;

	va_start( app, pFormat );

	if ( vsprintf(_buf, pFormat, app) >= enStringMaxSize )
	{
		va_end( app );
		return;
	}

	va_end( app );

	CDeviceManager::GetInstance()->ToastDisp( _buf );

}


//BlueToothをONにする
void gxLib::BlueToothOn( gxBool bBlueToothOn )
{
	CBlueToothManager::GetInstance()->SetEnable( bBlueToothOn );
}


//BlueToothがONになっているか？
gxBool gxLib::IsBlueToothOn()
{
	if( CBlueToothManager::GetInstance()->IsBlueToothExist() )
	{
		if( CBlueToothManager::GetInstance()->IsEnable() )
		{
			return gxTrue;
		}
	}
	return gxFalse;
}


void gxLib::RecordReplay( gxBool bRecord )
{
	gxPadManager::GetInstance()->RecordInput( bRecord );

	if( bRecord )
	{
		gxLib::Rand( 0x01234567 );
	}
}


gxBool gxLib::LoadReplayFile( const gxChar *pFileName )
{
	//リプレイデータを保存する
	size_t uSize = 0;
	Uint8 *pData = gxLib::LoadStorageFile( pFileName , &uSize );

	if( pData )
	{
		gxPadManager::GetInstance()->ReplayStart( pData , Uint32(uSize) );
		SAFE_DELETES( pData );
		gxLib::Rand( 0x01234567 );
		return gxTrue;
	}

	return gxFalse;
}


gxBool gxLib::SaveReplayFile( const gxChar *pFileName )
{
	//リプレイデータを保存する

	Uint32 uSize = 0;
	Uint8* pData = gxPadManager::GetInstance()->GetReplayData( &uSize );

	gxLib::SaveFile( pFileName , pData , uSize );

	return gxTrue;

}

gxBool gxLib::IsReplaying()
{
	if( gxPadManager::GetInstance()->GetReplayStat()&0x01 ) return gxTrue;

	return gxFalse;
}

gxBool gxLib::IsRecoedPlay()
{
	if( gxPadManager::GetInstance()->GetReplayStat()&0x02 ) return gxTrue;

	return gxFalse;
}

void gxLib::Exit()
{
	CGameGirl::GetInstance()->Exit();
}

void gxLib::Pause( gxBool bPauseOn )
{
	CGameGirl::GetInstance()->SetSoftPause( bPauseOn );
}


//アチーブメント関連情報の取得と設定
gxBool gxLib::GetAchievement( Uint32 achieveindex )
{
	if( achieveindex >= 256) return gxFalse;

	Uint32 idx = achieveindex/8;
	Uint32 bit = achieveindex%8;

	if( gxLib::SaveData.Achievement[idx] & ( 0x01<<bit ) )
	{
		return gxTrue;
	}

	return gxFalse;
}

gxBool gxLib::SetAchievement   ( Uint32 achieveindex )
{
	//Achievement向けのフラグを立てる
	//新規に立てられる場合はgxTrueを返す

	if( achieveindex >= 256) return gxFalse;

	Uint32 idx = achieveindex/8;
	Uint32 bit = achieveindex%8;

	Uint32 old = gxLib::SaveData.Achievement[idx];

	gxLib::SaveData.Achievement[idx] |= ( 0x01<<bit );

	gxBool bFirst = ( old != gxLib::SaveData.Achievement[idx]);

	if( bFirst )
	{
		CDeviceManager::GetInstance()->SetAchievement( achieveindex );
	}

	return bFirst;
}


Uint32 gxLib::Scissor( Sint32 x1, Sint32 y1, Sint32 w , Sint32 h , Sint32 prio )
{

	s_Order.sType = enOrderTypeChangeScissor;

	s_Order.sAtribute = 0;
	s_Order.x  = 0;
	s_Order.y  = 0;
	s_Order.prio = prio;

	s_Order.u  = x1;
	s_Order.v  = y1;
	s_Order.w  = w;
	s_Order.h  = h;

    s_Order.shader = 0;
    s_Order.option[0] = 0;
    s_Order.option[1] = 0;
    s_Order.option[2] = 0;
    s_Order.option[3] = 0;
	s_Order.pString = NULL;

	gxOrderManager::GetInstance()->set( &s_Order , gxFalse );

	return 0;
}


void gxLib::CreatePostEffectBlur( Sint32 prio , Float32 fRatio , Sint32 level )
{
	if( fRatio <= 0.0f ) return;

	s_Order.sType = enOrderTypeProcessingBlur;
	s_Order.prio = prio;
	s_Order.x1[0] = fRatio;
	s_Order.x1[1] = level;

	gxOrderManager::GetInstance()->set( &s_Order , gxFalse );
}


void gxLib::CreatePostEffectBloom( Sint32 prio , Float32 fRatio , Float32 fRange )
{
	if (fRatio <= 0.0f) return;

	fRatio = CLAMP(fRatio, 0, 1.0f);	//Bloomの強度（回数）
	fRange = CLAMP(fRange, 0, 1.0f);	//色域の広さ

	s_Order.sType = enOrderTypeProcessingBloom;
	s_Order.prio = prio;
	s_Order.x1[0] = fRatio;
	s_Order.x1[1] = 1.0f - fRange;

	gxOrderManager::GetInstance()->set(&s_Order , gxFalse );
}


void gxLib::ShaderTest( Sint32 prio , Float32 fRatio )
{
	s_Order.sType = enOrderTypeDevelop;
	s_Order.prio = prio;
	s_Order.opt   = 10 * fRatio;

	if( s_Order.opt == 0 ) return;

	gxOrderManager::GetInstance()->set(&s_Order , gxFalse );
}


void gxLib::CaptureScreen( Sint32 prio , RASTER_TYPE raserType , Sint32 timeOffset,  Float32 fRasterPow , Float32 fRasterRatio )
{
	//スクリーンをキャプチャバッファにコピーする

	s_Order.sType = enOrderTypeCaptureScreen;
	s_Order.prio = prio;
	
	if( timeOffset == -1 ) timeOffset = gxLib::GetGameCounter();

	fRasterPow   = CLAMP( fRasterPow , 0,1);	//画面中で何回折れ曲がらせるか
	fRasterRatio = CLAMP( fRasterRatio , 0,1);	//画面の？％を揺らすか？

	if( raserType == EnRASTER_NONE )
	{
		//ラスタースクロールなし
		s_Order.x1[0]  = 0;
		s_Order.x1[1]  = 0;
		s_Order.x1[2]  = 0;
	}
	else if( raserType == EnRASTER_V )
	{
		//横ラスタースクロール
		s_Order.x1[0]  = fRasterPow;
		s_Order.x1[1]  = fRasterRatio;
		s_Order.x1[2]  = timeOffset/100.0f;			//スクロールの速さ
	}
	else if( raserType == 2 )
	{
		//縦ラスタースクロール

		//未対応
	}



	gxOrderManager::GetInstance()->set(&s_Order , gxFalse );
}

EJoyBit gxLib::GetGamePadConfigButton(Uint32 playerID, EJoyBit btn1)
{
	//現在アサインされているボタンを取得する

	return gxPadManager::GetInstance()->GetConfiguredButtonBit( playerID, btn1 );
}

void gxLib::GamePadConfigSwapButton(Uint32 playerID, EJoyBit pos, EJoyBit key )
{
	//キー設定を入れ替える

	if( playerID < 0 || playerID >= PLAYER_MAX ) return;

	//Slot1にアサインされているキーを調べる
	EJoyBit assignKey = gxPadManager::GetInstance()->GetConfiguredButtonBit( playerID, pos );

	if (assignKey == key)
	{
		//JOY_UスロットにJOY_Uを割り当てようとした場合、なにもしない
		return;
	}

	//もともと入っていた値を探す
	Uint8 slot2 = 0;

	for (Sint8 ii = 0; ii < 32; ii++)
	{
		EJoyBit key2 = gxPadManager::GetInstance()->GetConfiguredButtonBit( playerID, (EJoyBit)(0x01<<ii) );

		if (key2 == key)
		{
			//割り当てようよしていたキーの入っているスロット番号を見つけた
			slot2 = ii;
		}
	}

	gxPadManager::GetInstance()->SetConfigKey( playerID, pos, key );
	gxPadManager::GetInstance()->SetConfigKey( playerID, (EJoyBit)(0x01<< slot2), assignKey );
}

void gxLib::EnablePadConfig(gxBool bConfigOn)
{
	//キーコンフィグの有効・無効を切り替える
	gxPadManager::GetInstance()->SetEnableConfig(bConfigOn);
}

void gxLib::GamePadConfigResetDefault(Uint32 playerID)
{
	//キーコンフィグをリセットする

	gxPadManager::GetInstance()->SetDefaultConfig( playerID );
}

void gxLib::EnableRumble( gxBool bRumbleOn )
{
	//振動機能のON/OFFを切り替える

	for( Sint32 ii=0; ii<PLAYER_MAX; ii++ )
	{
		gxPadManager::GetInstance()->EnableRumble( ii , bRumbleOn );
	}
}

void gxLib::OpenAppManual(gxChar *pFormat )
{
	//マニュアルの表示

	if(pFormat == nullptr)
	{
		pFormat = DOCUMENT_MANUAL_URL;
	}

	gxLib::OpenWebBrowser( pFormat );
}


void gxLib::OpenEULADocument(gxChar *pFormat )
{
	//ライセンスの表示

	if(pFormat == nullptr)
	{
		pFormat = DOCUMENT_LICENCE_URL;
	}

	gxLib::OpenWebView( pFormat );
}

Float32 gxLib::GetBenchmarkScore()
{
	//ベンチマークによるスコアの表示

	return CGameGirl::GetInstance()->GetBenchmarkScore();

}
Float32 gxLib::GetDeltaTime()
{
	return CGameGirl::GetInstance()->GetDeltaTime();
}


void gxLib::SetClipBoard(std::string clipBoardString )
{
  CDeviceManager::GetInstance()->SetClipBoardString(clipBoardString);
}

std::string gxLib::GetClipBoard()
{
    std::string str = CDeviceManager::GetInstance()->GetClipBoardString();

    return str;
}


void gxLib::OpenWebView( gxChar *pFormat , ... )
{
	//文字列の格納

	gxChar _buf[ enStringMaxSize ];
	va_list app;

	if( pFormat == NULL ) return;

	va_start( app, pFormat );

	if ( vsprintf(_buf, pFormat, app) >= enStringMaxSize )
	{
		va_end( app );
		return;
	}

	va_end( app );

	CDeviceManager::GetInstance()->OpenWebClient( _buf );
}


void gxLib::OpenWebBrowser( gxChar *pFormat , ... )
{
    //文字列の格納

    gxChar _buf[ enStringMaxSize ];
    va_list app;

    if( pFormat == NULL ) return;

    va_start( app, pFormat );

    if ( vsprintf(_buf, pFormat, app) >= enStringMaxSize )
    {
        va_end( app );
        return;
    }

    va_end( app );

    CDeviceManager::GetInstance()->OpenWebClient( _buf , gxTrue );
}



void gxLib::CloseWebView()
{
    CDeviceManager::GetInstance()->OpenWebClient( "CloseWebView" );
}
