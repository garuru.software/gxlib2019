//------------------------------------------
//
// ゲームアプリ側の実装
//
// gxLib 2022
//
// 2022.1.11 written by ragi.
//------------------------------------------
#define STB_RECT_PACK_IMPLEMENTATION
#include <gxLib.h>

#include <gxLib/util/CFileTarga.h>
#include "stb_rect_pack.h"

class CTexturePacker
{
public:

	struct FileInfo {
		Sint32 id;
		CFileTarga  tga;
		std::string FileName;
		gxSprite    Sprite;
		gxBool      bExist = gxFalse;
	};

	CTexturePacker()
	{
	}

	~CTexturePacker()
	{
	}

	void SetAtlas( Sint32 page , Sint32 x , Sint32 y , Sint32 w , Sint32 h )
	{
		m_Atlas.page = page;
		m_Atlas.u = x;
		m_Atlas.v = y;
		m_Atlas.w = w;
		m_Atlas.h = h;
	}

	void AddTexture( std::string FileName )
	{
		FileInfo info;
		info.FileName = FileName;
		m_FileList.push_back(info);
	}

	void LoadTextures()
	{
		std::vector<stbrp_rect> rectList;
		Sint32 num_rects = 0;
		for( auto itr = m_FileList.begin(); itr != m_FileList.end() ; ++itr )
		{
			if (!itr->tga.LoadFile( (gxChar*)itr->FileName.c_str(),0xff000000) )
			{
				itr->bExist = gxFalse;
				continue;
			}
			//スプライト情報を記録する
			itr->tga.MakeAutoAlphaChannel(0);

			itr->bExist = gxTrue;
			itr->Sprite.page = m_Atlas.page;
			itr->Sprite.u = m_Atlas.u;
			itr->Sprite.v = m_Atlas.v;
			itr->Sprite.w = itr->tga.GetWidth();
			itr->Sprite.h = itr->tga.GetHeight();
			itr->Sprite.cx = itr->Sprite.w/2;
			itr->Sprite.cy = itr->Sprite.h/2;

			stbrp_rect rect;
			rect.id = num_rects;
			rect.w = itr->Sprite.w;
			rect.h = itr->Sprite.h;
			rectList.push_back(rect);

			itr->id = rect.id;
			num_rects++;
		}

		if (num_rects == 0) return;

		stbrp_context con;
		std::vector<stbrp_node> node;
		node.resize(num_rects);

		stbrp_init_target(&con, m_Atlas.w, m_Atlas.h, &node[0], num_rects );
		int result = stbrp_pack_rects(&con, &rectList[0], num_rects);
		if (result == 1)
		{
			//すべて入った
		}

		//矩形リストをテクスチャリストに書き戻す

		for (auto itr = rectList.begin(); itr != rectList.end(); ++itr)
		{
			if (itr->was_packed == 1 )
			{
				//IDが一致するテクスチャを探して情報を書き込む
				auto* info = getFileInfoFromID(itr->id);
				info->Sprite.u += itr->x;
				info->Sprite.v += itr->y;
				//gxLib::ReadTexture2(info->Sprite.page, info->tga.GetFileImage(), info->tga.GetFileSize(), 0xff00ff00, info->Sprite.u, info->Sprite.v);
			}
		}
	}

	void Draw()
	{
		Float32 fAlpha = gxUtil::Cos((gxLib::GetGameCounter() * 5) % 360);
		fAlpha = ABS(fAlpha);

		gxLib::DrawBox(0, 0, m_Atlas.w, m_Atlas.h, 0, gxTrue, ATR_DFLT, 0xFF00ff00, 3.0f);

		for (Sint32 ii = 0; ii < m_FileList.size(); ii++)
		{
			auto& r = m_FileList[ii].Sprite;
			if (m_FileList[ii].bExist == 1)
			{
				gxLib::Printf(r.u + r.w / 2, r.v + r.h / 2, 100, ATR_STR_CM, ARGB_DFLT, "%d", m_FileList[ii].id);
				//gxLib::DrawBox(r.u, r.v, r.u + r.w, r.v + r.h, 0, gxTrue, ATR_DFLT, 0x8000ff00, 3.0f);
				gxLib::DrawBox(r.u, r.v, r.u + r.w, r.v + r.h, 0, gxFalse, ATR_DFLT, 0xFFff0000, 3.0f);

				gxLib::PutSprite(&m_FileList[ii].Sprite, m_FileList[ii].Sprite.u+ m_FileList[ii].Sprite.w/2, m_FileList[ii].Sprite.v+ m_FileList[ii].Sprite.h/2, 100);
				gxLib::PutSprite(&m_FileList[ii].Sprite, m_FileList[ii].Sprite.u + m_FileList[ii].Sprite.w / 2, m_FileList[ii].Sprite.v + m_FileList[ii].Sprite.h / 2, 100, ATR_ALPHA_PLUS, SET_ALPHA(fAlpha*0.5f, 0xffffffff),0.f , fAlpha*1.25f, fAlpha * 1.25f);
			}

		}

		gxLib::DrawBox(0, 0, m_Atlas.w, m_Atlas.h, 0, gxFalse, ATR_DFLT, 0xff00ff00, 3.0f);
	}

private:

	FileInfo* getFileInfoFromID(int id)
	{
		//for (auto itr = m_FileList.begin(); itr != m_FileList.end(); ++itr)
		for (size_t ii=0; ii<m_FileList.size(); ii++)
		{
			if (m_FileList[ii].id == id)
			{
				return &m_FileList[ii];
			}
		}

		return nullptr;
	}

	gxSprite m_Atlas;
	std::vector<FileInfo> m_FileList;

};
CTexturePacker* s_pCTexturePacker;

gxBool GameInit( std::vector<std::string> args )
{
	std::string fileName[]={
		"purplesquare2.PNG",
		"bluecircle.png",
		"bluediamond.PNG",
		"greensquare.PNG",
		"icons.PNG",
		"indigotriangle.PNG",
		"orangetriangle.PNG",
		"particle.PNG",
		"pinkpinwheel.PNG",
		"powerups.PNG",
		"purplesquare1.png",
		"redcircle.png",
		"redclone.PNG",
		"snakehead.png",
		"snaketail.PNG",
		"whiteplayer.PNG",
		"whitestar.PNG",
		"yellowshot.PNG",
	};

	s_pCTexturePacker = new CTexturePacker();
	s_pCTexturePacker->SetAtlas(0, 0, 0, 640, 512);

	for (size_t ii = 0; ii < ARRAY_LENGTH(fileName); ii++)
	{
		s_pCTexturePacker->AddTexture(fileName[ii]);
	}

	s_pCTexturePacker->LoadTextures();

	gxLib::UploadTexture();

	return gxTrue;
}

gxBool GameMain()
{
	s_pCTexturePacker->Draw();

	return gxTrue;
}


gxBool GamePause()
{

	return gxTrue;
}


gxBool GameSleep()
{
	//SLEEP時に１度だけ呼び出される

	return gxTrue;
}

gxBool GameResume()
{
	//SLEEPから回復したときに１度だけ呼び出される

	return gxTrue;
}

gxBool GameEnd()
{
	//アプリ終了時に１度だけ呼び出される

	return gxTrue;
}

gxBool GameReset()
{
	//リセット時に１度だけ呼び出される

	return gxTrue;
}

gxBool DragAndDrop( gxChar *pFileName )
{
	//ドラッグ＆ドロップされたときにドラッグされたファイル数分だけ呼び出される

	return gxTrue;
}


